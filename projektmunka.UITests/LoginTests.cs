﻿using NUnit.Framework;
using System.Linq;
using Xamarin.UITest;

namespace Projektmunka.UITests
{
    [TestFixture(Platform.Android)]
    public class LoginTests
    {
        IApp app;
        Platform platform;

        public LoginTests(Platform platform) 
        {
            this.platform = platform;
        }

        [SetUp]
        public void BeforeEachTest()
        {
            app = ConfigureApp.Android.InstalledApp("hu.FBVSDL.Projektmunka").StartApp();
        }

        [Test]
        public void LoginPageElementsAreDisplayed()
        {
            app.WaitForElement(c => c.Marked("image"));
            app.WaitForElement(c => c.Marked("input_username"));
            app.WaitForElement(c => c.Marked("label_username_error"));
            app.WaitForElement(c => c.Marked("input_password"));
            app.WaitForElement(c => c.Marked("label_password_error"));
            app.WaitForElement(c => c.Marked("login_button"));
            app.WaitForElement(c => c.Marked("checkbox"));
            app.WaitForElement(c => c.Marked("remember_me"));
            app.WaitForElement(c => c.Marked("login_reg_button"));
        }

        [Test]
        public void LoginPageUserCannotLoginWithIncorrectCredentialsAndGetErrorMessage()
        {
            // Enter an incorrect username and password
            app.EnterText(c => c.Marked("input_username"), "incorrect_username");
            app.EnterText(c => c.Marked("input_password"), "incorrect_password");

            // Tap the login button
            app.Tap(c => c.Marked("login_button"));

            app.WaitForElement(c => c.Marked("Understood").Parent().Class("AlertDialogLayout"));
        }

        [Test]
        public void LoginPageUserCannotLoginWithIncorrectCredentials()
        {
            // Enter an incorrect username and password
            app.EnterText(c => c.Marked("input_username"), "incorrect_username");
            app.EnterText(c => c.Marked("input_password"), "incorrect_password");

            // Tap the login button
            app.Tap(c => c.Marked("login_button"));
            app.WaitForElement(c => c.Marked("Understood").Parent().Class("AlertDialogLayout"));
            app.Tap("Understood");

            var pageTitle = app.Query(x => x.Marked("Login")).FirstOrDefault()?.Text;
            Assert.AreEqual("Login", pageTitle);
        }

        [Test]
        public void LoginPageUserCanRegister()
        {
            app.Tap(c => c.Marked("login_reg_button"));

            var pageTitle = app.Query(x => x.Marked("Registration")).FirstOrDefault()?.Text;

            Assert.AreEqual("Registration", pageTitle);
        }
    }
}
