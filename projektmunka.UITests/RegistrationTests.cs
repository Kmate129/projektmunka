﻿using NUnit.Framework;
using Xamarin.UITest;

namespace Projektmunka.UITests
{
    [TestFixture(Platform.Android)]
    public class RegistrationTests
    {
        IApp app;
        Platform platform;

        public RegistrationTests(Platform platform)
        {
            this.platform = platform;
        }

        [SetUp]
        public void BeforeEachTest()
        {
            app = ConfigureApp.Android.InstalledApp("hu.FBVSDL.Projektmunka").StartApp();
        }

        [Test]
        public void RegistrationPage_ElementsDisplayedCorrectly()
        {
            // Navigate to the RegistrationPage
            app.WaitForElement(x => x.Marked("Login"));
            app.Tap(x => x.Marked("login_reg_button"));
            app.WaitForElement(x => x.Marked("Registration"));

            // Check that all the labels, entries, and buttons are displayed correctly
            app.WaitForElement(x => x.Marked("reg_label_username"));
            app.WaitForElement(x => x.Marked("reg_entry_username"));
            app.WaitForElement(x => x.Marked("reg_label_username_error"));
            app.WaitForElement(x => x.Marked("reg_label_password"));
            app.WaitForElement(x => x.Marked("reg_entry_password"));
            app.WaitForElement(x => x.Marked("reg_label_password_error"));
            app.WaitForElement(x => x.Marked("reg_label_height"));
            app.WaitForElement(x => x.Marked("reg_entry_height"));
            app.WaitForElement(x => x.Marked("reg_label_height_error"));
            app.WaitForElement(x => x.Marked("reg_label_curweight"));
            app.WaitForElement(x => x.Marked("reg_entry_curweight"));
            app.WaitForElement(x => x.Marked("reg_label_curweight_error"));
            app.WaitForElement(x => x.Marked("reg_label_age"));
            app.WaitForElement(x => x.Marked("reg_entry_age"));
            app.WaitForElement(x => x.Marked("reg_label_age_error"));
            app.WaitForElement(x => x.Marked("reg_label_gender"));
            app.WaitForElement(x => x.Marked("reg_label_gender_error"));

            app.ScrollDown();
            app.WaitForElement(x => x.Marked("reg_label_goalweight"));
            app.WaitForElement(x => x.Marked("reg_entry_goalweight"));
            app.WaitForElement(x => x.Marked("reg_label_goalweight_error"));
            app.WaitForElement(x => x.Marked("reg_label_equipment"));
            app.WaitForElement(x => x.Marked("reg_label_equipment_error"));
            app.WaitForElement(x => x.Marked("reg_label_nodays"));
            app.WaitForElement(x => x.Marked("reg_label_nodays_error"));
            app.WaitForElement(x => x.Marked("reg_button"));
        }

        /*[Test]
        public async Task RegistrationPage_WhenUsernameIsNotProvided_ShouldDisplayErrorMessage()
        {
            var viewModel = new RegistrationViewModel(Mock.Of<IUserService>());
            var page = new RegistrationPage { BindingContext = viewModel };
            var button = page.FindByName<Button>("reg_button");
            await button.PerformClickAsync();
            var errorLabel = page.FindByName<Label>("reg_label_username_error");
            Assert.AreEqual("Please enter a username", errorLabel.Text);
        }*/
    }
}
