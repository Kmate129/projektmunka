﻿using Projektmunka.Data.Model;

namespace Projektmunka.UnitTests.ModelTests
{
    [TestFixture]
    public class WeightHistoryTests
    {
        [Test]
        public void Constructor_WithValidInputs_ShouldCreateInstance()
        {
            float currentWeight = 80f;
            float difference = 0.1f;
            int numberOfWeeks = 52;

            var history = new WeightHistory(currentWeight, difference, numberOfWeeks);

            Assert.That(history, Is.InstanceOf<WeightHistory>());
        }

        [Test]
        public void Constructor_GivenValidInput_InitializingInnerVariablesCorrectly_IncreasingWeights()
        {
            float currentWeight = 60f;
            float difference = 0.5f;
            int numberOfWeeks = 12;

            WeightHistory history = new WeightHistory(currentWeight, difference, numberOfWeeks);
            float[,] expectedResult = new float[,]
            {
                { 60f, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
                { 60f, 60.5f, 61f, 61.5f, 62f, 62.5f, 63f, 63.5f, 64f, 64.5f, 65f, 65.5f, 66f}
            };

            Assert.That(history.Data, Is.EqualTo(expectedResult));
        }

        [Test]
        public void Constructor_GivenValidInput_InitializingInnerVariablesCorrectly_DecreasingWeights()
        {
            float currentWeight = 87f;
            float difference = -0.5f;
            int numberOfWeeks = 13;

            WeightHistory history = new WeightHistory(currentWeight, difference, numberOfWeeks);
            float[,] expectedResult = new float[,]
            {
                { 87f, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
                { 87f, 86.5f, 86f, 85.5f, 85f, 84.5f, 84f, 83.5f, 83f, 82.5f, 82f, 81.5f, 81f, 80.5f}
            };

            Assert.That(history.Data, Is.EqualTo(expectedResult));
        }

        [Test]
        public void Constructor_GivenValidInput_InitializingInnerVariablesCorrectly_DecreasingWeights_NotWithMaxDifferenceValue()
        {
            float currentWeight = 100f;
            float difference = -0.3f;
            int numberOfWeeks = 12;

            WeightHistory history = new WeightHistory(currentWeight, difference, numberOfWeeks);
            float[,] expectedResult = new float[,]
            {
                { 100f, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
                { 100f, 99.7f, 99.4f, 99.1f, 98.8f, 98.5f, 98.2f, 97.9f, 97.6f, 97.3f, 97f, 96.7f, 96.4f }
            };

            Assert.That(history.Data, Is.EqualTo(expectedResult));
        }

        [Test]
        public void Constructor_GivenInvalidInput_Throwing_DifferenceValueNotInRange_Upper()
        {
            float currentWeight = 100f;
            float difference = 6f;
            int numberOfWeeks = 12;

            WeightHistory history;

            Assert.Throws<ArgumentException>((TestDelegate)(() =>
            {
                history = new WeightHistory(currentWeight, difference, numberOfWeeks);
            }));
        }

        [Test]
        public void Constructor_GivenInvalidInput_Throwing_DifferenceValueNotInRange_Lower()
        {
            float currentWeight = 100f;
            float difference = -0.6f;
            int numberOfWeeks = 12;

            WeightHistory history;

            Assert.Throws<ArgumentException>((TestDelegate)(() =>
            {
                history = new WeightHistory(currentWeight, difference, numberOfWeeks);
            }));
        }

        [Test]
        public void Method_GetExpectedValues_AfterInit()
        {
            WeightHistory history = new WeightHistory(100f, -0.3f, 12);
            float[] expectedResult = new float[]
            {
                100f, 99.7f, 99.4f, 99.1f, 98.8f, 98.5f, 98.2f, 97.9f, 97.6f, 97.3f, 97f, 96.7f, 96.4f
            };

            float[] result = history.GetExpectedValues();

            Assert.That(result, Is.EqualTo(expectedResult));
        }

        [Test]
        public void Method_GetRealValues_AfterInit()
        {
            WeightHistory history = new WeightHistory(100f, -0.3f, 12);
            float[] expectedResult = new float[]
            {
                100f, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
            };

            float[] result = history.GetRealValues();

            Assert.That(result, Is.EqualTo(expectedResult));
        }

        [Test]
        public void Method_GetGoalWeight()
        {
            WeightHistory history = new WeightHistory(100f, -0.3f, 12);
            float expectedResult = 96.4f;

            float result = history.GetGoalWeight();

            Assert.That(result, Is.EqualTo(expectedResult));
        }

        [Test]
        public void Method_GetStartingWeight()
        {
            WeightHistory history = new WeightHistory(100f, -0.3f, 12);
            float expectedResult = 100f;

            float result = history.GetStartingWeight();

            Assert.That(result, Is.EqualTo(expectedResult));
        }

        [Test]
        public void Method_GetCurrentWeight_AfterInit()
        {
            WeightHistory history = new WeightHistory(100f, -0.3f, 12);
            float expectedResult = 100f;

            float result = history.GetStartingWeight();

            Assert.That(result, Is.EqualTo(expectedResult));
        }

        [Test]
        public void Method_Insert_ValidValue_InsertsCorrectly_AfterInit()
        {
            WeightHistory history = new WeightHistory(100f, -0.3f, 12);
            float[,] expectedResult = new float[,]
            {
                { 100f, 75f, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
                { 100f, 99.7f, 99.4f, 99.1f, 98.8f, 98.5f, 98.2f, 97.9f, 97.6f, 97.3f, 97f, 96.7f, 96.4f }
            };

            history.Insert(75f);

            float[,] result = history.Data;

            Assert.That(result, Is.EqualTo(expectedResult));
        }

        [Test]
        public void Method_Insert_ValidValue_InsertsCorrectly_MoreCall()
        {
            WeightHistory history = new WeightHistory(100f, -0.3f, 12);
            float[,] expectedResult = new float[,]
            {
                { 100f, 75f, 45f, 15f, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
                { 100f, 99.7f, 99.4f, 99.1f, 98.8f, 98.5f, 98.2f, 97.9f, 97.6f, 97.3f, 97f, 96.7f, 96.4f }
            };

            history.Insert(75f);
            history.Insert(45f);
            history.Insert(15f);

            float[,] result = history.Data;

            Assert.That(result, Is.EqualTo(expectedResult));
        }

        [Test]
        public void Method_Insert_ValidValue_InsertsCorrectly_TestingOverflow()
        {
            WeightHistory history = new WeightHistory(100f, -0.3f, 12);
            float[,] expectedResult = new float[,]
            {
                { 100f, 75f, 45f, 15f, 60f, 65f, 63f, 61f, 76f, 65f, 68f, 61f, 40f },
                { 100f, 99.7f, 99.4f, 99.1f, 98.8f, 98.5f, 98.2f, 97.9f, 97.6f, 97.3f, 97f, 96.7f, 96.4f }
            };

            history.Insert(75f);
            history.Insert(45f);
            history.Insert(15f);
            history.Insert(60f);
            history.Insert(65f);
            history.Insert(63f);
            history.Insert(61f);
            history.Insert(76f);
            history.Insert(65f);
            history.Insert(68f);
            history.Insert(61f);
            history.Insert(40f);
            bool bool_result = history.Insert(20f);

            float[,] result = history.Data;

            Assert.That(result, Is.EqualTo(expectedResult));
            Assert.That(bool_result, Is.EqualTo(false));
        }

        [Test]
        public void Method_Insert_InvalidValue_ThrowsException()
        {
            WeightHistory history = new WeightHistory(100f, -0.3f, 12);

            Assert.Throws<ArgumentException>(() =>
            {
                history.Insert(-20f);
            });
        }

        [Test]
        public void Method_GetCurrentWeight_AfterInsertion()
        {
            WeightHistory history = new WeightHistory(100f, -0.3f, 12);
            float expectedResult = 34f;

            history.Insert(30);
            history.Insert(45f);
            history.Insert(34f);

            float result = history.GetCurrentWeight();

            Assert.That(result, Is.EqualTo(expectedResult));
        }

        [Test]
        public void Method_GetRealValues_AfterInsertion()
        {
            WeightHistory history = new WeightHistory(100f, -0.3f, 12);
            float[] expectedResult = new float[]
            {
                100f, 79f, 43f, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
            };

            history.Insert(79f);
            history.Insert(43f);

            float[] result = history.GetRealValues();

            Assert.That(result, Is.EqualTo(expectedResult));
        }
    }
}
