﻿using Projektmunka.Data.Enums;
using Projektmunka.Data.Model;

namespace Projektmunka.UnitTests.ModelTests
{
    [TestFixture]
    public class OwnExerciseTests
    {
        [Test]
        public void Id_ShouldReturnValue_WhenSetInConstructor()
        {
            // Arrange
            Exercise exercise = new RepExercise(1,"Test Exercise", PhaseType.Core, TargetType.Chest, 4, 20, GenderPreference.Both  );
            OwnExercise ownExercise = new OwnExercise(exercise);
            int expected = 0;

            // Act
            int result = ownExercise.Id;

            // Assert
            Assert.That(result, Is.EqualTo(expected));
        }

        [Test]
        public void Rating_ShouldReturnValue_WhenSetInConstructor()
        {
            // Arrange
            Exercise exercise = new RepExercise(1, "Test Exercise", PhaseType.Core, TargetType.Chest, 4, 20, GenderPreference.Both);
            OwnExercise ownExercise = new OwnExercise(exercise);
            float expected = 1;

            // Act
            float result = ownExercise.Rating;

            // Assert
            Assert.That(result, Is.EqualTo(expected));
        }

        [Test]
        public void Exercise_ShouldReturnValue_WhenSetInConstructor()
        {
            // Arrange
            Exercise exercise = new RepExercise(1, "Test Exercise", PhaseType.Core, TargetType.Chest, 4, 20, GenderPreference.Both);
            OwnExercise ownExercise = new OwnExercise(exercise);
            Exercise expected = new RepExercise(1, "Test Exercise", PhaseType.Core, TargetType.Chest, 4, 20, GenderPreference.Both);

            // Act
            Exercise result = ownExercise.Exercise;

            // Assert
            Assert.That(result, Is.EqualTo(expected));
        }

        [Test]
        public void Like_ShouldChangeRatingValue_ToHIGHER_RATE_MULTIPLIER()
        {
            // Arrange
            Exercise exercise = new RepExercise(1, "Test Exercise", PhaseType.Core, TargetType.Chest, 4, 20, GenderPreference.Both);
            OwnExercise ownExercise = new OwnExercise(exercise);
            float expected = ConstParameters.HIGHER_RATE_MULTIPLIER;

            // Act
            ownExercise.Like();

            // Assert
            Assert.That(ownExercise.Rating, Is.EqualTo(expected));
        }

        [Test]
        public void SuperLike_ShouldChangeRatingValue_ToHIGHEST_RATE_MULTIPLIER()
        {
            // Arrange
            Exercise exercise = new RepExercise(1, "Test Exercise", PhaseType.Core, TargetType.Chest, 4, 20, GenderPreference.Both);
            OwnExercise ownExercise = new OwnExercise(exercise);
            float expected = ConstParameters.HIGHEST_RATE_MULTIPLIER;

            // Act
            ownExercise.SuperLike();

            // Assert
            Assert.That(ownExercise.Rating, Is.EqualTo(expected));
        }

        [Test]
        public void Neutral_ShouldChangeRatingValue_ToBASE_RATE_MULTIPLIER()
        {
            // Arrange
            Exercise exercise = new RepExercise(1, "Test Exercise", PhaseType.Core, TargetType.Chest, 4, 20, GenderPreference.Both);
            OwnExercise ownExercise = new OwnExercise(exercise);
            float expected = ConstParameters.BASE_RATE_MULTIPLIER;

            // Act
            ownExercise.Neutral();

            // Assert
            Assert.That(ownExercise.Rating, Is.EqualTo(expected));
        }

        [Test]
        public void Dislike_ShouldChangeRatingValue_ToLOWER_RATE_MULTIPLIER()
        {
            // Arrange
            Exercise exercise = new RepExercise(1, "Test Exercise", PhaseType.Core, TargetType.Chest, 4, 20, GenderPreference.Both);
            OwnExercise ownExercise = new OwnExercise(exercise);
            float expected = ConstParameters.LOWER_RATE_MULTIPLIER;

            // Act
            ownExercise.Dislike();

            // Assert
            Assert.That(ownExercise.Rating, Is.EqualTo(expected));
        }

        [Test]
        public void SuperDislike_ShouldChangeRatingValue_ToLOWER_RATE_MULTIPLIER()
        {
            // Arrange
            Exercise exercise = new RepExercise(1, "Test Exercise", PhaseType.Core, TargetType.Chest, 4, 20, GenderPreference.Both);
            OwnExercise ownExercise = new OwnExercise(exercise);
            float expected = ConstParameters.LOWEST_RATE_MULTIPLIER;

            // Act
            ownExercise.SuperDislike();

            // Assert
            Assert.That(ownExercise.Rating, Is.EqualTo(expected));
        }
    }
}
