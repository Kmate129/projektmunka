﻿using Projektmunka.Data.Model;

namespace Projektmunka.UnitTests.ModelTests
{
    [TestFixture]
    public class WeeklyPlanTests
    {
        private WeeklyPlan weeklyPlan;

        [SetUp]
        public void SetUp()
        {
            weeklyPlan = new WeeklyPlan();
        }

        [Test]
        public void Method_AddToDailyPlans_ShouldAddDailyPlanToList()
        {
            DailyPlan dailyPlan = new DailyPlan();

            weeklyPlan.AddToDailyPlans(dailyPlan);

            Assert.That(weeklyPlan.DailyPlans.Count, Is.EqualTo(1));
        }

        [Test]
        public void Method_GetNumberOfDays_ShouldReturnCorrectValue()
        {
            int result = weeklyPlan.GetNumberOfDays();

            Assert.That(result, Is.EqualTo(0));
        }

        [Test]
        public void Method_GetNumberOfDays_ShouldReturnCorrectValueAfterInsertion()
        {
            DailyPlan dailyPlan = new DailyPlan();
            DailyPlan dailyPlan2 = new DailyPlan();
            weeklyPlan.AddToDailyPlans(dailyPlan);
            weeklyPlan.AddToDailyPlans(dailyPlan2);

            int result = weeklyPlan.GetNumberOfDays();

            Assert.That(result, Is.EqualTo(2));
        }
    }
}
