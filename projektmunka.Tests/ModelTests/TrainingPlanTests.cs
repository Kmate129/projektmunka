﻿using Projektmunka.Data.Enums;
using Projektmunka.Data.Model;

namespace Projektmunka.UnitTests.ModelTests
{
    [TestFixture]
    public class TrainingPlanTests
    {
        [Test]
        public void TrainingPlan_Constructor_ShouldInitializeCorrectly()
        {
            // Arrange
            bool equipmentRequirement = true;
            int numberOfDays = 7;

            // Act
            TrainingPlan trainingPlan = new TrainingPlan(equipmentRequirement, numberOfDays);

            // Assert
            Assert.That(trainingPlan.EquipmentRequirement, Is.EqualTo(equipmentRequirement));
            Assert.That(trainingPlan.NumberOfDays, Is.EqualTo(numberOfDays));
        }

        [Test]
        public void TrainingPlan_AddToWeeklyPlans_ShouldAddWeeklyPlan()
        {
            // Arrange
            TrainingPlan trainingPlan = new TrainingPlan(false, 7);
            WeeklyPlan weeklyPlan = new WeeklyPlan();

            // Act
            trainingPlan.AddToWeeklyPlans(weeklyPlan);

            // Assert
            Assert.That(trainingPlan.WeeklyPlans.Count, Is.EqualTo(1));
            Assert.Contains(weeklyPlan, (System.Collections.ICollection?)trainingPlan.WeeklyPlans);
        }

        [Test]
        public void TrainingPlan_GetNumberOfWeeks_ShouldReturnCorrectValue()
        {
            // Arrange
            TrainingPlan trainingPlan = new TrainingPlan(false, 7);
            trainingPlan.WeeklyPlans = new List<WeeklyPlan>
            {
                new WeeklyPlan(),
                new WeeklyPlan(),
                new WeeklyPlan(),
            };

            // Act
            int numberOfWeeks = trainingPlan.GetNumberOfWeeks();

            // Assert
            Assert.That(numberOfWeeks, Is.EqualTo(3));
        }

        [Test]
        public void TrainingPlan_GetProgression_ShouldReturnCorrectValue()
        {
            // Arrange
            TrainingPlan trainingPlan = new TrainingPlan(false, 7);
            trainingPlan.WeeklyPlans = new List<WeeklyPlan>
            {
                new WeeklyPlan { Status = Status.Done },
                new WeeklyPlan { Status = Status.Done },
                new WeeklyPlan { Status = Status.Ready },
            };

            // Act
            float progression = trainingPlan.GetProgression();

            // Assert
            Assert.That(progression, Is.EqualTo(2 / 3f).Within(0.0001));
        }

        [Test]
        public void TrainingPlan_GetCurrentWeek_ShouldReturnCorrectValue()
        {
            //Arrange
            TrainingPlan trainingPlan = new TrainingPlan(false, 4);
            WeeklyPlan expected = new WeeklyPlan { Status = Status.Ready };

            trainingPlan.WeeklyPlans = new List<WeeklyPlan>
            {
                new WeeklyPlan { Status = Status.Done },
                new WeeklyPlan { Status = Status.Done },
                new WeeklyPlan { Status = Status.Done },
                expected,
                new WeeklyPlan { Status = Status.Unallowed },
                new WeeklyPlan { Status = Status.Unallowed },
            };

            //Act
            WeeklyPlan currentWeek = trainingPlan.GetCurrentWeek();

            // Assert
            Assert.That(currentWeek, Is.EqualTo(expected));
        }
    }
}
