﻿using Projektmunka.Data.Enums;
using Projektmunka.Data.Model;

namespace Projektmunka.UnitTests.ModelTests
{
    [TestFixture]
    public class DailyPlanTests
    {
        private DailyPlan dailyPlan;

        [SetUp]
        public void Setup()
        {
            dailyPlan = new DailyPlan();
        }

        [Test]
        public void DailyPlan_Initialization_ShouldSetDefaultValues()
        {
            // Assert
            Assert.Multiple(() =>
            {
                Assert.That(dailyPlan.Id, Is.EqualTo(0));
                Assert.That(dailyPlan.Status, Is.EqualTo(Status.Unallowed));
                Assert.That(dailyPlan.Workouts, Is.Empty);
                Assert.That(dailyPlan.WeeklyPlan, Is.Null);
            });
        }

        [Test]
        public void DailyPlan_InitializationWithWorkouts_ShouldSetWorkouts()
        {
            // Arrange
            ICollection<Workout> workouts = new List<Workout>();

            // Act
            dailyPlan = new DailyPlan(workouts);

            // Assert
            Assert.Multiple(() =>
            {
                Assert.That(dailyPlan.Workouts, Is.EqualTo(workouts));
                Assert.That(dailyPlan.Status, Is.EqualTo(Status.Unallowed));
            });
        }

        [Test]
        public void AddToWorkouts_ShouldAddWorkoutToTheList()
        {
            // Arrange
            ICollection<Workout> workouts = new List<Workout>();
            dailyPlan = new DailyPlan(workouts);
            Workout workout = new Workout();

            // Act
            dailyPlan.AddToWorkouts(workout);

            // Assert
            Assert.That(dailyPlan.Workouts, Has.Count.EqualTo(1));
            Assert.That(dailyPlan.Workouts, Contains.Item(workout));
        }
    }
}
