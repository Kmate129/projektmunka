﻿using Moq;
using Projektmunka.Data.Enums;
using Projektmunka.Data.Model;

namespace Projektmunka.UnitTests.ModelTests
{
    [TestFixture]
    public class RepExerciseTests
    {
        private Exercise exercise;

        [SetUp]
        public void SetUp()
        {
            exercise = new RepExercise(1, "Test Exercise", PhaseType.Core, TargetType.Abs, 5, 60, GenderPreference.Both);
        }

        [Test]
        public void Id_ShouldReturnExerciseId()
        {
            int expected = 1;
            int actual = exercise.Id;

            Assert.That(actual, Is.EqualTo(expected));
        }

        [Test]
        public void Name_ShouldReturnExerciseName()
        {
            string expected = "Test Exercise";
            string actual = exercise.Name;

            Assert.That(actual, Is.EqualTo(expected));
        }

        [Test]
        public void Phase_ShouldReturnExercisePhase()
        {
            PhaseType expected = PhaseType.Core;
            PhaseType actual = exercise.Phase;

            Assert.That(actual, Is.EqualTo(expected));
        }

        [Test]
        public void TargetMuscle_ShouldReturnExerciseTargetMuscle()
        {
            TargetType expected = TargetType.Abs;
            TargetType actual = exercise.TargetMuscle;

            Assert.That(actual, Is.EqualTo(expected));
        }

        [Test]
        public void TargetMuscleGroup_ShouldReturnExerciseTargetMuscleGroup()
        {
            MuscleGroupType expected = MuscleGroupType.Abs;
            MuscleGroupType actual = exercise.TargetMuscleGroup;

            Assert.That(actual, Is.EqualTo(expected));
        }

        [Test]
        public void BaseRound_ShouldReturnExerciseBaseRound()
        {
            int expected = 5;
            int actual = exercise.BaseRound;

            Assert.That(actual, Is.EqualTo(expected));
        }

        [Test]
        public void BaseRep_ShouldReturnExerciseBaseRep()
        {
            int expected = 60;
            int actual = ((RepExercise)exercise).BaseRep;

            Assert.That(actual, Is.EqualTo(expected));
        }

        [Test]
        public void Preference_ShouldReturnExercisePreference()
        {
            GenderPreference expected = GenderPreference.Both;
            GenderPreference actual = exercise.Preference;

            Assert.That(actual, Is.EqualTo(expected));
        }

        [Test]
        public void Visual_ShouldReturnExerciseVisual()
        {
            string expected = "test_exercise";
            string actual = exercise.Visual;

            Assert.That(actual, Is.EqualTo(expected));
        }

        [Test]
        public void IsEquipmentRequired_ShouldReturnTrue_WhenPrimaryEquipmentListIsNotEmpty()
        {
            // Arrange
            EquipmentTypeEntity equipment1 = new EquipmentTypeEntity(1, EquipmentType.Rope);
            EquipmentTypeEntity equipment2 = new EquipmentTypeEntity(2, EquipmentType.Adductor_machine);
            EquipmentTypeEntity equipment3 = new EquipmentTypeEntity(3, EquipmentType.Nothing);
            exercise.PrimaryEquipments = new List<EquipmentTypeEntity>();
            exercise.SecondaryEquipments = new List<EquipmentTypeEntity>();
            exercise.PrimaryEquipments.Add(equipment1);
            exercise.PrimaryEquipments.Add(equipment2);
            exercise.SecondaryEquipments.Add(equipment3);

            // Act
            bool result = exercise.IsEquipmentRequired();

            // Assert
            Assert.That(result, Is.True);
        }

        [Test]
        public void IsEquipmentRequired_ShouldReturnFalse_WhenPrimaryEquipmentListAreEmpty()
        {
            // Arrange
            EquipmentTypeEntity equipment1 = new EquipmentTypeEntity(1, EquipmentType.Nothing);
            EquipmentTypeEntity equipment2 = new EquipmentTypeEntity(2, EquipmentType.Adductor_machine);
            EquipmentTypeEntity equipment3 = new EquipmentTypeEntity(3, EquipmentType.Bench);
            exercise.PrimaryEquipments = new List<EquipmentTypeEntity>();
            exercise.SecondaryEquipments = new List<EquipmentTypeEntity>();
            exercise.PrimaryEquipments.Add(equipment1);
            exercise.SecondaryEquipments.Add(equipment2);
            exercise.SecondaryEquipments.Add(equipment3);

            // Act
            bool result = exercise.IsEquipmentRequired();

            // Assert
            Assert.That(result, Is.False);
        }

        [Test]
        public void TestingVisualConversion()
        {
            Exercise exercise = new RepExercise(0, "Single-leg lying cross-over stretch", PhaseType.WarmUp, TargetType.Chest, 5, 20, GenderPreference.Man);

            Assert.That(exercise.Visual, Is.EqualTo("single_leg_lying_cross_over_stretch"));
        }
    }
}
