﻿using Projektmunka.Data.Model;

namespace Projektmunka.UnitTests.ModelTests
{
    [TestFixture]
    public class ExerciseDetailTests
    {
        [Test]
        public void ExerciseDetail_Constructor_InitializesIdAndRestTime()
        {
            // Arrange and Act
            var exerciseDetail = new ExerciseDetail();

            // Assert
            Assert.Multiple(() =>
            {
                Assert.That(exerciseDetail.Id, Is.EqualTo(0));
                Assert.That(exerciseDetail.RestTime, Is.EqualTo(60));
            });
        }

        [Test]
        public void ExerciseDetail_RestTime_CanBeSet()
        {
            // Arrange
            var exerciseDetail = new ExerciseDetail();

            // Act
            exerciseDetail.RestTime = 90;

            // Assert
            Assert.That(exerciseDetail.RestTime, Is.EqualTo(90));
        }

        [Test]
        public void ExerciseDetail_WorkoutWarmup_CanBeSet()
        {
            // Arrange
            var exerciseDetail = new ExerciseDetail();
            var workout = new Workout();

            // Act
            exerciseDetail.WorkoutWarmup = workout;

            // Assert
            Assert.That(exerciseDetail.WorkoutWarmup, Is.SameAs(workout));
        }

        [Test]
        public void ExerciseDetail_WorkoutCore_CanBeSet()
        {
            // Arrange
            var exerciseDetail = new ExerciseDetail();
            var workout = new Workout();

            // Act
            exerciseDetail.WorkoutCore = workout;

            // Assert
            Assert.That(exerciseDetail.WorkoutCore, Is.SameAs(workout));
        }

        [Test]
        public void ExerciseDetail_WorkoutStretching_CanBeSet()
        {
            // Arrange
            var exerciseDetail = new ExerciseDetail();
            var workout = new Workout();

            // Act
            exerciseDetail.WorkoutStretching = workout;

            // Assert
            Assert.That(exerciseDetail.WorkoutStretching, Is.SameAs(workout));
        }

        [Test]
        public void ExerciseDetail_OwnExercise_CanBeSet()
        {
            // Arrange
            var exerciseDetail = new ExerciseDetail();
            var ownExercise = new OwnExercise();

            // Act
            exerciseDetail.OwnExercise = ownExercise;

            // Assert
            Assert.That(exerciseDetail.OwnExercise, Is.SameAs(ownExercise));
        }
    }
}
