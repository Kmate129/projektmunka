﻿using Projektmunka.Data.Model;

namespace Projektmunka.UnitTests.ModelTests
{
    [TestFixture]
    public class NutritionSchemeTests
    {
        [Test]
        public void TestDefaultConstructor()
        {
            var nutritionScheme = new NutritionScheme();

            Assert.That(nutritionScheme, Is.Not.Null);
        }

        [Test]
        public void TestId()
        {
            NutritionScheme nutritionScheme = new NutritionScheme(1, 3000, 400, 20, 120, 50, 10, 3);
            int expected = 1;

            Assert.That(nutritionScheme.Id, Is.EqualTo(expected));
        }

        [Test]
        public void TestEnergy()
        {
            NutritionScheme nutritionScheme = new NutritionScheme(1, 3000, 400, 20, 120, 50, 10, 3);
            int expected = 3000;

            Assert.That(nutritionScheme.Energy, Is.EqualTo(expected));
        }

        [Test]
        public void TestCarbohydrates()
        {
            NutritionScheme nutritionScheme = new NutritionScheme(1, 3000, 400, 20, 120, 50, 10, 3);
            int expected = 400;

            Assert.That(nutritionScheme.Carbohydrates, Is.EqualTo(expected));
        }

        [Test]
        public void TestSugar()
        {
            NutritionScheme nutritionScheme = new NutritionScheme(1, 3000, 400, 20, 120, 50, 10, 3);
            int expected = 20;

            Assert.That(nutritionScheme.Sugar, Is.EqualTo(expected));
        }

        [Test]
        public void TestProtein()
        {
            NutritionScheme nutritionScheme = new NutritionScheme(1, 3000, 400, 20, 120, 50, 10, 3);
            int expected = 120;

            Assert.That(nutritionScheme.Protein, Is.EqualTo(expected));
        }

        [Test]
        public void TestFat()
        {
            NutritionScheme nutritionScheme = new NutritionScheme(1, 3000, 400, 20, 120, 50, 10, 3);
            int expected = 50;

            Assert.That(nutritionScheme.Fat, Is.EqualTo(expected));
        }

        [Test]
        public void TestSaturatedFat()
        {
            NutritionScheme nutritionScheme = new NutritionScheme(1, 3000, 400, 20, 120, 50, 10, 3);
            int expected = 10;

            Assert.That(nutritionScheme.SaturatedFat, Is.EqualTo(expected));
        }

        [Test]
        public void TestSalt()
        {
            NutritionScheme nutritionScheme = new NutritionScheme(1, 3000, 400, 20, 120, 50, 10, 3);
            int expected = 3;

            Assert.That(nutritionScheme.Salt, Is.EqualTo(expected));
        }
    }
}
