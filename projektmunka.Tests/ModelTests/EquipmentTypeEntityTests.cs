﻿using Projektmunka.Data.Enums;
using Projektmunka.Data.Model;

namespace Projektmunka.UnitTests.ModelTests
{
    [TestFixture]
    public class EquipmentTypeEntityTests
    {
        private EquipmentTypeEntity entity;

        [SetUp]
        public void SetUp()
        {
            this.entity = new EquipmentTypeEntity(1, EquipmentType.Ankle_strap);
        }

        [Test]
        public void EquipmentTypeEntityId_ShouldReturnCorrectValue()
        {
            int expected = 1;

            Assert.That(entity.Id, Is.EqualTo(expected));
        }

        [Test]
        public void Value_ShouldReturnCorrectValue()
        {
            EquipmentType expected = EquipmentType.Ankle_strap;

            Assert.That(entity.Value, Is.EqualTo(expected));
        }

        [Test]
        public void EquipmentsPrim_ShouldReturnCorrectValue()
        {
            this.entity = new EquipmentTypeEntity { EquipmentsPrim = new List<Exercise>() };

            Assert.That(entity.EquipmentsPrim, Is.InstanceOf<List<Exercise>>());
        }

        [Test]
        public void EquipmentsSec_ShouldReturnCorrectValue()
        {
            this.entity = new EquipmentTypeEntity { EquipmentsSec = new List<Exercise>() };

            Assert.That(entity.EquipmentsSec, Is.InstanceOf<List<Exercise>>());
        }
    }
}
