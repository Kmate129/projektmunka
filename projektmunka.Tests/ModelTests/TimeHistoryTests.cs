﻿using Projektmunka.Data.Model;
using System.Reflection;

namespace Projektmunka.UnitTests.ModelTests
{
    [TestFixture]
    internal class TimeHistoryTests
    {
        [Test]
        public void Constructor_ThrowsException_WhenNumberOfWeeksIsLessThan12()
        {
            Assert.Throws<ArgumentException>(() => new TimeHistory(10));
        }

        [Test]
        public void Constructor_InitializesProperties_WhenNumberOfWeeksIsGreaterThanOrEqualTo12()
        {
            var timeHistory = new TimeHistory(12);
            
            Assert.Multiple(() =>
            {
                Assert.That(timeHistory.CurrentWeek, Is.EqualTo(0));
                Assert.That(timeHistory.TimeData.GetLength(1), Is.EqualTo(12));
            });
        }

        [Test]
        public void StoreTotalTime()
        {
            var timeHistory = new TimeHistory(12);

            timeHistory.PushToTotalStack(new TimeSpan(0, 40, 00));
            timeHistory.PushToTotalStack(new TimeSpan(0, 20, 00));
            timeHistory.PushToTotalStack(new TimeSpan(0, 40, 00));
            timeHistory.PushToTotalStack(new TimeSpan(0, 20, 00));

            timeHistory.SaveTotalStackAverage();

            Assert.That(timeHistory.GetTotalValues(), Is.EqualTo(new int[] { 30, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 }));
        }

        [Test]
        public void StoreActiveTime()
        {
            var timeHistory = new TimeHistory(12);

            timeHistory.PushToActiveStack(new TimeSpan(0, 40, 00));
            timeHistory.PushToActiveStack(new TimeSpan(0, 20, 00));
            timeHistory.PushToActiveStack(new TimeSpan(0, 40, 00));
            timeHistory.PushToActiveStack(new TimeSpan(0, 20, 00));

            timeHistory.SaveActiveStackAverage();

            Assert.That(timeHistory.GetActiveValues(), Is.EqualTo(new int[] { 30, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 }));
        }


        [Test]
        public void GetActiveValues_ReturnsActiveValuesArray()
        {
            var timeHistory = new TimeHistory(12);
            int[,] expectedTimeData = new int[,] { { 10, 20, 30 }, { 40, 50, 60 } };
            timeHistory.TimeData = expectedTimeData;

            int[] activeValues = timeHistory.GetActiveValues();

            Assert.That(activeValues.Length, Is.EqualTo(3));
            Assert.That(activeValues[0], Is.EqualTo(10));
            Assert.That(activeValues[1], Is.EqualTo(20));
            Assert.That(activeValues[2], Is.EqualTo(30));
        }

        [Test]
        public void GetTotalValues_ReturnsTotalValuesArray()
        {
            var timeHistory = new TimeHistory(12);

            int[,] expectedTimeData = new int[,] { { 10, 20, 30 }, { 40, 50, 60 } };
            timeHistory.TimeData = expectedTimeData;

            int[] totalValues = timeHistory.GetTotalValues();

            Assert.That(totalValues.Length, Is.EqualTo(3));
            Assert.That(totalValues[0], Is.EqualTo(40));
            Assert.That(totalValues[1], Is.EqualTo(50));
            Assert.That(totalValues[2], Is.EqualTo(60));
        }

        [Test]
        public void StoreTotalValue_UpdatesSerializedTotalStack()
        {
            var timeHistory = new TimeHistory(12);

            timeHistory.PushToTotalStack(new TimeSpan(0, 40, 00));
            timeHistory.PushToTotalStack(new TimeSpan(0, 20, 00));
            timeHistory.PushToTotalStack(new TimeSpan(0, 40, 00));
            timeHistory.PushToTotalStack(new TimeSpan(0, 20, 00));

            timeHistory.SaveTotalStackAverage();

            timeHistory.PushToTotalStack(new TimeSpan(0, 60, 00));
            timeHistory.PushToTotalStack(new TimeSpan(0, 20, 00));
            timeHistory.PushToTotalStack(new TimeSpan(0, 60, 00));
            timeHistory.PushToTotalStack(new TimeSpan(0, 20, 00));

            timeHistory.SaveTotalStackAverage();

            Assert.That(timeHistory.GetTotalValues(), Is.EqualTo(new int[] { 30, 40, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 }));
        }

        [Test]
        public void StoreActiveValue_UpdatesSerializedActiveStack()
        {
            var timeHistory = new TimeHistory(12);

            timeHistory.PushToActiveStack(new TimeSpan(0, 40, 00));
            timeHistory.PushToActiveStack(new TimeSpan(0, 20, 00));
            timeHistory.PushToActiveStack(new TimeSpan(0, 40, 00));
            timeHistory.PushToActiveStack(new TimeSpan(0, 20, 00));

            timeHistory.SaveActiveStackAverage();

            timeHistory.PushToActiveStack(new TimeSpan(0, 60, 00));
            timeHistory.PushToActiveStack(new TimeSpan(0, 20, 00));
            timeHistory.PushToActiveStack(new TimeSpan(0, 60, 00));
            timeHistory.PushToActiveStack(new TimeSpan(0, 20, 00));

            timeHistory.SaveActiveStackAverage();

            Assert.That(timeHistory.GetActiveValues(), Is.EqualTo(new int[] { 30, 40, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 }));
        }
    }
}
