﻿using Projektmunka.Data.Enums;
using Projektmunka.Data.Model;

namespace Projektmunka.UnitTests.ModelTests
{
    [TestFixture]
    public class WorkoutTests
    {
        [Test]
        public void DefaultConstructor_InitializesProperties()
        {
            var workout = new Workout();

            Assert.Multiple(() =>
            {
                Assert.That(workout.WarmUps, Is.Not.Null);
                Assert.That(workout.Cores, Is.Not.Null);
                Assert.That(workout.Stretches, Is.Not.Null);
            });
            Assert.Multiple(() =>
            {
                Assert.That(workout.Id, Is.EqualTo(0));
                Assert.That(workout.MuscleGroup, Is.EqualTo(default(MuscleGroupType)));
            });
            Assert.Multiple(() =>
            {
                //Assert.That(workout.Status, Is.);
                Assert.That(workout.DailyPlan, Is.Null);
            });
        }

        [TestCase(MuscleGroupType.Chest, Status.Done)]
        [TestCase(MuscleGroupType.Back, Status.Ready)]
        public void Properties_GetAndSetValuesCorrectly(MuscleGroupType muscleGroup, Status status)
        {
            var workout = new Workout();
            int expedtecId = 0;

            workout.MuscleGroup = muscleGroup;
            workout.Status = status;

            Assert.Multiple(() =>
            {
                Assert.That(workout.Id, Is.EqualTo(expedtecId));
                Assert.That(workout.MuscleGroup, Is.EqualTo(muscleGroup));
                Assert.That(workout.Status, Is.EqualTo(status));
            });
        }

        [Test]
        public void WarmUps_AddsAndRetrievesItemsCorrectly()
        {
            var workout = new Workout();
            var warmUp = new ExerciseDetail();

            workout.WarmUps.Add(warmUp);

            Assert.That(workout.WarmUps, Has.Count.EqualTo(1));
            Assert.That(workout.WarmUps.Single(), Is.SameAs(warmUp));
        }

        [Test]
        public void Cores_AddsAndRetrievesItemsCorrectly()
        {
            var workout = new Workout();
            var core = new ExerciseDetail();

            workout.Cores.Add(core);

            Assert.That(workout.Cores, Has.Count.EqualTo(1));
            Assert.That(workout.Cores.Single(), Is.SameAs(core));
        }

        [Test]
        public void Stretches_AddsAndRetrievesItemsCorrectly()
        {
            var workout = new Workout();
            var stretch = new ExerciseDetail();

            workout.Stretches.Add(stretch);

            Assert.That(workout.Stretches, Has.Count.EqualTo(1));
            Assert.That(workout.Stretches.Single(), Is.SameAs(stretch));
        }
    }
}
