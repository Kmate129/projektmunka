using Projektmunka.Logic;
using Projektmunka.Data.Enums;
using Projektmunka.Logic.Interfaces;

namespace Projektmunka.UnitTests.LogicTests
{
    [TestFixture]
    public class NutritionCalculatorTests
    {
        private INutritionCalculator calculator;

        [SetUp]
        public void SetUp()
        {
            this.calculator = new NutritionCalculator();
        }

        [Test]
        public void Calculate_ReturnsCorrectMacrosForMale()
        {
            // Arrange
            var goal = GoalType.MassGain;
            var gender = GenderType.Male;
            var height = 180f;
            var weight = 80f;
            var age = 30;
            var numberOfDays = 5;
            var kgPerWeek = 0.5f;

            // Act
            var result = this.calculator.Calculate(gender, height, weight, age, numberOfDays, kgPerWeek);

            // Assert
            Assert.Multiple(() =>
            {
                Assert.That(result.Energy, Is.InRange(3350,3450));
                Assert.That(result.Protein, Is.InRange(100,110));
                Assert.That(result.Fat, Is.InRange(95,105));
                Assert.That(result.Carbohydrates, Is.InRange(450,550));
            });
        }

        [Test]
        public void Calculate_ReturnsCorrectMacrosForFemale()
        {
            // Arrange
            var goal = GoalType.WeightLoss;
            var gender = GenderType.Female;
            var height = 160f;
            var weight = 60f;
            var age = 25;
            var numberOfDays = 4;
            var kgPerWeek = -0.3f;

            // Act
            var result = this.calculator.Calculate(gender, height, weight, age, numberOfDays, kgPerWeek);

            // Assert
            Assert.That(result.Energy, Is.InRange(1750,1850));
            Assert.That(result.Protein, Is.InRange(50, 60));
            Assert.That(result.Fat, Is.InRange(50, 60));
            Assert.That(result.Carbohydrates, Is.InRange(260,280));
        }

        [Test]
        public void Calculate_ReturnsCorrectMacrosWhenGoalIsStagnation()
        {
            // Arrange
            var goal = GoalType.Stagnation;
            var gender = GenderType.Male;
            var height = 170f;
            var weight = 75f;
            var age = 40;
            var numberOfDays = 3;
            var kgPerWeek = 0f;

            // Act
            var result = this.calculator.Calculate(gender, height, weight, age, numberOfDays, kgPerWeek);

            // Assert
            Assert.That(result.Energy, Is.InRange(2450,2550));
            Assert.That(result.Protein, Is.InRange(70,80));
            Assert.That(result.Fat, Is.InRange(70,80));
            Assert.That(result.Carbohydrates, Is.InRange(370,380));
        }
    }
}