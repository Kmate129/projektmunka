﻿using Projektmunka.Logic;
using Projektmunka.Logic.Interfaces;
using Moq;
using Projektmunka.Repository.Interfaces;
using Projektmunka.Data.Model;
using Projektmunka.Data.Enums;
using Projektmunka.Platform.Interfaces;

namespace Projektmunka.UnitTests.LogicTests
{
    [TestFixture]
    internal class DailyPlanControlTests
    {
        private Mock<IApp> mockApp;
        private Mock<IAppProvider> mockAppProvider;
        private Mock<IPlanGenerator> mockPlanGenerator;
        private Mock<IUserRepository> repo;
        private IDailyPlanControl logic;

        [SetUp]
        public void SetUp()
        {
            this.mockApp = new Mock<IApp>();
            this.mockAppProvider = new Mock<IAppProvider>();
            this.mockPlanGenerator = new Mock<IPlanGenerator>();
            this.mockAppProvider.Setup(provider => provider.GetApp()).Returns(this.mockApp.Object);
            this.repo = new Mock<IUserRepository>();

            User user = new User("User name", "Password", 170, 30, GenderType.Male);
            TrainingPlan trainingPlan = this.SettingUpStartingTrainingPlan();
            user.TrainingPlan = trainingPlan;
            trainingPlan.User = user;

            mockApp.Setup(x => x.CurrentUser).Returns(user);
            repo.Setup(x=>x.GetUserById(It.IsAny<int>())).Returns(user);

            this.logic = new DailyPlanControl(this.repo.Object, this.mockAppProvider.Object, this.mockPlanGenerator.Object);
        }

        [Test]
        public void GetCurrentExercise_ReturnsCurrentExercise()
        {
            // Arrange
            Exercise expected = new RepExercise(0, "Warmpup name", PhaseType.WarmUp, TargetType.Chest, 2, 50, GenderPreference.Man);

            // Act
            var result = this.logic.GetCurrentExercise();

            // Assert
            Assert.That(result.OwnExercise.Exercise, Is.EqualTo(expected));
        }

        [Test]
        public void GetCurrentRestTime_ReturnCurrentRestTime()
        {
            // Arrange
            ExerciseDetail exerciseDetail = new ExerciseDetail(0);
            int expected = exerciseDetail.RestTime;

            // Act
            var result = this.logic.GetCurrentRestTime();

            // Assert
            Assert.That(result, Is.EqualTo(expected));
        }

        [Test]
        public void StepToNextExercise_ReturnCorrectCurrentExercise()
        {
            // Arrange
            Exercise expected = new RepExercise(0, "Warmup name", PhaseType.WarmUp, TargetType.Chest, 2, 50, GenderPreference.Man);

            // Act
            this.logic.CurrentExerciseDone();
            var result = this.logic.GetCurrentExercise();

            // Assert
            Assert.That(result.OwnExercise.Exercise, Is.EqualTo(expected));
        }

        [Test]
        public void StepToNextExerciseFromWarmupsToCores_ReturnCorrectCurrentExercise()
        {
            // Arrange
            Exercise expected = new RepExercise(1, "Core name", PhaseType.Core, TargetType.Chest, 2, 50, GenderPreference.Man);

            // Act
            this.logic.CurrentExerciseDone();
            this.logic.CurrentExerciseDone();
            var result = this.logic.GetCurrentExercise();

            // Assert
            Assert.That(result.OwnExercise.Exercise, Is.EqualTo(expected));
        }

        [Test]
        public void StepToNextExerciseFromWarmupsToStretches_ReturnCorrectCurrentExercise()
        {
            // Arrange
            Exercise expected = new RepExercise(2, "Stretch name", PhaseType.Stretching, TargetType.Chest, 2, 50, GenderPreference.Man);

            // Act
            this.logic.CurrentExerciseDone();
            this.logic.CurrentExerciseDone();
            this.logic.CurrentExerciseDone();
            this.logic.CurrentExerciseDone();
            this.logic.CurrentExerciseDone();
            this.logic.CurrentExerciseDone();
            var result = this.logic.GetCurrentExercise();

            // Assert
            Assert.That(result.OwnExercise.Exercise, Is.EqualTo(expected));
        }

        [Test]
        public void StepToNextWorkout_ReturnsCurrentExercise()
        {
            // Arrange
            Exercise expected = new RepExercise(0, "Warmup name", PhaseType.WarmUp, TargetType.Chest, 2, 50, GenderPreference.Man);

            // Act
            this.logic.CurrentExerciseDone();
            this.logic.CurrentExerciseDone();
            this.logic.CurrentExerciseDone();
            this.logic.CurrentExerciseDone();
            this.logic.CurrentExerciseDone();
            this.logic.CurrentExerciseDone();
            this.logic.CurrentExerciseDone();
            this.logic.CurrentExerciseDone();
            var result = this.logic.GetCurrentExercise();

            // Assert
            Assert.That(result.OwnExercise.Exercise, Is.EqualTo(expected));
        }

        [Test]
        public void StepToNextDailyPlan_ReturnsCurrentExercise()
        {
            // Arrange
            Exercise expected = new RepExercise(0, "Warmup name", PhaseType.WarmUp, TargetType.Chest, 2, 50, GenderPreference.Man);

            // Act
            // 1 workout
            this.logic.CurrentExerciseDone();
            this.logic.CurrentExerciseDone();
            this.logic.CurrentExerciseDone();
            this.logic.CurrentExerciseDone();
            this.logic.CurrentExerciseDone();
            this.logic.CurrentExerciseDone();
            this.logic.CurrentExerciseDone();
            this.logic.CurrentExerciseDone();
            // 2 workout
            this.logic.CurrentExerciseDone();
            this.logic.CurrentExerciseDone();
            this.logic.CurrentExerciseDone();
            this.logic.CurrentExerciseDone();
            this.logic.CurrentExerciseDone();
            this.logic.CurrentExerciseDone();
            this.logic.CurrentExerciseDone();
            this.logic.CurrentExerciseDone();
            var result = this.logic.GetCurrentExercise();

            // Assert
            Assert.That(result.OwnExercise.Exercise, Is.EqualTo(expected));
        }

        [Test]
        public void StepToNextWeek_ReturnsCurrentExercise()
        {
            // Arrange
            Exercise expected = new RepExercise(0, "Warmup name", PhaseType.WarmUp, TargetType.Chest, 2, 50, GenderPreference.Man);

            // Act

            // 1 week
            for (int k = 0; k < 6; k++)
            {
                // 1 workout
                for (int j = 0; j < 8; j++)
                {
                    this.logic.CurrentExerciseDone();
                }
            }

            var result = this.logic.GetCurrentExercise();

            // Assert
            Assert.That(result.OwnExercise.Exercise, Is.EqualTo(expected));
        }

        [Test]
        public void TrainingPlanDone_ReturnsNull()
        {
            for (int i = 0; i < 3; i++)
            {
                // 1 week
                for (int j = 0; j < 6; j++)
                {
                    // 1 workout
                    for (int k = 0; k < 8; k++)
                    {
                        this.logic.CurrentExerciseDone();
                    }
                }
            }

            var result = this.logic.GetCurrentExercise();

            Assert.That(result, Is.Null);
        }


        private TrainingPlan SettingUpStartingTrainingPlan()
        {
            TrainingPlan trainingPlan = new TrainingPlan(false, 4, 0);

            WeeklyPlan weeklyPlan1 = new WeeklyPlan(0);
            WeeklyPlan weeklyPlan2 = new WeeklyPlan(1);
            WeeklyPlan weeklyPlan3 = new WeeklyPlan(2);
            trainingPlan.AddToWeeklyPlans(weeklyPlan1);
            trainingPlan.AddToWeeklyPlans(weeklyPlan2);
            trainingPlan.AddToWeeklyPlans(weeklyPlan3);
            weeklyPlan1.TrainingPlan = trainingPlan;
            weeklyPlan2.TrainingPlan = trainingPlan;
            weeklyPlan3.TrainingPlan = trainingPlan;

            DailyPlan dailyPlan11 = new DailyPlan(0);
            DailyPlan dailyPlan12 = new DailyPlan(1);
            DailyPlan dailyPlan13 = new DailyPlan(2);
            DailyPlan dailyPlan14 = new DailyPlan(3);
            DailyPlan dailyPlan21 = new DailyPlan(4);
            DailyPlan dailyPlan22 = new DailyPlan(5);
            DailyPlan dailyPlan23 = new DailyPlan(6);
            DailyPlan dailyPlan24 = new DailyPlan(7);
            DailyPlan dailyPlan31 = new DailyPlan(8);
            DailyPlan dailyPlan32 = new DailyPlan(9);
            DailyPlan dailyPlan33 = new DailyPlan(10);
            DailyPlan dailyPlan34 = new DailyPlan(11);
            weeklyPlan1.DailyPlans.Add(dailyPlan11);
            weeklyPlan1.DailyPlans.Add(dailyPlan12);
            weeklyPlan1.DailyPlans.Add(dailyPlan13);
            weeklyPlan1.DailyPlans.Add(dailyPlan14);
            weeklyPlan2.DailyPlans.Add(dailyPlan21);
            weeklyPlan2.DailyPlans.Add(dailyPlan22);
            weeklyPlan2.DailyPlans.Add(dailyPlan23);
            weeklyPlan2.DailyPlans.Add(dailyPlan24);
            weeklyPlan3.DailyPlans.Add(dailyPlan31);
            weeklyPlan3.DailyPlans.Add(dailyPlan32);
            weeklyPlan3.DailyPlans.Add(dailyPlan33);
            weeklyPlan3.DailyPlans.Add(dailyPlan34);
            dailyPlan11.WeeklyPlan = weeklyPlan1;
            dailyPlan12.WeeklyPlan = weeklyPlan1;
            dailyPlan13.WeeklyPlan = weeklyPlan1;
            dailyPlan14.WeeklyPlan = weeklyPlan1;
            dailyPlan21.WeeklyPlan = weeklyPlan2;
            dailyPlan22.WeeklyPlan = weeklyPlan2;
            dailyPlan23.WeeklyPlan = weeklyPlan2;
            dailyPlan24.WeeklyPlan = weeklyPlan2;
            dailyPlan31.WeeklyPlan = weeklyPlan3;
            dailyPlan32.WeeklyPlan = weeklyPlan3;
            dailyPlan33.WeeklyPlan = weeklyPlan3;
            dailyPlan34.WeeklyPlan = weeklyPlan3;

            Workout workout1chest = new Workout(0);
            Workout workout1abs = new Workout(1);
            Workout workout1arm = new Workout(2);
            Workout workout1shoulder = new Workout(3);
            Workout workout1back = new Workout(4);
            Workout workout1leg = new Workout(5);

            Workout workout2chest = new Workout(6);
            Workout workout2abs = new Workout(7);
            Workout workout2arm = new Workout(8);
            Workout workout2shoulder = new Workout(9);
            Workout workout2back = new Workout(10);
            Workout workout2leg = new Workout(11);

            Workout workout3chest = new Workout(12);
            Workout workout3abs = new Workout(13);
            Workout workout3arm = new Workout(14);
            Workout workout3shoulder = new Workout(15);
            Workout workout3back = new Workout(16);
            Workout workout3leg = new Workout(17);

            dailyPlan11.Workouts.Add(workout1chest);
            dailyPlan11.Workouts.Add(workout1abs);
            dailyPlan12.Workouts.Add(workout1arm);
            dailyPlan12.Workouts.Add(workout1shoulder);
            dailyPlan13.Workouts.Add(workout1back);
            dailyPlan14.Workouts.Add(workout1leg);
            dailyPlan21.Workouts.Add(workout2chest);
            dailyPlan21.Workouts.Add(workout2abs);
            dailyPlan22.Workouts.Add(workout2arm);
            dailyPlan22.Workouts.Add(workout2shoulder);
            dailyPlan23.Workouts.Add(workout2back);
            dailyPlan24.Workouts.Add(workout2leg);
            dailyPlan31.Workouts.Add(workout3chest);
            dailyPlan31.Workouts.Add(workout3abs);
            dailyPlan32.Workouts.Add(workout3arm);
            dailyPlan32.Workouts.Add(workout3shoulder);
            dailyPlan33.Workouts.Add(workout3back);
            dailyPlan34.Workouts.Add(workout3leg);
            workout1chest.DailyPlan = dailyPlan11;
            workout1abs.DailyPlan = dailyPlan11;
            workout1arm.DailyPlan = dailyPlan12;
            workout1shoulder.DailyPlan = dailyPlan12;
            workout1back.DailyPlan = dailyPlan13;
            workout1leg.DailyPlan = dailyPlan14;
            workout2chest.DailyPlan = dailyPlan21;
            workout2abs.DailyPlan = dailyPlan21;
            workout2arm.DailyPlan = dailyPlan22;
            workout2shoulder.DailyPlan = dailyPlan22;
            workout2back.DailyPlan = dailyPlan23;
            workout2leg.DailyPlan = dailyPlan24;
            workout3chest.DailyPlan = dailyPlan31;
            workout3abs.DailyPlan = dailyPlan31;
            workout3arm.DailyPlan = dailyPlan32;
            workout3shoulder.DailyPlan = dailyPlan32;
            workout3back.DailyPlan = dailyPlan33;
            workout3leg.DailyPlan = dailyPlan34;

            ExerciseDetail exerciseDetail1chestW1 = new ExerciseDetail(0);
            ExerciseDetail exerciseDetail1chestW2 = new ExerciseDetail(1);
            ExerciseDetail exerciseDetail1chestC1 = new ExerciseDetail(2);
            ExerciseDetail exerciseDetail1chestC2 = new ExerciseDetail(3);
            ExerciseDetail exerciseDetail1chestC3 = new ExerciseDetail(4);
            ExerciseDetail exerciseDetail1chestC4 = new ExerciseDetail(5);
            ExerciseDetail exerciseDetail1chestS1 = new ExerciseDetail(6);
            ExerciseDetail exerciseDetail1chestS2 = new ExerciseDetail(7);
            ExerciseDetail exerciseDetail1absW1 = new ExerciseDetail(8);
            ExerciseDetail exerciseDetail1absW2 = new ExerciseDetail(9);
            ExerciseDetail exerciseDetail1absC1 = new ExerciseDetail(10);
            ExerciseDetail exerciseDetail1absC2 = new ExerciseDetail(11);
            ExerciseDetail exerciseDetail1absC3 = new ExerciseDetail(12);
            ExerciseDetail exerciseDetail1absC4 = new ExerciseDetail(13);
            ExerciseDetail exerciseDetail1absS1 = new ExerciseDetail(14);
            ExerciseDetail exerciseDetail1absS2 = new ExerciseDetail(15);
            ExerciseDetail exerciseDetail1armW1 = new ExerciseDetail(16);
            ExerciseDetail exerciseDetail1armW2 = new ExerciseDetail(17);
            ExerciseDetail exerciseDetail1armC1 = new ExerciseDetail(18);
            ExerciseDetail exerciseDetail1armC2 = new ExerciseDetail(19);
            ExerciseDetail exerciseDetail1armC3 = new ExerciseDetail(20);
            ExerciseDetail exerciseDetail1armC4 = new ExerciseDetail(21);
            ExerciseDetail exerciseDetail1armS1 = new ExerciseDetail(22);
            ExerciseDetail exerciseDetail1armS2 = new ExerciseDetail(23);
            ExerciseDetail exerciseDetail1shoulderW1 = new ExerciseDetail(24);
            ExerciseDetail exerciseDetail1shoulderW2 = new ExerciseDetail(25);
            ExerciseDetail exerciseDetail1shoulderC1 = new ExerciseDetail(26);
            ExerciseDetail exerciseDetail1shoulderC2 = new ExerciseDetail(27);
            ExerciseDetail exerciseDetail1shoulderC3 = new ExerciseDetail(28);
            ExerciseDetail exerciseDetail1shoulderC4 = new ExerciseDetail(29);
            ExerciseDetail exerciseDetail1shoulderS1 = new ExerciseDetail(30);
            ExerciseDetail exerciseDetail1shoulderS2 = new ExerciseDetail(31);
            ExerciseDetail exerciseDetail1backW1 = new ExerciseDetail(32);
            ExerciseDetail exerciseDetail1backW2 = new ExerciseDetail(33);
            ExerciseDetail exerciseDetail1backC1 = new ExerciseDetail(34);
            ExerciseDetail exerciseDetail1backC2 = new ExerciseDetail(35);
            ExerciseDetail exerciseDetail1backC3 = new ExerciseDetail(36);
            ExerciseDetail exerciseDetail1backC4 = new ExerciseDetail(37);
            ExerciseDetail exerciseDetail1backS1 = new ExerciseDetail(38);
            ExerciseDetail exerciseDetail1backS2 = new ExerciseDetail(39);
            ExerciseDetail exerciseDetail1legW1 = new ExerciseDetail(40);
            ExerciseDetail exerciseDetail1legW2 = new ExerciseDetail(41);
            ExerciseDetail exerciseDetail1legC1 = new ExerciseDetail(42);
            ExerciseDetail exerciseDetail1legC2 = new ExerciseDetail(43);
            ExerciseDetail exerciseDetail1legC3 = new ExerciseDetail(44);
            ExerciseDetail exerciseDetail1legC4 = new ExerciseDetail(45);
            ExerciseDetail exerciseDetail1legS1 = new ExerciseDetail(46);
            ExerciseDetail exerciseDetail1legS2 = new ExerciseDetail(47);

            ExerciseDetail exerciseDetail2chestW1 = new ExerciseDetail(48);
            ExerciseDetail exerciseDetail2chestW2 = new ExerciseDetail(49);
            ExerciseDetail exerciseDetail2chestC1 = new ExerciseDetail(50);
            ExerciseDetail exerciseDetail2chestC2 = new ExerciseDetail(51);
            ExerciseDetail exerciseDetail2chestC3 = new ExerciseDetail(52);
            ExerciseDetail exerciseDetail2chestC4 = new ExerciseDetail(53);
            ExerciseDetail exerciseDetail2chestS1 = new ExerciseDetail(54);
            ExerciseDetail exerciseDetail2chestS2 = new ExerciseDetail(55);
            ExerciseDetail exerciseDetail2absW1 = new ExerciseDetail(56);
            ExerciseDetail exerciseDetail2absW2 = new ExerciseDetail(57);
            ExerciseDetail exerciseDetail2absC1 = new ExerciseDetail(58);
            ExerciseDetail exerciseDetail2absC2 = new ExerciseDetail(59);
            ExerciseDetail exerciseDetail2absC3 = new ExerciseDetail(60);
            ExerciseDetail exerciseDetail2absC4 = new ExerciseDetail(61);
            ExerciseDetail exerciseDetail2absS1 = new ExerciseDetail(62);
            ExerciseDetail exerciseDetail2absS2 = new ExerciseDetail(63);
            ExerciseDetail exerciseDetail2armW1 = new ExerciseDetail(64);
            ExerciseDetail exerciseDetail2armW2 = new ExerciseDetail(65);
            ExerciseDetail exerciseDetail2armC1 = new ExerciseDetail(66);
            ExerciseDetail exerciseDetail2armC2 = new ExerciseDetail(67);
            ExerciseDetail exerciseDetail2armC3 = new ExerciseDetail(68);
            ExerciseDetail exerciseDetail2armC4 = new ExerciseDetail(69);
            ExerciseDetail exerciseDetail2armS1 = new ExerciseDetail(70);
            ExerciseDetail exerciseDetail2armS2 = new ExerciseDetail(71);
            ExerciseDetail exerciseDetail2shoulderW1 = new ExerciseDetail(72);
            ExerciseDetail exerciseDetail2shoulderW2 = new ExerciseDetail(73);
            ExerciseDetail exerciseDetail2shoulderC1 = new ExerciseDetail(74);
            ExerciseDetail exerciseDetail2shoulderC2 = new ExerciseDetail(75);
            ExerciseDetail exerciseDetail2shoulderC3 = new ExerciseDetail(76);
            ExerciseDetail exerciseDetail2shoulderC4 = new ExerciseDetail(77);
            ExerciseDetail exerciseDetail2shoulderS1 = new ExerciseDetail(78);
            ExerciseDetail exerciseDetail2shoulderS2 = new ExerciseDetail(79);
            ExerciseDetail exerciseDetail2backW1 = new ExerciseDetail(80);
            ExerciseDetail exerciseDetail2backW2 = new ExerciseDetail(81);
            ExerciseDetail exerciseDetail2backC1 = new ExerciseDetail(82);
            ExerciseDetail exerciseDetail2backC2 = new ExerciseDetail(83);
            ExerciseDetail exerciseDetail2backC3 = new ExerciseDetail(84);
            ExerciseDetail exerciseDetail2backC4 = new ExerciseDetail(85);
            ExerciseDetail exerciseDetail2backS1 = new ExerciseDetail(86);
            ExerciseDetail exerciseDetail2backS2 = new ExerciseDetail(87);
            ExerciseDetail exerciseDetail2legW1 = new ExerciseDetail(88);
            ExerciseDetail exerciseDetail2legW2 = new ExerciseDetail(89);
            ExerciseDetail exerciseDetail2legC1 = new ExerciseDetail(90);
            ExerciseDetail exerciseDetail2legC2 = new ExerciseDetail(91);
            ExerciseDetail exerciseDetail2legC3 = new ExerciseDetail(92);
            ExerciseDetail exerciseDetail2legC4 = new ExerciseDetail(93);
            ExerciseDetail exerciseDetail2legS1 = new ExerciseDetail(94);
            ExerciseDetail exerciseDetail2legS2 = new ExerciseDetail(95);

            ExerciseDetail exerciseDetail3chestW1 = new ExerciseDetail(96);
            ExerciseDetail exerciseDetail3chestW2 = new ExerciseDetail(97);
            ExerciseDetail exerciseDetail3chestC1 = new ExerciseDetail(98);
            ExerciseDetail exerciseDetail3chestC2 = new ExerciseDetail(99);
            ExerciseDetail exerciseDetail3chestC3 = new ExerciseDetail(100);
            ExerciseDetail exerciseDetail3chestC4 = new ExerciseDetail(101);
            ExerciseDetail exerciseDetail3chestS1 = new ExerciseDetail(102);
            ExerciseDetail exerciseDetail3chestS2 = new ExerciseDetail(103);
            ExerciseDetail exerciseDetail3absW1 = new ExerciseDetail(104);
            ExerciseDetail exerciseDetail3absW2 = new ExerciseDetail(105);
            ExerciseDetail exerciseDetail3absC1 = new ExerciseDetail(106);
            ExerciseDetail exerciseDetail3absC2 = new ExerciseDetail(107);
            ExerciseDetail exerciseDetail3absC3 = new ExerciseDetail(108);
            ExerciseDetail exerciseDetail3absC4 = new ExerciseDetail(109);
            ExerciseDetail exerciseDetail3absS1 = new ExerciseDetail(110);
            ExerciseDetail exerciseDetail3absS2 = new ExerciseDetail(111);
            ExerciseDetail exerciseDetail3armW1 = new ExerciseDetail(112);
            ExerciseDetail exerciseDetail3armW2 = new ExerciseDetail(113);
            ExerciseDetail exerciseDetail3armC1 = new ExerciseDetail(114);
            ExerciseDetail exerciseDetail3armC2 = new ExerciseDetail(115);
            ExerciseDetail exerciseDetail3armC3 = new ExerciseDetail(116);
            ExerciseDetail exerciseDetail3armC4 = new ExerciseDetail(117);
            ExerciseDetail exerciseDetail3armS1 = new ExerciseDetail(118);
            ExerciseDetail exerciseDetail3armS2 = new ExerciseDetail(119);
            ExerciseDetail exerciseDetail3shoulderW1 = new ExerciseDetail(120);
            ExerciseDetail exerciseDetail3shoulderW2 = new ExerciseDetail(121);
            ExerciseDetail exerciseDetail3shoulderC1 = new ExerciseDetail(122);
            ExerciseDetail exerciseDetail3shoulderC2 = new ExerciseDetail(123);
            ExerciseDetail exerciseDetail3shoulderC3 = new ExerciseDetail(124);
            ExerciseDetail exerciseDetail3shoulderC4 = new ExerciseDetail(125);
            ExerciseDetail exerciseDetail3shoulderS1 = new ExerciseDetail(126);
            ExerciseDetail exerciseDetail3shoulderS2 = new ExerciseDetail(127);
            ExerciseDetail exerciseDetail3backW1 = new ExerciseDetail(128);
            ExerciseDetail exerciseDetail3backW2 = new ExerciseDetail(129);
            ExerciseDetail exerciseDetail3backC1 = new ExerciseDetail(130);
            ExerciseDetail exerciseDetail3backC2 = new ExerciseDetail(131);
            ExerciseDetail exerciseDetail3backC3 = new ExerciseDetail(132);
            ExerciseDetail exerciseDetail3backC4 = new ExerciseDetail(133);
            ExerciseDetail exerciseDetail3backS1 = new ExerciseDetail(134);
            ExerciseDetail exerciseDetail3backS2 = new ExerciseDetail(135);
            ExerciseDetail exerciseDetail3legW1 = new ExerciseDetail(136);
            ExerciseDetail exerciseDetail3legW2 = new ExerciseDetail(137);
            ExerciseDetail exerciseDetail3legC1 = new ExerciseDetail(138);
            ExerciseDetail exerciseDetail3legC2 = new ExerciseDetail(139);
            ExerciseDetail exerciseDetail3legC3 = new ExerciseDetail(140);
            ExerciseDetail exerciseDetail3legC4 = new ExerciseDetail(141);
            ExerciseDetail exerciseDetail3legS1 = new ExerciseDetail(142);
            ExerciseDetail exerciseDetail3legS2 = new ExerciseDetail(143);

            workout1chest.WarmUps.Add(exerciseDetail1chestW1);
            workout1chest.WarmUps.Add(exerciseDetail1chestW2);
            workout1chest.Cores.Add(exerciseDetail1chestC1);
            workout1chest.Cores.Add(exerciseDetail1chestC2);
            workout1chest.Cores.Add(exerciseDetail1chestC3);
            workout1chest.Cores.Add(exerciseDetail1chestC4);
            workout1chest.Stretches.Add(exerciseDetail1chestS1);
            workout1chest.Stretches.Add(exerciseDetail1chestS2);
            workout1abs.WarmUps.Add(exerciseDetail1absW1);
            workout1abs.WarmUps.Add(exerciseDetail1absW2);
            workout1abs.Cores.Add(exerciseDetail1absC1);
            workout1abs.Cores.Add(exerciseDetail1absC2);
            workout1abs.Cores.Add(exerciseDetail1absC3);
            workout1abs.Cores.Add(exerciseDetail1absC4);
            workout1abs.Stretches.Add(exerciseDetail1absS1);
            workout1abs.Stretches.Add(exerciseDetail1absS2);
            workout1arm.WarmUps.Add(exerciseDetail1armW1);
            workout1arm.WarmUps.Add(exerciseDetail1armW2);
            workout1arm.Cores.Add(exerciseDetail1armC1);
            workout1arm.Cores.Add(exerciseDetail1armC2);
            workout1arm.Cores.Add(exerciseDetail1armC3);
            workout1arm.Cores.Add(exerciseDetail1armC4);
            workout1arm.Stretches.Add(exerciseDetail1armS1);
            workout1arm.Stretches.Add(exerciseDetail1armS2);
            workout1shoulder.WarmUps.Add(exerciseDetail1shoulderW1);
            workout1shoulder.WarmUps.Add(exerciseDetail1shoulderW2);
            workout1shoulder.Cores.Add(exerciseDetail1shoulderC1);
            workout1shoulder.Cores.Add(exerciseDetail1shoulderC2);
            workout1shoulder.Cores.Add(exerciseDetail1shoulderC3);
            workout1shoulder.Cores.Add(exerciseDetail1shoulderC4);
            workout1shoulder.Stretches.Add(exerciseDetail1shoulderS1);
            workout1shoulder.Stretches.Add(exerciseDetail1shoulderS2);
            workout1back.WarmUps.Add(exerciseDetail1backW1);
            workout1back.WarmUps.Add(exerciseDetail1backW2);
            workout1back.Cores.Add(exerciseDetail1backC1);
            workout1back.Cores.Add(exerciseDetail1backC2);
            workout1back.Cores.Add(exerciseDetail1backC3);
            workout1back.Cores.Add(exerciseDetail1backC4);
            workout1back.Stretches.Add(exerciseDetail1backS1);
            workout1back.Stretches.Add(exerciseDetail1backS2);
            workout1leg.WarmUps.Add(exerciseDetail1legW1);
            workout1leg.WarmUps.Add(exerciseDetail1legW2);
            workout1leg.Cores.Add(exerciseDetail1legC1);
            workout1leg.Cores.Add(exerciseDetail1legC2);
            workout1leg.Cores.Add(exerciseDetail1legC3);
            workout1leg.Cores.Add(exerciseDetail1legC4);
            workout1leg.Stretches.Add(exerciseDetail1legS1);
            workout1leg.Stretches.Add(exerciseDetail1legS2);

            workout2chest.WarmUps.Add(exerciseDetail2chestW1);
            workout2chest.WarmUps.Add(exerciseDetail2chestW2);
            workout2chest.Cores.Add(exerciseDetail2chestC1);
            workout2chest.Cores.Add(exerciseDetail2chestC2);
            workout2chest.Cores.Add(exerciseDetail2chestC3);
            workout2chest.Cores.Add(exerciseDetail2chestC4);
            workout2chest.Stretches.Add(exerciseDetail2chestS1);
            workout2chest.Stretches.Add(exerciseDetail2chestS2);
            workout2abs.WarmUps.Add(exerciseDetail2absW1);
            workout2abs.WarmUps.Add(exerciseDetail2absW2);
            workout2abs.Cores.Add(exerciseDetail2absC1);
            workout2abs.Cores.Add(exerciseDetail2absC2);
            workout2abs.Cores.Add(exerciseDetail2absC3);
            workout2abs.Cores.Add(exerciseDetail2absC4);
            workout2abs.Stretches.Add(exerciseDetail2absS1);
            workout2abs.Stretches.Add(exerciseDetail2absS2);
            workout2arm.WarmUps.Add(exerciseDetail2armW1);
            workout2arm.WarmUps.Add(exerciseDetail2armW2);
            workout2arm.Cores.Add(exerciseDetail2armC1);
            workout2arm.Cores.Add(exerciseDetail2armC2);
            workout2arm.Cores.Add(exerciseDetail2armC3);
            workout2arm.Cores.Add(exerciseDetail2armC4);
            workout2arm.Stretches.Add(exerciseDetail2armS1);
            workout2arm.Stretches.Add(exerciseDetail2armS2);
            workout2shoulder.WarmUps.Add(exerciseDetail2shoulderW1);
            workout2shoulder.WarmUps.Add(exerciseDetail2shoulderW2);
            workout2shoulder.Cores.Add(exerciseDetail2shoulderC1);
            workout2shoulder.Cores.Add(exerciseDetail2shoulderC2);
            workout2shoulder.Cores.Add(exerciseDetail2shoulderC3);
            workout2shoulder.Cores.Add(exerciseDetail2shoulderC4);
            workout2shoulder.Stretches.Add(exerciseDetail2shoulderS1);
            workout2shoulder.Stretches.Add(exerciseDetail2shoulderS2);
            workout2back.WarmUps.Add(exerciseDetail2backW1);
            workout2back.WarmUps.Add(exerciseDetail2backW2);
            workout2back.Cores.Add(exerciseDetail2backC1);
            workout2back.Cores.Add(exerciseDetail2backC2);
            workout2back.Cores.Add(exerciseDetail2backC3);
            workout2back.Cores.Add(exerciseDetail2backC4);
            workout2back.Stretches.Add(exerciseDetail2backS1);
            workout2back.Stretches.Add(exerciseDetail2backS2);
            workout2leg.WarmUps.Add(exerciseDetail2legW1);
            workout2leg.WarmUps.Add(exerciseDetail2legW2);
            workout2leg.Cores.Add(exerciseDetail2legC1);
            workout2leg.Cores.Add(exerciseDetail2legC2);
            workout2leg.Cores.Add(exerciseDetail2legC3);
            workout2leg.Cores.Add(exerciseDetail2legC4);
            workout2leg.Stretches.Add(exerciseDetail2legS1);
            workout2leg.Stretches.Add(exerciseDetail2legS2);

            workout3chest.WarmUps.Add(exerciseDetail3chestW1);
            workout3chest.WarmUps.Add(exerciseDetail3chestW2);
            workout3chest.Cores.Add(exerciseDetail3chestC1);
            workout3chest.Cores.Add(exerciseDetail3chestC2);
            workout3chest.Cores.Add(exerciseDetail3chestC3);
            workout3chest.Cores.Add(exerciseDetail3chestC4);
            workout3chest.Stretches.Add(exerciseDetail3chestS1);
            workout3chest.Stretches.Add(exerciseDetail3chestS2);
            workout3abs.WarmUps.Add(exerciseDetail3absW1);
            workout3abs.WarmUps.Add(exerciseDetail3absW2);
            workout3abs.Cores.Add(exerciseDetail3absC1);
            workout3abs.Cores.Add(exerciseDetail3absC2);
            workout3abs.Cores.Add(exerciseDetail3absC3);
            workout3abs.Cores.Add(exerciseDetail3absC4);
            workout3abs.Stretches.Add(exerciseDetail3absS1);
            workout3abs.Stretches.Add(exerciseDetail3absS2);
            workout3arm.WarmUps.Add(exerciseDetail3armW1);
            workout3arm.WarmUps.Add(exerciseDetail3armW2);
            workout3arm.Cores.Add(exerciseDetail3armC1);
            workout3arm.Cores.Add(exerciseDetail3armC2);
            workout3arm.Cores.Add(exerciseDetail3armC3);
            workout3arm.Cores.Add(exerciseDetail3armC4);
            workout3arm.Stretches.Add(exerciseDetail3armS1);
            workout3arm.Stretches.Add(exerciseDetail3armS2);
            workout3shoulder.WarmUps.Add(exerciseDetail3shoulderW1);
            workout3shoulder.WarmUps.Add(exerciseDetail3shoulderW2);
            workout3shoulder.Cores.Add(exerciseDetail3shoulderC1);
            workout3shoulder.Cores.Add(exerciseDetail3shoulderC2);
            workout3shoulder.Cores.Add(exerciseDetail3shoulderC3);
            workout3shoulder.Cores.Add(exerciseDetail3shoulderC4);
            workout3shoulder.Stretches.Add(exerciseDetail3shoulderS1);
            workout3shoulder.Stretches.Add(exerciseDetail3shoulderS2);
            workout3back.WarmUps.Add(exerciseDetail3backW1);
            workout3back.WarmUps.Add(exerciseDetail3backW2);
            workout3back.Cores.Add(exerciseDetail3backC1);
            workout3back.Cores.Add(exerciseDetail3backC2);
            workout3back.Cores.Add(exerciseDetail3backC3);
            workout3back.Cores.Add(exerciseDetail3backC4);
            workout3back.Stretches.Add(exerciseDetail3backS1);
            workout3back.Stretches.Add(exerciseDetail3backS2);
            workout3leg.WarmUps.Add(exerciseDetail3legW1);
            workout3leg.WarmUps.Add(exerciseDetail3legW2);
            workout3leg.Cores.Add(exerciseDetail3legC1);
            workout3leg.Cores.Add(exerciseDetail3legC2);
            workout3leg.Cores.Add(exerciseDetail3legC3);
            workout3leg.Cores.Add(exerciseDetail3legC4);
            workout3leg.Stretches.Add(exerciseDetail3legS1);
            workout3leg.Stretches.Add(exerciseDetail3legS2);

            exerciseDetail1chestW1.WorkoutWarmup = workout1chest;
            exerciseDetail1chestW2.WorkoutWarmup = workout1chest;
            exerciseDetail1chestC1.WorkoutCore = workout1chest;
            exerciseDetail1chestC2.WorkoutCore = workout1chest;
            exerciseDetail1chestC3.WorkoutCore = workout1chest;
            exerciseDetail1chestC4.WorkoutCore = workout1chest;
            exerciseDetail1chestS1.WorkoutStretching = workout1chest;
            exerciseDetail1chestS2.WorkoutStretching = workout1chest;
            exerciseDetail1absW1.WorkoutWarmup = workout1abs;
            exerciseDetail1absW2.WorkoutWarmup = workout1abs;
            exerciseDetail1absC1.WorkoutCore = workout1abs;
            exerciseDetail1absC2.WorkoutCore = workout1abs;
            exerciseDetail1absC3.WorkoutCore = workout1abs;
            exerciseDetail1absC4.WorkoutCore = workout1abs;
            exerciseDetail1absS1.WorkoutStretching = workout1abs;
            exerciseDetail1absS2.WorkoutStretching = workout1abs;
            exerciseDetail1armW1.WorkoutWarmup = workout1arm;
            exerciseDetail1armW2.WorkoutWarmup = workout1arm;
            exerciseDetail1armC1.WorkoutCore = workout1arm;
            exerciseDetail1armC2.WorkoutCore = workout1arm;
            exerciseDetail1armC3.WorkoutCore = workout1arm;
            exerciseDetail1armC4.WorkoutCore = workout1arm;
            exerciseDetail1armS1.WorkoutStretching = workout1arm;
            exerciseDetail1armS2.WorkoutStretching = workout1arm;
            exerciseDetail1shoulderW1.WorkoutWarmup = workout1shoulder;
            exerciseDetail1shoulderW2.WorkoutWarmup = workout1shoulder;
            exerciseDetail1shoulderC1.WorkoutCore = workout1shoulder;
            exerciseDetail1shoulderC2.WorkoutCore = workout1shoulder;
            exerciseDetail1shoulderC3.WorkoutCore = workout1shoulder;
            exerciseDetail1shoulderC4.WorkoutCore = workout1shoulder;
            exerciseDetail1shoulderS1.WorkoutStretching = workout1shoulder;
            exerciseDetail1shoulderS2.WorkoutStretching = workout1shoulder;
            exerciseDetail1backW1.WorkoutWarmup = workout1back;
            exerciseDetail1backW2.WorkoutWarmup = workout1back;
            exerciseDetail1backC1.WorkoutCore = workout1back;
            exerciseDetail1backC2.WorkoutCore = workout1back;
            exerciseDetail1backC3.WorkoutCore = workout1back;
            exerciseDetail1backC4.WorkoutCore = workout1back;
            exerciseDetail1backS1.WorkoutStretching = workout1back;
            exerciseDetail1backS2.WorkoutStretching = workout1back;
            exerciseDetail1legW1.WorkoutWarmup = workout1leg;
            exerciseDetail1legW2.WorkoutWarmup = workout1leg;
            exerciseDetail1legC1.WorkoutCore = workout1leg;
            exerciseDetail1legC2.WorkoutCore = workout1leg;
            exerciseDetail1legC3.WorkoutCore = workout1leg;
            exerciseDetail1legC4.WorkoutCore = workout1leg;
            exerciseDetail1legS1.WorkoutStretching = workout1leg;
            exerciseDetail1legS2.WorkoutStretching = workout1leg;

            exerciseDetail2chestW1.WorkoutWarmup = workout2chest;
            exerciseDetail2chestW2.WorkoutWarmup = workout2chest;
            exerciseDetail2chestC1.WorkoutCore = workout2chest;
            exerciseDetail2chestC2.WorkoutCore = workout2chest;
            exerciseDetail2chestC3.WorkoutCore = workout2chest;
            exerciseDetail2chestC4.WorkoutCore = workout2chest;
            exerciseDetail2chestS1.WorkoutStretching = workout2chest;
            exerciseDetail2chestS2.WorkoutStretching = workout2chest;
            exerciseDetail2absW1.WorkoutWarmup = workout2abs;
            exerciseDetail2absW2.WorkoutWarmup = workout2abs;
            exerciseDetail2absC1.WorkoutCore = workout2abs;
            exerciseDetail2absC2.WorkoutCore = workout2abs;
            exerciseDetail2absC3.WorkoutCore = workout2abs;
            exerciseDetail2absC4.WorkoutCore = workout2abs;
            exerciseDetail2absS1.WorkoutStretching = workout2abs;
            exerciseDetail2absS2.WorkoutStretching = workout2abs;
            exerciseDetail2armW1.WorkoutWarmup = workout2arm;
            exerciseDetail2armW2.WorkoutWarmup = workout2arm;
            exerciseDetail2armC1.WorkoutCore = workout2arm;
            exerciseDetail2armC2.WorkoutCore = workout2arm;
            exerciseDetail2armC3.WorkoutCore = workout2arm;
            exerciseDetail2armC4.WorkoutCore = workout2arm;
            exerciseDetail2armS1.WorkoutStretching = workout2arm;
            exerciseDetail2armS2.WorkoutStretching = workout2arm;
            exerciseDetail2shoulderW1.WorkoutWarmup = workout2shoulder;
            exerciseDetail2shoulderW2.WorkoutWarmup = workout2shoulder;
            exerciseDetail2shoulderC1.WorkoutCore = workout2shoulder;
            exerciseDetail2shoulderC2.WorkoutCore = workout2shoulder;
            exerciseDetail2shoulderC3.WorkoutCore = workout2shoulder;
            exerciseDetail2shoulderC4.WorkoutCore = workout2shoulder;
            exerciseDetail2shoulderS1.WorkoutStretching = workout2shoulder;
            exerciseDetail2shoulderS2.WorkoutStretching = workout2shoulder;
            exerciseDetail2backW1.WorkoutWarmup = workout2back;
            exerciseDetail2backW2.WorkoutWarmup = workout2back;
            exerciseDetail2backC1.WorkoutCore = workout2back;
            exerciseDetail2backC2.WorkoutCore = workout2back;
            exerciseDetail2backC3.WorkoutCore = workout2back;
            exerciseDetail2backC4.WorkoutCore = workout2back;
            exerciseDetail2backS1.WorkoutStretching = workout2back;
            exerciseDetail2backS2.WorkoutStretching = workout2back;
            exerciseDetail2legW1.WorkoutWarmup = workout2leg;
            exerciseDetail2legW2.WorkoutWarmup = workout2leg;
            exerciseDetail2legC1.WorkoutCore = workout2leg;
            exerciseDetail2legC2.WorkoutCore = workout2leg;
            exerciseDetail2legC3.WorkoutCore = workout2leg;
            exerciseDetail2legC4.WorkoutCore = workout2leg;
            exerciseDetail2legS1.WorkoutStretching = workout2leg;
            exerciseDetail2legS2.WorkoutStretching = workout2leg;

            exerciseDetail3chestW1.WorkoutWarmup = workout3chest;
            exerciseDetail3chestW2.WorkoutWarmup = workout3chest;
            exerciseDetail3chestC1.WorkoutCore = workout3chest;
            exerciseDetail3chestC2.WorkoutCore = workout3chest;
            exerciseDetail3chestC3.WorkoutCore = workout3chest;
            exerciseDetail3chestC4.WorkoutCore = workout3chest;
            exerciseDetail3chestS1.WorkoutStretching = workout3chest;
            exerciseDetail3chestS2.WorkoutStretching = workout3chest;
            exerciseDetail3absW1.WorkoutWarmup = workout3abs;
            exerciseDetail3absW2.WorkoutWarmup = workout3abs;
            exerciseDetail3absC1.WorkoutCore = workout3abs;
            exerciseDetail3absC2.WorkoutCore = workout3abs;
            exerciseDetail3absC3.WorkoutCore = workout3abs;
            exerciseDetail3absC4.WorkoutCore = workout3abs;
            exerciseDetail3absS1.WorkoutStretching = workout3abs;
            exerciseDetail3absS2.WorkoutStretching = workout3abs;
            exerciseDetail3armW1.WorkoutWarmup = workout3arm;
            exerciseDetail3armW2.WorkoutWarmup = workout3arm;
            exerciseDetail3armC1.WorkoutCore = workout3arm;
            exerciseDetail3armC2.WorkoutCore = workout3arm;
            exerciseDetail3armC3.WorkoutCore = workout3arm;
            exerciseDetail3armC4.WorkoutCore = workout3arm;
            exerciseDetail3armS1.WorkoutStretching = workout3arm;
            exerciseDetail3armS2.WorkoutStretching = workout3arm;
            exerciseDetail3shoulderW1.WorkoutWarmup = workout3shoulder;
            exerciseDetail3shoulderW2.WorkoutWarmup = workout3shoulder;
            exerciseDetail3shoulderC1.WorkoutCore = workout3shoulder;
            exerciseDetail3shoulderC2.WorkoutCore = workout3shoulder;
            exerciseDetail3shoulderC3.WorkoutCore = workout3shoulder;
            exerciseDetail3shoulderC4.WorkoutCore = workout3shoulder;
            exerciseDetail3shoulderS1.WorkoutStretching = workout3shoulder;
            exerciseDetail3shoulderS2.WorkoutStretching = workout3shoulder;
            exerciseDetail3backW1.WorkoutWarmup = workout3back;
            exerciseDetail3backW2.WorkoutWarmup = workout3back;
            exerciseDetail3backC1.WorkoutCore = workout3back;
            exerciseDetail3backC2.WorkoutCore = workout3back;
            exerciseDetail3backC3.WorkoutCore = workout3back;
            exerciseDetail3backC4.WorkoutCore = workout3back;
            exerciseDetail3backS1.WorkoutStretching = workout3back;
            exerciseDetail3backS2.WorkoutStretching = workout3back;
            exerciseDetail3legW1.WorkoutWarmup = workout3leg;
            exerciseDetail3legW2.WorkoutWarmup = workout3leg;
            exerciseDetail3legC1.WorkoutCore = workout3leg;
            exerciseDetail3legC2.WorkoutCore = workout3leg;
            exerciseDetail3legC3.WorkoutCore = workout3leg;
            exerciseDetail3legC4.WorkoutCore = workout3leg;
            exerciseDetail3legS1.WorkoutStretching = workout3leg;
            exerciseDetail3legS2.WorkoutStretching = workout3leg;

            Exercise exercise1 = new RepExercise(0, "Warmup name", PhaseType.WarmUp, TargetType.Chest, 2, 50, GenderPreference.Man);
            Exercise exercise2 = new RepExercise(1, "Core name", PhaseType.Core, TargetType.Chest, 2, 50, GenderPreference.Man);
            Exercise exercise3 = new RepExercise(2, "Stretch name", PhaseType.Stretching, TargetType.Chest, 2, 50, GenderPreference.Man);

            OwnExercise ownExercise1 = new OwnExercise(exercise1, 0);
            OwnExercise ownExercise2 = new OwnExercise(exercise2, 0);
            OwnExercise ownExercise3 = new OwnExercise(exercise3, 0);
            exercise1.OwnExercises.Add(ownExercise1);
            exercise2.OwnExercises.Add(ownExercise2);
            exercise3.OwnExercises.Add(ownExercise3);

            exerciseDetail1chestW1.OwnExercise = ownExercise1;
            exerciseDetail1chestW2.OwnExercise = ownExercise1;
            exerciseDetail1chestC1.OwnExercise = ownExercise2;
            exerciseDetail1chestC2.OwnExercise = ownExercise2;
            exerciseDetail1chestC3.OwnExercise = ownExercise2;
            exerciseDetail1chestC4.OwnExercise = ownExercise2;
            exerciseDetail1chestS1.OwnExercise = ownExercise3;
            exerciseDetail1chestS2.OwnExercise = ownExercise3;
            exerciseDetail1absW1.OwnExercise = ownExercise1;
            exerciseDetail1absW2.OwnExercise = ownExercise1;
            exerciseDetail1absC1.OwnExercise = ownExercise2;
            exerciseDetail1absC2.OwnExercise = ownExercise2;
            exerciseDetail1absC3.OwnExercise = ownExercise2;
            exerciseDetail1absC4.OwnExercise = ownExercise2;
            exerciseDetail1absS1.OwnExercise = ownExercise3;
            exerciseDetail1absS2.OwnExercise = ownExercise3;
            exerciseDetail1armW1.OwnExercise = ownExercise1;
            exerciseDetail1armW2.OwnExercise = ownExercise1;
            exerciseDetail1armC1.OwnExercise = ownExercise2;
            exerciseDetail1armC2.OwnExercise = ownExercise2;
            exerciseDetail1armC3.OwnExercise = ownExercise2;
            exerciseDetail1armC4.OwnExercise = ownExercise2;
            exerciseDetail1armS1.OwnExercise = ownExercise3;
            exerciseDetail1armS2.OwnExercise = ownExercise3;
            exerciseDetail1shoulderW1.OwnExercise = ownExercise1;
            exerciseDetail1shoulderW2.OwnExercise = ownExercise1;
            exerciseDetail1shoulderC1.OwnExercise = ownExercise2;
            exerciseDetail1shoulderC2.OwnExercise = ownExercise2;
            exerciseDetail1shoulderC3.OwnExercise = ownExercise2;
            exerciseDetail1shoulderC4.OwnExercise = ownExercise2;
            exerciseDetail1shoulderS1.OwnExercise = ownExercise3;
            exerciseDetail1shoulderS2.OwnExercise = ownExercise3;
            exerciseDetail1backW1.OwnExercise = ownExercise1;
            exerciseDetail1backW2.OwnExercise = ownExercise1;
            exerciseDetail1backC1.OwnExercise = ownExercise2;
            exerciseDetail1backC2.OwnExercise = ownExercise2;
            exerciseDetail1backC3.OwnExercise = ownExercise2;
            exerciseDetail1backC4.OwnExercise = ownExercise2;
            exerciseDetail1backS1.OwnExercise = ownExercise3;
            exerciseDetail1backS2.OwnExercise = ownExercise3;
            exerciseDetail1legW1.OwnExercise = ownExercise1;
            exerciseDetail1legW2.OwnExercise = ownExercise1;
            exerciseDetail1legC1.OwnExercise = ownExercise2;
            exerciseDetail1legC2.OwnExercise = ownExercise2;
            exerciseDetail1legC3.OwnExercise = ownExercise2;
            exerciseDetail1legC4.OwnExercise = ownExercise2;
            exerciseDetail1legS1.OwnExercise = ownExercise3;
            exerciseDetail1legS2.OwnExercise = ownExercise3;

            exerciseDetail2chestW1.OwnExercise = ownExercise1;
            exerciseDetail2chestW2.OwnExercise = ownExercise1;
            exerciseDetail2chestC1.OwnExercise = ownExercise2;
            exerciseDetail2chestC2.OwnExercise = ownExercise2;
            exerciseDetail2chestC3.OwnExercise = ownExercise2;
            exerciseDetail2chestC4.OwnExercise = ownExercise2;
            exerciseDetail2chestS1.OwnExercise = ownExercise3;
            exerciseDetail2chestS2.OwnExercise = ownExercise3;
            exerciseDetail2absW1.OwnExercise = ownExercise1;
            exerciseDetail2absW2.OwnExercise = ownExercise1;
            exerciseDetail2absC1.OwnExercise = ownExercise2;
            exerciseDetail2absC2.OwnExercise = ownExercise2;
            exerciseDetail2absC3.OwnExercise = ownExercise2;
            exerciseDetail2absC4.OwnExercise = ownExercise2;
            exerciseDetail2absS1.OwnExercise = ownExercise3;
            exerciseDetail2absS2.OwnExercise = ownExercise3;
            exerciseDetail2armW1.OwnExercise = ownExercise1;
            exerciseDetail2armW2.OwnExercise = ownExercise1;
            exerciseDetail2armC1.OwnExercise = ownExercise2;
            exerciseDetail2armC2.OwnExercise = ownExercise2;
            exerciseDetail2armC3.OwnExercise = ownExercise2;
            exerciseDetail2armC4.OwnExercise = ownExercise2;
            exerciseDetail2armS1.OwnExercise = ownExercise3;
            exerciseDetail2armS2.OwnExercise = ownExercise3;
            exerciseDetail2shoulderW1.OwnExercise = ownExercise1;
            exerciseDetail2shoulderW2.OwnExercise = ownExercise1;
            exerciseDetail2shoulderC1.OwnExercise = ownExercise2;
            exerciseDetail2shoulderC2.OwnExercise = ownExercise2;
            exerciseDetail2shoulderC3.OwnExercise = ownExercise2;
            exerciseDetail2shoulderC4.OwnExercise = ownExercise2;
            exerciseDetail2shoulderS1.OwnExercise = ownExercise3;
            exerciseDetail2shoulderS2.OwnExercise = ownExercise3;
            exerciseDetail2backW1.OwnExercise = ownExercise1;
            exerciseDetail2backW2.OwnExercise = ownExercise1;
            exerciseDetail2backC1.OwnExercise = ownExercise2;
            exerciseDetail2backC2.OwnExercise = ownExercise2;
            exerciseDetail2backC3.OwnExercise = ownExercise2;
            exerciseDetail2backC4.OwnExercise = ownExercise2;
            exerciseDetail2backS1.OwnExercise = ownExercise3;
            exerciseDetail2backS2.OwnExercise = ownExercise3;
            exerciseDetail2legW1.OwnExercise = ownExercise1;
            exerciseDetail2legW2.OwnExercise = ownExercise1;
            exerciseDetail2legC1.OwnExercise = ownExercise2;
            exerciseDetail2legC2.OwnExercise = ownExercise2;
            exerciseDetail2legC3.OwnExercise = ownExercise2;
            exerciseDetail2legC4.OwnExercise = ownExercise2;
            exerciseDetail2legS1.OwnExercise = ownExercise3;
            exerciseDetail2legS2.OwnExercise = ownExercise3;

            exerciseDetail3chestW1.OwnExercise = ownExercise1;
            exerciseDetail3chestW2.OwnExercise = ownExercise1;
            exerciseDetail3chestC1.OwnExercise = ownExercise2;
            exerciseDetail3chestC2.OwnExercise = ownExercise2;
            exerciseDetail3chestC3.OwnExercise = ownExercise2;
            exerciseDetail3chestC4.OwnExercise = ownExercise2;
            exerciseDetail3chestS1.OwnExercise = ownExercise3;
            exerciseDetail3chestS2.OwnExercise = ownExercise3;
            exerciseDetail3absW1.OwnExercise = ownExercise1;
            exerciseDetail3absW2.OwnExercise = ownExercise1;
            exerciseDetail3absC1.OwnExercise = ownExercise2;
            exerciseDetail3absC2.OwnExercise = ownExercise2;
            exerciseDetail3absC3.OwnExercise = ownExercise2;
            exerciseDetail3absC4.OwnExercise = ownExercise2;
            exerciseDetail3absS1.OwnExercise = ownExercise3;
            exerciseDetail3absS2.OwnExercise = ownExercise3;
            exerciseDetail3armW1.OwnExercise = ownExercise1;
            exerciseDetail3armW2.OwnExercise = ownExercise1;
            exerciseDetail3armC1.OwnExercise = ownExercise2;
            exerciseDetail3armC2.OwnExercise = ownExercise2;
            exerciseDetail3armC3.OwnExercise = ownExercise2;
            exerciseDetail3armC4.OwnExercise = ownExercise2;
            exerciseDetail3armS1.OwnExercise = ownExercise3;
            exerciseDetail3armS2.OwnExercise = ownExercise3;
            exerciseDetail3shoulderW1.OwnExercise = ownExercise1;
            exerciseDetail3shoulderW2.OwnExercise = ownExercise1;
            exerciseDetail3shoulderC1.OwnExercise = ownExercise2;
            exerciseDetail3shoulderC2.OwnExercise = ownExercise2;
            exerciseDetail3shoulderC3.OwnExercise = ownExercise2;
            exerciseDetail3shoulderC4.OwnExercise = ownExercise2;
            exerciseDetail3shoulderS1.OwnExercise = ownExercise3;
            exerciseDetail3shoulderS2.OwnExercise = ownExercise3;
            exerciseDetail3backW1.OwnExercise = ownExercise1;
            exerciseDetail3backW2.OwnExercise = ownExercise1;
            exerciseDetail3backC1.OwnExercise = ownExercise2;
            exerciseDetail3backC2.OwnExercise = ownExercise2;
            exerciseDetail3backC3.OwnExercise = ownExercise2;
            exerciseDetail3backC4.OwnExercise = ownExercise2;
            exerciseDetail3backS1.OwnExercise = ownExercise3;
            exerciseDetail3backS2.OwnExercise = ownExercise3;
            exerciseDetail3legW1.OwnExercise = ownExercise1;
            exerciseDetail3legW2.OwnExercise = ownExercise1;
            exerciseDetail3legC1.OwnExercise = ownExercise2;
            exerciseDetail3legC2.OwnExercise = ownExercise2;
            exerciseDetail3legC3.OwnExercise = ownExercise2;
            exerciseDetail3legC4.OwnExercise = ownExercise2;
            exerciseDetail3legS1.OwnExercise = ownExercise3;
            exerciseDetail3legS2.OwnExercise = ownExercise3;

            ownExercise1.ExerciseDetails.Add(exerciseDetail1chestW1);
            ownExercise1.ExerciseDetails.Add(exerciseDetail1chestW2);
            ownExercise1.ExerciseDetails.Add(exerciseDetail1chestC1);
            ownExercise1.ExerciseDetails.Add(exerciseDetail1chestC2);
            ownExercise1.ExerciseDetails.Add(exerciseDetail1chestC3);
            ownExercise1.ExerciseDetails.Add(exerciseDetail1chestC4);
            ownExercise1.ExerciseDetails.Add(exerciseDetail1chestS1);
            ownExercise1.ExerciseDetails.Add(exerciseDetail1chestS2);
            ownExercise1.ExerciseDetails.Add(exerciseDetail1absW1);
            ownExercise1.ExerciseDetails.Add(exerciseDetail1absW2);
            ownExercise1.ExerciseDetails.Add(exerciseDetail1absC1);
            ownExercise1.ExerciseDetails.Add(exerciseDetail1absC2);
            ownExercise1.ExerciseDetails.Add(exerciseDetail1absC3);
            ownExercise1.ExerciseDetails.Add(exerciseDetail1absC4);
            ownExercise1.ExerciseDetails.Add(exerciseDetail1absS1);
            ownExercise1.ExerciseDetails.Add(exerciseDetail1absS2);
            ownExercise1.ExerciseDetails.Add(exerciseDetail1armW1);
            ownExercise1.ExerciseDetails.Add(exerciseDetail1armW2);
            ownExercise1.ExerciseDetails.Add(exerciseDetail1armC1);
            ownExercise1.ExerciseDetails.Add(exerciseDetail1armC2);
            ownExercise1.ExerciseDetails.Add(exerciseDetail1armC3);
            ownExercise1.ExerciseDetails.Add(exerciseDetail1armC4);
            ownExercise1.ExerciseDetails.Add(exerciseDetail1armS1);
            ownExercise1.ExerciseDetails.Add(exerciseDetail1armS2);
            ownExercise1.ExerciseDetails.Add(exerciseDetail1shoulderW1);
            ownExercise1.ExerciseDetails.Add(exerciseDetail1shoulderW2);
            ownExercise1.ExerciseDetails.Add(exerciseDetail1shoulderC1);
            ownExercise1.ExerciseDetails.Add(exerciseDetail1shoulderC2);
            ownExercise1.ExerciseDetails.Add(exerciseDetail1shoulderC3);
            ownExercise1.ExerciseDetails.Add(exerciseDetail1shoulderC4);
            ownExercise1.ExerciseDetails.Add(exerciseDetail1shoulderS1);
            ownExercise1.ExerciseDetails.Add(exerciseDetail1shoulderS2);
            ownExercise1.ExerciseDetails.Add(exerciseDetail1backW1);
            ownExercise1.ExerciseDetails.Add(exerciseDetail1backW2);
            ownExercise1.ExerciseDetails.Add(exerciseDetail1backC1);
            ownExercise1.ExerciseDetails.Add(exerciseDetail1backC2);
            ownExercise1.ExerciseDetails.Add(exerciseDetail1backC3);
            ownExercise1.ExerciseDetails.Add(exerciseDetail1backC4);
            ownExercise1.ExerciseDetails.Add(exerciseDetail1backS1);
            ownExercise1.ExerciseDetails.Add(exerciseDetail1backS2);
            ownExercise1.ExerciseDetails.Add(exerciseDetail1legW1);
            ownExercise1.ExerciseDetails.Add(exerciseDetail1legW2);
            ownExercise1.ExerciseDetails.Add(exerciseDetail1legC1);
            ownExercise1.ExerciseDetails.Add(exerciseDetail1legC2);
            ownExercise1.ExerciseDetails.Add(exerciseDetail1legC3);
            ownExercise1.ExerciseDetails.Add(exerciseDetail1legC4);
            ownExercise1.ExerciseDetails.Add(exerciseDetail1legS1);
            ownExercise1.ExerciseDetails.Add(exerciseDetail1legS2);

            ownExercise2.ExerciseDetails.Add(exerciseDetail2chestW1);
            ownExercise2.ExerciseDetails.Add(exerciseDetail2chestW2);
            ownExercise2.ExerciseDetails.Add(exerciseDetail2chestC1);
            ownExercise2.ExerciseDetails.Add(exerciseDetail2chestC2);
            ownExercise2.ExerciseDetails.Add(exerciseDetail2chestC3);
            ownExercise2.ExerciseDetails.Add(exerciseDetail2chestC4);
            ownExercise2.ExerciseDetails.Add(exerciseDetail2chestS1);
            ownExercise2.ExerciseDetails.Add(exerciseDetail2chestS2);
            ownExercise2.ExerciseDetails.Add(exerciseDetail2absW1);
            ownExercise2.ExerciseDetails.Add(exerciseDetail2absW2);
            ownExercise2.ExerciseDetails.Add(exerciseDetail2absC1);
            ownExercise2.ExerciseDetails.Add(exerciseDetail2absC2);
            ownExercise2.ExerciseDetails.Add(exerciseDetail2absC3);
            ownExercise2.ExerciseDetails.Add(exerciseDetail2absC4);
            ownExercise2.ExerciseDetails.Add(exerciseDetail2absS1);
            ownExercise2.ExerciseDetails.Add(exerciseDetail2absS2);
            ownExercise2.ExerciseDetails.Add(exerciseDetail2armW1);
            ownExercise2.ExerciseDetails.Add(exerciseDetail2armW2);
            ownExercise2.ExerciseDetails.Add(exerciseDetail2armC1);
            ownExercise2.ExerciseDetails.Add(exerciseDetail2armC2);
            ownExercise2.ExerciseDetails.Add(exerciseDetail2armC3);
            ownExercise2.ExerciseDetails.Add(exerciseDetail2armC4);
            ownExercise2.ExerciseDetails.Add(exerciseDetail2armS1);
            ownExercise2.ExerciseDetails.Add(exerciseDetail2armS2);
            ownExercise2.ExerciseDetails.Add(exerciseDetail2shoulderW1);
            ownExercise2.ExerciseDetails.Add(exerciseDetail2shoulderW2);
            ownExercise2.ExerciseDetails.Add(exerciseDetail2shoulderC1);
            ownExercise2.ExerciseDetails.Add(exerciseDetail2shoulderC2);
            ownExercise2.ExerciseDetails.Add(exerciseDetail2shoulderC3);
            ownExercise2.ExerciseDetails.Add(exerciseDetail2shoulderC4);
            ownExercise2.ExerciseDetails.Add(exerciseDetail2shoulderS1);
            ownExercise2.ExerciseDetails.Add(exerciseDetail2shoulderS2);
            ownExercise2.ExerciseDetails.Add(exerciseDetail2backW1);
            ownExercise2.ExerciseDetails.Add(exerciseDetail2backW2);
            ownExercise2.ExerciseDetails.Add(exerciseDetail2backC1);
            ownExercise2.ExerciseDetails.Add(exerciseDetail2backC2);
            ownExercise2.ExerciseDetails.Add(exerciseDetail2backC3);
            ownExercise2.ExerciseDetails.Add(exerciseDetail2backC4);
            ownExercise2.ExerciseDetails.Add(exerciseDetail2backS1);
            ownExercise2.ExerciseDetails.Add(exerciseDetail2backS2);
            ownExercise2.ExerciseDetails.Add(exerciseDetail2legW1);
            ownExercise2.ExerciseDetails.Add(exerciseDetail2legW2);
            ownExercise2.ExerciseDetails.Add(exerciseDetail2legC1);
            ownExercise2.ExerciseDetails.Add(exerciseDetail2legC2);
            ownExercise2.ExerciseDetails.Add(exerciseDetail2legC3);
            ownExercise2.ExerciseDetails.Add(exerciseDetail2legC4);
            ownExercise2.ExerciseDetails.Add(exerciseDetail2legS1);
            ownExercise2.ExerciseDetails.Add(exerciseDetail2legS2);

            ownExercise3.ExerciseDetails.Add(exerciseDetail3chestW1);
            ownExercise3.ExerciseDetails.Add(exerciseDetail3chestW2);
            ownExercise3.ExerciseDetails.Add(exerciseDetail3chestC1);
            ownExercise3.ExerciseDetails.Add(exerciseDetail3chestC2);
            ownExercise3.ExerciseDetails.Add(exerciseDetail3chestC3);
            ownExercise3.ExerciseDetails.Add(exerciseDetail3chestC4);
            ownExercise3.ExerciseDetails.Add(exerciseDetail3chestS1);
            ownExercise3.ExerciseDetails.Add(exerciseDetail3chestS2);
            ownExercise3.ExerciseDetails.Add(exerciseDetail3absW1);
            ownExercise3.ExerciseDetails.Add(exerciseDetail3absW2);
            ownExercise3.ExerciseDetails.Add(exerciseDetail3absC1);
            ownExercise3.ExerciseDetails.Add(exerciseDetail3absC2);
            ownExercise3.ExerciseDetails.Add(exerciseDetail3absC3);
            ownExercise3.ExerciseDetails.Add(exerciseDetail3absC4);
            ownExercise3.ExerciseDetails.Add(exerciseDetail3absS1);
            ownExercise3.ExerciseDetails.Add(exerciseDetail3absS2);
            ownExercise3.ExerciseDetails.Add(exerciseDetail3armW1);
            ownExercise3.ExerciseDetails.Add(exerciseDetail3armW2);
            ownExercise3.ExerciseDetails.Add(exerciseDetail3armC1);
            ownExercise3.ExerciseDetails.Add(exerciseDetail3armC2);
            ownExercise3.ExerciseDetails.Add(exerciseDetail3armC3);
            ownExercise3.ExerciseDetails.Add(exerciseDetail3armC4);
            ownExercise3.ExerciseDetails.Add(exerciseDetail3armS1);
            ownExercise3.ExerciseDetails.Add(exerciseDetail3armS2);
            ownExercise3.ExerciseDetails.Add(exerciseDetail3shoulderW1);
            ownExercise3.ExerciseDetails.Add(exerciseDetail3shoulderW2);
            ownExercise3.ExerciseDetails.Add(exerciseDetail3shoulderC1);
            ownExercise3.ExerciseDetails.Add(exerciseDetail3shoulderC2);
            ownExercise3.ExerciseDetails.Add(exerciseDetail3shoulderC3);
            ownExercise3.ExerciseDetails.Add(exerciseDetail3shoulderC4);
            ownExercise3.ExerciseDetails.Add(exerciseDetail3shoulderS1);
            ownExercise3.ExerciseDetails.Add(exerciseDetail3shoulderS2);
            ownExercise3.ExerciseDetails.Add(exerciseDetail3backW1);
            ownExercise3.ExerciseDetails.Add(exerciseDetail3backW2);
            ownExercise3.ExerciseDetails.Add(exerciseDetail3backC1);
            ownExercise3.ExerciseDetails.Add(exerciseDetail3backC2);
            ownExercise3.ExerciseDetails.Add(exerciseDetail3backC3);
            ownExercise3.ExerciseDetails.Add(exerciseDetail3backC4);
            ownExercise3.ExerciseDetails.Add(exerciseDetail3backS1);
            ownExercise3.ExerciseDetails.Add(exerciseDetail3backS2);
            ownExercise3.ExerciseDetails.Add(exerciseDetail3legW1);
            ownExercise3.ExerciseDetails.Add(exerciseDetail3legW2);
            ownExercise3.ExerciseDetails.Add(exerciseDetail3legC1);
            ownExercise3.ExerciseDetails.Add(exerciseDetail3legC2);
            ownExercise3.ExerciseDetails.Add(exerciseDetail3legC3);
            ownExercise3.ExerciseDetails.Add(exerciseDetail3legC4);
            ownExercise3.ExerciseDetails.Add(exerciseDetail3legS1);
            ownExercise3.ExerciseDetails.Add(exerciseDetail3legS2);


            trainingPlan.WeeklyPlans.First().Status = Status.Ready;
            trainingPlan.WeeklyPlans.First().DailyPlans.First().Status = Status.Ready;
            trainingPlan.WeeklyPlans.First().DailyPlans.First().Workouts.First().Status = Status.Ready;
            trainingPlan.WeeklyPlans.First().DailyPlans.First().Workouts.First().WarmUps.First().Status = Status.Ready;

            return trainingPlan;
        }
    }
}
