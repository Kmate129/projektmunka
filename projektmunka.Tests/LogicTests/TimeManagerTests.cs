﻿using Moq;
using Projektmunka.Logic;
using Projektmunka.Platform.Interfaces;
using Projektmunka.Repository.Interfaces;
using Projektmunka.UnitTests.TestClasses;

namespace Projektmunka.UnitTests.LogicTests
{
    [TestFixture]
    public class TimeManagerTests
    {
        private TimeManager timeManager;
        private PreferencesWrapper preferencesWrapper;
        private Mock<IAppProvider> mockAppProvider;
        private Mock<IUserRepository> mockUserRepo;

        [SetUp]
        public void Setup()
        {
            preferencesWrapper = new PreferencesWrapper();
            this.mockAppProvider = new Mock<IAppProvider>();
            this.mockUserRepo = new Mock<IUserRepository>();
            timeManager = new TimeManager(preferencesWrapper, mockAppProvider.Object, mockUserRepo.Object);
        }

        [Test]
        public void StartTotalTimer_SetsStartDateTotalInPreferences()
        {
            // Act
            timeManager.StartTotalTimer();
            var starterTime = DateTime.Now.ToString();

            Thread.Sleep(1000);

            timeManager.StopTotalTimer();
            var endingTime = DateTime.Now.ToString();

            var result = timeManager.GetTotalTime();
            var expected = DateTime.Parse(endingTime) - DateTime.Parse(starterTime);

            // Assert
            Assert.That(result, Is.EqualTo(expected));
        }

        [Test]
        public void StartActiveTimer_SetsExerciseStartInPreferences()
        {
            // Act
            timeManager.StartActiveTimer();
            var starterTime = DateTime.Now.ToString();

            Thread.Sleep(2000);

            timeManager.StopAndAddActiveTimer();
            var endingTime = DateTime.Now.ToString();

            var activeTime = timeManager.GetActiveTime();
            var result = new TimeSpan(activeTime.Hours, activeTime.Minutes, activeTime.Seconds);
            var expected = DateTime.Parse(endingTime) - DateTime.Parse(starterTime);

            // Assert
            Assert.That(result, Is.EqualTo(expected));
        }

        [Test]
        public void StartAndStopMultipleTimesActiveTimer()
        {
            // Arrenge
            var sum = new TimeSpan();

            // Act
            timeManager.StartActiveTimer();
            var stepStart = DateTime.Now;

            Thread.Sleep(8000);

            timeManager.StopAndAddActiveTimer();
            var stepEnd = DateTime.Now;
            sum = stepEnd - stepStart;

            timeManager.StartActiveTimer();
            stepStart = DateTime.Now;

            Thread.Sleep(5000);

            timeManager.StopAndAddActiveTimer();
            stepEnd = DateTime.Now;
            sum += stepEnd - stepStart;

            var activeTime = timeManager.GetActiveTime();
            var result = new TimeSpan(activeTime.Hours, activeTime.Minutes, activeTime.Seconds);
            var expected = new TimeSpan(sum.Hours, sum.Minutes, sum.Seconds);

            // Assert
            Assert.That(result, Is.EqualTo(expected));
        }
    }
}
