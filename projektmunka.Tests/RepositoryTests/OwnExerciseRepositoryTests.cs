﻿using Moq;
using Projektmunka.Repository.Interfaces;
using Projektmunka.Repository;
using Microsoft.EntityFrameworkCore;
using Projektmunka.Data.Model;
using Projektmunka.Data.Enums;

namespace Projektmunka.UnitTests.RepositoryTests
{
    [TestFixture]
    public class OwnExerciseRepositoryTests
    {
        private IOwnExerciseRepository repository;
        private Mock<DbContext> contextMock;

        [SetUp]
        public void SetUp()
        {
            contextMock = new Mock<DbContext>();
            repository = new OwnExerciseRepository(contextMock.Object);
        }

        [Test]
        public void Method_GetOwnExerciseById_ShouldReturnCorrectOwnExercise()
        {
            Exercise exercise = new RepExercise(1, "TestName", PhaseType.Core, TargetType.Biceps, 5, 20, GenderPreference.Both);
            OwnExercise ownExercise = new OwnExercise(exercise);
            IList<OwnExercise> exercises = new List<OwnExercise> { ownExercise };
            Mock<DbSet<OwnExercise>> dbSetMock = new Mock<DbSet<OwnExercise>>();
            dbSetMock.Setup(m => m.Find(It.IsAny<object[]>())).Returns(ownExercise);
            contextMock.Setup(m => m.Set<OwnExercise>()).Returns(dbSetMock.Object);

            var result = repository.GetOwnExerciseById(1);

            Assert.That(result, Is.EqualTo(ownExercise));
        }

        [Test]
        public void Method_GetExerciseById_ShouldReturnNull()
        {
            Exercise exercise = new RepExercise(1, "TestName", PhaseType.Core, TargetType.Biceps, 5, 20, GenderPreference.Both);
            OwnExercise ownExercise = new OwnExercise(exercise);
            IList<OwnExercise> exercises = new List<OwnExercise> { ownExercise };
            Mock<DbSet<OwnExercise>> dbSetMock = new Mock<DbSet<OwnExercise>>();
            dbSetMock.Setup(m => m.Find(It.IsAny<object[]>())).Returns(ownExercise);
            contextMock.Setup(m => m.Set<OwnExercise>()).Returns(dbSetMock.Object);

            var result = repository.GetOwnExerciseById(2);

            Assert.That(result, Is.EqualTo(ownExercise));
        }

        [Test]
        public void Method_GetAllOwnExercisesOfUser_ReturnsCorrectOwnExercises()
        {
            Mock<DbSet<OwnExercise>> mockDbSet = new Mock<DbSet<OwnExercise>>();
            int userId = 1;
            var exercises = new List<OwnExercise>
            {
                new OwnExercise(new RepExercise(1, "TestName", PhaseType.Core, TargetType.Biceps, 5, 20, GenderPreference.Both),1)
                {
                    User = new User("User1", "secret password", 180, 30, GenderType.Male, 1),
                },
                new OwnExercise(new RepExercise(2, "TestName2", PhaseType.Core, TargetType.Triceps, 5, 20, GenderPreference.Man),2)
                {
                    User = new User("User1", "secret password", 180, 30, GenderType.Male, 1),
                },
                new OwnExercise(new RepExercise(3, "TestName3", PhaseType.Core, TargetType.Traps, 5, 20, GenderPreference.Both),3)
                {
                    User = new User("User2", "very secret password", 170, 35, GenderType.Female, 2),
                }
            };

            mockDbSet.As<IQueryable<OwnExercise>>().Setup(m => m.Provider).Returns(exercises.AsQueryable().Provider);
            mockDbSet.As<IQueryable<OwnExercise>>().Setup(m => m.Expression).Returns(exercises.AsQueryable().Expression);
            mockDbSet.As<IQueryable<OwnExercise>>().Setup(m => m.ElementType).Returns(exercises.AsQueryable().ElementType);
            mockDbSet.As<IQueryable<OwnExercise>>().Setup(m => m.GetEnumerator()).Returns(exercises.AsQueryable().GetEnumerator());
            contextMock.Setup(x => x.Set<OwnExercise>()).Returns(mockDbSet.Object);

            var result = repository.GetAllOwnExercisesOfUser(userId);

            Assert.That(result, Is.Not.Null);
            Assert.That(result.Count(), Is.EqualTo(2));
        }

        [Test]
        public void Method_GetAllOwnExercisesOfUser_ReturnsEmptyResult()
        {
            Mock<DbSet<OwnExercise>> mockDbSet = new Mock<DbSet<OwnExercise>>();
            int userId = 3;
            var exercises = new List<OwnExercise>
            {
                new OwnExercise(new RepExercise(1, "TestName", PhaseType.Core, TargetType.Biceps, 5, 20, GenderPreference.Both),1)
                {
                    User = new User("User1", "secret password", 180, 30, GenderType.Male, 1),
                },
                new OwnExercise(new RepExercise(2, "TestName2", PhaseType.Core, TargetType.Triceps, 5, 20, GenderPreference.Man),2)
                {
                    User = new User("User1", "secret password", 180, 30, GenderType.Male, 1),
                },
                new OwnExercise(new RepExercise(3, "TestName3", PhaseType.Core, TargetType.Traps, 5, 20, GenderPreference.Both),3)
                {
                    User = new User("User2", "very secret password", 170, 35, GenderType.Female, 2),
                }
            };
            mockDbSet.As<IQueryable<OwnExercise>>().Setup(m => m.Provider).Returns(exercises.AsQueryable().Provider);
            mockDbSet.As<IQueryable<OwnExercise>>().Setup(m => m.Expression).Returns(exercises.AsQueryable().Expression);
            mockDbSet.As<IQueryable<OwnExercise>>().Setup(m => m.ElementType).Returns(exercises.AsQueryable().ElementType);
            mockDbSet.As<IQueryable<OwnExercise>>().Setup(m => m.GetEnumerator()).Returns(exercises.AsQueryable().GetEnumerator());
            contextMock.Setup(x => x.Set<OwnExercise>()).Returns(mockDbSet.Object);

            var result = repository.GetAllOwnExercisesOfUser(userId);

            Assert.That(result, Is.Empty);
        }
    }
}
