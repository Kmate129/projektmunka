﻿using Moq;
using Projektmunka.Repository.Interfaces;
using Projektmunka.Repository;
using Microsoft.EntityFrameworkCore;
using Projektmunka.Data.Model;
using Projektmunka.Data.Enums;

namespace Projektmunka.UnitTests.RepositoryTests
{
    [TestFixture]
    public class UserRepositoryTests
    {
        private IUserRepository repository;
        private Mock<DbContext> contextMock;

        [SetUp]
        public void SetUp()
        {
            contextMock = new Mock<DbContext>();
            repository = new UserRepository(contextMock.Object);
        }

        [Test]
        public void Method_GetAllOwnExercisesOfUser_ReturnsCorrectOwnExercises()
        {
            Mock<DbSet<User>> mockDbSet = new Mock<DbSet<User>>();
            string username = "User2";
            var users = new List<User>
            {
                new User("User1", "secret password", 180, 30, GenderType.Male, 1),
                new User("User2", "very secret password", 170, 35, GenderType.Female, 2),
                new User("User3", "ultra secret password", 160, 25, GenderType.Female, 3)
            };

            mockDbSet.As<IQueryable<User>>().Setup(m => m.Provider).Returns(users.AsQueryable().Provider);
            mockDbSet.As<IQueryable<User>>().Setup(m => m.Expression).Returns(users.AsQueryable().Expression);
            mockDbSet.As<IQueryable<User>>().Setup(m => m.ElementType).Returns(users.AsQueryable().ElementType);
            mockDbSet.As<IQueryable<User>>().Setup(m => m.GetEnumerator()).Returns(users.AsQueryable().GetEnumerator());
            contextMock.Setup(x => x.Set<User>()).Returns(mockDbSet.Object);

            var result = repository.GetUserByUsername(username);

            Assert.That(result, Is.Not.Null);
            Assert.That(result.Id, Is.EqualTo(2));
            Assert.That(result.Username, Is.EqualTo("User2"));
        }

        [Test]
        public void Method_GetAllOwnExercisesOfUser_ReturnsEmptyResult()
        {
            Mock<DbSet<User>> mockDbSet = new Mock<DbSet<User>>();
            string username = "User4";
            var users = new List<User>
            {
                new User("User1", "secret password", 180, 30, GenderType.Male, 1),
                new User("User2", "very secret password", 170, 35, GenderType.Female, 2),
                new User("User3", "ultra secret password", 160, 25, GenderType.Female, 3)
            };
            mockDbSet.As<IQueryable<User>>().Setup(m => m.Provider).Returns(users.AsQueryable().Provider);
            mockDbSet.As<IQueryable<User>>().Setup(m => m.Expression).Returns(users.AsQueryable().Expression);
            mockDbSet.As<IQueryable<User>>().Setup(m => m.ElementType).Returns(users.AsQueryable().ElementType);
            mockDbSet.As<IQueryable<User>>().Setup(m => m.GetEnumerator()).Returns(users.AsQueryable().GetEnumerator());
            contextMock.Setup(x => x.Set<User>()).Returns(mockDbSet.Object);

            var result = repository.GetUserByUsername(username);

            Assert.That(result, Is.Null);
        }

    }
}
