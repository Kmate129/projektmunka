﻿using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestPlatform.ObjectModel;
using Moq;
using Projektmunka.Data.Enums;
using Projektmunka.Data.Model;
using Projektmunka.Repository;
using Projektmunka.Repository.Interfaces;

namespace Projektmunka.UnitTests.RepositoryTests
{
    [TestFixture]
    public class ExerciseRepositoryTests
    {
        private IExerciseRepository repository;
        private Mock<DbContext> contextMock;

        [SetUp]
        public void SetUp()
        {
            contextMock = new Mock<DbContext>();
            repository = new ExerciseRepository(contextMock.Object);
        }

        [Test]
        public void Method_GetExerciseById_ShouldReturnExercise()
        {
            Exercise exercise = new RepExercise(1, "TestName", PhaseType.Core, TargetType.Biceps, 5, 20, GenderPreference.Both);
            IList<Exercise> exercises = new List<Exercise> { exercise };
            Mock<DbSet<Exercise>> dbSetMock = new Mock<DbSet<Exercise>>();
            dbSetMock.Setup(m => m.Find(It.IsAny<object[]>())).Returns(exercise);
            contextMock.Setup(m => m.Set<Exercise>()).Returns(dbSetMock.Object);

            var result = repository.GetExerciseById(1);

            Assert.That(result, Is.EqualTo(exercise));
        }

        [Test]
        public void Method_GetExerciseById_ShouldReturnNull()
        {
            Exercise exercise = new RepExercise(1, "TestName", PhaseType.Core, TargetType.Biceps, 5, 20, GenderPreference.Both);
            IList<Exercise> exercises = new List<Exercise> { exercise };
            Mock<DbSet<Exercise>> dbSetMock = new Mock<DbSet<Exercise>>();
            dbSetMock.Setup(m => m.Find(It.IsAny<object[]>())).Returns(exercise);
            contextMock.Setup(m => m.Set<Exercise>()).Returns(dbSetMock.Object);

            var result = repository.GetExerciseById(2);

            Assert.That(result, Is.EqualTo(exercise));
        }

        [Test]
        public void Method_GetAllExercises_ShouldReturnCorrectExercises()
        {
            var mockDbSet = new Mock<DbSet<Exercise>>();
            var exercises = new List<Exercise>
            {
                new RepExercise(1, "Pushup", PhaseType.Core, TargetType.Biceps, 5, 20, GenderPreference.Both),
                new RepExercise(2, "Squats", PhaseType.Stretching, TargetType.Lats, 3, 14, GenderPreference.Both),
                new PerExercise(3, "Lunges", PhaseType.Core, TargetType.Quadriceps, 4, 20, GenderPreference.Woman),
            };
            mockDbSet.As<IQueryable<Exercise>>().Setup(x => x.Provider).Returns(exercises.AsQueryable().Provider);
            mockDbSet.As<IQueryable<Exercise>>().Setup(x => x.Expression).Returns(exercises.AsQueryable().Expression);
            mockDbSet.As<IQueryable<Exercise>>().Setup(x => x.ElementType).Returns(exercises.AsQueryable().ElementType);
            mockDbSet.As<IQueryable<Exercise>>().Setup(x => x.GetEnumerator()).Returns(exercises.AsQueryable().GetEnumerator());
            contextMock.Setup(x => x.Set<Exercise>()).Returns(mockDbSet.Object);
            var repository = new ExerciseRepository(contextMock.Object);

            var result = repository.GetAllExercises().ToList();

            Assert.That(result, Is.Not.Null);
            Assert.That(result.Count, Is.EqualTo(3));
            Assert.That(result[0].Id, Is.EqualTo(1));
            Assert.That(result[0].Name, Is.EqualTo("Pushup"));
            Assert.That(result[1].Id, Is.EqualTo(2));
            Assert.That(result[1].Name, Is.EqualTo("Squats"));
            Assert.That(result[2].Id, Is.EqualTo(3));
            Assert.That(result[2].Name, Is.EqualTo("Lunges"));
        }

        [Test]
        public void Method_GetAllExercises_ShouldReturnEmptyResult()
        {
            var mockDbSet = new Mock<DbSet<Exercise>>();
            var exercises = new List<Exercise>
            {
            };
            mockDbSet.As<IQueryable<Exercise>>().Setup(x => x.Provider).Returns(exercises.AsQueryable().Provider);
            mockDbSet.As<IQueryable<Exercise>>().Setup(x => x.Expression).Returns(exercises.AsQueryable().Expression);
            mockDbSet.As<IQueryable<Exercise>>().Setup(x => x.ElementType).Returns(exercises.AsQueryable().ElementType);
            mockDbSet.As<IQueryable<Exercise>>().Setup(x => x.GetEnumerator()).Returns(exercises.AsQueryable().GetEnumerator());
            contextMock.Setup(x => x.Set<Exercise>()).Returns(mockDbSet.Object);
            var repository = new ExerciseRepository(contextMock.Object);

            var result = repository.GetAllExercises().ToList();

            Assert.That(result, Is.Empty);
        }
    }
}
