﻿using Projektmunka.Platform.Interfaces;

namespace Projektmunka.UnitTests.TestClasses
{
    public class PreferencesWrapper : IPreferences
    {
        private readonly IDictionary<string, string> dictionary;

        public PreferencesWrapper()
        {
            this.dictionary = new Dictionary<string, string>();
        }

        public void Clear()
        {
            this.dictionary.Clear();
        }

        public string Get(string key, string defaultValue = null)
        {
            if (this.dictionary.TryGetValue(key, out string value))
            {
                return value;
            }
            else
            {
                return defaultValue;
            }
        }

        public void Remove(string key)
        {
            this.dictionary.Remove(key);
        }

        public void Set(string key, string value)
        {
            if(this.dictionary.TryGetValue(key, out string result))
            {
                this.Remove(key);
            }

            this.dictionary.Add(key, value);
        }
    }
}
