﻿using FluentValidation.Results;
using Moq;
using Projektmunka.Data.Enums;
using Projektmunka.Data.Model;
using Projektmunka.Logic.Interfaces;
using Projektmunka.Platform.Interfaces;
using Projektmunka.View.Tools;
using Projektmunka.View.Validators;
using Projektmunka.View.ViewModels;
using System.Windows.Input;

namespace Projektmunka.UnitTests.ViewTests
{
    [TestFixture]
    public class LoginViewModelTests
    {
        private Mock<IApp> mockApp;
        private Mock<IAppProvider> mockAppProvider;
        private Mock<IUserManagement> mockUserManagement;
        private Mock<IMessageService> mockMessageService;
        private Mock<LoginValidation> mockLoginValidation;
        private Mock<INavigationService> mockNavigationService;

        [SetUp]
        public void Setup()
        {
            this.mockApp = new Mock<IApp>();
            this.mockAppProvider = new Mock<IAppProvider>();
            this.mockAppProvider.Setup(provider => provider.GetApp()).Returns(this.mockApp.Object);
            this.mockUserManagement = new Mock<IUserManagement>();
            this.mockMessageService = new Mock<IMessageService>();
            this.mockLoginValidation = new Mock<LoginValidation>();
            this.mockNavigationService = new Mock<INavigationService>();
        }

        [Test]
        public void Property_Username_ShouldSetUserDTOsUsername()
        {
            // Arrange
            User currentUser = new User("Test User", "secret pass", 180, 20, GenderType.Male);
            this.mockApp.Setup(x => x.CurrentUser).Returns(currentUser);
            LoginViewModel viewModel = new LoginViewModel(mockUserManagement.Object, mockMessageService.Object, mockLoginValidation.Object, mockNavigationService.Object);
            var username = "testusername";

            // Act
            viewModel.Username = username;

            // Assert
            Assert.That(viewModel.Username, Is.EqualTo(username));
        }

        [Test]
        public void LoginViewModel_Password_ShouldSetUserDTOsPassword()
        {
            // Arrange
            User currentUser = new User("Test User", "secret pass", 180, 20, GenderType.Male);
            this.mockApp.Setup(x => x.CurrentUser).Returns(currentUser);
            LoginViewModel viewModel = new LoginViewModel(mockUserManagement.Object, mockMessageService.Object, mockLoginValidation.Object, mockNavigationService.Object);
            var password = "testPassword";

            // Act
            viewModel.Password = password;

            // Assert
            Assert.That(viewModel.Password, Is.EqualTo(password));
        }

        [Test]
        public void LoginViewModel_AutoLogin_ShouldSetAutoLogin()
        {
            // Arrange
            User currentUser = new User("Test User", "secret pass", 180, 20, GenderType.Male);
            this.mockApp.Setup(x => x.CurrentUser).Returns(currentUser);
            LoginViewModel viewModel = new LoginViewModel(mockUserManagement.Object, mockMessageService.Object, mockLoginValidation.Object, mockNavigationService.Object);
            var autoLogin = true;

            // Act
            viewModel.AutoLogin = autoLogin;

            // Assert
            Assert.That(viewModel.AutoLogin, Is.EqualTo(autoLogin));
        }

        [Test]
        public void TestGoToRegistrationCommand()
        {
            // Arrange
            LoginViewModel viewModel = new LoginViewModel(mockUserManagement.Object, mockMessageService.Object, mockLoginValidation.Object, mockNavigationService.Object);

            // Act
            viewModel.GoToRegistrationCommand.Execute(null);

            // Assert
            mockNavigationService.Verify(x => x.ShellGoToAsync("registrationPage"), Times.Once);
        }

        [Test]
        public void TestLoginCommand()
        {
            // Arrange
            LoginViewModel viewModel = new LoginViewModel(mockUserManagement.Object, mockMessageService.Object, mockLoginValidation.Object, mockNavigationService.Object);
            this.mockLoginValidation.Setup(x => x.Validate(It.IsAny<LoginViewModel>())).Returns(new ValidationResult());

            // Act
            viewModel.LoginCommand.Execute(null);

            // Assert
            mockUserManagement.Verify(x => x.Login(null, null, false), Times.Once);
        }
    }
}
