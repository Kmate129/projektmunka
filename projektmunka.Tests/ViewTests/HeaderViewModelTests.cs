﻿using Moq;
using Projektmunka.Data.Enums;
using Projektmunka.Data.Model;
using Projektmunka.Logic.Interfaces;
using Projektmunka.Platform.Interfaces;
using Projektmunka.View.ViewModels;

namespace Projektmunka.UnitTests.ViewTests
{
    [TestFixture]
    public class HeaderViewModelTests
    {
        private Mock<IApp> mockApp;
        private Mock<IAppProvider> mockAppProvider;
        private Mock<ITrainingPlanHandler> trainingPlanHandlerMock;

        [SetUp]
        public void SetUp()
        {
            this.mockApp = new Mock<IApp>();
            this.mockAppProvider = new Mock<IAppProvider>();
            this.mockAppProvider.Setup(provider => provider.GetApp()).Returns(this.mockApp.Object);
            this.trainingPlanHandlerMock = new Mock<ITrainingPlanHandler>();
        }

        [Test]
        public void Constructor_CurrentUserIsNull_ShouldNotSetProperties()
        {
            // Arrange
            mockApp.Setup(x => x.CurrentUser).Returns(() => null);

            // Act
            HeaderViewModel viewModel = new HeaderViewModel(trainingPlanHandlerMock.Object, this.mockAppProvider.Object);

            // Assert
            Assert.Multiple(() =>
            {
                Assert.That(viewModel.Username, Is.Null);
                Assert.That(viewModel.Avatar, Is.Null);
                Assert.That(viewModel.RemainingDailyPlans, Is.EqualTo(0));
            });
        }

        [Test]
        public void Constructor_CurrentUserIsNotNull_ShouldSetProperties()
        {
            // Arrange
            User currentUser = new User("Test User", "secret pass", 180, 20, GenderType.Male);
            this.mockApp.Setup(x => x.CurrentUser).Returns(currentUser);
            this.trainingPlanHandlerMock.Setup(x => x.GetCountOfRemainingDailyPlans()).Returns(5);

            HeaderViewModel loggedIn = new HeaderViewModel(trainingPlanHandlerMock.Object, this.mockAppProvider.Object);

            // Assert
            Assert.Multiple(() =>
            {
                Assert.That(loggedIn.Username, Is.EqualTo(currentUser.Username));
                Assert.That(loggedIn.Avatar, Is.EqualTo(currentUser.Avatar));
                Assert.That(loggedIn.RemainingDailyPlans, Is.EqualTo(5));
            });
        }

        [Test]
        public void Username_Set_ShouldSetProperty()
        {
            // Arrange
            User currentUser = new User("Test User", "secret pass", 180, 20, GenderType.Male);
            this.mockApp.Setup(x => x.CurrentUser).Returns(currentUser);
            HeaderViewModel viewModel = new HeaderViewModel(trainingPlanHandlerMock.Object, mockAppProvider.Object);

            // Act
            viewModel.Username = "testuser2";

            // Assert
            Assert.That(viewModel.Username, Is.EqualTo("testuser2"));
        }

        [Test]
        public void Avatar_Set_ShouldSetProperty()
        {
            // Arrange
            User currentUser = new User("Test User", "secret pass", 180, 20, GenderType.Male);
            this.mockApp.Setup(x => x.CurrentUser).Returns(currentUser);
            HeaderViewModel viewModel = new HeaderViewModel(trainingPlanHandlerMock.Object, mockAppProvider.Object);

            // Act
            viewModel.Avatar = "testavatar";

            // Assert
            Assert.That(viewModel.Avatar, Is.EqualTo("testavatar"));
        }

        [Test]
        public void RemainingDailyPlans_Set_ShouldSetProperty()
        {
            // Arrange
            User currentUser = new User("Test User", "secret pass", 180, 20, GenderType.Male);
            this.mockApp.Setup(x => x.CurrentUser).Returns(currentUser);
            HeaderViewModel viewModel = new HeaderViewModel(trainingPlanHandlerMock.Object, mockAppProvider.Object);

            // Act
            viewModel.RemainingDailyPlans = 5;

            // Assert
            Assert.That(viewModel.RemainingDailyPlans, Is.EqualTo(5));
        }
    }
}
