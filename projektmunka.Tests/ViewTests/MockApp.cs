﻿using Projektmunka;
using Projektmunka.Data.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Projektmunka.UnitTests.ViewTests
{
    public class MockApp : App
    {
        public virtual User CurrentUser { get; set; }
    }
}
