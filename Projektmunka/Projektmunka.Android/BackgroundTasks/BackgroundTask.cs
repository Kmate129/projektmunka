﻿using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Projektmunka.Droid.BackgroundTasks;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

[assembly: Xamarin.Forms.Dependency(typeof(BackgroundTask))]
namespace Projektmunka.Droid.BackgroundTasks
{
    public class BackgroundTask : AsyncTask<string, int, string>, IMyBackgroundTask
    {
        protected override string RunInBackground(params string[] @params)
        {
            //your background task implementation here
            return "Task Completed";
        }

        void IMyBackgroundTask.RunTaskInBackground()
        {
            this.Execute("");
        }
    }
}