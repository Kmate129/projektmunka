﻿// <copyright file="PerExercise.cs" company="OE-NIK">
// Copyright (c) OE-NIK. All rights reserved.
// </copyright>

using Projektmunka.Data.Enums;

namespace Projektmunka.Data.Model
{
    /// <summary>
    /// Entity class represents periodic exercise.
    /// </summary>
    public class PerExercise : Exercise
    {
        private readonly int baseTime;

        /// <summary>
        /// Initializes a new instance of the <see cref="PerExercise"/> class.
        /// </summary>
        public PerExercise()
            : base()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="PerExercise"/> class.
        /// </summary>
        /// <param name="id">Identification number.</param>
        /// <param name="name">Name.</param>
        /// <param name="phase">Phase.</param>
        /// <param name="target">TargetMuscle type list.</param>
        /// <param name="baseRound">Base round.</param>
        /// <param name="baseTime">Base time.</param>
        /// <param name="preference">Exercise preference.</param>
        public PerExercise(int id, string name, PhaseType phase, TargetType target, int baseRound, int baseTime, GenderPreference preference)
            : base(id, name, phase, target, baseRound, preference)
        {
            this.baseTime = baseTime;
        }

        /// <summary>
        /// Gets base time.
        /// </summary>
        public int BaseTime
        {
            get { return this.baseTime; }
        }
    }
}
