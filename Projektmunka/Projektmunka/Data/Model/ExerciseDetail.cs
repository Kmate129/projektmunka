﻿// <copyright file="ExerciseDetail.cs" company="OE-NIK">
// Copyright (c) OE-NIK. All rights reserved.
// </copyright>

using System.ComponentModel.DataAnnotations.Schema;
using Projektmunka.Data.Enums;

namespace Projektmunka.Data.Model
{
    /// <summary>
    /// Workout specific details about exercise.
    /// </summary>
    public class ExerciseDetail
    {
        private readonly int id;
        private readonly int index;

        /// <summary>
        /// Initializes a new instance of the <see cref="ExerciseDetail"/> class.
        /// </summary>
        /// <param name="index">Index.</param>
        public ExerciseDetail(int index = 0)
            : this(index, 0)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ExerciseDetail"/> class.
        /// </summary>
        /// <param name="id">ID.</param>
        /// <param name="index">Index.</param>
        public ExerciseDetail(int index, int id = 0)
        {
            this.id = id;
            this.RestTime = 60;
            this.Status = Status.Unallowed;
            this.index = index;
        }

        /// <summary>
        /// Gets ID of exercise detail.
        /// </summary>
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id
        {
            get { return this.id; }
        }

        /// <summary>
        /// Gets index of exercise.
        /// </summary>
        public int Index
        {
            get { return this.index; }
        }

        /// <summary>
        /// Gets or sets rest time.
        /// </summary>
        public int RestTime { get; set; }

        /// <summary>
        /// Gets or sets status.
        /// </summary>
        public Status Status { get; set; }

        /// <summary>
        /// Gets or sets related workout.
        /// </summary>
        public virtual Workout WorkoutWarmup { get; set; }

        /// <summary>
        /// Gets or sets related workout.
        /// </summary>
        public virtual Workout WorkoutCore { get; set; }

        /// <summary>
        /// Gets or sets related workout.
        /// </summary>
        public virtual Workout WorkoutStretching { get; set; }

        /// <summary>
        /// Gets or sets related rated exercise.
        /// </summary>
        public virtual OwnExercise OwnExercise { get; set; }

        /// <inheritdoc/>
        public override string ToString()
        {
            return $"ID: {this.Id}, EX: {this.OwnExercise.Exercise.Name}, RT: {this.RestTime}";
        }

        /// <inheritdoc/>
        public override bool Equals(object obj)
        {
            if (obj is null)
            {
                return false;
            }

            if (ReferenceEquals(this, obj))
            {
                return true;
            }

            if (this.GetType() != obj.GetType())
            {
                return false;
            }

            return (obj as Exercise).Id == this.Id;
        }
    }
}
