﻿// <copyright file="Workout.cs" company="OE-NIK">
// Copyright (c) OE-NIK. All rights reserved.
// </copyright>

using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using Projektmunka.Data.Enums;

namespace Projektmunka.Data.Model
{
    /// <summary>
    /// Entity class represents a workout.
    /// </summary>
    public class Workout
    {
        private readonly int id;
        private readonly int index;

        /// <summary>
        /// Initializes a new instance of the <see cref="Workout"/> class.
        /// </summary>
        /// <param name="index">Index.</param>
        public Workout(int index = 0)
            : this(index, 0)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Workout"/> class.
        /// </summary>
        /// <param name="id">ID.</param>
        /// <param name="index">Index.</param>
        public Workout(int index, int id = 0)
        {
            this.WarmUps = new List<ExerciseDetail>();
            this.Cores = new List<ExerciseDetail>();
            this.Stretches = new List<ExerciseDetail>();
            this.Status = Status.Unallowed;
            this.id = id;
            this.index = index;
        }

        /// <summary>
        /// Gets ID of workout.
        /// </summary>
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id
        {
            get { return this.id; }
        }

        /// <summary>
        /// Gets index of workout.
        /// </summary>
        public int Index
        {
            get { return this.index; }
        }

        /// <summary>
        /// Gets or sets muscle group.
        /// </summary>
        public MuscleGroupType MuscleGroup { get; set; }

        /// <summary>
        /// Gets or sets status.
        /// </summary>
        public Status Status { get; set; }

        /// <summary>
        /// Gets or sets related warmup exercises.
        /// </summary>
        public virtual ICollection<ExerciseDetail> WarmUps { get; set; }

        /// <summary>
        /// Gets or sets related core exercises.
        /// </summary>
        public virtual ICollection<ExerciseDetail> Cores { get; set; }

        /// <summary>
        /// Gets or sets related stretching exercises.
        /// </summary>
        public virtual ICollection<ExerciseDetail> Stretches { get; set; }

        /// <summary>
        /// Gets or sets related weekly plan.
        /// </summary>
        public virtual DailyPlan DailyPlan { get; set; }

        /// <inheritdoc/>
        public override string ToString()
        {
            return $"ID: {this.Id}, MG: {this.MuscleGroup}, ST: {this.Status}";
        }

        /// <inheritdoc/>
        public override bool Equals(object obj)
        {
            if (obj is null)
            {
                return false;
            }

            if (ReferenceEquals(this, obj))
            {
                return true;
            }

            if (this.GetType() != obj.GetType())
            {
                return false;
            }

            return (obj as Exercise).Id == this.Id;
        }
    }
}
