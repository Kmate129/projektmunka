﻿// <copyright file="RepExercise.cs" company="OE-NIK">
// Copyright (c) OE-NIK. All rights reserved.
// </copyright>

using Projektmunka.Data.Enums;

namespace Projektmunka.Data.Model
{
    /// <summary>
    /// Entity class represents a repetitive exercise.
    /// </summary>
    public class RepExercise : Exercise
    {
        private readonly int baseRepetition;

        /// <summary>
        /// Initializes a new instance of the <see cref="RepExercise"/> class.
        /// </summary>
        public RepExercise()
            : base()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="RepExercise"/> class.
        /// </summary>
        /// <param name="id">Identification number.</param>
        /// <param name="name">Name.</param>
        /// <param name="phase">Phase.</param>
        /// <param name="target">TargetMuscle type list.</param>
        /// <param name="baseRound">Base round.</param>
        /// <param name="baseRep">Base repetition.</param>
        /// <param name="preference">Exercise preference.</param>
        public RepExercise(int id, string name, PhaseType phase, TargetType target, int baseRound, int baseRep, GenderPreference preference)
            : base(id, name, phase, target, baseRound, preference)
        {
            this.baseRepetition = baseRep;
        }

        /// <summary>
        /// Gets base repetition.
        /// </summary>
        public int BaseRep
        {
            get { return this.baseRepetition; }
        }
    }
}
