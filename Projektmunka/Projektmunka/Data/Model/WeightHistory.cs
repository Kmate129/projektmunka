﻿// <copyright file="WeightHistory.cs" company="OE-NIK">
// Copyright (c) OE-NIK. All rights reserved.
// </copyright>

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using Castle.Core.Internal;

namespace Projektmunka.Data.Model
{
    /// <summary>
    /// Entity class represents weight history.
    /// </summary>
    public class WeightHistory
    {
        private readonly int id;
        private string serializedData;

        /// <summary>
        /// Initializes a new instance of the <see cref="WeightHistory"/> class.
        /// </summary>
        public WeightHistory()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="WeightHistory"/> class.
        /// </summary>
        /// <param name="startingWeight">Starting weight.</param>
        /// <param name="difference">Difference between values by week by week.</param>
        /// <param name="numberOfWeeks">Number of weeks.</param>
        /// <param name="id">Optional ID to testing.</param>
        public WeightHistory(float startingWeight, float difference, int numberOfWeeks, int id = 0)
        {
            if (difference > 0.5f || difference < -0.5f)
            {
                throw new ArgumentException("Difference value not in range");
            }

            if (numberOfWeeks < 12)
            {
                throw new ArgumentException("Number of weeks can not be lower than 12");
            }

            float[,] dataValue = new float[2, numberOfWeeks + 1];
            for (int i = 0; i < numberOfWeeks + 1; i++)
            {
                dataValue[1, i] = startingWeight + (i * difference);
            }

            dataValue[0, 0] = startingWeight;
            this.Data = dataValue;
            this.CurrentWeek = 1;
            this.id = id;
        }

        /// <summary>
        /// Gets the ID of history.
        /// </summary>
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id
        {
            get { return this.id; }
        }

        public string SerializedData
        {
            get { return this.serializedData; }
        }

        /// <summary>
        /// Gets or sets encoded data.
        /// </summary>
        [NotMapped]
        public float[,] Data
        {
            get { return this.Map(this.serializedData); }
            set { this.serializedData = this.Map(value); }
        }

        /// <summary>
        /// Gets or sets the index of current week.
        /// </summary>
        public int CurrentWeek { get; set; }

        /// <summary>
        /// Gets or sets related user identification number.
        /// </summary>
        public int UserId { get; set; }

        /// <summary>
        /// Gets or sets related user.
        /// </summary>
        public virtual User User { get; set; }

        /// <summary>
        /// Gets all expected values.
        /// </summary>
        /// <returns>Array of expected values.</returns>
        public float[] GetExpectedValues()
        {
            float[] returnValues = new float[this.Data.GetLength(1)];
            for (int i = 0; i < this.Data.GetLength(1); i++)
            {
                returnValues[i] = this.Data[1, i];
            }

            return returnValues;
        }

        /// <summary>
        /// Gets all real values.
        /// </summary>
        /// <returns>Array of real values.</returns>
        public float[] GetRealValues()
        {
            float[] returnValues = new float[this.Data.GetLength(1)];
            for (int i = 0; i < this.Data.GetLength(1); i++)
            {
                returnValues[i] = this.Data[0, i];
            }

            return returnValues;
        }

        /// <summary>
        /// Gets goal weight.
        /// </summary>
        /// <returns>Goal weight in float.</returns>
        public float GetGoalWeight()
        {
            return this.GetExpectedValues().Last();
        }

        /// <summary>
        /// Gets starting weight.
        /// </summary>
        /// <returns>Starting weight in float.</returns>
        public float GetStartingWeight()
        {
            return this.GetRealValues().First();
        }

        /// <summary>
        /// Gets current weight.
        /// </summary>
        /// <returns>Current weight in float.</returns>
        public float GetCurrentWeight()
        {
            return this.GetRealValues()[this.CurrentWeek - 1];
        }

        /// <summary>
        /// Inserts weight into the given week.
        /// </summary>
        /// <param name="weight">Weight.</param>
        /// <returns>If true, insertion was successfull. If false, insertion faild. Probably history is full.</returns>
        public bool Insert(float weight)
        {
            if (weight < 0)
            {
                throw new ArgumentException("Weight can not be negative");
            }

            if (this.CurrentWeek >= this.Data.GetLength(1))
            {
                return false;
            }

            var temp = this.Data;
            temp[0, this.CurrentWeek] = weight;
            this.Data = temp;
            this.CurrentWeek++;

            return true;
        }

        /// <summary>
        /// Maps from matrix structure to string.
        /// </summary>
        /// <param name="matrix">values in matrix.</param>
        /// <returns>Mapped string.</returns>
        private string Map(float[,] matrix)
        {
            StringBuilder stringBuilder = new StringBuilder();
            for (int i = 0; i < matrix.GetLength(1); i++)
            {
                stringBuilder.Append(string.Concat("[", matrix[0, i], ";", matrix[1, i], "]"));
            }

            return stringBuilder.ToString();
        }

        /// <summary>
        /// Maps from string to matrix structure.
        /// </summary>
        /// <param name="text">values in string.</param>
        /// <returns>Mapped matrix.</returns>
        private float[,] Map(string text)
        {
            if (text.IsNullOrEmpty())
            {
                return null;
            }

            List<Tuple<float, float>> valueList = new List<Tuple<float, float>>();
            string column = string.Empty;
            foreach (var t in text)
            {
                if (t == '[')
                {
                    column = string.Empty;
                }
                else if (t == ']')
                {
                    string[] values = column.Split(';');
                    valueList.Add(new Tuple<float, float>(float.Parse(values[0]), float.Parse(values[1])));
                }
                else
                {
                    column += t;
                }
            }

            float[,] returnValue = new float[2, valueList.Count];

            for (int i = 0; i < valueList.Count; i++)
            {
                returnValue[0, i] = valueList.ElementAt(i).Item1;
                returnValue[1, i] = valueList.ElementAt(i).Item2;
            }

            return returnValue;
        }
    }
}
