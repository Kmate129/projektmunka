﻿// <copyright file="User.cs" company="OE-NIK">
// Copyright (c) OE-NIK. All rights reserved.
// </copyright>

using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using Projektmunka.Data.Enums;

namespace Projektmunka.Data.Model
{
    /// <summary>
    /// Entity class represents a user.
    /// </summary>
    public class User
    {
        private readonly int id;
        private readonly string username;
        private readonly string password;
        private readonly float height;
        private readonly int age;
        private readonly GenderType gender;
        private readonly string avatar;

        /// <summary>
        /// Initializes a new instance of the <see cref="User"/> class.
        /// </summary>
        public User()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="User"/> class.
        /// </summary>
        /// <param name="id">ID.</param>
        /// <param name="name">Username.</param>
        /// <param name="password">Password.</param>
        /// <param name="height">Height.</param>
        /// <param name="age">Age.</param>
        /// <param name="gender">Gender.</param>
        public User(string name, string password, float height, int age, GenderType gender, int id = 0)
        {
            this.username = name;
            this.password = password;
            this.height = height;
            this.age = age;
            this.gender = gender;
            this.avatar = this.Gender == GenderType.Male ? "avatar_man.jpg" : "avatar_woman.jpg";
            this.id = id;
        }

        /// <summary>
        /// Gets identification number.
        /// </summary>
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id
        {
            get { return this.id; }
        }

        /// <summary>
        /// Gets username.
        /// </summary>
        public string Username
        {
            get { return this.username; }
        }

        /// <summary>
        /// Gets password.
        /// </summary>
        public string Password
        {
            get { return this.password; }
        }

        /// <summary>
        /// Gets the height.
        /// </summary>
        public float Height
        {
            get { return this.height; }
        }

        /// <summary>
        /// Gets the current weight.
        /// </summary>
        [NotMapped]
        public float CurrentWeight
        {
            get
            {
                return this.WeightHistory.GetCurrentWeight();
            }
        }

        /// <summary>
        /// Gets the goal weight.
        /// </summary>
        [NotMapped]
        public float GoalWeight
        {
            get
            {
                return this.WeightHistory.GetGoalWeight();
            }
        }

        /// <summary>
        /// Gets age.
        /// </summary>
        public int Age
        {
            get { return this.age; }
        }

        /// <summary>
        /// Gets gender.
        /// </summary>
        public GenderType Gender
        {
            get { return this.gender; }
        }

        /// <summary>
        /// Gets or sets the goal of the user.
        /// </summary>
        public GoalType Goal { get; set; }

        /// <summary>
        /// Gets path of avatar picture.
        /// </summary>
        public string Avatar
        {
            get { return this.avatar; }
        }

        /// <summary>
        /// Gets or sets the nutrition scheme.
        /// </summary>
        public virtual NutritionScheme Nutrients { get; set; }

        /// <summary>
        /// Gets or sets the training plan.
        /// </summary>
        public virtual TrainingPlan TrainingPlan { get; set; }

        /// <summary>
        /// Gets or sets the weight history.
        /// </summary>
        public virtual WeightHistory WeightHistory { get; set; }

        /// <summary>
        /// Gets or sets the weight history.
        /// </summary>
        public virtual TimeHistory TimeHistory { get; set; }

        /// <summary>
        /// Gets or sets the rated exercises.
        /// </summary>
        public virtual ICollection<OwnExercise> OwnExercises { get; set; }

        /// <inheritdoc/>
        public override bool Equals(object obj)
        {
            if (obj == null)
            {
                return false;
            }

            if (this == obj)
            {
                return true;
            }

            if (!(obj is User))
            {
                return false;
            }

            User that = (User)obj;
            return this.Id.Equals(that.Id);
        }

        /// <inheritdoc/>
        public override int GetHashCode()
        {
            return (this.Username.GetHashCode() * this.Password.GetHashCode()) + this.Age.GetHashCode();
        }
    }
}
