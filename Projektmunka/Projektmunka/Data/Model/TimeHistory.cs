﻿// <copyright file="TimeHistory.cs" company="OE-NIK">
// Copyright (c) OE-NIK. All rights reserved.
// </copyright>

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;

namespace Projektmunka.Data.Model
{
    /// <summary>
    /// Entity class represents time history.
    /// </summary>
    public class TimeHistory
    {
        private readonly int id;
        private string serializedTimeData;
        private string serializedTotalStack;
        private string serializedActiveStack;

        /// <summary>
        /// Initializes a new instance of the <see cref="TimeHistory"/> class.
        /// </summary>
        public TimeHistory()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="TimeHistory"/> class.
        /// </summary>
        /// <param name="numberOfWeeks">Number of weeks.</param>
        /// <param name="id">Optional ID to testing.</param>
        public TimeHistory(int numberOfWeeks, int id = 0)
        {
            if (numberOfWeeks < 12)
            {
                throw new ArgumentException("Number of weeks can not be lower than 12");
            }

            this.TimeData = new int[2, numberOfWeeks];
            this.CurrentWeek = 0;
            this.id = id;
            this.serializedTotalStack = string.Empty;
            this.serializedActiveStack = string.Empty;
        }

        /// <summary>
        /// Gets the ID of history.
        /// </summary>
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id
        {
            get { return this.id; }
        }

        public string SerializedTimeData
        {
            get { return this.serializedTimeData; }
        }

        public string SerializedTotalStack
        {
            get { return this.serializedTotalStack; }
        }

        public string SerializedActiveStack
        {
            get { return this.serializedActiveStack; }
        }

        /// <summary>
        /// Gets or sets encoded data.
        /// </summary>
        [NotMapped]
        public int[,] TimeData
        {
            get { return this.Map(this.serializedTimeData); }
            set { this.serializedTimeData = this.Map(value); }
        }

        /// <summary>
        /// Gets or sets the index of current week.
        /// </summary>
        public int CurrentWeek { get; set; }

        /// <summary>
        /// Gets or sets related user identification number.
        /// </summary>
        public int UserId { get; set; }

        /// <summary>
        /// Gets or sets related user.
        /// </summary>
        public virtual User User { get; set; }

        /// <summary>
        /// Gets all real values.
        /// </summary>
        /// <returns>Array of real values.</returns>
        public int[] GetActiveValues()
        {
            int[] returnValues = new int[this.TimeData.GetLength(1)];
            for (int i = 0; i < this.TimeData.GetLength(1); i++)
            {
                returnValues[i] = this.TimeData[0, i];
            }

            return returnValues;
        }

        /// <summary>
        /// Gets all expected values.
        /// </summary>
        /// <returns>Array of expected values.</returns>
        public int[] GetTotalValues()
        {
            int[] returnValues = new int[this.TimeData.GetLength(1)];
            for (int i = 0; i < this.TimeData.GetLength(1); i++)
            {
                returnValues[i] = this.TimeData[1, i];
            }

            return returnValues;
        }

        public void PushToTotalStack(TimeSpan time)
        {
            var values = this.MapFromStack(this.serializedTotalStack);
            values.Add(time);
            this.serializedTotalStack = this.MapToStack(values);
        }

        public void PushToActiveStack(TimeSpan time)
        {
            var values = this.MapFromStack(this.serializedActiveStack);
            values.Add(time);
            this.serializedActiveStack = this.MapToStack(values);
        }
        public void SaveStackAverages()
        {
            this.SaveActiveStackAverage();
            this.SaveTotalStackAverage();
            this.CurrentWeek++;
        }

        private void SaveTotalStackAverage()
        {
            var values = this.MapFromStack(this.serializedTotalStack);
            int sumSeconds = 0;
            foreach (var item in values)
            {
                sumSeconds += item.Hours * 3600;
                sumSeconds += item.Minutes * 60;
                sumSeconds += item.Seconds;
            }

            this.InsertTotalTime(sumSeconds / values.Count);
            this.serializedTotalStack = string.Empty;
        }

        private void SaveActiveStackAverage()
        {
            var values = this.MapFromStack(this.serializedActiveStack);
            int sumSeconds = 0;
            foreach (var item in values)
            {
                sumSeconds += item.Hours * 3600;
                sumSeconds += item.Minutes * 60;
                sumSeconds += item.Seconds;
            }

            this.InsertActiveTime(sumSeconds / values.Count);
            this.serializedActiveStack = string.Empty;
        }

        public int GetTotalStackValues()
        {
            var values = this.MapFromStack(this.serializedTotalStack);
            int sumSeconds = 0;
            foreach (var item in values)
            {
                sumSeconds += item.Hours * 3600;
                sumSeconds += item.Minutes * 60;
                sumSeconds += item.Seconds;
            }

            return sumSeconds;
        }

        /// <summary>
        /// Inserts weight into the given week.
        /// </summary>
        /// <param name="time">Weight.</param>
        /// <returns>If true, insertion was successfull. If false, insertion faild. Probably history is full.</returns>
        private bool InsertTotalTime(int minutes)
        {
            if (this.CurrentWeek >= this.TimeData.GetLength(1))
            {
                return false;
            }

            var temp = this.TimeData;
            temp[1, this.CurrentWeek] = minutes;
            this.TimeData = temp;

            return true;
        }

        private bool InsertActiveTime(int minutes)
        {
            if (this.CurrentWeek >= this.TimeData.GetLength(1))
            {
                return false;
            }

            var temp = this.TimeData;
            temp[0, this.CurrentWeek] = minutes;
            this.TimeData = temp;

            return true;
        }

        /// <summary>
        /// Maps from matrix structure to string.
        /// </summary>
        /// <param name="matrix">values in matrix.</param>
        /// <returns>Mapped string.</returns>
        private string Map(int[,] matrix)
        {
            StringBuilder stringBuilder = new StringBuilder();
            for (int i = 0; i < matrix.GetLength(1); i++)
            {
                stringBuilder.Append(string.Concat("[", matrix[0, i], ";", matrix[1, i], "]"));
            }

            return stringBuilder.ToString();
        }

        /// <summary>
        /// Maps from string to matrix structure.
        /// </summary>
        /// <param name="text">values in string.</param>
        /// <returns>Mapped matrix.</returns>
        private int[,] Map(string text)
        {
            List<Tuple<int, int>> valueList = new List<Tuple<int, int>>();
            string column = string.Empty;
            foreach (var t in text)
            {
                if (t == '[')
                {
                    column = string.Empty;
                }
                else if (t == ']')
                {
                    string[] values = column.Split(';');
                    valueList.Add(new Tuple<int, int>(int.Parse(values[0]), int.Parse(values[1])));
                }
                else
                {
                    column += t;
                }
            }

            int[,] returnValue = new int[2, valueList.Count];

            for (int i = 0; i < valueList.Count; i++)
            {
                returnValue[0, i] = valueList.ElementAt(i).Item1;
                returnValue[1, i] = valueList.ElementAt(i).Item2;
            }

            return returnValue;
        }

        private IList<TimeSpan> MapFromStack(string text)
        {
            IList<TimeSpan> valueList = new List<TimeSpan>();
            string column = string.Empty;
            foreach (var t in text)
            {
                if (t == '[')
                {
                    column = string.Empty;
                }
                else if (t == ']')
                {
                    valueList.Add(TimeSpan.Parse(column));
                }
                else
                {
                    column += t;
                }
            }

            return valueList;
        }

        private string MapToStack(IList<TimeSpan> values)
        {
            StringBuilder stringBuilder = new StringBuilder();
            for (int i = 0; i < values.Count; i++)
            {
                stringBuilder.Append(string.Concat("[", values.ElementAt(i), "]"));
            }

            return stringBuilder.ToString();
        }
    }
}
