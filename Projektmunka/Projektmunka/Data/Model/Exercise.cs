﻿// <copyright file="Exercise.cs" company="OE-NIK">
// Copyright (c) OE-NIK. All rights reserved.
// </copyright>

using System;
using System.Collections.Generic;
using System.Linq;
using Projektmunka.Data.Enums;

namespace Projektmunka.Data.Model
{
    /// <summary>
    /// Entity class represents an exercise.
    /// </summary>
    public abstract class Exercise
    {
        private readonly int id;
        private readonly string name;
        private readonly PhaseType phase;
        private readonly TargetType muscle;
        private readonly MuscleGroupType muscleGroup;
        private readonly int baseRound;
        private readonly GenderPreference preference;
        private readonly string visual;

        /// <summary>
        /// Initializes a new instance of the <see cref="Exercise"/> class.
        /// </summary>
        protected Exercise()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Exercise"/> class.
        /// </summary>
        /// <param name="id">Identification number.</param>
        /// <param name="name">Name.</param>
        /// <param name="phase">Phase.</param>
        /// <param name="target">TargetMuscle type.</param>
        /// <param name="baseRound">Base round.</param>
        /// <param name="preference">Exercise preference.</param>
        protected Exercise(int id, string name, PhaseType phase, TargetType target, int baseRound, GenderPreference preference)
        {
            this.id = id;
            this.name = name;
            this.phase = phase;
            this.muscle = target;
            this.muscleGroup = this.Convert(target);
            this.baseRound = baseRound;
            this.preference = preference;
            this.visual = this.Map(name);
            this.OwnExercises = new List<OwnExercise>();
        }

        /// <summary>
        /// Gets ID of the exercise.
        /// </summary>
        public int Id
        {
            get { return this.id; }
        }

        /// <summary>
        /// Gets name.
        /// </summary>
        public string Name
        {
            get { return this.name; }
        }

        /// <summary>
        /// Gets phase.
        /// </summary>
        public PhaseType Phase
        {
            get { return this.phase; }
        }

        /// <summary>
        /// Gets related muscle.
        /// </summary>
        public TargetType TargetMuscle
        {
            get { return this.muscle; }
        }

        /// <summary>
        /// Gets related muscle group.
        /// </summary>
        public MuscleGroupType TargetMuscleGroup
        {
            get { return this.muscleGroup; }
        }

        /// <summary>
        /// Gets base round of the exercise.
        /// </summary>
        public int BaseRound
        {
            get { return this.baseRound; }
        }

        /// <summary>
        /// Gets a value indicating whether exercise is for only man or not.
        /// </summary>
        public GenderPreference Preference
        {
            get { return this.preference; }
        }

        /// <summary>
        /// Gets path of visual of the exercise.
        /// </summary>
        public string Visual
        {
            get { return this.visual; }
        }

        /// <summary>
        /// Gets or sets related OwnExercises.
        /// </summary>
        public virtual ICollection<OwnExercise> OwnExercises { get; set; }

        /// <summary>
        /// Gets or sets primary value of the equipment.
        /// </summary>
        public virtual ICollection<EquipmentTypeEntity> PrimaryEquipments { get; set; }

        /// <summary>
        /// Gets or sets secondary values of the equipment.
        /// </summary>
        public virtual ICollection<EquipmentTypeEntity> SecondaryEquipments { get; set; }

        /// <summary>
        /// Returns whether exercise requires equipment (primarily) or not.
        /// </summary>
        /// <returns>True or false.</returns>
        public bool IsEquipmentRequired()
        {
            return !this.PrimaryEquipments.All(x => x.Value.Equals(EquipmentType.Nothing));
        }

        /// <inheritdoc/>
        public override bool Equals(object obj)
        {
            if (obj is null)
            {
                return false;
            }

            if (ReferenceEquals(this, obj))
            {
                return true;
            }

            if (this.GetType() != obj.GetType())
            {
                return false;
            }

            return (obj as Exercise).Id == this.Id;
        }

        /// <inheritdoc/>
        public override int GetHashCode()
        {
            return this.Id.GetHashCode() + this.Name.GetHashCode();
        }

        /// <inheritdoc/>
        public override string ToString()
        {
            return this.Id + " " + this.Name;
        }

        private string Map(string name)
        {
            return name.ToLower().Replace("'", "_").Replace(" ", "_").Replace("-", "_");
        }

        private MuscleGroupType Convert(TargetType target)
        {
            return target switch
            {
                TargetType.Abductors => MuscleGroupType.Leg,
                TargetType.Abs => MuscleGroupType.Abs,
                TargetType.Adductors => MuscleGroupType.Leg,
                TargetType.Biceps => MuscleGroupType.Arm,
                TargetType.Calves => MuscleGroupType.Leg,
                TargetType.Chest => MuscleGroupType.Chest,
                TargetType.Forearms => MuscleGroupType.Arm,
                TargetType.FrontDelt => MuscleGroupType.Shoulder,
                TargetType.Glutes => MuscleGroupType.Leg,
                TargetType.Hamstrings => MuscleGroupType.Leg,
                TargetType.Lats => MuscleGroupType.Back,
                TargetType.LowerBack => MuscleGroupType.Back,
                TargetType.MiddleBack => MuscleGroupType.Back,
                TargetType.Quadriceps => MuscleGroupType.Leg,
                TargetType.RearDelt => MuscleGroupType.Shoulder,
                TargetType.SideDelt => MuscleGroupType.Shoulder,
                TargetType.Traps => MuscleGroupType.Back,
                TargetType.Triceps => MuscleGroupType.Arm,
                _ => throw new Exception("TargetMuscle does not exist"),
            };
        }
    }
}
