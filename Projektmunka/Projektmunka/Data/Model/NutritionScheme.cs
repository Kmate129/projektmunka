﻿// <copyright file="NutritionScheme.cs" company="OE-NIK">
// Copyright (c) OE-NIK. All rights reserved.
// </copyright>

namespace Projektmunka.Data.Model
{
    /// <summary>
    /// Entity class represents a nutrition scheme.
    /// </summary>
    public class NutritionScheme
    {
        private readonly int id;
        private readonly int energy;
        private readonly int carbohydrates;
        private readonly int sugar;
        private readonly int protein;
        private readonly int fat;
        private readonly int saturatedFat;
        private readonly int salt;

        /// <summary>
        /// Initializes a new instance of the <see cref="NutritionScheme"/> class.
        /// </summary>
        public NutritionScheme()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="NutritionScheme"/> class.
        /// </summary>
        /// <param name="id">ID of nutrition scheme</param>
        /// <param name="energy">Energy.</param>
        /// <param name="carbohydrates">Carbohydrates.</param>
        /// <param name="sugar">Sugar.</param>
        /// <param name="protein">Protein.</param>
        /// <param name="fat">Fat</param>
        /// <param name="saturatedFat">Saturated fat.</param>
        /// <param name="salt">Salt.</param>
        public NutritionScheme(int id, int energy, int carbohydrates, int sugar, int protein, int fat, int saturatedFat, int salt)
        {
            this.id = id;
            this.energy = energy;
            this.carbohydrates = carbohydrates;
            this.sugar = sugar;
            this.protein = protein;
            this.fat = fat;
            this.saturatedFat = saturatedFat;
            this.salt = salt;
        }

        /// <summary>
        /// Gets ID of nutrition scheme.
        /// </summary>
        public int Id
        {
            get { return this.id; }
        }

        /// <summary>
        /// Gets energy.
        /// </summary>
        public int Energy
        {
            get { return this.energy; }
        }

        /// <summary>
        /// Gets carbohydrates.
        /// </summary>
        public int Carbohydrates
        {
            get { return this.carbohydrates; }
        }

        /// <summary>
        /// Gets sugar.
        /// </summary>
        public int Sugar
        {
            get { return this.sugar; }
        }

        /// <summary>
        /// Gets protein.
        /// </summary>
        public int Protein
        {
            get { return this.protein; }
        }

        /// <summary>
        /// Gets fat.
        /// </summary>
        public int Fat
        {
            get { return this.fat; }
        }

        /// <summary>
        /// Gets saturated fat.
        /// </summary>
        public int SaturatedFat
        {
            get { return this.saturatedFat; }
        }

        /// <summary>
        /// Gets salt.
        /// </summary>
        public int Salt
        {
            get { return this.salt; }
        }

        /// <summary>
        /// Gets or sets related user identification number.
        /// </summary>
        public int UserEntityId { get; set; }

        /// <summary>
        /// Gets or sets related user.
        /// </summary>
        public virtual User User { get; set; }
    }
}
