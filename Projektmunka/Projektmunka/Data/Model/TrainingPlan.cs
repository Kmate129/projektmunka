﻿// <copyright file="TrainingPlan.cs" company="OE-NIK">
// Copyright (c) OE-NIK. All rights reserved.
// </copyright>

using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using Projektmunka.Data.Enums;

namespace Projektmunka.Data.Model
{
    /// <summary>
    /// Class represents a training plan.
    /// </summary>
    public class TrainingPlan
    {
        private readonly int id;
        private readonly bool equipmentRequirement;
        private readonly int numberOfDays;

        /// <summary>
        /// Initializes a new instance of the <see cref="TrainingPlan"/> class.
        /// <param name="equipmentRequirement">Equipment requipment.</param>
        /// <param name="numberOfDays">Number of training days.</param>
        /// <param name="id">Optinal ID to testing.</param>
        /// </summary>
        public TrainingPlan(bool equipmentRequirement, int numberOfDays, int id = 0)
        {
            this.equipmentRequirement = equipmentRequirement;
            this.numberOfDays = numberOfDays;
            this.WeeklyPlans = new List<WeeklyPlan>();
            this.Status = Status.Ready;
            this.id = id;
        }

        /// <summary>
        /// Gets ID of TrainingPlan.
        /// </summary>
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id
        {
            get { return this.id; }
        }

        /// <summary>
        /// Gets a value indicating whether user has equipment or not.
        /// </summary>
        public bool EquipmentRequirement
        {
            get { return this.equipmentRequirement; }
        }

        /// <summary>
        /// Gets the number of training days.
        /// </summary>
        public int NumberOfDays
        {
            get { return this.numberOfDays; }
        }

        /// <summary>
        /// Gets or sets status.
        /// </summary>
        public Status Status { get; set; }

        /// <summary>
        /// Gets or sets weekly plans.
        /// </summary>
        public virtual ICollection<WeeklyPlan> WeeklyPlans { get; set; }

        /// <summary>
        /// Gets or sets related ID of user.
        /// </summary>
        public int UserId { get; set; }

        /// <summary>
        /// Gets or sets related user.
        /// </summary>
        public virtual User User { get; set; }

        /// <summary>
        /// Gets number of weeks.
        /// </summary>
        /// <returns>Integer.</returns>
        public int GetNumberOfWeeks()
        {
            return this.WeeklyPlans.Count;
        }

        /// <summary>
        /// Adds weekly plan to weekly plans.
        /// </summary>
        /// <param name="weeklyPlan">Weekly plan to add.</param>
        public void AddToWeeklyPlans(WeeklyPlan weeklyPlan)
        {
            this.WeeklyPlans.Add(weeklyPlan);
        }

        /// <summary>
        /// Gets progression ( done weeks / sum weeks).
        /// </summary>
        /// <returns>Floating point represents progression.</returns>
        public float GetProgression()
        {
            return (float)this.WeeklyPlans.Count(x => x.Status.Equals(Status.Done)) / this.WeeklyPlans.Count;
        }

        public int RemainingWeeks()
        {
            return this.WeeklyPlans.Count(x => !x.Status.Equals(Status.Done));
        }

        /// <summary>
        /// Gets current weekly plan.
        /// </summary>
        /// <returns>Weekly plan.</returns>
        public WeeklyPlan GetCurrentWeek()
        {
            return this.WeeklyPlans.First(x => x.Status.Equals(Status.Ready));
        }
    }
}
