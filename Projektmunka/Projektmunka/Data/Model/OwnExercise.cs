﻿// <copyright file="OwnExercise.cs" company="OE-NIK">
// Copyright (c) OE-NIK. All rights reserved.
// </copyright>

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace Projektmunka.Data.Model
{
    /// <summary>
    /// Entity class represents a OwnExercise.
    /// </summary>
    public class OwnExercise : IComparable<OwnExercise>
    {
        private readonly int id;
        private float rating;

        /// <summary>
        /// Initializes a new instance of the <see cref="OwnExercise"/> class.
        /// </summary>
        public OwnExercise()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="OwnExercise"/> class.
        /// </summary>
        /// <param name="exercise">Exercise to rate.</param>
        /// <param name="id">ID of rated exercise.</param>
        public OwnExercise(Exercise exercise, int id = 0)
        {
            this.ExerciseDetails = new List<ExerciseDetail>();
            this.Exercise = exercise;
            this.rating = 1;
            this.id = id;
        }

        /// <summary>
        /// Gets ID of OwnExercise.
        /// </summary>
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id
        {
            get { return this.id; }
        }

        /// <summary>
        /// Gets rating.
        /// </summary>
        public float Rating
        {
            get { return this.rating; }
        }

        /// <summary>
        /// Gets or sets related exercise.
        /// </summary>
        public virtual Exercise Exercise { get; set; }

        /// <summary>
        /// Gets or sets related exercise.
        /// </summary>
        public virtual ICollection<ExerciseDetail> ExerciseDetails { get; set; }

        /// <summary>
        /// Gets or sets related user.
        /// </summary>
        public virtual User User { get; set; }

        /// <summary>
        /// Rates up this exercise.
        /// </summary>
        public void Like()
        {
            this.rating = ConstParameters.HIGHER_RATE_MULTIPLIER;
        }

        /// <summary>
        /// Highly rates up this exercise.
        /// </summary>
        public void SuperLike()
        {
            this.rating = ConstParameters.HIGHEST_RATE_MULTIPLIER;
        }

        /// <summary>
        /// Rates this exercise as neutral.
        /// </summary>
        public void Neutral()
        {
            this.rating = ConstParameters.BASE_RATE_MULTIPLIER;
        }

        /// <summary>
        /// Rates down this exercise.
        /// </summary>
        public void Dislike()
        {
            this.rating = ConstParameters.LOWER_RATE_MULTIPLIER;
        }

        /// <summary>
        /// Highly rates down this exercise.
        /// </summary>
        public void SuperDislike()
        {
            this.rating = ConstParameters.LOWEST_RATE_MULTIPLIER;
        }

        /// <inheritdoc/>
        public int CompareTo(OwnExercise other)
        {
            return this.Rating.CompareTo(other.Rating);
        }
    }
}
