﻿// <copyright file="WeeklyPlan.cs" company="OE-NIK">
// Copyright (c) OE-NIK. All rights reserved.
// </copyright>

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using Projektmunka.Data.Enums;

namespace Projektmunka.Data.Model
{
    /// <summary>
    /// Entity class represents a weekly plan.
    /// </summary>
    public class WeeklyPlan : IComparable
    {
        private readonly int id;
        private readonly int index;

        /// <summary>
        /// Initializes a new instance of the <see cref="WeeklyPlan"/> class.
        /// </summary>
        /// <param name="index">Index.</param>
        public WeeklyPlan(int index = 0)
            : this(index, 0)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="WeeklyPlan"/> class.
        /// </summary>
        /// <param name="id">ID.</param>
        /// <param name="index">Index.</param>
        public WeeklyPlan(int index, int id = 0)
        {
            this.DailyPlans = new List<DailyPlan>();
            this.Status = Status.Unallowed;
            this.id = id;
            this.index = index;
        }

        /// <summary>
        /// Gets ID of weekly plan.
        /// </summary>
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id
        {
            get { return this.id; }
        }

        /// <summary>
        /// Gets index of weekly plan.
        /// </summary>
        public int Index
        {
            get { return this.index; }
        }

        /// <summary>
        /// Gets or sets status.
        /// </summary>
        public Status Status { get; set; }

        /// <summary>
        /// Gets or sets related daily plans.
        /// </summary>
        public virtual ICollection<DailyPlan> DailyPlans { get; set; }

        /// <summary>
        /// Gets or sets related training plan.
        /// </summary>
        public virtual TrainingPlan TrainingPlan { get; set; }

        /// <summary>
        /// Gets the number of training days.
        /// </summary>
        /// <returns>Integer.</returns>
        public int GetNumberOfDays()
        {
            return this.DailyPlans.Count;
        }

        /// <summary>
        /// Adds daily plan to the list of daily workouts.
        /// </summary>
        /// <param name="dailyPlan">Daily plan to add.</param>
        public void AddToDailyPlans(DailyPlan dailyPlan)
        {
            this.DailyPlans.Add(dailyPlan);
        }

        /// <inheritdoc/>
        public override string ToString()
        {
            return $"ID: {this.Id}, ST: {this.Status}, WK: {this.DailyPlans.Count}";
        }

        /// <inheritdoc/>
        public int CompareTo(object obj)
        {
            return this.Index - (obj as WeeklyPlan).Index;
        }

        /// <inheritdoc/>
        public override bool Equals(object obj)
        {
            if (obj is null)
            {
                return false;
            }

            if (ReferenceEquals(this, obj))
            {
                return true;
            }

            if (this.GetType() != obj.GetType())
            {
                return false;
            }

            return (obj as WeeklyPlan).Id == this.Id;
        }

        /// <inheritdoc/>
        public override int GetHashCode()
        {
            return this.GetHashCode();
        }
    }
}
