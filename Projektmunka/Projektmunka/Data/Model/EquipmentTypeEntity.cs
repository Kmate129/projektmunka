﻿// <copyright file="EquipmentTypeEntity.cs" company="OE-NIK">
// Copyright (c) OE-NIK. All rights reserved.
// </copyright>

using System.Collections.Generic;
using Projektmunka.Data.Enums;

namespace Projektmunka.Data.Model
{
    /// <summary>
    /// Entity class represents an equipment type.
    /// </summary>
    public class EquipmentTypeEntity
    {
        private readonly int id;
        private readonly EquipmentType type;

        /// <summary>
        /// Initializes a new instance of the <see cref="EquipmentTypeEntity"/> class.
        /// </summary>
        public EquipmentTypeEntity()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="EquipmentTypeEntity"/> class.
        /// </summary>
        /// <param name="id">ID of equipment type.</param>
        /// <param name="type">Equipment type value.</param>
        public EquipmentTypeEntity(int id, EquipmentType type)
        {
            this.id = id;
            this.type = type;
        }

        /// <summary>
        /// Gets ID of the equipment type.
        /// </summary>
        public int Id
        {
            get { return this.id; }
        }

        /// <summary>
        /// Gets value of the equipment type.
        /// </summary>
        public EquipmentType Value
        {
            get { return this.type; }
        }

        /// <summary>
        /// Gets or sets the related equipments.
        /// </summary>
        public virtual ICollection<Exercise> EquipmentsPrim { get; set; }

        /// <summary>
        /// Gets or sets the related equipments.
        /// </summary>
        public virtual ICollection<Exercise> EquipmentsSec { get; set; }
    }
}
