﻿// <copyright file="DailyPlan.cs" company="OE-NIK">
// Copyright (c) OE-NIK. All rights reserved.
// </copyright>

using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using Projektmunka.Data.Enums;

namespace Projektmunka.Data.Model
{
    /// <summary>
    /// Entity class represents a daily plan.
    /// </summary>
    public class DailyPlan
    {
        private readonly int id;
        private readonly int index;

        /// <summary>
        /// Initializes a new instance of the <see cref="DailyPlan"/> class.
        /// </summary>
        /// <param name="index">Index.</param>
        public DailyPlan(int index = 0)
            : this(index, 0)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="DailyPlan"/> class.
        /// </summary>
        /// <param name="id">ID.</param>
        /// <param name="index">Index.</param>
        public DailyPlan(int index, int id = 0)
        {
            this.Workouts = new List<Workout>();
            this.Status = Status.Unallowed;
            this.id = id;
            this.index = index;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="DailyPlan"/> class.
        /// </summary>
        /// <param name="workouts">List of workouts.</param>
        public DailyPlan(ICollection<Workout> workouts)
        {
            this.Workouts = workouts;
            this.Status = Status.Unallowed;
        }

        /// <summary>
        /// Gets identification number of daily plan.
        /// </summary>
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id
        {
            get { return this.id; }
        }

        /// <summary>
        /// Gets index of daily plan.
        /// </summary>
        public int Index
        {
            get { return this.index; }
        }

        /// <summary>
        /// Gets or sets status.
        /// </summary>
        public Status Status { get; set; }

        /// <summary>
        /// Gets or sets related workouts.
        /// </summary>
        public virtual ICollection<Workout> Workouts { get; set; }

        /// <summary>
        /// Gets or sets related weekly plan.
        /// </summary>
        public virtual WeeklyPlan WeeklyPlan { get; set; }

        /// <summary>
        /// Adds workout to the list of workouts.
        /// </summary>
        /// <param name="workout">Workout to add.</param>
        public void AddToWorkouts(Workout workout)
        {
            this.Workouts.Add(workout);
        }

        /// <inheritdoc/>
        public override string ToString()
        {
            return $"ID: {this.Id}, ST: {this.Status}, WO: {this.Workouts.Count}";
        }

        /// <inheritdoc/>
        public override bool Equals(object obj)
        {
            if (obj is null)
            {
                return false;
            }

            if (ReferenceEquals(this, obj))
            {
                return true;
            }

            if (this.GetType() != obj.GetType())
            {
                return false;
            }

            return (obj as Exercise).Id == this.Id;
        }
    }
}
