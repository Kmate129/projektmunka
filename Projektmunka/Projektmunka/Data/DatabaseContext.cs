﻿// <copyright file="DatabaseContext.cs" company="OE-NIK">
// Copyright (c) OE-NIK. All rights reserved.
// </copyright>

using System;
using System.IO;
using Microsoft.EntityFrameworkCore;
using Projektmunka.Data.Enums;
using Projektmunka.Data.Model;

namespace Projektmunka.Data
{
    /// <summary>
    /// Database context to application.
    /// </summary>
    public class DatabaseContext : DbContext
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="DatabaseContext"/> class.
        /// </summary>
        public DatabaseContext()
        {
            this.Database.IsSqlite();
            this.Database.IsRelational();
            this.Database.EnsureCreated();
        }

        /// <summary>
        /// Gets or sets exercises.
        /// </summary>
        public virtual DbSet<Exercise> Exercises { get; set; }

        /// <summary>
        /// Gets or sets periodic exercises.
        /// </summary>
        public virtual DbSet<PerExercise> PerExercises { get; set; }

        /// <summary>
        /// Gets or sets repetitive exercises.
        /// </summary>
        public virtual DbSet<RepExercise> RepExercises { get; set; }

        /// <summary>
        /// Gets or sets equipment types.
        /// </summary>
        public virtual DbSet<EquipmentTypeEntity> EquipmentTypes { get; set; }

        /// <summary>
        /// Gets or sets rated exercises.
        /// </summary>
        public virtual DbSet<OwnExercise> OwnExercises { get; set; }

        /// <summary>
        /// Gets or sets exercise details.
        /// </summary>
        public virtual DbSet<ExerciseDetail> ExerciseDetails { get; set; }

        /// <summary>
        /// Gets or sets users.
        /// </summary>
        public virtual DbSet<User> Users { get; set; }

        /// <summary>
        /// Gets or sets weekly plans.
        /// </summary>
        public virtual DbSet<WeeklyPlan> WeeklyPlans { get; set; }

        /// <summary>
        /// Gets or sets daily plans.
        /// </summary>
        public virtual DbSet<DailyPlan> DailyPlans { get; set; }

        /// <summary>
        /// Gets or sets Workouts.
        /// </summary>
        public virtual DbSet<Workout> Workouts { get; set; }

        /// <inheritdoc/>
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (optionsBuilder == null)
            {
                throw new ArgumentNullException(nameof(optionsBuilder));
            }
            else
            {
                if (!optionsBuilder.IsConfigured)
                {
                    optionsBuilder.EnableSensitiveDataLogging();
                    optionsBuilder.LogTo(Console.WriteLine);

                    var basePath = Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData);
                    var databasePath = Path.Combine(basePath, "m1.db3");
                    if (!File.Exists(databasePath))
                    {
                        File.Create(databasePath);
                    }

                    optionsBuilder.UseLazyLoadingProxies().UseSqlite($"Data Source={databasePath}");

                    // "/data/user/0/com.companyname.projektmunka/files/.local/share/md.db"
                }
            }
        }

        /// <inheritdoc/>
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            if (modelBuilder == null)
            {
                throw new ArgumentNullException(nameof(modelBuilder));
            }

            this.DescriptingEquipmentTypeEntity(modelBuilder);
            this.SeedingEquipmentTypeEntity(modelBuilder);
            this.SeedingSecondaryEquipments(modelBuilder);
            this.SeedingPrimaryEquipments(modelBuilder);

            this.DescriptingUser(modelBuilder);
            this.DescriptingWeightHistory(modelBuilder);
            this.DescriptingTimeHistory(modelBuilder);
            this.DescriptingTrainingPlan(modelBuilder);
            this.DescriptingNutritionScheme(modelBuilder);

            this.DescribingWeeklyPlan(modelBuilder);
            this.DescriptingDailyPlan(modelBuilder);
            this.DescriptingWorkout(modelBuilder);

            this.DescriptingExerciseDetail(modelBuilder);
            this.DescriptingOwnExercise(modelBuilder);

            this.DescriptingExercise(modelBuilder);
            this.DescriptingRepExercise(modelBuilder);
            this.DescriptingPerExercise(modelBuilder);
            this.SeedingRepExercise(modelBuilder);
            this.SeedingPerExercise(modelBuilder);
        }

        private void DescriptingEquipmentTypeEntity(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<EquipmentTypeEntity>().HasKey(x => x.Id);

            modelBuilder.Entity<EquipmentTypeEntity>().Property(x => x.Id)
                .HasField("id").UsePropertyAccessMode(PropertyAccessMode.Field);

            modelBuilder.Entity<EquipmentTypeEntity>().Property(x => x.Value)
                .HasField("type").UsePropertyAccessMode(PropertyAccessMode.Field);
        }

        private void SeedingEquipmentTypeEntity(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<EquipmentTypeEntity>().HasData(
                new EquipmentTypeEntity(1, EquipmentType.Band),
                new EquipmentTypeEntity(2, EquipmentType.Barbell),
                new EquipmentTypeEntity(3, EquipmentType.Bench),
                new EquipmentTypeEntity(4, EquipmentType.Box),
                new EquipmentTypeEntity(5, EquipmentType.Cable),
                new EquipmentTypeEntity(6, EquipmentType.Dumbbell),
                new EquipmentTypeEntity(7, EquipmentType.Kettlebell),
                new EquipmentTypeEntity(8, EquipmentType.Nothing),
                new EquipmentTypeEntity(9, EquipmentType.Plate),
                new EquipmentTypeEntity(10, EquipmentType.Ez_barbell),
                new EquipmentTypeEntity(11, EquipmentType.Smith_machine),
                new EquipmentTypeEntity(12, EquipmentType.Ankle_strap),
                new EquipmentTypeEntity(13, EquipmentType.Adductor_machine),
                new EquipmentTypeEntity(14, EquipmentType.Hand_grip),
                new EquipmentTypeEntity(15, EquipmentType.Scott_bench),
                new EquipmentTypeEntity(16, EquipmentType.Curl_bar),
                new EquipmentTypeEntity(17, EquipmentType.Straight_bar),
                new EquipmentTypeEntity(18, EquipmentType.Decline_bench),
                new EquipmentTypeEntity(19, EquipmentType.Abs_machine),
                new EquipmentTypeEntity(20, EquipmentType.Step_pad),
                new EquipmentTypeEntity(21, EquipmentType.Calf_raise_machine),
                new EquipmentTypeEntity(22, EquipmentType.Dip_frame),
                new EquipmentTypeEntity(23, EquipmentType.Chest_press_machine),
                new EquipmentTypeEntity(24, EquipmentType.Chest_fly_machine),
                new EquipmentTypeEntity(25, EquipmentType.Tricep_rope),
                new EquipmentTypeEntity(26, EquipmentType.Shoulder_press_machine),
                new EquipmentTypeEntity(27, EquipmentType.Rope),
                new EquipmentTypeEntity(28, EquipmentType.Reverse_hyper_machine),
                new EquipmentTypeEntity(29, EquipmentType.Pullup_frame),
                new EquipmentTypeEntity(30, EquipmentType.Pulldown_machine),
                new EquipmentTypeEntity(31, EquipmentType.Ring),
                new EquipmentTypeEntity(32, EquipmentType.Rowing_machine),
                new EquipmentTypeEntity(33, EquipmentType.Hyperextension_bench),
                new EquipmentTypeEntity(34, EquipmentType.Leg_press_machine),
                new EquipmentTypeEntity(35, EquipmentType.Dip_machine),
                new EquipmentTypeEntity(36, EquipmentType.Ball),
                new EquipmentTypeEntity(37, EquipmentType.Roller));
        }

        private void SeedingSecondaryEquipments(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Exercise>().HasMany(x => x.SecondaryEquipments).WithMany(x => x.EquipmentsSec)
                .UsingEntity(x => x.HasData(
                    new { EquipmentsSecId = 1, SecondaryEquipmentsId = 6 },
                    new { EquipmentsSecId = 1, SecondaryEquipmentsId = 9 },
                    new { EquipmentsSecId = 2, SecondaryEquipmentsId = 6 },
                    new { EquipmentsSecId = 2, SecondaryEquipmentsId = 9 },
                    new { EquipmentsSecId = 3, SecondaryEquipmentsId = 1 },
                    new { EquipmentsSecId = 4, SecondaryEquipmentsId = 8 },
                    new { EquipmentsSecId = 5, SecondaryEquipmentsId = 3 },
                    new { EquipmentsSecId = 6, SecondaryEquipmentsId = 6 },
                    new { EquipmentsSecId = 7, SecondaryEquipmentsId = 8 },
                    new { EquipmentsSecId = 8, SecondaryEquipmentsId = 8 },
                    new { EquipmentsSecId = 9, SecondaryEquipmentsId = 8 },
                    new { EquipmentsSecId = 10, SecondaryEquipmentsId = 8 },
                    new { EquipmentsSecId = 11, SecondaryEquipmentsId = 8 },
                    new { EquipmentsSecId = 12, SecondaryEquipmentsId = 8 },
                    new { EquipmentsSecId = 13, SecondaryEquipmentsId = 8 },
                    new { EquipmentsSecId = 14, SecondaryEquipmentsId = 8 },
                    new { EquipmentsSecId = 15, SecondaryEquipmentsId = 8 },
                    new { EquipmentsSecId = 16, SecondaryEquipmentsId = 8 },
                    new { EquipmentsSecId = 17, SecondaryEquipmentsId = 8 },
                    new { EquipmentsSecId = 18, SecondaryEquipmentsId = 6 },
                    new { EquipmentsSecId = 18, SecondaryEquipmentsId = 2 },
                    new { EquipmentsSecId = 19, SecondaryEquipmentsId = 8 },
                    new { EquipmentsSecId = 20, SecondaryEquipmentsId = 8 },
                    new { EquipmentsSecId = 21, SecondaryEquipmentsId = 8 },
                    new { EquipmentsSecId = 22, SecondaryEquipmentsId = 8 },
                    new { EquipmentsSecId = 23, SecondaryEquipmentsId = 8 },
                    new { EquipmentsSecId = 24, SecondaryEquipmentsId = 8 },
                    new { EquipmentsSecId = 25, SecondaryEquipmentsId = 8 },
                    new { EquipmentsSecId = 26, SecondaryEquipmentsId = 8 },
                    new { EquipmentsSecId = 27, SecondaryEquipmentsId = 8 },
                    new { EquipmentsSecId = 28, SecondaryEquipmentsId = 8 },
                    new { EquipmentsSecId = 29, SecondaryEquipmentsId = 8 },
                    new { EquipmentsSecId = 30, SecondaryEquipmentsId = 8 },
                    new { EquipmentsSecId = 31, SecondaryEquipmentsId = 8 },
                    new { EquipmentsSecId = 32, SecondaryEquipmentsId = 9 },
                    new { EquipmentsSecId = 33, SecondaryEquipmentsId = 6 },
                    new { EquipmentsSecId = 34, SecondaryEquipmentsId = 8 },
                    new { EquipmentsSecId = 35, SecondaryEquipmentsId = 8 },
                    new { EquipmentsSecId = 36, SecondaryEquipmentsId = 8 },
                    new { EquipmentsSecId = 37, SecondaryEquipmentsId = 3 },
                    new { EquipmentsSecId = 37, SecondaryEquipmentsId = 4 },
                    new { EquipmentsSecId = 38, SecondaryEquipmentsId = 8 },
                    new { EquipmentsSecId = 39, SecondaryEquipmentsId = 8 },
                    new { EquipmentsSecId = 40, SecondaryEquipmentsId = 8 },
                    new { EquipmentsSecId = 41, SecondaryEquipmentsId = 8 },
                    new { EquipmentsSecId = 42, SecondaryEquipmentsId = 8 },
                    new { EquipmentsSecId = 43, SecondaryEquipmentsId = 8 },
                    new { EquipmentsSecId = 44, SecondaryEquipmentsId = 9 },
                    new { EquipmentsSecId = 45, SecondaryEquipmentsId = 8 },
                    new { EquipmentsSecId = 46, SecondaryEquipmentsId = 8 },
                    new { EquipmentsSecId = 47, SecondaryEquipmentsId = 8 },
                    new { EquipmentsSecId = 48, SecondaryEquipmentsId = 8 },
                    new { EquipmentsSecId = 49, SecondaryEquipmentsId = 8 },
                    new { EquipmentsSecId = 50, SecondaryEquipmentsId = 8 },
                    new { EquipmentsSecId = 51, SecondaryEquipmentsId = 8 },
                    new { EquipmentsSecId = 52, SecondaryEquipmentsId = 8 },
                    new { EquipmentsSecId = 53, SecondaryEquipmentsId = 8 },
                    new { EquipmentsSecId = 54, SecondaryEquipmentsId = 8 },
                    new { EquipmentsSecId = 55, SecondaryEquipmentsId = 8 },
                    new { EquipmentsSecId = 56, SecondaryEquipmentsId = 8 },
                    new { EquipmentsSecId = 57, SecondaryEquipmentsId = 8 },
                    new { EquipmentsSecId = 58, SecondaryEquipmentsId = 4 },
                    new { EquipmentsSecId = 58, SecondaryEquipmentsId = 11 },
                    new { EquipmentsSecId = 59, SecondaryEquipmentsId = 8 },
                    new { EquipmentsSecId = 60, SecondaryEquipmentsId = 8 },
                    new { EquipmentsSecId = 61, SecondaryEquipmentsId = 6 },
                    new { EquipmentsSecId = 62, SecondaryEquipmentsId = 8 },
                    new { EquipmentsSecId = 63, SecondaryEquipmentsId = 8 },
                    new { EquipmentsSecId = 64, SecondaryEquipmentsId = 8 },
                    new { EquipmentsSecId = 65, SecondaryEquipmentsId = 6 },
                    new { EquipmentsSecId = 66, SecondaryEquipmentsId = 10 },
                    new { EquipmentsSecId = 67, SecondaryEquipmentsId = 10 },
                    new { EquipmentsSecId = 67, SecondaryEquipmentsId = 2 },
                    new { EquipmentsSecId = 68, SecondaryEquipmentsId = 9 },
                    new { EquipmentsSecId = 69, SecondaryEquipmentsId = 8 },
                    new { EquipmentsSecId = 70, SecondaryEquipmentsId = 8 },
                    new { EquipmentsSecId = 71, SecondaryEquipmentsId = 8 },
                    new { EquipmentsSecId = 72, SecondaryEquipmentsId = 8 },
                    new { EquipmentsSecId = 73, SecondaryEquipmentsId = 8 },
                    new { EquipmentsSecId = 74, SecondaryEquipmentsId = 8 },
                    new { EquipmentsSecId = 75, SecondaryEquipmentsId = 8 },
                    new { EquipmentsSecId = 76, SecondaryEquipmentsId = 8 },
                    new { EquipmentsSecId = 77, SecondaryEquipmentsId = 8 },
                    new { EquipmentsSecId = 78, SecondaryEquipmentsId = 8 },
                    new { EquipmentsSecId = 79, SecondaryEquipmentsId = 8 },
                    new { EquipmentsSecId = 80, SecondaryEquipmentsId = 8 },
                    new { EquipmentsSecId = 81, SecondaryEquipmentsId = 8 },
                    new { EquipmentsSecId = 82, SecondaryEquipmentsId = 9 },
                    new { EquipmentsSecId = 83, SecondaryEquipmentsId = 8 },
                    new { EquipmentsSecId = 84, SecondaryEquipmentsId = 8 },
                    new { EquipmentsSecId = 85, SecondaryEquipmentsId = 8 },
                    new { EquipmentsSecId = 86, SecondaryEquipmentsId = 8 },
                    new { EquipmentsSecId = 87, SecondaryEquipmentsId = 8 },
                    new { EquipmentsSecId = 88, SecondaryEquipmentsId = 11 },
                    new { EquipmentsSecId = 89, SecondaryEquipmentsId = 11 },
                    new { EquipmentsSecId = 90, SecondaryEquipmentsId = 8 },
                    new { EquipmentsSecId = 91, SecondaryEquipmentsId = 8 },
                    new { EquipmentsSecId = 92, SecondaryEquipmentsId = 6 },
                    new { EquipmentsSecId = 93, SecondaryEquipmentsId = 8 },
                    new { EquipmentsSecId = 94, SecondaryEquipmentsId = 8 },
                    new { EquipmentsSecId = 95, SecondaryEquipmentsId = 8 },
                    new { EquipmentsSecId = 96, SecondaryEquipmentsId = 8 },
                    new { EquipmentsSecId = 97, SecondaryEquipmentsId = 4 },
                    new { EquipmentsSecId = 98, SecondaryEquipmentsId = 8 },
                    new { EquipmentsSecId = 99, SecondaryEquipmentsId = 8 },
                    new { EquipmentsSecId = 100, SecondaryEquipmentsId = 8 },
                    new { EquipmentsSecId = 101, SecondaryEquipmentsId = 8 },
                    new { EquipmentsSecId = 102, SecondaryEquipmentsId = 8 },
                    new { EquipmentsSecId = 103, SecondaryEquipmentsId = 8 },
                    new { EquipmentsSecId = 104, SecondaryEquipmentsId = 8 },
                    new { EquipmentsSecId = 105, SecondaryEquipmentsId = 8 },
                    new { EquipmentsSecId = 106, SecondaryEquipmentsId = 8 },
                    new { EquipmentsSecId = 107, SecondaryEquipmentsId = 8 },
                    new { EquipmentsSecId = 108, SecondaryEquipmentsId = 8 },
                    new { EquipmentsSecId = 109, SecondaryEquipmentsId = 3 },
                    new { EquipmentsSecId = 110, SecondaryEquipmentsId = 6 },
                    new { EquipmentsSecId = 111, SecondaryEquipmentsId = 6 },
                    new { EquipmentsSecId = 112, SecondaryEquipmentsId = 7 },
                    new { EquipmentsSecId = 113, SecondaryEquipmentsId = 9 },
                    new { EquipmentsSecId = 114, SecondaryEquipmentsId = 3 },
                    new { EquipmentsSecId = 115, SecondaryEquipmentsId = 9 },
                    new { EquipmentsSecId = 116, SecondaryEquipmentsId = 9 },
                    new { EquipmentsSecId = 117, SecondaryEquipmentsId = 9 },
                    new { EquipmentsSecId = 118, SecondaryEquipmentsId = 10 },
                    new { EquipmentsSecId = 119, SecondaryEquipmentsId = 9 },
                    new { EquipmentsSecId = 120, SecondaryEquipmentsId = 8 },
                    new { EquipmentsSecId = 121, SecondaryEquipmentsId = 9 },
                    new { EquipmentsSecId = 122, SecondaryEquipmentsId = 8 },
                    new { EquipmentsSecId = 123, SecondaryEquipmentsId = 8 },
                    new { EquipmentsSecId = 124, SecondaryEquipmentsId = 8 },
                    new { EquipmentsSecId = 125, SecondaryEquipmentsId = 8 },
                    new { EquipmentsSecId = 126, SecondaryEquipmentsId = 8 },
                    new { EquipmentsSecId = 127, SecondaryEquipmentsId = 8 },
                    new { EquipmentsSecId = 128, SecondaryEquipmentsId = 4 },
                    new { EquipmentsSecId = 129, SecondaryEquipmentsId = 8 },
                    new { EquipmentsSecId = 130, SecondaryEquipmentsId = 8 },
                    new { EquipmentsSecId = 131, SecondaryEquipmentsId = 8 },
                    new { EquipmentsSecId = 132, SecondaryEquipmentsId = 8 },
                    new { EquipmentsSecId = 133, SecondaryEquipmentsId = 8 },
                    new { EquipmentsSecId = 134, SecondaryEquipmentsId = 8 },
                    new { EquipmentsSecId = 135, SecondaryEquipmentsId = 8 },
                    new { EquipmentsSecId = 136, SecondaryEquipmentsId = 8 },
                    new { EquipmentsSecId = 137, SecondaryEquipmentsId = 8 },
                    new { EquipmentsSecId = 138, SecondaryEquipmentsId = 8 },
                    new { EquipmentsSecId = 139, SecondaryEquipmentsId = 8 },
                    new { EquipmentsSecId = 140, SecondaryEquipmentsId = 8 },
                    new { EquipmentsSecId = 141, SecondaryEquipmentsId = 8 },
                    new { EquipmentsSecId = 142, SecondaryEquipmentsId = 8 },
                    new { EquipmentsSecId = 143, SecondaryEquipmentsId = 8 },
                    new { EquipmentsSecId = 144, SecondaryEquipmentsId = 8 },
                    new { EquipmentsSecId = 145, SecondaryEquipmentsId = 36 },
                    new { EquipmentsSecId = 146, SecondaryEquipmentsId = 8 },
                    new { EquipmentsSecId = 147, SecondaryEquipmentsId = 8 },
                    new { EquipmentsSecId = 148, SecondaryEquipmentsId = 8 },
                    new { EquipmentsSecId = 149, SecondaryEquipmentsId = 8 },
                    new { EquipmentsSecId = 150, SecondaryEquipmentsId = 8 },
                    new { EquipmentsSecId = 151, SecondaryEquipmentsId = 8 },
                    new { EquipmentsSecId = 152, SecondaryEquipmentsId = 8 },
                    new { EquipmentsSecId = 153, SecondaryEquipmentsId = 8 },
                    new { EquipmentsSecId = 154, SecondaryEquipmentsId = 8 },
                    new { EquipmentsSecId = 155, SecondaryEquipmentsId = 8 },
                    new { EquipmentsSecId = 156, SecondaryEquipmentsId = 8 },
                    new { EquipmentsSecId = 157, SecondaryEquipmentsId = 8 },
                    new { EquipmentsSecId = 158, SecondaryEquipmentsId = 8 },
                    new { EquipmentsSecId = 159, SecondaryEquipmentsId = 8 },
                    new { EquipmentsSecId = 160, SecondaryEquipmentsId = 8 },
                    new { EquipmentsSecId = 161, SecondaryEquipmentsId = 8 }));
        }

        private void SeedingPrimaryEquipments(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Exercise>().HasMany(x => x.PrimaryEquipments).WithMany(x => x.EquipmentsPrim)
                .UsingEntity(x => x.HasData(
                    new { EquipmentsPrimId = 1, PrimaryEquipmentsId = 2 },
                    new { EquipmentsPrimId = 2, PrimaryEquipmentsId = 2 },
                    new { EquipmentsPrimId = 2, PrimaryEquipmentsId = 3 },
                    new { EquipmentsPrimId = 3, PrimaryEquipmentsId = 5 },
                    new { EquipmentsPrimId = 3, PrimaryEquipmentsId = 12 },
                    new { EquipmentsPrimId = 4, PrimaryEquipmentsId = 8 },
                    new { EquipmentsPrimId = 5, PrimaryEquipmentsId = 4 },
                    new { EquipmentsPrimId = 6, PrimaryEquipmentsId = 7 },
                    new { EquipmentsPrimId = 7, PrimaryEquipmentsId = 2 },
                    new { EquipmentsPrimId = 8, PrimaryEquipmentsId = 3 },
                    new { EquipmentsPrimId = 9, PrimaryEquipmentsId = 8 },
                    new { EquipmentsPrimId = 10, PrimaryEquipmentsId = 8 },
                    new { EquipmentsPrimId = 11, PrimaryEquipmentsId = 8 },
                    new { EquipmentsPrimId = 12, PrimaryEquipmentsId = 8 },
                    new { EquipmentsPrimId = 13, PrimaryEquipmentsId = 8 },
                    new { EquipmentsPrimId = 14, PrimaryEquipmentsId = 13 },
                    new { EquipmentsPrimId = 15, PrimaryEquipmentsId = 1 },
                    new { EquipmentsPrimId = 16, PrimaryEquipmentsId = 8 },
                    new { EquipmentsPrimId = 17, PrimaryEquipmentsId = 2 },
                    new { EquipmentsPrimId = 18, PrimaryEquipmentsId = 10 },
                    new { EquipmentsPrimId = 18, PrimaryEquipmentsId = 3 },
                    new { EquipmentsPrimId = 19, PrimaryEquipmentsId = 5 },
                    new { EquipmentsPrimId = 19, PrimaryEquipmentsId = 14 },
                    new { EquipmentsPrimId = 20, PrimaryEquipmentsId = 10 },
                    new { EquipmentsPrimId = 21, PrimaryEquipmentsId = 6 },
                    new { EquipmentsPrimId = 22, PrimaryEquipmentsId = 6 },
                    new { EquipmentsPrimId = 22, PrimaryEquipmentsId = 15 },
                    new { EquipmentsPrimId = 23, PrimaryEquipmentsId = 5 },
                    new { EquipmentsPrimId = 23, PrimaryEquipmentsId = 16 },
                    new { EquipmentsPrimId = 24, PrimaryEquipmentsId = 5 },
                    new { EquipmentsPrimId = 24, PrimaryEquipmentsId = 17 },
                    new { EquipmentsPrimId = 25, PrimaryEquipmentsId = 8 },
                    new { EquipmentsPrimId = 26, PrimaryEquipmentsId = 18 },
                    new { EquipmentsPrimId = 27, PrimaryEquipmentsId = 8 },
                    new { EquipmentsPrimId = 28, PrimaryEquipmentsId = 2 },
                    new { EquipmentsPrimId = 29, PrimaryEquipmentsId = 8 },
                    new { EquipmentsPrimId = 30, PrimaryEquipmentsId = 8 },
                    new { EquipmentsPrimId = 31, PrimaryEquipmentsId = 19 },
                    new { EquipmentsPrimId = 32, PrimaryEquipmentsId = 6 },
                    new { EquipmentsPrimId = 33, PrimaryEquipmentsId = 9 },
                    new { EquipmentsPrimId = 34, PrimaryEquipmentsId = 18 },
                    new { EquipmentsPrimId = 35, PrimaryEquipmentsId = 8 },
                    new { EquipmentsPrimId = 36, PrimaryEquipmentsId = 8 },
                    new { EquipmentsPrimId = 37, PrimaryEquipmentsId = 20 },
                    new { EquipmentsPrimId = 38, PrimaryEquipmentsId = 8 },
                    new { EquipmentsPrimId = 39, PrimaryEquipmentsId = 20 },
                    new { EquipmentsPrimId = 40, PrimaryEquipmentsId = 21 },
                    new { EquipmentsPrimId = 41, PrimaryEquipmentsId = 2 },
                    new { EquipmentsPrimId = 42, PrimaryEquipmentsId = 20 },
                    new { EquipmentsPrimId = 43, PrimaryEquipmentsId = 11 },
                    new { EquipmentsPrimId = 43, PrimaryEquipmentsId = 20 },
                    new { EquipmentsPrimId = 44, PrimaryEquipmentsId = 3 },
                    new { EquipmentsPrimId = 44, PrimaryEquipmentsId = 6 },
                    new { EquipmentsPrimId = 44, PrimaryEquipmentsId = 20 },
                    new { EquipmentsPrimId = 45, PrimaryEquipmentsId = 8 },
                    new { EquipmentsPrimId = 46, PrimaryEquipmentsId = 1 },
                    new { EquipmentsPrimId = 47, PrimaryEquipmentsId = 3 },
                    new { EquipmentsPrimId = 47, PrimaryEquipmentsId = 6 },
                    new { EquipmentsPrimId = 48, PrimaryEquipmentsId = 8 },
                    new { EquipmentsPrimId = 49, PrimaryEquipmentsId = 2 },
                    new { EquipmentsPrimId = 49, PrimaryEquipmentsId = 3 },
                    new { EquipmentsPrimId = 50, PrimaryEquipmentsId = 2 },
                    new { EquipmentsPrimId = 50, PrimaryEquipmentsId = 3 },
                    new { EquipmentsPrimId = 51, PrimaryEquipmentsId = 3 },
                    new { EquipmentsPrimId = 51, PrimaryEquipmentsId = 6 },
                    new { EquipmentsPrimId = 52, PrimaryEquipmentsId = 5 },
                    new { EquipmentsPrimId = 52, PrimaryEquipmentsId = 14 },
                    new { EquipmentsPrimId = 53, PrimaryEquipmentsId = 2 },
                    new { EquipmentsPrimId = 53, PrimaryEquipmentsId = 3 },
                    new { EquipmentsPrimId = 54, PrimaryEquipmentsId = 22 },
                    new { EquipmentsPrimId = 55, PrimaryEquipmentsId = 6 },
                    new { EquipmentsPrimId = 55, PrimaryEquipmentsId = 18 },
                    new { EquipmentsPrimId = 56, PrimaryEquipmentsId = 2 },
                    new { EquipmentsPrimId = 56, PrimaryEquipmentsId = 18 },
                    new { EquipmentsPrimId = 57, PrimaryEquipmentsId = 2 },
                    new { EquipmentsPrimId = 57, PrimaryEquipmentsId = 3 },
                    new { EquipmentsPrimId = 58, PrimaryEquipmentsId = 3 },
                    new { EquipmentsPrimId = 59, PrimaryEquipmentsId = 23 },
                    new { EquipmentsPrimId = 59, PrimaryEquipmentsId = 9 },
                    new { EquipmentsPrimId = 60, PrimaryEquipmentsId = 24 },
                    new { EquipmentsPrimId = 61, PrimaryEquipmentsId = 3 },
                    new { EquipmentsPrimId = 61, PrimaryEquipmentsId = 2 },
                    new { EquipmentsPrimId = 62, PrimaryEquipmentsId = 3 },
                    new { EquipmentsPrimId = 62, PrimaryEquipmentsId = 6 },
                    new { EquipmentsPrimId = 63, PrimaryEquipmentsId = 6 },
                    new { EquipmentsPrimId = 63, PrimaryEquipmentsId = 15 },
                    new { EquipmentsPrimId = 64, PrimaryEquipmentsId = 5 },
                    new { EquipmentsPrimId = 64, PrimaryEquipmentsId = 25 },
                    new { EquipmentsPrimId = 65, PrimaryEquipmentsId = 7 },
                    new { EquipmentsPrimId = 66, PrimaryEquipmentsId = 2 },
                    new { EquipmentsPrimId = 67, PrimaryEquipmentsId = 3 },
                    new { EquipmentsPrimId = 67, PrimaryEquipmentsId = 6 },
                    new { EquipmentsPrimId = 68, PrimaryEquipmentsId = 6 },
                    new { EquipmentsPrimId = 69, PrimaryEquipmentsId = 9 },
                    new { EquipmentsPrimId = 70, PrimaryEquipmentsId = 11 },
                    new { EquipmentsPrimId = 70, PrimaryEquipmentsId = 3 },
                    new { EquipmentsPrimId = 70, PrimaryEquipmentsId = 9 },
                    new { EquipmentsPrimId = 71, PrimaryEquipmentsId = 26 },
                    new { EquipmentsPrimId = 72, PrimaryEquipmentsId = 5 },
                    new { EquipmentsPrimId = 72, PrimaryEquipmentsId = 14 },
                    new { EquipmentsPrimId = 73, PrimaryEquipmentsId = 27 },
                    new { EquipmentsPrimId = 74, PrimaryEquipmentsId = 8 },
                    new { EquipmentsPrimId = 75, PrimaryEquipmentsId = 8 },
                    new { EquipmentsPrimId = 76, PrimaryEquipmentsId = 8 },
                    new { EquipmentsPrimId = 77, PrimaryEquipmentsId = 8 },
                    new { EquipmentsPrimId = 78, PrimaryEquipmentsId = 8 },
                    new { EquipmentsPrimId = 79, PrimaryEquipmentsId = 8 },
                    new { EquipmentsPrimId = 80, PrimaryEquipmentsId = 8 },
                    new { EquipmentsPrimId = 81, PrimaryEquipmentsId = 2 },
                    new { EquipmentsPrimId = 82, PrimaryEquipmentsId = 6 },
                    new { EquipmentsPrimId = 83, PrimaryEquipmentsId = 28 },
                    new { EquipmentsPrimId = 84, PrimaryEquipmentsId = 7 },
                    new { EquipmentsPrimId = 85, PrimaryEquipmentsId = 8 },
                    new { EquipmentsPrimId = 86, PrimaryEquipmentsId = 8 },
                    new { EquipmentsPrimId = 87, PrimaryEquipmentsId = 8 },
                    new { EquipmentsPrimId = 88, PrimaryEquipmentsId = 29 },
                    new { EquipmentsPrimId = 89, PrimaryEquipmentsId = 29 },
                    new { EquipmentsPrimId = 90, PrimaryEquipmentsId = 30 },
                    new { EquipmentsPrimId = 91, PrimaryEquipmentsId = 31 },
                    new { EquipmentsPrimId = 92, PrimaryEquipmentsId = 5 },
                    new { EquipmentsPrimId = 92, PrimaryEquipmentsId = 14 },
                    new { EquipmentsPrimId = 93, PrimaryEquipmentsId = 30 },
                    new { EquipmentsPrimId = 94, PrimaryEquipmentsId = 30 },
                    new { EquipmentsPrimId = 95, PrimaryEquipmentsId = 32 },
                    new { EquipmentsPrimId = 96, PrimaryEquipmentsId = 5 },
                    new { EquipmentsPrimId = 96, PrimaryEquipmentsId = 17 },
                    new { EquipmentsPrimId = 97, PrimaryEquipmentsId = 5 },
                    new { EquipmentsPrimId = 97, PrimaryEquipmentsId = 14 },
                    new { EquipmentsPrimId = 97, PrimaryEquipmentsId = 3 },
                    new { EquipmentsPrimId = 98, PrimaryEquipmentsId = 3 },
                    new { EquipmentsPrimId = 98, PrimaryEquipmentsId = 2 },
                    new { EquipmentsPrimId = 99, PrimaryEquipmentsId = 8 },
                    new { EquipmentsPrimId = 100, PrimaryEquipmentsId = 8 },
                    new { EquipmentsPrimId = 101, PrimaryEquipmentsId = 33 },
                    new { EquipmentsPrimId = 102, PrimaryEquipmentsId = 2 },
                    new { EquipmentsPrimId = 102, PrimaryEquipmentsId = 9 },
                    new { EquipmentsPrimId = 103, PrimaryEquipmentsId = 8 },
                    new { EquipmentsPrimId = 104, PrimaryEquipmentsId = 8 },
                    new { EquipmentsPrimId = 105, PrimaryEquipmentsId = 1 },
                    new { EquipmentsPrimId = 106, PrimaryEquipmentsId = 8 },
                    new { EquipmentsPrimId = 107, PrimaryEquipmentsId = 34 },
                    new { EquipmentsPrimId = 107, PrimaryEquipmentsId = 9 },
                    new { EquipmentsPrimId = 108, PrimaryEquipmentsId = 2 },
                    new { EquipmentsPrimId = 109, PrimaryEquipmentsId = 2 },
                    new { EquipmentsPrimId = 109, PrimaryEquipmentsId = 4 },
                    new { EquipmentsPrimId = 110, PrimaryEquipmentsId = 2 },
                    new { EquipmentsPrimId = 111, PrimaryEquipmentsId = 7 },
                    new { EquipmentsPrimId = 112, PrimaryEquipmentsId = 6 },
                    new { EquipmentsPrimId = 113, PrimaryEquipmentsId = 6 },
                    new { EquipmentsPrimId = 113, PrimaryEquipmentsId = 3 },
                    new { EquipmentsPrimId = 114, PrimaryEquipmentsId = 5 },
                    new { EquipmentsPrimId = 114, PrimaryEquipmentsId = 25 },
                    new { EquipmentsPrimId = 115, PrimaryEquipmentsId = 6 },
                    new { EquipmentsPrimId = 116, PrimaryEquipmentsId = 6 },
                    new { EquipmentsPrimId = 117, PrimaryEquipmentsId = 6 },
                    new { EquipmentsPrimId = 117, PrimaryEquipmentsId = 3 },
                    new { EquipmentsPrimId = 118, PrimaryEquipmentsId = 2 },
                    new { EquipmentsPrimId = 119, PrimaryEquipmentsId = 6 },
                    new { EquipmentsPrimId = 120, PrimaryEquipmentsId = 11 },
                    new { EquipmentsPrimId = 120, PrimaryEquipmentsId = 9 },
                    new { EquipmentsPrimId = 121, PrimaryEquipmentsId = 6 },
                    new { EquipmentsPrimId = 122, PrimaryEquipmentsId = 2 },
                    new { EquipmentsPrimId = 123, PrimaryEquipmentsId = 8 },
                    new { EquipmentsPrimId = 124, PrimaryEquipmentsId = 8 },
                    new { EquipmentsPrimId = 125, PrimaryEquipmentsId = 10 },
                    new { EquipmentsPrimId = 126, PrimaryEquipmentsId = 6 },
                    new { EquipmentsPrimId = 127, PrimaryEquipmentsId = 5 },
                    new { EquipmentsPrimId = 127, PrimaryEquipmentsId = 17 },
                    new { EquipmentsPrimId = 128, PrimaryEquipmentsId = 3 },
                    new { EquipmentsPrimId = 129, PrimaryEquipmentsId = 5 },
                    new { EquipmentsPrimId = 129, PrimaryEquipmentsId = 17 },
                    new { EquipmentsPrimId = 130, PrimaryEquipmentsId = 5 },
                    new { EquipmentsPrimId = 130, PrimaryEquipmentsId = 25 },
                    new { EquipmentsPrimId = 131, PrimaryEquipmentsId = 5 },
                    new { EquipmentsPrimId = 131, PrimaryEquipmentsId = 25 },
                    new { EquipmentsPrimId = 132, PrimaryEquipmentsId = 3 },
                    new { EquipmentsPrimId = 132, PrimaryEquipmentsId = 6 },
                    new { EquipmentsPrimId = 133, PrimaryEquipmentsId = 6 },
                    new { EquipmentsPrimId = 134, PrimaryEquipmentsId = 35 },
                    new { EquipmentsPrimId = 135, PrimaryEquipmentsId = 8 },
                    new { EquipmentsPrimId = 136, PrimaryEquipmentsId = 2 },
                    new { EquipmentsPrimId = 137, PrimaryEquipmentsId = 3 },
                    new { EquipmentsPrimId = 137, PrimaryEquipmentsId = 6 },
                    new { EquipmentsPrimId = 138, PrimaryEquipmentsId = 2 },
                    new { EquipmentsPrimId = 138, PrimaryEquipmentsId = 9 },
                    new { EquipmentsPrimId = 139, PrimaryEquipmentsId = 3 },
                    new { EquipmentsPrimId = 139, PrimaryEquipmentsId = 6 },
                    new { EquipmentsPrimId = 140, PrimaryEquipmentsId = 3 },
                    new { EquipmentsPrimId = 140, PrimaryEquipmentsId = 5 },
                    new { EquipmentsPrimId = 140, PrimaryEquipmentsId = 14 },
                    new { EquipmentsPrimId = 141, PrimaryEquipmentsId = 8 },
                    new { EquipmentsPrimId = 142, PrimaryEquipmentsId = 37 },
                    new { EquipmentsPrimId = 143, PrimaryEquipmentsId = 8 },
                    new { EquipmentsPrimId = 144, PrimaryEquipmentsId = 8 },
                    new { EquipmentsPrimId = 145, PrimaryEquipmentsId = 8 },
                    new { EquipmentsPrimId = 146, PrimaryEquipmentsId = 8 },
                    new { EquipmentsPrimId = 147, PrimaryEquipmentsId = 8 },
                    new { EquipmentsPrimId = 148, PrimaryEquipmentsId = 8 },
                    new { EquipmentsPrimId = 149, PrimaryEquipmentsId = 8 },
                    new { EquipmentsPrimId = 150, PrimaryEquipmentsId = 8 },
                    new { EquipmentsPrimId = 151, PrimaryEquipmentsId = 8 },
                    new { EquipmentsPrimId = 152, PrimaryEquipmentsId = 8 },
                    new { EquipmentsPrimId = 153, PrimaryEquipmentsId = 8 },
                    new { EquipmentsPrimId = 154, PrimaryEquipmentsId = 8 },
                    new { EquipmentsPrimId = 155, PrimaryEquipmentsId = 8 },
                    new { EquipmentsPrimId = 156, PrimaryEquipmentsId = 8 },
                    new { EquipmentsPrimId = 157, PrimaryEquipmentsId = 8 },
                    new { EquipmentsPrimId = 158, PrimaryEquipmentsId = 2 },
                    new { EquipmentsPrimId = 159, PrimaryEquipmentsId = 8 },
                    new { EquipmentsPrimId = 160, PrimaryEquipmentsId = 8 },
                    new { EquipmentsPrimId = 161, PrimaryEquipmentsId = 8 }));
        }

        private void DescriptingUser(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<User>()
                .HasOne(x => x.Nutrients)
                .WithOne(x => x.User)
                .HasForeignKey<NutritionScheme>(x => x.UserEntityId)
                .OnDelete(DeleteBehavior.Cascade);

            modelBuilder.Entity<User>()
                .HasOne(x => x.TrainingPlan)
                .WithOne(x => x.User)
                .HasForeignKey<TrainingPlan>(x => x.UserId)
                .OnDelete(DeleteBehavior.Cascade);

            modelBuilder.Entity<User>()
                .HasOne(x => x.WeightHistory)
                .WithOne(x => x.User)
                .HasForeignKey<WeightHistory>(x => x.UserId)
                .OnDelete(DeleteBehavior.Cascade);

            modelBuilder.Entity<User>()
                .HasOne(x => x.TimeHistory)
                .WithOne(x => x.User)
                .HasForeignKey<TimeHistory>(x => x.UserId)
                .OnDelete(DeleteBehavior.Cascade);

            modelBuilder.Entity<User>()
                .HasMany(x => x.OwnExercises)
                .WithOne(x => x.User)
                .OnDelete(DeleteBehavior.Cascade);

            modelBuilder.Entity<User>().HasKey(x => x.Id);

            modelBuilder.Entity<User>().Property(x => x.Id)
                .HasField("id").UsePropertyAccessMode(PropertyAccessMode.Field);

            modelBuilder.Entity<User>().Property(x => x.Username)
                .HasField("username").UsePropertyAccessMode(PropertyAccessMode.Field);

            modelBuilder.Entity<User>().Property(x => x.Password)
                .HasField("password").UsePropertyAccessMode(PropertyAccessMode.Field);

            modelBuilder.Entity<User>().Property(x => x.Height)
                .HasField("height").UsePropertyAccessMode(PropertyAccessMode.Field);

            modelBuilder.Entity<User>().Property(x => x.Age)
                .HasField("age").UsePropertyAccessMode(PropertyAccessMode.Field);

            modelBuilder.Entity<User>().Property(x => x.Gender)
                .HasField("gender").UsePropertyAccessMode(PropertyAccessMode.Field);

            modelBuilder.Entity<User>().Property(x => x.Avatar)
                .HasField("avatar").UsePropertyAccessMode(PropertyAccessMode.Field);
        }

        private void DescriptingWeightHistory(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<WeightHistory>().HasKey(x => x.Id);

            modelBuilder.Entity<WeightHistory>().Property(x => x.Id)
                .HasField("id").UsePropertyAccessMode(PropertyAccessMode.Field);

            modelBuilder.Entity<WeightHistory>().Property(x => x.SerializedData)
                .HasField("serializedData").UsePropertyAccessMode(PropertyAccessMode.Field);
        }

        private void DescriptingTimeHistory(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<TimeHistory>().HasKey(x => x.Id);

            modelBuilder.Entity<TimeHistory>().Property(x => x.Id)
                .HasField("id").UsePropertyAccessMode(PropertyAccessMode.Field);

            modelBuilder.Entity<TimeHistory>().Property(x => x.SerializedTimeData)
                .HasField("serializedTimeData").UsePropertyAccessMode(PropertyAccessMode.Field);

            modelBuilder.Entity<TimeHistory>().Property(x => x.SerializedTotalStack)
                .HasField("serializedTotalStack").UsePropertyAccessMode(PropertyAccessMode.Field);

            modelBuilder.Entity<TimeHistory>().Property(x => x.SerializedActiveStack)
                .HasField("serializedActiveStack").UsePropertyAccessMode(PropertyAccessMode.Field);
        }

        private void DescriptingTrainingPlan(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<TrainingPlan>()
                .HasMany(x => x.WeeklyPlans)
                .WithOne(x => x.TrainingPlan)
                .OnDelete(DeleteBehavior.Cascade);

            modelBuilder.Entity<TrainingPlan>().HasKey(x => x.Id);

            modelBuilder.Entity<TrainingPlan>().Property(x => x.Id)
                .HasField("id").UsePropertyAccessMode(PropertyAccessMode.Field);

            modelBuilder.Entity<TrainingPlan>().Property(x => x.EquipmentRequirement)
                .HasField("equipmentRequirement").UsePropertyAccessMode(PropertyAccessMode.Field);

            modelBuilder.Entity<TrainingPlan>().Property(x => x.NumberOfDays)
               .HasField("numberOfDays").UsePropertyAccessMode(PropertyAccessMode.Field);
        }

        private void DescriptingNutritionScheme(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<User>()
                .HasOne(x => x.Nutrients)
                .WithOne(x => x.User)
                .HasForeignKey<NutritionScheme>(x => x.UserEntityId)
                .OnDelete(DeleteBehavior.Cascade);

            modelBuilder.Entity<NutritionScheme>().HasKey(x => x.Id);

            modelBuilder.Entity<NutritionScheme>().Property(x => x.Id)
                .HasField("id").UsePropertyAccessMode(PropertyAccessMode.Field);

            modelBuilder.Entity<NutritionScheme>().Property(x => x.Energy)
                .HasField("energy").UsePropertyAccessMode(PropertyAccessMode.Field);

            modelBuilder.Entity<NutritionScheme>().Property(x => x.Carbohydrates)
                .HasField("carbohydrates").UsePropertyAccessMode(PropertyAccessMode.Field);

            modelBuilder.Entity<NutritionScheme>().Property(x => x.Sugar)
                .HasField("sugar").UsePropertyAccessMode(PropertyAccessMode.Field);

            modelBuilder.Entity<NutritionScheme>().Property(x => x.Protein)
                .HasField("protein").UsePropertyAccessMode(PropertyAccessMode.Field);

            modelBuilder.Entity<NutritionScheme>().Property(x => x.Fat)
                .HasField("fat").UsePropertyAccessMode(PropertyAccessMode.Field);

            modelBuilder.Entity<NutritionScheme>().Property(x => x.SaturatedFat)
                .HasField("saturatedFat").UsePropertyAccessMode(PropertyAccessMode.Field);

            modelBuilder.Entity<NutritionScheme>().Property(x => x.Salt)
                .HasField("salt").UsePropertyAccessMode(PropertyAccessMode.Field);
        }

        private void DescribingWeeklyPlan(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<WeeklyPlan>()
                .HasMany(x => x.DailyPlans)
                .WithOne(x => x.WeeklyPlan)
                .OnDelete(DeleteBehavior.Cascade);

            modelBuilder.Entity<WeeklyPlan>().HasKey(x => x.Id);

            modelBuilder.Entity<WeeklyPlan>().Property(x => x.Id)
                .HasField("id").UsePropertyAccessMode(PropertyAccessMode.Field);

            modelBuilder.Entity<WeeklyPlan>().Property(x => x.Index)
                .HasField("index").UsePropertyAccessMode(PropertyAccessMode.Field);

            modelBuilder.Entity<WeeklyPlan>().HasIndex(x => x.Index).HasDatabaseName("WeeklyPlanIndex");
        }

        private void DescriptingDailyPlan(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<DailyPlan>()
                .HasMany(x => x.Workouts)
                .WithOne(x => x.DailyPlan)
                .OnDelete(DeleteBehavior.Cascade);

            modelBuilder.Entity<DailyPlan>().HasKey(x => x.Id);

            modelBuilder.Entity<DailyPlan>().Property(x => x.Id)
                .HasField("id").UsePropertyAccessMode(PropertyAccessMode.Field);

            modelBuilder.Entity<DailyPlan>().Property(x => x.Index)
                .HasField("index").UsePropertyAccessMode(PropertyAccessMode.Field);

            modelBuilder.Entity<DailyPlan>().HasIndex(x => x.Index).HasDatabaseName("DailyPlanIndex");
        }

        private void DescriptingWorkout(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Workout>()
                .HasMany(x => x.WarmUps)
                .WithOne(x => x.WorkoutWarmup);

            modelBuilder.Entity<Workout>()
                .HasMany(x => x.Cores)
                .WithOne(x => x.WorkoutCore);

            modelBuilder.Entity<Workout>()
                .HasMany(x => x.Stretches)
                .WithOne(x => x.WorkoutStretching);

            modelBuilder.Entity<Workout>().HasKey(x => x.Id);

            modelBuilder.Entity<Workout>().Property(x => x.Id)
                .HasField("id").UsePropertyAccessMode(PropertyAccessMode.Field);

            modelBuilder.Entity<Workout>().Property(x => x.Index)
               .HasField("index").UsePropertyAccessMode(PropertyAccessMode.Field);

            modelBuilder.Entity<Workout>().HasIndex(x => x.Index).HasDatabaseName("WorkoutIndex");
        }

        private void DescriptingExerciseDetail(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<ExerciseDetail>()
                .HasOne(x => x.OwnExercise)
                .WithMany(x => x.ExerciseDetails);

            modelBuilder.Entity<ExerciseDetail>().HasKey(x => x.Id);

            modelBuilder.Entity<ExerciseDetail>().Property(x => x.Id)
                .HasField("id").UsePropertyAccessMode(PropertyAccessMode.Field);

            modelBuilder.Entity<ExerciseDetail>().Property(x => x.Index)
               .HasField("index").UsePropertyAccessMode(PropertyAccessMode.Field);
        }

        private void DescriptingOwnExercise(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<OwnExercise>()
                .HasOne(x => x.Exercise)
                .WithMany(x => x.OwnExercises)
                .OnDelete(DeleteBehavior.SetNull);

            modelBuilder.Entity<OwnExercise>().HasKey(x => x.Id);

            modelBuilder.Entity<OwnExercise>().Property(x => x.Id)
                .HasField("id").UsePropertyAccessMode(PropertyAccessMode.Field);

            modelBuilder.Entity<OwnExercise>().Property(x => x.Rating)
                .HasField("rating").UsePropertyAccessMode(PropertyAccessMode.Field);
        }

        private void DescriptingExercise(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Exercise>(entity =>
            {
                entity.HasDiscriminator<string>("exercise_class")
                    .HasValue<PerExercise>("perexercise")
                    .HasValue<RepExercise>("repexercise");
            });

            modelBuilder.Entity<Exercise>().HasKey(x => x.Id);

            modelBuilder.Entity<Exercise>().Property(x => x.Id)
                .HasField("id").UsePropertyAccessMode(PropertyAccessMode.Field);

            modelBuilder.Entity<Exercise>().Property(x => x.Name)
                .HasField("name").UsePropertyAccessMode(PropertyAccessMode.Field);

            modelBuilder.Entity<Exercise>().Property(x => x.Phase)
                .HasField("phase").UsePropertyAccessMode(PropertyAccessMode.Field);

            modelBuilder.Entity<Exercise>().Property(x => x.TargetMuscle)
                .HasField("muscle").UsePropertyAccessMode(PropertyAccessMode.Field);

            modelBuilder.Entity<Exercise>().Property(x => x.TargetMuscleGroup)
                .HasField("muscleGroup").UsePropertyAccessMode(PropertyAccessMode.Field);

            modelBuilder.Entity<Exercise>().Property(x => x.BaseRound)
                .HasField("baseRound").UsePropertyAccessMode(PropertyAccessMode.Field);

            modelBuilder.Entity<Exercise>().Property(x => x.Preference)
                .HasField("preference").UsePropertyAccessMode(PropertyAccessMode.Field);

            modelBuilder.Entity<Exercise>().Property(x => x.Visual)
                .HasField("visual").UsePropertyAccessMode(PropertyAccessMode.Field);
        }

        private void DescriptingRepExercise(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<RepExercise>().Property(x => x.BaseRep)
                .HasField("baseRepetition").UsePropertyAccessMode(PropertyAccessMode.Field);
        }

        private void DescriptingPerExercise(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<PerExercise>().Property(x => x.BaseTime)
                .HasField("baseTime").UsePropertyAccessMode(PropertyAccessMode.Field);
        }

        private void SeedingRepExercise(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<RepExercise>().HasData(
                /* 1A   glutes_warmup */
                /* 1B   glutes_core */
                new RepExercise(1, "Barbell glute bridge", PhaseType.Core, TargetType.Glutes, 4, 12, GenderPreference.Both),
                new RepExercise(2, "Barbell hip thrust", PhaseType.Core, TargetType.Glutes, 4, 12, GenderPreference.Both),
                new RepExercise(3, "Single-leg cable hip extension", PhaseType.Core, TargetType.Glutes, 4, 12, GenderPreference.Both),
                new RepExercise(4, "Glute bridge", PhaseType.Core, TargetType.Glutes, 4, 12, GenderPreference.Woman),
                new RepExercise(5, "Step-up with knee raise", PhaseType.Core, TargetType.Glutes, 4, 12, GenderPreference.Both),
                new RepExercise(6, "Kettlebell thruster", PhaseType.Core, TargetType.Glutes, 4, 12, GenderPreference.Both),
                new RepExercise(7, "Kneeling squat", PhaseType.Core, TargetType.Glutes, 4, 12, GenderPreference.Both),
                new RepExercise(8, "Flutter kicks", PhaseType.Core, TargetType.Glutes, 4, 12, GenderPreference.Woman),
                /* 1C   glutes_stretch */
                new RepExercise(9, "Knee across the body", PhaseType.Stretching, TargetType.Glutes, 1, 2, GenderPreference.Both),
                new RepExercise(10, "One knee to chest", PhaseType.Stretching, TargetType.Glutes, 1, 2, GenderPreference.Both),
                new RepExercise(11, "Lunge glute stretch", PhaseType.Stretching, TargetType.Glutes, 1, 2, GenderPreference.Both),
                /* 2A   abductors_warmup */
                new RepExercise(161, "Standing hip circles", PhaseType.WarmUp, TargetType.Abductors, 1, 30, GenderPreference.Both),
                /* 2B   abductors_core */
                new RepExercise(12, "Fire hydrant", PhaseType.Core, TargetType.Abductors, 4, 12, GenderPreference.Both),
                /* 2C   abductors_stretch */
                new RepExercise(142, "Iliotibial band SMR", PhaseType.Stretching, TargetType.Abductors, 2, 20, GenderPreference.Both),
                /* 3A   adductors_warmup */
                /* 3B   adductors_core */
                new RepExercise(14, "Thigh adductor", PhaseType.Core, TargetType.Adductors, 4, 12, GenderPreference.Both),
                new RepExercise(15, "Band hip adductions", PhaseType.Core, TargetType.Adductors, 4, 12, GenderPreference.Both),
                /* 3C   adductors_stretch */
                /* 4A   biceps_warmup */
                /* 4B   biceps_core */
                new RepExercise(17, "Wide-grip barbell curl", PhaseType.Core, TargetType.Biceps, 4, 16, GenderPreference.Both),
                new RepExercise(18, "EZ-bar spider curl", PhaseType.Core, TargetType.Biceps, 4, 16, GenderPreference.Man),
                new RepExercise(19, "Overhead cable curl", PhaseType.Core, TargetType.Biceps, 4, 12, GenderPreference.Both),
                new RepExercise(20, "Close-grip EZ-bar curl", PhaseType.Core, TargetType.Biceps, 4, 12, GenderPreference.Both),
                new RepExercise(21, "Cross-body hammer curl", PhaseType.Core, TargetType.Biceps, 4, 12, GenderPreference.Both),
                new RepExercise(22, "Single-arm dumbbell preacher curl", PhaseType.Core, TargetType.Biceps, 4, 12, GenderPreference.Both),
                new RepExercise(23, "Reverse Cable Curl", PhaseType.Core, TargetType.Biceps, 4, 12, GenderPreference.Both),
                new RepExercise(24, "Standing biceps cable curl", PhaseType.Core, TargetType.Biceps, 4, 12, GenderPreference.Both),
                /* 4C   biceps_stretch */
                new RepExercise(152, "Seated biceps stretch", PhaseType.Stretching, TargetType.Biceps, 1, 15, GenderPreference.Both),
                /* 5A   abs_warmup */
                new RepExercise(145, "Torso rotation", PhaseType.WarmUp, TargetType.Abs, 1, 20, GenderPreference.Both),
                new RepExercise(146, "Crab single-arm reach", PhaseType.WarmUp, TargetType.Abs, 2, 15, GenderPreference.Both),
                /* 5B   abs_core */
                new RepExercise(25, "Scissor kick", PhaseType.Core, TargetType.Abs, 3, 20, GenderPreference.Both),
                new RepExercise(26, "Decline reverse crunch", PhaseType.Core, TargetType.Abs, 4, 16, GenderPreference.Both),
                new RepExercise(27, "Bottoms up", PhaseType.Core, TargetType.Abs, 4, 16, GenderPreference.Both),
                new RepExercise(28, "Landmine twist", PhaseType.Core, TargetType.Abs, 4, 12, GenderPreference.Both),
                new RepExercise(29, "Toe touchers", PhaseType.Core, TargetType.Abs, 4, 12, GenderPreference.Both),
                new RepExercise(31, "Ab crunch machine", PhaseType.Core, TargetType.Abs, 4, 12, GenderPreference.Both),
                new RepExercise(32, "Weighted crunches", PhaseType.Core, TargetType.Abs, 4, 12, GenderPreference.Both),
                new RepExercise(33, "Plate twist", PhaseType.Core, TargetType.Abs, 4, 12, GenderPreference.Both),
                new RepExercise(34, "Decline crunch", PhaseType.Core, TargetType.Abs, 4, 12, GenderPreference.Both),
                new RepExercise(35, "Cross-Body Crunch", PhaseType.Core, TargetType.Abs, 4, 12, GenderPreference.Both),
                new RepExercise(36, "Cocoons", PhaseType.Core, TargetType.Abs, 4, 12, GenderPreference.Both),
                /* 5C   abs_stretch */
                new RepExercise(148, "Cat-cow stretch", PhaseType.Stretching, TargetType.Abs, 1, 30, GenderPreference.Both),
                /* 6A   calves_warmup */
                /* 6B   calves_core */
                new RepExercise(39, "Standing calf raises", PhaseType.Core, TargetType.Calves, 4, 30, GenderPreference.Both),
                new RepExercise(40, "Seated calf raise", PhaseType.Core, TargetType.Calves, 4, 30, GenderPreference.Both),
                new RepExercise(41, "Rocking standing calf raise", PhaseType.Core, TargetType.Calves, 4, 16, GenderPreference.Both),
                new RepExercise(42, "Donkey calf raise", PhaseType.Core, TargetType.Calves, 4, 20, GenderPreference.Both),
                new RepExercise(43, "Smith machine calf raise", PhaseType.Core, TargetType.Calves, 4, 20, GenderPreference.Both),
                new RepExercise(44, "Dumbbell Seated One-Leg Calf Raise", PhaseType.Core, TargetType.Calves, 4, 20, GenderPreference.Both),
                /* 6C   calves_stretch */
                /* 7A   chest_warmup */
                new RepExercise(45, "Dynamic chest stretch", PhaseType.WarmUp, TargetType.Chest, 1, 30, GenderPreference.Both),
                new RepExercise(46, "Pass-through stretch with band", PhaseType.WarmUp, TargetType.Chest, 1, 40, GenderPreference.Both),
                /* 7B   chest_core */
                new RepExercise(47, "Dumbbell bench press", PhaseType.Core, TargetType.Chest, 4, 16, GenderPreference.Both),
                new RepExercise(48, "Pushups", PhaseType.Core, TargetType.Chest, 4, 16, GenderPreference.Both),
                new RepExercise(49, "Close-grip bench press", PhaseType.Core, TargetType.Chest, 4, 16, GenderPreference.Both),
                new RepExercise(50, "Dumbbell flyes", PhaseType.Core, TargetType.Chest, 4, 16, GenderPreference.Both),
                new RepExercise(51, "Incline dumbbell bench press", PhaseType.Core, TargetType.Chest, 4, 16, GenderPreference.Both),
                new RepExercise(52, "Low-cable cross-over", PhaseType.Core, TargetType.Chest, 4, 16, GenderPreference.Both),
                new RepExercise(53, "Barbell bench press", PhaseType.Core, TargetType.Chest, 4, 16, GenderPreference.Both),
                new RepExercise(54, "Chest dip", PhaseType.Core, TargetType.Chest, 4, 16, GenderPreference.Both),
                new RepExercise(55, "Decline dumbbell flyes", PhaseType.Core, TargetType.Chest, 4, 16, GenderPreference.Both),
                new RepExercise(56, "Decline barbell bench press", PhaseType.Core, TargetType.Chest, 4, 16, GenderPreference.Both),
                new RepExercise(57, "Wide-grip bench press", PhaseType.Core, TargetType.Chest, 4, 16, GenderPreference.Both),
                new RepExercise(58, "Incline push-up", PhaseType.Core, TargetType.Chest, 4, 16, GenderPreference.Both),
                new RepExercise(59, "Leverage incline chest press", PhaseType.Core, TargetType.Chest, 4, 16, GenderPreference.Both),
                new RepExercise(60, "Butterfly", PhaseType.Core, TargetType.Chest, 4, 16, GenderPreference.Both),
                /* 7C   chest_stretch */
                new RepExercise(159, "Bent-arm wall stretch", PhaseType.Stretching, TargetType.Chest, 1, 20, GenderPreference.Both),
                /* 8A   forearm_warmup */
                /* 8B   forearm_core */
                new RepExercise(61, "Palms-down wrist curl over bench", PhaseType.Core, TargetType.Forearms, 3, 12, GenderPreference.Both),
                new RepExercise(62, "Seated one-arm bumbbell palms-up wrist curl", PhaseType.Core, TargetType.Forearms, 3, 12, GenderPreference.Both),
                new RepExercise(63, "Preacher hammer dumbbell curl", PhaseType.Core, TargetType.Forearms, 3, 12, GenderPreference.Both),
                new RepExercise(64, "Cable rope hammer curl", PhaseType.Core, TargetType.Forearms, 4, 12, GenderPreference.Both),
                /* 8C   forearm_stretch */
                new RepExercise(154, "Wrist circles", PhaseType.Stretching, TargetType.Forearms, 1, 30, GenderPreference.Both),
                /* 9A   frontdelt_warmup */
                new RepExercise(157, "Arm circle", PhaseType.WarmUp, TargetType.FrontDelt, 1, 30, GenderPreference.Both),
                new RepExercise(158, "Bar shoulder extension", PhaseType.WarmUp, TargetType.FrontDelt, 2, 20, GenderPreference.Both),
                /* 9B   frontdelt_core */
                new RepExercise(65, "Single-arm kettlebell push-press", PhaseType.Core, TargetType.FrontDelt, 4, 12, GenderPreference.Both),
                new RepExercise(66, "Military press", PhaseType.Core, TargetType.FrontDelt, 4, 16, GenderPreference.Both),
                new RepExercise(67, "Seated dumbbell press", PhaseType.Core, TargetType.FrontDelt, 4, 16, GenderPreference.Both),
                new RepExercise(68, "Overhead dumbbell front raise", PhaseType.Core, TargetType.FrontDelt, 4, 16, GenderPreference.Both),
                new RepExercise(69, "Car driver", PhaseType.Core, TargetType.FrontDelt, 4, 20, GenderPreference.Both),
                new RepExercise(70, "Smith machine shoulder press", PhaseType.Core, TargetType.FrontDelt, 4, 16, GenderPreference.Both),
                new RepExercise(71, "Machine shoulder press", PhaseType.Core, TargetType.FrontDelt, 4, 16, GenderPreference.Both),
                new RepExercise(72, "Single-arm cable front raise", PhaseType.Core, TargetType.FrontDelt, 4, 16, GenderPreference.Both),
                new RepExercise(73, "Battle ropes", PhaseType.Core, TargetType.FrontDelt, 4, 16, GenderPreference.Both),
                /* 9C   frontdelt_stretch */
                /* 10A   hamstrings_warmup */
                /* 10B   hamstrings_core */
                new RepExercise(81, "Barbell deadlift", PhaseType.Core, TargetType.Hamstrings, 4, 16, GenderPreference.Both),
                new RepExercise(82, "Romanian deadlift with dumbbells", PhaseType.Core, TargetType.Hamstrings, 4, 16, GenderPreference.Both),
                new RepExercise(83, "Lying leg curls", PhaseType.Core, TargetType.Hamstrings, 4, 16, GenderPreference.Both),
                new RepExercise(84, "Single-arm kettlebell clean", PhaseType.Core, TargetType.Hamstrings, 4, 16, GenderPreference.Both),
                new RepExercise(85, "Linear acceleration wall drill", PhaseType.Core, TargetType.Hamstrings, 4, 16, GenderPreference.Both),
                new RepExercise(86, "Knee tuck jump", PhaseType.Core, TargetType.Hamstrings, 4, 16, GenderPreference.Both),
                /* 10C   hamstrings_stretch */
                /* 11A   lats_warmup */
                new RepExercise(155, "Standing side bend stretch", PhaseType.WarmUp, TargetType.Lats, 1, 20, GenderPreference.Both),
                new RepExercise(156, "Dynamic back stretch", PhaseType.WarmUp, TargetType.Lats, 1, 30, GenderPreference.Both),
                /* 11B   lats_core */
                new RepExercise(89, "Pullups", PhaseType.Core, TargetType.Lats, 4, 12, GenderPreference.Both),
                new RepExercise(90, "Close-grip pull-down", PhaseType.Core, TargetType.Lats, 4, 12, GenderPreference.Both),
                new RepExercise(91, "Muscle up", PhaseType.Core, TargetType.Lats, 4, 5, GenderPreference.Both),
                new RepExercise(92, "Shotgun row", PhaseType.Core, TargetType.Lats, 4, 16, GenderPreference.Both),
                new RepExercise(93, "Lat pull-down", PhaseType.Core, TargetType.Lats, 4, 16, GenderPreference.Both),
                new RepExercise(94, "Reverse-grip lat pull-down", PhaseType.Core, TargetType.Lats, 4, 16, GenderPreference.Both),
                new RepExercise(95, "Machine seated row", PhaseType.Core, TargetType.Lats, 4, 16, GenderPreference.Both),
                new RepExercise(96, "Straight-arm pulldown", PhaseType.Core, TargetType.Lats, 4, 16, GenderPreference.Both),
                new RepExercise(97, "Elevated cable rows", PhaseType.Core, TargetType.Lats, 4, 16, GenderPreference.Both),
                new RepExercise(98, "Bent-arm barbell pull-over", PhaseType.Core, TargetType.Lats, 4, 16, GenderPreference.Both),
                /* 11C   lats_stretch */
                /* 12A   lowerback_warmup */
                /* 12B   lowerback_core */
                new RepExercise(101, "Back extension", PhaseType.Core, TargetType.LowerBack, 4, 20, GenderPreference.Both),
                new RepExercise(102, "Axle deadlift", PhaseType.Core, TargetType.LowerBack, 4, 16, GenderPreference.Both),
                new RepExercise(103, "Superman", PhaseType.Core, TargetType.LowerBack, 4, 16, GenderPreference.Both),
                /* 12C   lowerback_stretch */
                /* 13A   quad_warmup */
                new RepExercise(143, "Sit squats", PhaseType.WarmUp, TargetType.Quadriceps, 2, 20, GenderPreference.Both),
                new RepExercise(144, "Standing hip flexors", PhaseType.WarmUp, TargetType.Quadriceps, 2, 20, GenderPreference.Both),
                /* 13B   quad_core */
                new RepExercise(107, "Leg press", PhaseType.Core, TargetType.Quadriceps, 5, 16, GenderPreference.Both),
                new RepExercise(108, "Barbell full squat", PhaseType.Core, TargetType.Quadriceps, 5, 16, GenderPreference.Both),
                new RepExercise(109, "Barbell back squat to box", PhaseType.Core, TargetType.Quadriceps, 5, 16, GenderPreference.Both),
                new RepExercise(110, "Barbell walking lunge", PhaseType.Core, TargetType.Quadriceps, 5, 16, GenderPreference.Both),
                new RepExercise(111, "Kettlebell Pistol Squat", PhaseType.Core, TargetType.Quadriceps, 5, 16, GenderPreference.Both),
                new RepExercise(112, "Dumbbell goblet squat", PhaseType.Core, TargetType.Quadriceps, 5, 16, GenderPreference.Both),
                /* 13C   quad_stretch */
                /* 14A   reardelt_warmup */
                /* 14B   reardelt_core */
                new RepExercise(113, "Incline dumbbell reverse fly", PhaseType.Core, TargetType.RearDelt, 4, 20, GenderPreference.Both),
                new RepExercise(114, "Seated face pull", PhaseType.Core, TargetType.RearDelt, 4, 20, GenderPreference.Both),
                new RepExercise(115, "Dumbbell raise", PhaseType.Core, TargetType.RearDelt, 4, 20, GenderPreference.Both),
                new RepExercise(116, "Bent-over dumbbell rear delt row", PhaseType.Core, TargetType.RearDelt, 4, 20, GenderPreference.Both),
                /* 14C   reardelt_stretch */
                /* 15A   sidedelt_warmup */
                /* 15B   sidedelt_core */
                new RepExercise(117, "Single-arm incline lateral raise", PhaseType.Core, TargetType.SideDelt, 4, 20, GenderPreference.Both),
                new RepExercise(118, "Standing bradford press", PhaseType.Core, TargetType.SideDelt, 4, 14, GenderPreference.Both),
                new RepExercise(119, "Side lateral raise", PhaseType.Core, TargetType.SideDelt, 4, 14, GenderPreference.Both),
                /* 15C   sidedelt_stretch */
                /* 16A   traps_warmup */
                /* 16B   traps_core */
                new RepExercise(120, "Smith machine shrug", PhaseType.Core, TargetType.Traps, 4, 14, GenderPreference.Both),
                new RepExercise(121, "Standing dumbbell shrug", PhaseType.Core, TargetType.Traps, 4, 14, GenderPreference.Both),
                new RepExercise(122, "Barbell shrug", PhaseType.Core, TargetType.Traps, 4, 14, GenderPreference.Both),
                /* 16C   traps_stretch */
                /* 17A   triceps_warmup */
                new RepExercise(149, "Dynamic triceps warmup", PhaseType.WarmUp, TargetType.Triceps, 1, 40, GenderPreference.Both),
                new RepExercise(150, "Light weight overhead triceps extensions", PhaseType.WarmUp, TargetType.Triceps, 1, 30, GenderPreference.Both),
                /* 17B   triceps_core */
                new RepExercise(125, "Decline EZ-bar skullcrusher", PhaseType.Core, TargetType.Triceps, 4, 14, GenderPreference.Both),
                new RepExercise(126, "Dumbbell floor press", PhaseType.Core, TargetType.Triceps, 4, 14, GenderPreference.Both),
                new RepExercise(127, "Cable V-bar push-down", PhaseType.Core, TargetType.Triceps, 4, 14, GenderPreference.Both),
                new RepExercise(128, "Bench dip", PhaseType.Core, TargetType.Triceps, 4, 14, GenderPreference.Both),
                new RepExercise(129, "Reverse grip triceps pushdown", PhaseType.Core, TargetType.Triceps, 4, 14, GenderPreference.Both),
                new RepExercise(130, "Single-arm cable triceps extension", PhaseType.Core, TargetType.Triceps, 4, 14, GenderPreference.Both),
                new RepExercise(131, "Triceps pushdown with rope", PhaseType.Core, TargetType.Triceps, 4, 14, GenderPreference.Both),
                new RepExercise(132, "Seated triceps press", PhaseType.Core, TargetType.Triceps, 4, 14, GenderPreference.Both),
                new RepExercise(133, "Tricep dumbbell kickback", PhaseType.Core, TargetType.Triceps, 4, 14, GenderPreference.Both),
                new RepExercise(134, "Dip machine", PhaseType.Core, TargetType.Triceps, 4, 14, GenderPreference.Both),
                /* 17C   triceps_stretch */
                /* 18A   middleback_warmup */
                /* 18B   middleback_core */
                new RepExercise(136, "Reverse-grip bent-over row", PhaseType.Core, TargetType.MiddleBack, 4, 14, GenderPreference.Both),
                new RepExercise(137, "One-arm bumbbell row", PhaseType.Core, TargetType.MiddleBack, 4, 14, GenderPreference.Both),
                new RepExercise(138, "T-bar Row", PhaseType.Core, TargetType.MiddleBack, 4, 14, GenderPreference.Both),
                new RepExercise(139, "Incline dumbbell row", PhaseType.Core, TargetType.MiddleBack, 4, 14, GenderPreference.Both),
                new RepExercise(140, "Seated cable rows", PhaseType.Core, TargetType.MiddleBack, 4, 14, GenderPreference.Both)
                /* 18C   middleback_stretch */
                );
        }

        private void SeedingPerExercise(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<PerExercise>().HasData(
                /* 1A   glutes_warmup */
                /* 1B   glutes_core */
                /* 1C   glutes_stretch */
                /* 2A   abductors_warmup */
                /* 2B   abductors_core */
                /* 2C   abductors_stretch */
                new PerExercise(13, "Windmills", PhaseType.Stretching, TargetType.Abductors, 2, 20, GenderPreference.Both),
                new PerExercise(141, "Single-leg lying cross-over stretch", PhaseType.Stretching, TargetType.Abductors, 2, 30, GenderPreference.Both),
                /* 3A   adductors_warmup */
                /* 3B   adductors_core */
                /* 3C   adductors_stretch */
                new PerExercise(16, "Groin and back stretch", PhaseType.Stretching, TargetType.Adductors, 1, 30, GenderPreference.Both),
                /* 4A   biceps_warmup */
                /* 4B   biceps_core */
                /* 4C   biceps_stretch */
                new PerExercise(151, "Standing biceps stretch", PhaseType.Stretching, TargetType.Biceps, 1, 30, GenderPreference.Both),
                /* 5A   abs_warmup */
                /* 5B   abs_core */
                new PerExercise(30, "Elbow plank", PhaseType.Core, TargetType.Abs, 3, 60, GenderPreference.Both),
                /* 5C   abs_stretch */
                new PerExercise(147, "Cobra pose", PhaseType.Stretching, TargetType.Abs, 1, 40, GenderPreference.Both),
                /* 6A   calves_warmup */
                /* 6B   calves_core */
                /* 6C   calves_stretch */
                new PerExercise(37, "Standing gastrocnemius calf stretch", PhaseType.Stretching, TargetType.Calves, 1, 20, GenderPreference.Both),
                new PerExercise(38, "Calf stretch elbows against wall", PhaseType.Stretching, TargetType.Calves, 1, 30, GenderPreference.Both),
                /* 7A   chest_warmup */
                /* 7B   chest_core */
                /* 7C   chest_stretch */
                new PerExercise(160, "Elbows back", PhaseType.Stretching, TargetType.Chest, 1, 30, GenderPreference.Both),
                /* 8A   forearm_warmup */
                /* 8B   forearm_core */
                /* 8C   forearm_stretch */
                new PerExercise(153, "Kneeling forearm stretch", PhaseType.Stretching, TargetType.Forearms, 1, 30, GenderPreference.Both),
                /* 9A   frontdelt_warmup */
                new PerExercise(75, "Elbow circles", PhaseType.Stretching, TargetType.FrontDelt, 1, 30, GenderPreference.Both),
                /* 9B   frontdelt_core */
                /* 9C   frontdelt_stretch */
                new PerExercise(74, "Upward stretch", PhaseType.Stretching, TargetType.FrontDelt, 1, 10, GenderPreference.Both),
                new PerExercise(76, "Cross-body shoulder stretch", PhaseType.Stretching, TargetType.FrontDelt, 1, 20, GenderPreference.Both),
                new PerExercise(77, "Side wrist pull", PhaseType.Stretching, TargetType.FrontDelt, 1, 20, GenderPreference.Both),
                /* 10A   hamstrings_warmup */
                /* 10B   hamstrings_core */
                /* 10C   hamstrings_stretch */
                new PerExercise(78, "Leg-up hamstring stretch", PhaseType.Stretching, TargetType.Hamstrings, 1, 20, GenderPreference.Both),
                new PerExercise(79, "Standing toe touches", PhaseType.Stretching, TargetType.Hamstrings, 1, 30, GenderPreference.Both),
                new PerExercise(80, "Seated floor hamstring stretch", PhaseType.Stretching, TargetType.Hamstrings, 1, 30, GenderPreference.Both),
                /* 11A   lats_warmup */
                /* 11B   lats_core */
                /* 11C   lats_stretch */
                new PerExercise(87, "One arm against wall", PhaseType.Stretching, TargetType.Lats, 1, 30, GenderPreference.Both),
                new PerExercise(88, "One handed hang", PhaseType.Stretching, TargetType.Lats, 1, 30, GenderPreference.Both),
                /* 12A   lowerback_warmup */
                /* 12B   lowerback_core */
                /* 12C   lowerback_stretch */
                new PerExercise(99, "Dancer's stretch", PhaseType.Stretching, TargetType.LowerBack, 1, 30, GenderPreference.Both),
                new PerExercise(100, "Child's pose", PhaseType.Stretching, TargetType.LowerBack, 1, 30, GenderPreference.Both),
                /* 13A   quad_warmup */
                /* 13B   quad_core */
                /* 13C   quad_stretch */
                new PerExercise(104, "Iron crosses", PhaseType.Stretching, TargetType.Quadriceps, 1, 30, GenderPreference.Both),
                new PerExercise(105, "Lying quad stretch with band", PhaseType.Stretching, TargetType.Quadriceps, 1, 30, GenderPreference.Both),
                new PerExercise(106, "Looking at ceiling", PhaseType.Stretching, TargetType.Quadriceps, 1, 30, GenderPreference.Both),
                /* 14A   reardelt_warmup */
                /* 14B   reardelt_core */
                /* 14C   reardelt_stretch */
                /* 15A   sidedelt_warmup */
                /* 15B   sidedelt_core */
                /* 15C   sidedelt_stretch */
                /* 16A   traps_warmup */
                /* 16B   traps_core */
                /* 16C   traps_stretch */
                /* 17A   triceps_warmup */
                /* 17B   triceps_core */
                /* 17C   triceps_stretch */
                new PerExercise(123, "Triceps side stretch", PhaseType.Stretching, TargetType.Triceps, 1, 30, GenderPreference.Both),
                new PerExercise(124, "Triceps back stretch", PhaseType.Stretching, TargetType.Triceps, 1, 30, GenderPreference.Both),
                /* 18A   middleback_warmup */
                /* 18B   middleback_core */
                /* 18C   middleback_stretch */
                new PerExercise(135, "Upper back stretch", PhaseType.Stretching, TargetType.MiddleBack, 1, 10, GenderPreference.Both));
        }
    }
}
