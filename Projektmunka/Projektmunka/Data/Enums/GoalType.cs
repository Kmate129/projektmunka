﻿// <copyright file="GoalType.cs" company="OE-NIK">
// Copyright (c) OE-NIK. All rights reserved.
// </copyright>

namespace Projektmunka.Data.Enums
{
    /// <summary>
    /// Represents goals.
    /// </summary>
    public enum GoalType
    {
        /// <summary>
        /// Weight loss.
        /// </summary>
        WeightLoss,

        /// <summary>
        /// Stagnation
        /// </summary>
        Stagnation,

        /// <summary>
        /// Mass gain
        /// </summary>
        MassGain,
    }
}
