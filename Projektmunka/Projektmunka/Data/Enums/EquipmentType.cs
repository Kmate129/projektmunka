﻿// <copyright file="EquipmentType.cs" company="OE-NIK">
// Copyright (c) OE-NIK. All rights reserved.
// </copyright>

namespace Projektmunka.Data.Enums
{
    /// <summary>
    /// Represent an gym equipment.
    /// </summary>
    public enum EquipmentType
    {
        /// <summary>
        /// Nothing.
        /// </summary>
        Nothing,

        /// <summary>
        /// Barbell
        /// </summary>
        Barbell,

        /// <summary>
        /// Ez-barbell
        /// </summary>
        Ez_barbell,

        /// <summary>
        /// Cable.
        /// </summary>
        Cable,

        /// <summary>
        /// Bench.
        /// </summary>
        Bench,

        /// <summary>
        /// Box.
        /// </summary>
        Box,

        /// <summary>
        /// Dumbbell.
        /// </summary>
        Dumbbell,

        /// <summary>
        /// Plate.
        /// </summary>
        Plate,

        /// <summary>
        /// Kettlebell.
        /// </summary>
        Kettlebell,

        /// <summary>
        /// Band.
        /// </summary>
        Band,

        /// <summary>
        /// Abductor machine.
        /// </summary>
        Abductor_machine,

        /// <summary>
        /// Adductor machine.
        /// </summary>
        Adductor_machine,

        /// <summary>
        /// Scott bench.
        /// </summary>
        Scott_bench,

        /// <summary>
        /// Abs machine.
        /// </summary>
        Abs_machine,

        /// <summary>
        /// Decline bench.
        /// </summary>
        Decline_bench,

        /// <summary>
        /// Step pad.
        /// </summary>
        Step_pad,

        /// <summary>
        /// Calf raise machine.
        /// </summary>
        Calf_raise_machine,

        /// <summary>
        /// Smith machine.
        /// </summary>
        Smith_machine,

        /// <summary>
        /// Dip frame.
        /// </summary>
        Dip_frame,

        /// <summary>
        /// Chest press machine.
        /// </summary>
        Chest_press_machine,

        /// <summary>
        /// Chest fly machine.
        /// </summary>
        Chest_fly_machine,

        /// <summary>
        /// Shoulder press machine.
        /// </summary>
        Shoulder_press_machine,

        /// <summary>
        /// Rope.
        /// </summary>
        Rope,

        /// <summary>
        /// Reverse hyper machine.
        /// </summary>
        Reverse_hyper_machine,

        /// <summary>
        /// Pullup frame.
        /// </summary>
        Pullup_frame,

        /// <summary>
        /// Pulldown machine.
        /// </summary>
        Pulldown_machine,

        /// <summary>
        /// Ring.
        /// </summary>
        Ring,

        /// <summary>
        /// Rowing machine.
        /// </summary>
        Rowing_machine,

        /// <summary>
        /// Hyperextension bench.
        /// </summary>
        Hyperextension_bench,

        /// <summary>
        /// Leg press machine.
        /// </summary>
        Leg_press_machine,

        /// <summary>
        /// Dip machine.
        /// </summary>
        Dip_machine,

        /// <summary>
        /// Ankle strap.
        /// </summary>
        Ankle_strap,

        /// <summary>
        /// Hand grip.
        /// </summary>
        Hand_grip,

        /// <summary>
        /// Curl bar.
        /// </summary>
        Curl_bar,

        /// <summary>
        /// Straight bar.
        /// </summary>
        Straight_bar,

        /// <summary>
        /// Tricep rope.
        /// </summary>
        Tricep_rope,

        /// <summary>
        /// Ball.
        /// </summary>
        Ball,

        /// <summary>
        /// Roller.
        /// </summary>
        Roller,
    }
}
