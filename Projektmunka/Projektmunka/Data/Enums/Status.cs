﻿// <copyright file="Status.cs" company="OE-NIK">
// Copyright (c) OE-NIK. All rights reserved.
// </copyright>

namespace Projektmunka.Data.Enums
{
    /// <summary>
    /// Represents a status.
    /// </summary>
    public enum Status
    {
        /// <summary>
        /// Is done.
        /// </summary>
        Done,

        /// <summary>
        /// Ready to start.
        /// </summary>
        Ready,

        /// <summary>
        /// Unallowed to start.
        /// </summary>
        Unallowed,
    }
}
