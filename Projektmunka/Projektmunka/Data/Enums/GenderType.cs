﻿// <copyright file="GenderType.cs" company="OE-NIK">
// Copyright (c) OE-NIK. All rights reserved.
// </copyright>

namespace Projektmunka.Data.Enums
{
    /// <summary>
    /// Represent genders.
    /// </summary>
    public enum GenderType
    {
        /// <summary>
        /// Male.
        /// </summary>
        Male,

        /// <summary>
        /// Female.
        /// </summary>
        Female,
    }
}
