﻿// <copyright file="GenderPreference.cs" company="OE-NIK">
// Copyright (c) OE-NIK. All rights reserved.
// </copyright>

namespace Projektmunka.Data.Enums
{
    /// <summary>
    /// Gender preference of exercise.
    /// </summary>
    public enum GenderPreference
    {
        /// <summary>
        /// Man.
        /// </summary>
        Man,

        /// <summary>
        /// Woman.
        /// </summary>
        Woman,

        /// <summary>
        /// Both.
        /// </summary>
        Both,
    }
}
