﻿// <copyright file="RateFlag.cs" company="OE-NIK">
// Copyright (c) OE-NIK. All rights reserved.
// </copyright>

namespace Projektmunka.Data.Enums
{
    /// <summary>
    /// Represents a rating.
    /// </summary>
    public enum RateFlag
    {
        /// <summary>
        /// Super dislike.
        /// </summary>
        SuperDislike,

        /// <summary>
        /// Simple dislike.
        /// </summary>
        Dislike,

        /// <summary>
        /// Neutral rating.
        /// </summary>
        Neutral,

        /// <summary>
        /// Simple like.
        /// </summary>
        Like,

        /// <summary>
        /// Super like.
        /// </summary>
        SuperLike,
    }
}