﻿// <copyright file="MuscleGroupType.cs" company="OE-NIK">
// Copyright (c) OE-NIK. All rights reserved.
// </copyright>

namespace Projektmunka.Data.Enums
{
    /// <summary>
    /// Represents muscle groups.
    /// </summary>
    public enum MuscleGroupType
    {
        /// <summary>
        /// Leg.
        /// </summary>
        Leg,

        /// <summary>
        /// Back.
        /// </summary>
        Back,

        /// <summary>
        /// Abs.
        /// </summary>
        Abs,

        /// <summary>
        /// Chest.
        /// </summary>
        Chest,

        /// <summary>
        /// Arm.
        /// </summary>
        Arm,

        /// <summary>
        /// Shoulder.
        /// </summary>
        Shoulder,
    }
}
