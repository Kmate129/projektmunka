﻿// <copyright file="Valami.cs" company="OE-NIK">
// Copyright (c) OE-NIK. All rights reserved.
// </copyright>

namespace Projektmunka.Data.Enums
{
    public enum Valami
    {
        Regular,
        LastExerciseOfWorkout,
        LastWorkoutOfDailyPlan,
        LastDailyPlanOfWeeklyPlan,
        LastWeeklyPlanOfTrainingPlan,
    }
}
