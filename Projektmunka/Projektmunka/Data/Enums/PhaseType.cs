﻿// <copyright file="PhaseType.cs" company="OE-NIK">
// Copyright (c) OE-NIK. All rights reserved.
// </copyright>

namespace Projektmunka.Data.Enums
{
    /// <summary>
    /// Represents phases.
    /// </summary>
    public enum PhaseType
    {
        /// <summary>
        /// Warm-up.
        /// </summary>
        WarmUp,

        /// <summary>
        /// Core.
        /// </summary>
        Core,

        /// <summary>
        /// Stretching.
        /// </summary>
        Stretching,
    }
}
