﻿// <copyright file="TargetType.cs" company="OE-NIK">
// Copyright (c) OE-NIK. All rights reserved.
// </copyright>

namespace Projektmunka.Data.Enums
{
    /// <summary>
    /// Represents targets.
    /// </summary>
    public enum TargetType
    {
        /// <summary>
        /// Muscle group: chest
        /// </summary>
        Chest,

        /// <summary>
        /// Muscle group: shoulder
        /// </summary>
        FrontDelt,

        /// <summary>
        /// Muscle group: shoulder
        /// </summary>
        SideDelt,

        /// <summary>
        /// Muscle group: shoulder
        /// </summary>
        RearDelt,

        /// <summary>
        /// Muscle group: arm
        /// </summary>
        Triceps,

        /// <summary>
        /// Muscle group: arm
        /// </summary>
        Biceps,

        /// <summary>
        /// Muscle group: arm
        /// </summary>
        Forearms,

        /// <summary>
        /// Muscle group: leg
        /// </summary>
        Calves,

        /// <summary>
        /// Muscle group: leg
        /// </summary>
        Hamstrings,

        /// <summary>
        /// Muscle group: leg
        /// </summary>
        Quadriceps,

        /// <summary>
        /// Muscle group: leg
        /// </summary>
        Glutes,

        /// <summary>
        /// Muscle group: leg
        /// </summary>
        Abductors,

        /// <summary>
        /// Muscle group: leg
        /// </summary>
        Adductors,

        /// <summary>
        /// Muscle group: back
        /// </summary>
        LowerBack,

        /// <summary>
        /// Muscle group: back
        /// </summary>
        Lats,

        /// <summary>
        /// Muscle group: back
        /// </summary>
        Traps,

        /// <summary>
        /// Muscle group: back
        /// </summary>
        MiddleBack,

        /// <summary>
        /// Muscle group: abs
        /// </summary>
        Abs,
    }
}
