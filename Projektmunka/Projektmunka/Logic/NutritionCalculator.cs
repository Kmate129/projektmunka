﻿// <copyright file="NutritionCalculator.cs" company="OE-NIK">
// Copyright (c) OE-NIK. All rights reserved.
// </copyright>

using System;
using Projektmunka.Data.Enums;
using Projektmunka.Data.Model;
using Projektmunka.Logic.Interfaces;

namespace Projektmunka.Logic
{
    /// <summary>
    /// Implementation of nutrition calculator.
    /// </summary>
    public class NutritionCalculator : INutritionCalculator
    {
        /// <summary>
        /// Base increase compared to BME.
        /// </summary>
        private const float BASE_INCREASE = 1.55f;

        /// <summary>
        /// Daily difference.
        /// </summary>
        private const float DAILY_DIFF = 0.04375f;

        /// <inheritdoc/>
        public NutritionScheme Calculate(GenderType gender, float height, float weight, int age, int numberOfDays, float kgPerWeek)
        {
            float bmr = this.CalculateBMR(gender, weight, height, age);
            float bmr_withExercise = this.CalculateBMR_withExercises(numberOfDays, bmr);
            float calorieIntake = this.CalculateCalories(bmr_withExercise, kgPerWeek);
            return this.SettingMacros(calorieIntake, gender);
        }

        private NutritionScheme SettingMacros(float calorieIntake, GenderType gender)
        {
            NutritionScheme scheme = new NutritionScheme(
                0,
                (int)Math.Round(calorieIntake),
                (int)Math.Round(calorieIntake * 0.6 / 4),
                gender == GenderType.Male ? 25 : 35,
                (int)Math.Round(calorieIntake * 0.125 / 4),
                (int)Math.Round(calorieIntake * 0.275 / 9),
                (int)Math.Round(calorieIntake * 0.1 / 9),
                3);

            return scheme;
        }

        private float CalculateBMR(GenderType gender, float weight, float height, int age)
        {
            if (gender == GenderType.Male)
            {
                return (10f * weight) + (6.25f * height) - (5f * age) + 5f;
            }

            return (10f * weight) + (6.25f * height) - (5f * age) - 161f;
        }

        private float CalculateBMR_withExercises(int numberOfDays, float bmr)
        {
            if (numberOfDays == 3)
            {
                return bmr * BASE_INCREASE;
            }
            else if (numberOfDays == 4)
            {
                return bmr * (BASE_INCREASE + DAILY_DIFF);
            }

            return bmr * (BASE_INCREASE + (2 * DAILY_DIFF));
        }

        private float CalculateCalories(float bmr_withExercise, float kgPerWeek)
        {
            // 500 kalória/nap 0.5 kg/héthez
            return bmr_withExercise + (kgPerWeek * 1000);
        }
    }
}
