﻿// <copyright file="DailyPlanControl.cs" company="OE-NIK">
// Copyright (c) OE-NIK. All rights reserved.
// </copyright>

using System.Linq;
using Projektmunka.Data.Enums;
using Projektmunka.Data.Model;
using Projektmunka.Logic.Interfaces;
using Projektmunka.Platform.Interfaces;
using Projektmunka.Repository.Interfaces;
using Xamarin.Forms;

namespace Projektmunka.Logic
{
    /// <summary>
    /// Controll implementation to transact workouts.
    /// </summary>
    public class DailyPlanControl : IDailyPlanControl
    {
        private static ExerciseDetail current;
        private static int WeekIndex = -1;
        private static int DayIndex;
        private static int WorkoutIndex;
        private static PhaseType Phase;
        private static int ExerciseIndex;
        private readonly TrainingPlan trainingPlan;
        private readonly IUserRepository userRepository;
        private bool coherent;

        /// <summary>
        /// Initializes a new instance of the <see cref="DailyPlanControl"/> class.
        /// </summary>
        public DailyPlanControl()
            : this(DependencyService.Get<IUserRepository>(), DependencyService.Get<IAppProvider>(), DependencyService.Get<IPlanGenerator>())
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="DailyPlanControl"/> class.
        /// </summary>
        /// <param name="userRepository">User repository.</param>
        /// <param name="appProvider">Application wrapper.</param>
        public DailyPlanControl(IUserRepository userRepository, IAppProvider appProvider, IPlanGenerator gen)
        {
            this.userRepository = userRepository;
            this.trainingPlan = appProvider.GetApp().CurrentUser.TrainingPlan;

            if (WeekIndex == -1)
            {
                WeekIndex = this.trainingPlan.WeeklyPlans.First(x => x.Status.Equals(Status.Ready)).Index;
                DayIndex = this.trainingPlan.WeeklyPlans.First(x => x.Index.Equals(WeekIndex)).DailyPlans.First(x => x.Status.Equals(Status.Ready)).Index;
                WorkoutIndex = this.trainingPlan.WeeklyPlans.First(x => x.Index.Equals(WeekIndex)).DailyPlans.First(x => x.Index.Equals(DayIndex)).Workouts.First(x => x.Status.Equals(Status.Ready)).Index;

                var workout = this.GetWorkout(WeekIndex, DayIndex, WorkoutIndex);
                var exercise = workout.WarmUps.FirstOrDefault(x => x.Status.Equals(Status.Ready));
                if (exercise == null)
                {
                    exercise = workout.Cores.FirstOrDefault(x => x.Status.Equals(Status.Ready));
                    if (exercise == null)
                    {
                        exercise = workout.Stretches.FirstOrDefault(x => x.Status.Equals(Status.Ready));
                    }
                }

                ExerciseIndex = exercise.Index;
                Phase = exercise.OwnExercise.Exercise.Phase;
            }
        }

        private enum Position
        {
            LastOfWarmups,
            LastOfCores,
            LastOfStretches,
        }

        /// <inheritdoc/>
        public ExerciseDetail GetCurrentExercise()
        {
            if (this.trainingPlan.Status == Status.Done)
            {
                return null;
            }

            if (this.coherent)
            {
                return current;
            }
            else
            {
                var workout = this.GetCurrentWorkout();
                current = this.GetCurrentExerciseDetailOf(workout);
                this.coherent = true;
                return current;
            }
        }

        /// <inheritdoc/>
        public int GetCurrentRestTime()
        {
            return this.GetCurrentExercise().RestTime;
        }

        /// <inheritdoc/>
        public void CurrentExerciseDone()
        {
            this.SetNextExerciseDetail();

            this.userRepository.UpdateUser(this.trainingPlan.User);
            this.coherent = false;
        }

        /// <inheritdoc/>
        public bool IsCurrentExerciseTheLastInDailyPlan()
        {
            var lastWorkout = this.trainingPlan.WeeklyPlans.First(x => x.Index.Equals(WeekIndex)).DailyPlans.First(x => x.Index.Equals(DayIndex)).Workouts.Count - 1;
            return WorkoutIndex == lastWorkout && Phase == PhaseType.Stretching && ExerciseIndex == 1;
        }

        /// <inheritdoc/>
        public bool IsCurrentExerciseTheFirstInWeek()
        {
            return this.GetCurrentExercise().Id == this.GetWorkout(WeekIndex, 0, 0).WarmUps.First(x => x.Index.Equals(0)).Id;
        }

        private ExerciseDetail GetCurrentExerciseDetailOf(Workout workout)
        {
            if (Phase == PhaseType.WarmUp)
            {
                return workout.WarmUps.First(x => x.Index.Equals(ExerciseIndex));
            }
            else
            {
                if (Phase == PhaseType.Core)
                {
                    return workout.Cores.First(x => x.Index.Equals(ExerciseIndex));
                }
                else
                {
                    return workout.Stretches.First(x => x.Index.Equals(ExerciseIndex));
                }
            }
        }

        private Workout GetCurrentWorkout()
        {
            return this.trainingPlan.WeeklyPlans.First(x=>x.Index.Equals(WeekIndex)).DailyPlans.First(x=>x.Index.Equals(DayIndex)).Workouts.First(x=>x.Index.Equals(WorkoutIndex));
        }

        private void SetNextExerciseDetail()
        {
            // search pos
            bool enumSet = false;
            Position positionEnum = Position.LastOfWarmups;
            if (Phase == PhaseType.WarmUp && ExerciseIndex == 1)
            {
                positionEnum = Position.LastOfWarmups;
                enumSet = true;
            }
            else
            {
                if (Phase == PhaseType.Core && ExerciseIndex == 3)
                {
                    positionEnum = Position.LastOfCores;
                    enumSet = true;
                }
                else
                {
                    if (Phase == PhaseType.Stretching && ExerciseIndex == 1)
                    {
                        positionEnum = Position.LastOfStretches;
                        enumSet = true;
                    }
                }
            }

            // look for next
            // if its last of dailyplan
            if (enumSet)
            {
                if (positionEnum == Position.LastOfWarmups)
                {
                    this.GetWorkout(WeekIndex, DayIndex, WorkoutIndex).WarmUps.First(x => x.Index.Equals(ExerciseIndex)).Status = Status.Done;
                    Phase = PhaseType.Core;
                    ExerciseIndex = 0;
                    this.GetWorkout(WeekIndex, DayIndex, WorkoutIndex).Cores.First(x => x.Index.Equals(ExerciseIndex)).Status = Status.Ready;
                }
                else
                {
                    if (positionEnum == Position.LastOfCores)
                    {
                        this.GetWorkout(WeekIndex, DayIndex, WorkoutIndex).Cores.First(x => x.Index.Equals(ExerciseIndex)).Status = Status.Done;
                        Phase = PhaseType.Stretching;
                        ExerciseIndex = 0;
                        this.GetWorkout(WeekIndex, DayIndex, WorkoutIndex).Stretches.First(x => x.Index.Equals(ExerciseIndex)).Status = Status.Ready;
                    }
                    else
                    {
                        this.GetWorkout(WeekIndex, DayIndex, WorkoutIndex).Stretches.First(x => x.Index.Equals(ExerciseIndex)).Status = Status.Done;
                        this.CheckWorkout();
                        Phase = PhaseType.WarmUp;
                        ExerciseIndex = 0;
                        this.GetWorkout(WeekIndex, DayIndex, WorkoutIndex).WarmUps.First(x => x.Index.Equals(ExerciseIndex)).Status = Status.Ready;
                    }
                }
            }
            else
            {
                if (Phase == PhaseType.WarmUp)
                {
                    this.GetWorkout(WeekIndex, DayIndex, WorkoutIndex).WarmUps.First(x => x.Index.Equals(ExerciseIndex)).Status = Status.Done;
                    ExerciseIndex++;
                    this.GetWorkout(WeekIndex, DayIndex, WorkoutIndex).WarmUps.First(x => x.Index.Equals(ExerciseIndex)).Status = Status.Ready;
                }
                else
                {
                    if (Phase == PhaseType.Core)
                    {
                        this.GetWorkout(WeekIndex, DayIndex, WorkoutIndex).Cores.First(x => x.Index.Equals(ExerciseIndex)).Status = Status.Done;
                        ExerciseIndex++;
                        this.GetWorkout(WeekIndex, DayIndex, WorkoutIndex).Cores.First(x => x.Index.Equals(ExerciseIndex)).Status = Status.Ready;
                    }
                    else
                    {
                        this.GetWorkout(WeekIndex, DayIndex, WorkoutIndex).Stretches.First(x => x.Index.Equals(ExerciseIndex)).Status = Status.Done;
                        ExerciseIndex++;
                        this.GetWorkout(WeekIndex, DayIndex, WorkoutIndex).Stretches.First(x => x.Index.Equals(ExerciseIndex)).Status = Status.Ready;
                    }
                }
            }
        }

        private void CheckWorkout()
        {
            if (WorkoutIndex == this.GetDailyPlan(WeekIndex, DayIndex).Workouts.Count - 1)
            {
                this.GetWorkout(WeekIndex, DayIndex, WorkoutIndex).Status = Status.Done;
                this.CheckDaily();
                WorkoutIndex = 0;
                this.GetWorkout(WeekIndex, DayIndex, WorkoutIndex).Status = Status.Ready;
            }
            else
            {
                this.GetWorkout(WeekIndex, DayIndex, WorkoutIndex).Status = Status.Done;
                WorkoutIndex++;
                this.GetWorkout(WeekIndex, DayIndex, WorkoutIndex).Status = Status.Ready;
            }
        }

        private void CheckDaily()
        {
            if (DayIndex == this.trainingPlan.NumberOfDays - 1)
            {
                this.GetDailyPlan(WeekIndex, DayIndex).Status = Status.Done;
                this.CheckWeekly();
                DayIndex = 0;
                this.GetDailyPlan(WeekIndex, DayIndex).Status = Status.Ready;
            }
            else
            {
                this.GetDailyPlan(WeekIndex, DayIndex).Status = Status.Done;
                DayIndex++;
                this.GetDailyPlan(WeekIndex, DayIndex).Status = Status.Ready;
            }
        }

        private void CheckWeekly()
        {
            if (WeekIndex == this.trainingPlan.GetNumberOfWeeks() - 1)
            {
                this.GetWeek(WeekIndex).Status = Status.Done;
                this.trainingPlan.Status = Status.Done;
            }
            else
            {
                this.GetWeek(WeekIndex).Status = Status.Done;
                WeekIndex++;
                this.GetWeek(WeekIndex).Status = Status.Ready;
            }
        }

        private Workout GetWorkout(int week, int day, int workout)
        {
            return this.trainingPlan.WeeklyPlans.First(x => x.Index.Equals(week)).DailyPlans.First(x => x.Index.Equals(day)).Workouts.First(x => x.Index.Equals(workout));
        }

        private DailyPlan GetDailyPlan(int week, int day)
        {
            return this.trainingPlan.WeeklyPlans.First(x => x.Index.Equals(week)).DailyPlans.First(x => x.Index.Equals(day));
        }

        private WeeklyPlan GetWeek(int week)
        {
            return this.trainingPlan.WeeklyPlans.First(x => x.Index.Equals(week));
        }
    }
}
