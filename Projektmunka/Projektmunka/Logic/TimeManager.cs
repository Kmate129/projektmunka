﻿// <copyright file="TimeManager.cs" company="OE-NIK">
// Copyright (c) OE-NIK. All rights reserved.
// </copyright>

using System;
using Projektmunka.Logic.Interfaces;
using Projektmunka.Platform.Interfaces;
using Projektmunka.Repository.Interfaces;
using Xamarin.Forms;

namespace Projektmunka.Logic
{
    /// <summary>
    /// Implements time manager with Preferences.
    /// </summary>
    public class TimeManager : ITimeManager
    {
        private readonly IPreferences preferences;
        private readonly IAppProvider appProvider;
        private readonly IUserRepository userRepository;

        /// <summary>
        /// Initializes a new instance of the <see cref="TimeManager"/> class.
        /// </summary>
        public TimeManager()
            : this(DependencyService.Get<IPreferences>(), DependencyService.Get<IAppProvider>(), DependencyService.Get<IUserRepository>())
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="TimeManager"/> class.
        /// </summary>
        /// <param name="preferences">Xamarin preferences implementation.</param>
        /// <param name="appProvider">App provider.</param>
        /// <param name="userRepository">Repository of users.</param>
        public TimeManager(IPreferences preferences, IAppProvider appProvider, IUserRepository userRepository)
        {
            this.preferences = preferences;
            this.appProvider = appProvider;
            this.userRepository = userRepository;
        }

        /// <inheritdoc/>
        public void StartTotalTimer()
        {
            this.preferences.Set("start_date_total", DateTime.Now.ToString());
        }

        /// <inheritdoc/>
        public void StopTotalTimer()
        {
            this.preferences.Set("end_date_total", DateTime.Now.ToString());
        }

        /// <inheritdoc/>
        public TimeSpan GetTotalTime()
        {
            var end_date = this.preferences.Get("end_date_total", "default");
            var start_date = this.preferences.Get("start_date_total", "default");
            var totalTimeDiff = DateTime.Parse(end_date) - DateTime.Parse(start_date);
            this.preferences.Remove("start_date_total");
            this.preferences.Remove("end_date_total");
            return totalTimeDiff;
        }

        /// <inheritdoc/>
        public void StoreDailyTotalTime(TimeSpan timespan)
        {
            this.appProvider.GetApp().CurrentUser.TimeHistory.PushToTotalStack(timespan);
            this.userRepository.UpdateUser(this.appProvider.GetApp().CurrentUser);
        }

        /// <inheritdoc/>
        public void StoreAverages()
        {
            this.appProvider.GetApp().CurrentUser.TimeHistory.SaveStackAverages();
            this.userRepository.UpdateUser(this.appProvider.GetApp().CurrentUser);
        }

        /// <inheritdoc/>
        public void StartActiveTimer()
        {
            this.preferences.Set("exercise_start", DateTime.Now.ToString());
        }

        /// <inheritdoc/>
        public void StopAndAddActiveTimer()
        {
            DateTime end_time = DateTime.Now;
            DateTime start_time = DateTime.Parse(this.preferences.Get("exercise_start", "default"));
            TimeSpan diff = end_time - start_time;

            string activeTime = this.preferences.Get("active_time", "default");
            if (activeTime == "default")
            {
                this.preferences.Set("active_time", diff.ToString());
            }
            else
            {
                TimeSpan current = TimeSpan.Parse(this.preferences.Get("active_time", "default"));
                var added = current + diff;
                this.preferences.Set("active_time", added.ToString());
            }
        }

        /// <inheritdoc/>
        public void ClearActiveTimer()
        {
            this.preferences.Remove("active_time");
        }

        /// <inheritdoc/>
        public TimeSpan GetActiveTime()
        {
            return TimeSpan.Parse(this.preferences.Get("active_time", "default"));
        }

        /// <inheritdoc/>
        public void StoreDailyActiveTime(TimeSpan timespan)
        {
            this.appProvider.GetApp().CurrentUser.TimeHistory.PushToActiveStack(timespan);
            this.userRepository.UpdateUser(this.appProvider.GetApp().CurrentUser);
        }

        /// <inheritdoc/>
        public int GetWeeklyTotalTime()
        {
            return this.appProvider.GetApp().CurrentUser.TimeHistory.GetTotalStackValues();
        }
    }
}
