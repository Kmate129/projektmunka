﻿// <copyright file="PlanGenerator.cs" company="OE-NIK">
// Copyright (c) OE-NIK. All rights reserved.
// </copyright>

using System.Collections.Generic;
using System.Linq;
using Castle.Core.Internal;
using Projektmunka.Data.Enums;
using Projektmunka.Data.Model;
using Projektmunka.Logic.Interfaces;

namespace Projektmunka.Logic
{
    /// <summary>
    /// Common plan generator class to unify methods.
    /// </summary>
    public abstract class PlanGenerator : IPlanGenerator
    {
        /// <summary>
        /// Number of levels (number of exercises in one workout).
        /// </summary>
        protected const int N = 8;

        /// <summary>
        /// Initializes a new instance of the <see cref="PlanGenerator"/> class.
        /// </summary>
        protected PlanGenerator()
        {
            this.Warmups = new List<OwnExercise>();
            this.Cores = new List<OwnExercise>();
            this.Stretches = new List<OwnExercise>();
            this.SpecificWarmups = new List<OwnExercise>();
            this.SpecificCores = new List<OwnExercise>();
            this.SpecificStretches = new List<OwnExercise>();
        }

        /// <summary>
        /// Gets or sets array of size of each exercise pool on the certain level.
        /// </summary>
        protected static int[] M { get; set; }

        /// <summary>
        /// Gets or sets list of all Warmups.
        /// </summary>
        protected IList<OwnExercise> Warmups { get; set; }

        /// <summary>
        /// Gets or sets  list of all Cores.
        /// </summary>
        protected IList<OwnExercise> Cores { get; set; }

        /// <summary>
        /// Gets or sets  list of all Stretches.
        /// </summary>
        protected IList<OwnExercise> Stretches { get; set; }

        /// <summary>
        /// Gets or sets list of all Warmups.
        /// </summary>
        protected List<OwnExercise> SpecificWarmups { get; set; }

        /// <summary>
        /// Gets or sets  list of all Cores.
        /// </summary>
        protected List<OwnExercise> SpecificCores { get; set; }

        /// <summary>
        /// Gets or sets  list of all Stretches.
        /// </summary>
        protected List<OwnExercise> SpecificStretches { get; set; }

        /// <inheritdoc/>
        public TrainingPlan GenerateTrainingPlan(int numberOfDays, int numberOfWeeks, ICollection<OwnExercise> exercises)
        {
            // 3: mell - láb, hát - has, váll - kar,
            // 4: mell - has, kar - váll, hát - has, láb
            // 5: mell, láb, hát, kar, váll - has
            this.FieldsInit(exercises);
            var oneWeek = this.GenerateWeeklyPlan(numberOfDays, exercises);

            TrainingPlan returnValue = this.CopyEachWeek(oneWeek, exercises.First().Exercise.IsEquipmentRequired(), numberOfDays, numberOfWeeks);

            this.SetFlags(returnValue);

            return returnValue;
        }

        public WeeklyPlan GenerateWeeklyPlan(int numberOfDays, ICollection<OwnExercise> exercises, int startingIndex = 0)
        {
            if (this.Warmups.IsNullOrEmpty())
            {
                this.FieldsInit(exercises);
            }

            WeeklyPlan oneWeek = new WeeklyPlan(startingIndex);

            int[] order;
            if (numberOfDays == 5)
            {
                order = new int[6];
                order[0] = 0;
                order[1] = 0;
                order[2] = 0;
                order[3] = 0;
                order[4] = 0;
                order[5] = 1;
            }
            else if (numberOfDays == 4)
            {
                order = new int[6];
                order[0] = 0;
                order[1] = 0;
                order[2] = 0;
                order[3] = 0;
                order[4] = 1;
                order[5] = 1;
            }
            else
            {
                order = new int[6];
                order[0] = 0;
                order[1] = 1;
                order[2] = 0;
                order[3] = 1;
                order[4] = 0;
                order[5] = 1;
            }

            var chest = this.GenerateWorkOut(MuscleGroupType.Chest, order[0]);
            var leg = this.GenerateWorkOut(MuscleGroupType.Leg, order[1]);
            var back = this.GenerateWorkOut(MuscleGroupType.Back, order[2]);
            var arm = this.GenerateWorkOut(MuscleGroupType.Arm, order[3]);
            var shoulder = this.GenerateWorkOut(MuscleGroupType.Shoulder, order[4]);
            var abs = this.GenerateWorkOut(MuscleGroupType.Abs, order[5]);

            if (numberOfDays == 5)
            {
                DailyPlan dayOne = new DailyPlan(0);
                dayOne.AddToWorkouts(chest);
                DailyPlan dayTwo = new DailyPlan(1);
                dayTwo.AddToWorkouts(leg);
                DailyPlan dayThree = new DailyPlan(2);
                dayThree.AddToWorkouts(back);
                DailyPlan dayFour = new DailyPlan(3);
                dayFour.AddToWorkouts(arm);
                DailyPlan dayFive = new DailyPlan(4);
                dayFive.AddToWorkouts(shoulder);
                dayFive.AddToWorkouts(abs);

                oneWeek.DailyPlans.Add(dayOne);
                oneWeek.DailyPlans.Add(dayTwo);
                oneWeek.DailyPlans.Add(dayThree);
                oneWeek.DailyPlans.Add(dayFour);
                oneWeek.DailyPlans.Add(dayFive);
            }
            else if (numberOfDays == 4)
            {
                DailyPlan dayOne = new DailyPlan(0);
                dayOne.AddToWorkouts(chest);
                dayOne.AddToWorkouts(abs);
                DailyPlan dayTwo = new DailyPlan(1);
                dayTwo.AddToWorkouts(arm);
                dayTwo.AddToWorkouts(shoulder);
                DailyPlan dayThree = new DailyPlan(2);
                dayThree.AddToWorkouts(back);
                DailyPlan dayFour = new DailyPlan(3);
                dayFour.AddToWorkouts(leg);

                oneWeek.DailyPlans.Add(dayOne);
                oneWeek.DailyPlans.Add(dayTwo);
                oneWeek.DailyPlans.Add(dayThree);
                oneWeek.DailyPlans.Add(dayFour);
            }
            else
            {
                DailyPlan dayOne = new DailyPlan(0);
                dayOne.AddToWorkouts(chest);
                dayOne.AddToWorkouts(leg);
                DailyPlan dayTwo = new DailyPlan(1);
                dayTwo.AddToWorkouts(back);
                dayTwo.AddToWorkouts(abs);
                DailyPlan dayThree = new DailyPlan(2);
                dayThree.AddToWorkouts(shoulder);
                dayThree.AddToWorkouts(arm);

                oneWeek.DailyPlans.Add(dayOne);
                oneWeek.DailyPlans.Add(dayTwo);
                oneWeek.DailyPlans.Add(dayThree);
            }

            return oneWeek;
        }

        /// <summary>
        /// Loads in exercises from regular properties to specific ones.
        /// </summary>
        /// <param name="type">Type of exercises to load in.</param>
        protected void LoadInSpecs(MuscleGroupType type)
        {
            this.SpecificWarmups.Clear();
            var selected = this.Warmups.Where(item => item.Exercise.TargetMuscleGroup == type).ToList();
            this.SpecificWarmups.AddRange(selected);
            selected.ForEach(x => this.Warmups.Remove(x));

            this.SpecificCores.Clear();
            selected = this.Cores.Where(item => item.Exercise.TargetMuscleGroup == type).ToList();
            this.SpecificCores.AddRange(selected);
            selected.ForEach(x => this.Cores.Remove(x));

            this.SpecificStretches.Clear();
            selected = this.Stretches.Where(item => item.Exercise.TargetMuscleGroup == type).ToList();
            this.SpecificStretches.AddRange(selected);
            selected.ForEach(x => this.Stretches.Remove(x));

            M = new int[N] { this.SpecificWarmups.Count, this.SpecificWarmups.Count, this.SpecificCores.Count, this.SpecificCores.Count, this.SpecificCores.Count, this.SpecificCores.Count, this.SpecificStretches.Count, this.SpecificStretches.Count };
        }

        /// <summary>
        /// Generates a workout to given muscle group.
        /// </summary>
        /// <param name="type">Muscle group.</param>
        /// <param name="index">Index of workout.</param>
        /// <returns>Complete workout.</returns>
        protected abstract Workout GenerateWorkOut(MuscleGroupType type, int index);

        /// <summary>
        /// Checkes the given OwnExercise completes all requirement to be accepted on given level.
        /// </summary>
        /// <param name="R">Array of current solution.</param>
        /// <param name="E">OwnExercise to check.</param>
        /// <returns>True or false.</returns>
        protected bool Fk(OwnExercise R, OwnExercise[] E, int level)
        {
            for (int i = 0; i < level; i++)
            {
                if (E[i].Exercise.Id == R.Exercise.Id)
                {
                    return false;
                }
            }

            return true;
        }

        /// <summary>
        /// Returns the OwnExercise on the given index on the given level.
        /// </summary>
        /// <param name="level">Level.</param>
        /// <param name="index">Index.</param>
        /// <returns>OwnExercise.</returns>
        protected OwnExercise Lookup(int level, int index)
        {
            if (level == 0 || level == 1)
            {
                return this.SpecificWarmups[index];
            }
            else if (level == 6 || level == 7)
            {
                return this.SpecificStretches[index];
            }
            else
            {
                return this.SpecificCores[index];
            }
        }

        /// <summary>
        /// Calculates the fitness of the given the exercise array ( solution ).
        /// </summary>
        /// <param name="E">Array of OwnExercise.</param>
        /// <returns>Fitness value in float.</returns>
        protected float FitnessOf(OwnExercise[] E)
        {
            float fitness = 0;
            foreach (OwnExercise ownExercise in E)
            {
                fitness += ownExercise.Rating;
            }

            return fitness;
        }

        /// <summary>
        /// Checks that tha given exercise belongs to the given muscle group.
        /// </summary>
        /// <param name="exercise">Exercise that should be checked.</param>
        /// <param name="muscleGroup">The required muscle group.</param>
        /// <returns>True or false, whether exercise acceptable or not.</returns>
        protected bool IsCorrectTarget(OwnExercise exercise, MuscleGroupType muscleGroup)
        {
            if (exercise.Exercise.TargetMuscleGroup == muscleGroup)
            {
                return true;
            }

            return false;
        }

        private void FieldsInit(ICollection<OwnExercise> exercises)
        {
            foreach (var exercise in exercises)
            {
                if (exercise.Exercise.Phase.Equals(PhaseType.Core))
                {
                    this.Cores.Add(exercise);
                }
                else
                {
                    if (exercise.Exercise.Phase.Equals(PhaseType.WarmUp))
                    {
                        this.Warmups.Add(exercise);
                    }
                    else
                    {
                        this.Stretches.Add(exercise);
                    }
                }
            }
        }

        private void SetFlags(TrainingPlan trainingPlan)
        {
            trainingPlan.WeeklyPlans.First().Status = Status.Ready;
            trainingPlan.WeeklyPlans.First().DailyPlans.First().Status = Status.Ready;
            trainingPlan.WeeklyPlans.First().DailyPlans.First().Workouts.First().Status = Status.Ready;
            trainingPlan.WeeklyPlans.First().DailyPlans.First().Workouts.First().WarmUps.First().Status = Status.Ready;
        }

        private TrainingPlan CopyEachWeek(WeeklyPlan currentWeek, bool isEquipmentRequired, int numberOfDays, int numberOfWeeks)
        {
            TrainingPlan returnValue = new TrainingPlan(isEquipmentRequired, numberOfDays);

            returnValue.AddToWeeklyPlans(currentWeek);

            for (int i = 1; i < numberOfWeeks; i++)
            {
                WeeklyPlan newWeeklyplan = new WeeklyPlan(i);
                for (int j = 0; j < currentWeek.DailyPlans.Count; j++)
                {
                    DailyPlan dailyPlan = currentWeek.DailyPlans.First(x => x.Index.Equals(j));
                    DailyPlan newDailyPlan = new DailyPlan(j);
                    for (int k = 0; k < dailyPlan.Workouts.Count; k++)
                    {
                        Workout workout = dailyPlan.Workouts.First(x => x.Index.Equals(k));
                        Workout newWorkout = new Workout(k);
                        newWorkout.MuscleGroup = workout.MuscleGroup;
                        for (int l = 0; l < workout.Stretches.Count; l++)
                        {
                            ExerciseDetail newDetail = new ExerciseDetail(l);
                            newDetail.OwnExercise = workout.Stretches.First(x => x.Index.Equals(l)).OwnExercise;
                            newWorkout.Stretches.Add(newDetail);
                        }

                        for (int l = 0; l < workout.Cores.Count; l++)
                        {
                            ExerciseDetail newDetail = new ExerciseDetail(l);
                            newDetail.OwnExercise = workout.Cores.First(x => x.Index.Equals(l)).OwnExercise;
                            newWorkout.Cores.Add(newDetail);
                        }

                        for (int l = 0; l < workout.WarmUps.Count; l++)
                        {
                            ExerciseDetail newDetail = new ExerciseDetail(l);
                            newDetail.OwnExercise = workout.WarmUps.First(x => x.Index.Equals(l)).OwnExercise;
                            newWorkout.WarmUps.Add(newDetail);
                        }

                        newDailyPlan.AddToWorkouts(newWorkout);
                    }

                    newWeeklyplan.AddToDailyPlans(newDailyPlan);
                }

                returnValue.AddToWeeklyPlans(newWeeklyplan);
            }

            return returnValue;
        }
    }
}
