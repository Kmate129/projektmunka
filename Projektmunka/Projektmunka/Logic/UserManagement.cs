﻿// <copyright file="UserManagement.cs" company="OE-NIK">
// Copyright (c) OE-NIK. All rights reserved.
// </copyright>

using System;
using Projektmunka.Data.Model;
using Projektmunka.Logic.Interfaces;
using Projektmunka.Repository.Interfaces;
using Xamarin.Essentials;
using Xamarin.Forms;

namespace Projektmunka.Logic
{
    /// <summary>
    /// Main logic implementation.
    /// </summary>
    public class UserManagement : IUserManagement
    {
        private readonly IUserRepository userRepository;
        private readonly IUserComposer userComposer;

        /// <summary>
        /// Initializes a new instance of the <see cref="UserManagement"/> class.
        /// </summary>
        public UserManagement()
            : this(DependencyService.Get<IUserRepository>(), DependencyService.Get<IUserComposer>())
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="UserManagement"/> class.
        /// </summary>
        /// <param name="userRepository">User repository.</param>
        /// <param name="userComposer">User composer.</param>
        private UserManagement(IUserRepository userRepository, IUserComposer userComposer)
        {
            this.userRepository = userRepository ?? throw new ArgumentNullException(nameof(userRepository));
            this.userComposer = userComposer ?? throw new ArgumentNullException(nameof(userComposer));
        }

        /// <inheritdoc/>
        public bool Login(string name, string password, bool auto = false)
        {
            User user = this.userRepository.GetUserByUsername(name);
            if (user != null)
            {
                if (user.Password == password)
                {
                    ((App)Application.Current).CurrentUser = user;
                    if (auto)
                    {
                        Preferences.Set(ConstParameters.AUTO_LOGIN_KEY, user.Id);
                    }

                    return true;
                }

                return false;
            }

            return false;
        }

        /// <inheritdoc/>
        public bool AutoLogin()
        {
            if (Preferences.ContainsKey(ConstParameters.AUTO_LOGIN_KEY))
            {
                var pref = Preferences.Get(ConstParameters.AUTO_LOGIN_KEY, string.Empty);
                User user = this.userRepository.GetUserById(int.Parse(pref));
                if (user != null)
                {
                    ((App)Application.Current).CurrentUser = user;
                    return true;
                }

                return false;
            }

            return false;
        }

        /// <inheritdoc/>
        public void Logout()
        {
            Preferences.Remove(ConstParameters.AUTO_LOGIN_KEY);
            ((App)Application.Current).CurrentUser = null;
        }

        /// <inheritdoc/>
        public void SaveUser()
        {
            this.userRepository.UpdateUser(((App)Application.Current).CurrentUser);
        }

        /// <inheritdoc/>
        public bool CreateUser(User user, float currentWeight, float goalWeight, int numberOfDays, bool hasEquipment)
        {
            this.userComposer.Compose(user, currentWeight, goalWeight, numberOfDays, hasEquipment);

            if (this.userRepository.InsertUser(user))
            {
                return true;
            }

            return false;
        }
    }
}
