﻿// <copyright file="TrainingPlanHandler.cs" company="OE-NIK">
// Copyright (c) OE-NIK. All rights reserved.
// </copyright>

using System;
using System.Linq;
using Projektmunka.BLE.Models;
using Projektmunka.Data.Enums;
using Projektmunka.Data.Model;
using Projektmunka.Logic.Interfaces;
using Projektmunka.Repository.Interfaces;
using Xamarin.Forms;

namespace Projektmunka.Logic
{
    /// <summary>
    /// Implements methods required to manage training plans.
    /// </summary>
    public class TrainingPlanHandler : ITrainingPlanHandler
    {
        private readonly IOwnExerciseRepository ownExerciseRepository;

        /// <summary>
        /// Initializes a new instance of the <see cref="TrainingPlanHandler"/> class.
        /// </summary>
        public TrainingPlanHandler()
            : this(DependencyService.Get<IOwnExerciseRepository>())
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="TrainingPlanHandler"/> class.
        /// </summary>
        /// <param name="ownExerciseRepository">OwnExercise repository.</param>
        private TrainingPlanHandler(IOwnExerciseRepository ownExerciseRepository)
        {
            this.ownExerciseRepository = ownExerciseRepository ?? throw new ArgumentNullException(nameof(ownExerciseRepository));
        }

        /// <inheritdoc/>
        public OwnExercise GetOwnExerciseById(int id)
        {
            return this.ownExerciseRepository.GetOwnExerciseById(id);
        }

        /// <inheritdoc/>
        public int GetCountOfRemainingDailyPlans()
        {
            return ((App)Application.Current).CurrentUser.TrainingPlan.GetCurrentWeek().DailyPlans.Count(x => !x.Status.Equals(Status.Done));
        }

        /// <inheritdoc/>
        public void RateExercise(int myExerciseId, RateFlag rateResult)
        {
            var toRate = this.ownExerciseRepository.GetOwnExerciseById(myExerciseId);
            if (toRate != null)
            {
                switch (rateResult)
                {
                    case RateFlag.SuperDislike:
                        toRate.SuperDislike();
                        break;
                    case RateFlag.Dislike:
                        toRate.Dislike();
                        break;
                    case RateFlag.Neutral:
                        toRate.Neutral();
                        break;
                    case RateFlag.Like:
                        toRate.Like();
                        break;
                    case RateFlag.SuperLike:
                        toRate.SuperLike();
                        break;
                }

                this.ownExerciseRepository.UpdateOwnExercise(toRate);
            }
        }

        public void ModifyExercise(ExerciseDetail exerciseDetail, Intensity intensity)
        {
            var diff = 0;
            switch (intensity)
            {
                case Intensity.Light:
                    diff = -30;
                    break;
                case Intensity.Vigorious:
                    diff = 30;
                    break;
                case Intensity.Maximum:
                    diff = 60;
                    break;
            }

            exerciseDetail.RestTime += diff;
        }
    }
}
