﻿// <copyright file="PlanGeneratorBB.cs" company="OE-NIK">
// Copyright (c) OE-NIK. All rights reserved.
// </copyright>

using System.Collections.Generic;
using System.Linq;
using Castle.Core.Internal;
using Projektmunka.Data.Enums;
using Projektmunka.Data.Model;

namespace Projektmunka.Logic
{
    /// <summary>
    /// Plan generator implementation with branch and bound.
    /// </summary>
    public class PlanGeneratorBB : PlanGenerator
    {
        /// <summary>
        /// Generates a workout to the given muscle group.
        /// </summary>
        /// <param name="type">Muscle group.</param>
        /// <param name="index">Index of workout.</param>
        /// <returns>Workout.</returns>
        protected override Workout GenerateWorkOut(MuscleGroupType type, int index)
        {
            Workout returnValue = new Workout(index);

            OwnExercise[] E = new OwnExercise[N];
            OwnExercise[] OPT = new OwnExercise[N];

            this.BranchAndBound(0, ref E, ref OPT);

            returnValue.MuscleGroup = type;

            for (int i = 0; i < OPT.Length; i++)
            {
                ExerciseDetail newExercise = new ExerciseDetail(i)
                {
                    RestTime = 60,
                    OwnExercise = OPT[i],
                };

                if (i == 0 || i == 1)
                {
                    returnValue.WarmUps.Add(newExercise);
                }
                else
                {
                    if (i == 2 || i == 3 || i == 4 || i == 5)
                    {
                        returnValue.Cores.Add(newExercise);
                    }
                    else
                    {
                        returnValue.Stretches.Add(newExercise);
                    }
                }
            }

            return returnValue;
        }

        private void BranchAndBound(int level, ref OwnExercise[] e, ref OwnExercise[] opt)
        {
            int i = -1;
            while (i < M[level] - 1)
            {
                i++;
                if (this.Fk(this.Lookup(level, i), e, level))
                {
                    e[level] = this.Lookup(level, i);
                    if (level == N - 1)
                    {
                        if (this.BBFitnessOf(N, e) < this.BBFitnessOf(N, opt))
                        {
                            opt = e;
                        }
                    }
                    else
                    {
                        if (this.BBFitnessOf(level, e) + this.Fb(level, e) < this.BBFitnessOf(N, opt))
                        {
                            this.BranchAndBound(level + 1, ref e, ref opt);
                        }
                    }
                }
            }
        }

        private float Fb(int level, OwnExercise[] e)
        {
            float pfk = 0;
            for (int i = level + 1; i < N; i++)
            {
                IEnumerable<OwnExercise> filter;
                if (i == 1)
                {
                    filter = this.SpecificWarmups.Where(x => this.Fk(x, e, level));
                }
                else if (i == 6 || i == 7)
                {
                    filter = this.SpecificStretches.Where(x => this.Fk(x, e, level));
                }
                else
                {
                    filter = this.SpecificCores.Where(x => this.Fk(x, e, level));
                }

                if (!filter.IsNullOrEmpty())
                {
                    pfk += filter.Min().Rating;
                }
                else
                {
                    return 1000;
                }
            }

            return pfk;
        }

        /// <summary>
        /// Calculates the fitness of the given the exercise array ( solution ).
        /// </summary>
        /// <param name="level">Level the solution made on.</param>
        /// <param name="E">Array of OwnExercise.</param>
        /// <returns>Fitness value in float.</returns>
        private float BBFitnessOf(int level, OwnExercise[] E)
        {
            if (E[0] == null)
            {
                return 1000;
            }

            float fitness = 0;
            int index = 0;
            while (index < level && E[index] != null)
            {
                fitness += E[index].Rating;
                index++;
            }

            return fitness;
        }
    }
}
