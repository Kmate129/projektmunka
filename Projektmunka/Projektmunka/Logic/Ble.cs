﻿// <copyright file="Ble.cs" company="OE-NIK">
// Copyright (c) OE-NIK. All rights reserved.
// </copyright>

using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Plugin.BLE.Abstractions.Contracts;
using Plugin.BLE.Abstractions.Exceptions;
using Projektmunka.Logic.Interfaces;

namespace Projektmunka.Logic
{
    public class Ble : IBle
    {
        private readonly IBluetoothLE ble;
        private readonly IAdapter adapter;
        private readonly IDevice device;

        /// <summary>
        /// Gets list of scanned devcies.
        /// </summary>
        public IList<IDevice> DeviceList { get; }

        /// <summary>
        /// Gets or serts connected device.
        /// </summary>
        public IDevice ConnectedDevice { get; }

        /// <inheritdoc/>
        public async Task Scan()
        {
            try
            {
                this.DeviceList.Clear();
                this.adapter.DeviceDiscovered += (s, a) =>
                {
                    this.DeviceList.Add(a.Device);
                };

                if (!this.ble.Adapter.IsScanning)
                {
                    await this.adapter.StartScanningForDevicesAsync();
                }
            }
            catch (Exception ex)
            {
                // this.ConnectMessage = ex.Message;
            }
        }

        /// <inheritdoc/>
        public async Task<bool> Connect(string UUID)
        {
            try
            {
                if (this.device != null)
                {
                    await this.adapter.ConnectToDeviceAsync(this.device);
                }
                else
                {
                    // this.ConnectMessage = "No Device selected!";
                }
            }
            catch (DeviceConnectionException ex)
            {
                // this.ConnectMessage = ex.Message;
            }

            return true;
        }
    }
}
