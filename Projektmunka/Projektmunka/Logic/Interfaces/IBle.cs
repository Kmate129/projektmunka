﻿// <copyright file="IBle.cs" company="OE-NIK">
// Copyright (c) OE-NIK. All rights reserved.
// </copyright>

using System.Threading.Tasks;
using Plugin.BLE.Abstractions.Contracts;

namespace Projektmunka.Logic.Interfaces
{
    public interface IBle
    {
        public IDevice ConnectedDevice { get; }

        public Task Scan();

        public Task<bool> Connect(string UUID);
    }
}
