﻿// <copyright file="INutritionCalculator.cs" company="OE-NIK">
// Copyright (c) OE-NIK. All rights reserved.
// </copyright>

using Projektmunka.Data.Enums;
using Projektmunka.Data.Model;

namespace Projektmunka.Logic.Interfaces
{
    /// <summary>
    /// Requires methods a nutrition calculator must be able to.
    /// </summary>
    public interface INutritionCalculator
    {
        /// <summary>
        /// Calculates and returns nutrition scheme.
        /// </summary>
        /// <param name="gender">Gender.</param>
        /// <param name="height">Height.</param>
        /// <param name="weight">Weight.</param>
        /// <param name="age">Age.</param>
        /// <param name="numberOfDays">Number of training days.</param>
        /// <param name="kgPerWeek">Weekly weight change.</param>
        /// <returns>Nutrition scheme.</returns>
        public NutritionScheme Calculate(GenderType gender, float height, float weight, int age, int numberOfDays, float kgPerWeek);
    }
}
