﻿// <copyright file="IPlanGenerator.cs" company="OE-NIK">
// Copyright (c) OE-NIK. All rights reserved.
// </copyright>

using System.Collections.Generic;
using Projektmunka.Data.Model;

namespace Projektmunka.Logic.Interfaces
{
    /// <summary>
    /// Functions that needed to plan generations.
    /// </summary>
    public interface IPlanGenerator
    {
        /// <summary>
        /// Generates a training plan.
        /// </summary>
        /// <param name="numberOfDays">Number of training day.</param>
        /// <param name="numberOfWeeks">Number of weeks to generate.</param>
        /// <param name="exercises">Exercises pool.</param>
        /// <returns>Complete training plan.</returns>
        public TrainingPlan GenerateTrainingPlan(int numberOfDays, int numberOfWeeks, ICollection<OwnExercise> exercises);

        public WeeklyPlan GenerateWeeklyPlan(int numberOfDays, ICollection<OwnExercise> exercises, int startingIndex = 0);
    }
}
