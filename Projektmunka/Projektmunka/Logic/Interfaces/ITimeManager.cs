﻿// <copyright file="ITimeManager.cs" company="OE-NIK">
// Copyright (c) OE-NIK. All rights reserved.
// </copyright>

using System;

namespace Projektmunka.Logic.Interfaces
{
    /// <summary>
    /// Requires methods time spent with different activities.
    /// </summary>
    public interface ITimeManager
    {
        /// <summary>
        /// Starts to measure total time.
        /// </summary>
        public void StartTotalTimer();

        /// <summary>
        /// Stops measuring total time.
        /// </summary>
        public void StopTotalTimer();

        /// <summary>
        /// Gets and deletes total time.
        /// </summary>
        /// <returns>TimeSpan.</returns>
        public TimeSpan GetTotalTime();

        /// <summary>
        /// Stores total time of daily plan execution.
        /// </summary>
        /// <param name="timespan">Timespan of daily plan.</param>
        public void StoreDailyTotalTime(TimeSpan timespan);

        /// <summary>
        /// Stores average total and active time of daily plan execution.
        /// </summary>
        public void StoreAverages();

        /// <summary>
        /// Starts to measure active time.
        /// </summary>
        public void StartActiveTimer();

        /// <summary>
        /// Stops measuring active time and adds to previous one.
        /// </summary>
        public void StopAndAddActiveTimer();

        /// <summary>
        /// Clears active times.
        /// </summary>
        public void ClearActiveTimer();

        /// <summary>
        /// Gets active time.
        /// </summary>
        /// <returns>TimeSpan.</returns>
        public TimeSpan GetActiveTime();

        /// <summary>
        /// Stores active time of daily plan execution.
        /// </summary>
        /// <param name="timespan">Timespan of daily plan.</param>
        public void StoreDailyActiveTime(TimeSpan timespan);

        /// <summary>
        /// Returns total time sum in seconds stored in the stack.
        /// </summary>
        /// <returns>Seconds as integer.</returns>
        public int GetWeeklyTotalTime();
    }
}
