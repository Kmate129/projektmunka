﻿// <copyright file="IUserComposer.cs" company="OE-NIK">
// Copyright (c) OE-NIK. All rights reserved.
// </copyright>

using Projektmunka.Data.Model;

namespace Projektmunka.Logic.Interfaces
{
    /// <summary>
    /// Requires methods to initialize valid user object.
    /// </summary>
    public interface IUserComposer
    {
        /// <summary>
        /// Composes a user object.
        /// </summary>
        /// <param name="user">User reference to compose other properties.</param>
        /// <param name="currentWeight">Current weight.</param>
        /// <param name="goalWeight">Goal weight.</param>
        /// <param name="numberOfDays">Number of training days.</param>
        /// <param name="hasEquipment">Whether user has equipment or not.</param>
        public void Compose(User user, float currentWeight, float goalWeight, int numberOfDays, bool hasEquipment);

        public void RegenerateTrainingPlan(User user);

        public void RegenerateNutritionScheme(User user);
    }
}
