﻿// <copyright file="IDailyPlanControl.cs" company="OE-NIK">
// Copyright (c) OE-NIK. All rights reserved.
// </copyright>

using Projektmunka.Data.Model;

namespace Projektmunka.Logic.Interfaces
{
    /// <summary>
    /// Requires methods a daily plan control must be able to.
    /// </summary>
    public interface IDailyPlanControl
    {
        /// <summary>
        /// Gets the current exercise which is ready to execute.
        /// </summary>
        /// <returns>Returns the current exercise. If training plan is done, returns null.</returns>
        public ExerciseDetail GetCurrentExercise();

        /// <summary>
        /// Sets the status of current exercise to done and sets the next one's to ready.
        /// </summary>
        public void CurrentExerciseDone();

        /// <summary>
        /// Gets the rest time belongs to the current exercise.
        /// </summary>
        /// <returns>Time in seconds.</returns>
        public int GetCurrentRestTime();

        /// <summary>
        /// Incidicates whether current exercise is the last in the current daily plan.
        /// </summary>
        /// <returns>True or false.</returns>
        public bool IsCurrentExerciseTheLastInDailyPlan();

        /// <summary>
        /// Incidicates whether current exercise is the first exercise at a week.
        /// </summary>
        /// <returns>True or false.</returns>
        public bool IsCurrentExerciseTheFirstInWeek();
    }
}