﻿// <copyright file="IUserManagement.cs" company="OE-NIK">
// Copyright (c) OE-NIK. All rights reserved.
// </copyright>

using Projektmunka.Data.Model;

namespace Projektmunka.Logic.Interfaces
{
    /// <summary>
    /// Requires methods that a main logic must have.
    /// </summary>
    public interface IUserManagement
    {
        /// <summary>
        /// Logs in user with the given parameters.
        /// </summary>
        /// <param name="name">Username.</param>
        /// <param name="password">Password.</param>
        /// <param name="auto">Set auto login.</param>
        /// <returns>The task result contains whether login was successful or not.</returns>
        public bool Login(string name, string password, bool auto = false);

        /// <summary>
        /// Logs in user where automatic login set.
        /// </summary>
        /// <returns>The task result contains whether auto login was successful or not.</returns>
        public bool AutoLogin();

        /// <summary>
        /// Logs out currently logged in user.
        /// </summary>
        public void Logout();

        /// <summary>
        /// Saves current user to repository.
        /// </summary>
        public void SaveUser();

        /// <summary>
        /// Creates user profile.
        /// </summary>
        /// <param name="user">Model got from view.</param>
        /// <param name="currentWeight">Current weight.</param>
        /// <param name="goalWeight">Goal weight.</param>
        /// <param name="numberOfDays">Number of training days.</param>
        /// <param name="hasEquipment">Equipment flag.</param>
        /// <returns>The task result contains whether creation was successful or not.</returns>
        public bool CreateUser(User user, float currentWeight, float goalWeight, int numberOfDays, bool hasEquipment);
    }
}
