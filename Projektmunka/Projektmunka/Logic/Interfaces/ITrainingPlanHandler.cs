﻿// <copyright file="ITrainingPlanHandler.cs" company="OE-NIK">
// Copyright (c) OE-NIK. All rights reserved.
// </copyright>

using Projektmunka.BLE.Models;
using Projektmunka.Data.Enums;
using Projektmunka.Data.Model;

namespace Projektmunka.Logic.Interfaces
{
    /// <summary>
    /// Requires methods to manage training plans.
    /// </summary>
    public interface ITrainingPlanHandler
    {
        /// <summary>
        /// Gets exercise with the given identification number.
        /// </summary>
        /// <param name="id">Identification number.</param>
        /// <returns>Exercise or null.</returns>
        public OwnExercise GetOwnExerciseById(int id);

        /// <summary>
        /// Gets number of remaining daily plans.
        /// </summary>
        /// <returns>Integer.</returns>
        public int GetCountOfRemainingDailyPlans();

        /// <summary>
        /// Rates the exercise with the given ID.
        /// </summary>
        /// <param name="myExerciseId">ID of exercise to rate.</param>
        /// <param name="rateResult">Rate.</param>
        public void RateExercise(int myExerciseId, RateFlag rateResult);

        public void ModifyExercise(ExerciseDetail exerciseDetail, Intensity intensity);
    }
}
