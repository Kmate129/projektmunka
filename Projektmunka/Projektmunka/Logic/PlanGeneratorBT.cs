﻿// <copyright file="PlanGeneratorBT.cs" company="OE-NIK">
// Copyright (c) OE-NIK. All rights reserved.
// </copyright>

using System;
using Projektmunka.Data.Enums;
using Projektmunka.Data.Model;

namespace Projektmunka.Logic
{
    /// <summary>
    /// Plan generator implementation with backtrack.
    /// </summary>
    public class PlanGeneratorBT : PlanGenerator
    {
        /// <summary>
        /// Generates a workout to the given muscle group.
        /// </summary>
        /// <param name="type">Muscle group.</param>
        /// <param name="index">Index of workout.</param>
        /// <returns>Workout.</returns>
        protected override Workout GenerateWorkOut(MuscleGroupType type, int index)
        {
            this.LoadInSpecs(type);
            Workout returnValue = new Workout(index);

            OwnExercise[] E = new OwnExercise[N];
            OwnExercise[] OPT = new OwnExercise[N];
            bool exists = false;

            this.Backtrack(0, E, ref exists, OPT);

            returnValue.MuscleGroup = type;

            for (int i = 0; i < OPT.Length; i++)
            {
                if (i == 0 || i == 1)
                {
                    ExerciseDetail newExercise = new ExerciseDetail(i)
                    {
                        RestTime = 60,
                        OwnExercise = OPT[i],
                    };

                    returnValue.WarmUps.Add(newExercise);
                }
                else
                {
                    if (i == 2 || i == 3 || i == 4 || i == 5)
                    {
                        ExerciseDetail newExercise = new ExerciseDetail(i - 2)
                        {
                            RestTime = 60,
                            OwnExercise = OPT[i],
                        };

                        returnValue.Cores.Add(newExercise);
                    }
                    else
                    {
                        ExerciseDetail newExercise = new ExerciseDetail(i - 6)
                        {
                            RestTime = 60,
                            OwnExercise = OPT[i],
                        };

                        returnValue.Stretches.Add(newExercise);
                    }
                }
            }

            return returnValue;
        }

        private void Backtrack(int level, OwnExercise[] E, ref bool exists, OwnExercise[] OPT)
        {
            int i = -1;
            while (i < M[level] - 1)
            {
                i++;
                var exercise = this.Lookup(level, i);
                if (this.Fk(exercise, E, level))
                {
                    E[level] = exercise;
                    if (level == N - 1)
                    {
                        if (!exists || this.FitnessOf(E) < this.FitnessOf(OPT))
                        {
                            Array.Copy(E, OPT, E.Length);
                        }

                        exists = true;
                    }
                    else
                    {
                        this.Backtrack(level + 1, E, ref exists, OPT);
                    }
                }
            }
        }
    }
}
