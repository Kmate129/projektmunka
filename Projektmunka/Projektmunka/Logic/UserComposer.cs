﻿// <copyright file="UserComposer.cs" company="OE-NIK">
// Copyright (c) OE-NIK. All rights reserved.
// </copyright>

using System;
using System.Collections.Generic;
using System.Linq;
using Projektmunka.Data.Enums;
using Projektmunka.Data.Model;
using Projektmunka.Logic.Interfaces;
using Projektmunka.Repository.Interfaces;
using Xamarin.Forms;

namespace Projektmunka.Logic
{
    /// <summary>
    /// Implements composer functions.
    /// </summary>
    public class UserComposer : IUserComposer
    {
        private readonly IExerciseRepository exerciseRepository;
        private readonly INutritionCalculator nutritionCalculator;
        private readonly IPlanGenerator planGenerator;

        /// <summary>
        /// Initializes a new instance of the <see cref="UserComposer"/> class.
        /// </summary>
        public UserComposer()
        : this(DependencyService.Get<IExerciseRepository>(), DependencyService.Get<INutritionCalculator>(), DependencyService.Get<IPlanGenerator>())
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="UserComposer"/> class.
        /// </summary>
        /// <param name="exerciseRepository">Exercise repository.</param>
        /// <param name="calculator">Nutrition calculator.</param>
        /// <param name="generator">Training plan generator.</param>
        private UserComposer(IExerciseRepository exerciseRepository, INutritionCalculator calculator, IPlanGenerator generator)
        {
            this.exerciseRepository = exerciseRepository;
            this.nutritionCalculator = calculator;
            this.planGenerator = generator;
        }

        /// <inheritdoc/>
        public void Compose(User user, float currentWeight, float goalWeight, int numberOfDays, bool hasEquipment)
        {
            float difference = goalWeight - currentWeight;
            if (difference > 0f)
            {
                user.Goal = GoalType.MassGain;
            }
            else if (difference < 0f)
            {
                user.Goal = GoalType.WeightLoss;
            }
            else
            {
                user.Goal = GoalType.Stagnation;
            }

            int noOfWeeks = 12;
            float kgPerWeek = difference / 12f;
            if (Math.Abs(kgPerWeek) > 0.5f)
            {
                noOfWeeks = (int)Math.Ceiling((double)Math.Abs(difference) / 0.5);
                kgPerWeek = difference / noOfWeeks;
            }

            user.WeightHistory = new WeightHistory(currentWeight, kgPerWeek, noOfWeeks);
            user.TimeHistory = new TimeHistory(noOfWeeks);
            user.OwnExercises = this.InitializeOwnExercises(user.Gender, hasEquipment);
            user.TrainingPlan = this.planGenerator.GenerateTrainingPlan(numberOfDays, noOfWeeks, user.OwnExercises);
            user.Nutrients = this.nutritionCalculator.Calculate(
                user.Gender, user.Height, user.CurrentWeight, user.Age, user.TrainingPlan.NumberOfDays, kgPerWeek);
        }

        /// <inheritdoc/>
        public void RegenerateTrainingPlan(User user)
        {
            int startingIndex = user.TrainingPlan.GetCurrentWeek().Index;
            var endingIndex = user.TrainingPlan.GetNumberOfWeeks();

            var oneWeek = this.planGenerator.GenerateWeeklyPlan(user.TrainingPlan.NumberOfDays, user.OwnExercises, startingIndex);
            List<WeeklyPlan> weeks = new List<WeeklyPlan>
            {
                oneWeek
            };

            for (int i = startingIndex + 1; i < endingIndex; i++)
            {
                WeeklyPlan newWeeklyplan = new WeeklyPlan(i);
                for (int j = 0; j < oneWeek.DailyPlans.Count; j++)
                {
                    DailyPlan dailyPlan = oneWeek.DailyPlans.First(x => x.Index.Equals(j));
                    DailyPlan newDailyPlan = new DailyPlan(j);
                    for (int k = 0; k < dailyPlan.Workouts.Count; k++)
                    {
                        Workout workout = dailyPlan.Workouts.First(x => x.Index.Equals(k));
                        Workout newWorkout = new Workout(k);
                        newWorkout.MuscleGroup = workout.MuscleGroup;
                        for (int l = 0; l < workout.Stretches.Count; l++)
                        {
                            ExerciseDetail newDetail = new ExerciseDetail(l);
                            newDetail.OwnExercise = workout.Stretches.First(x => x.Index.Equals(l)).OwnExercise;
                            newDetail.RestTime = workout.Stretches.First(x => x.Index.Equals(l)).RestTime;
                            newWorkout.Stretches.Add(newDetail);
                        }

                        for (int l = 0; l < workout.Cores.Count; l++)
                        {
                            ExerciseDetail newDetail = new ExerciseDetail(l);
                            newDetail.OwnExercise = workout.Cores.First(x => x.Index.Equals(l)).OwnExercise;
                            newDetail.RestTime = workout.Stretches.First(x => x.Index.Equals(l)).RestTime;
                            newWorkout.Cores.Add(newDetail);
                        }

                        for (int l = 0; l < workout.WarmUps.Count; l++)
                        {
                            ExerciseDetail newDetail = new ExerciseDetail(l);
                            newDetail.OwnExercise = workout.WarmUps.First(x => x.Index.Equals(l)).OwnExercise;
                            newDetail.RestTime = workout.WarmUps.First(x => x.Index.Equals(l)).RestTime;
                            newWorkout.WarmUps.Add(newDetail);
                        }

                        newDailyPlan.AddToWorkouts(newWorkout);
                    }

                    newWeeklyplan.AddToDailyPlans(newDailyPlan);
                }

                weeks.Add(newWeeklyplan);
            }

            ;

            for (int i = startingIndex; i < endingIndex; i++)
            {
                user.TrainingPlan.WeeklyPlans.Remove(user.TrainingPlan.WeeklyPlans.First(x => x.Index.Equals(i)));
            }

            foreach (var week in weeks)
            {
                user.TrainingPlan.AddToWeeklyPlans(week);
            }

            user.TrainingPlan.WeeklyPlans.ElementAt(startingIndex).Status = Status.Ready;
            user.TrainingPlan.WeeklyPlans.ElementAt(startingIndex).DailyPlans.First().Status = Status.Ready;
            user.TrainingPlan.WeeklyPlans.ElementAt(startingIndex).DailyPlans.First().Workouts.First().Status = Status.Ready;
            user.TrainingPlan.WeeklyPlans.ElementAt(startingIndex).DailyPlans.First().Workouts.First().WarmUps.First().Status = Status.Ready;
        }

        /// <inheritdoc/>
        public void RegenerateNutritionScheme(User user)
        {
            float difference = user.GoalWeight - user.CurrentWeight;

            float kgPerWeek = difference / user.TrainingPlan.RemainingWeeks();
            if (Math.Abs(kgPerWeek) > 1f)
            {
                if (kgPerWeek < 0)
                {
                    kgPerWeek = -1f;
                }
                else
                {
                    kgPerWeek = 1f;
                }
            }

            user.Nutrients = this.nutritionCalculator.Calculate(user.Gender, user.Height, user.CurrentWeight, user.Age, user.TrainingPlan.NumberOfDays, kgPerWeek);
        }

        /// <summary>
        /// Initializines and filters all exercises stored in userRepository to create a pool of own exercises of user from which training plan can be generated.
        /// </summary>
        /// <returns>Coolection of own exercises.</returns>
        private ICollection<OwnExercise> InitializeOwnExercises(GenderType gender, bool requiresEquipment)
        {
            var exercises = this.exerciseRepository.GetAllExercises();
            ICollection<OwnExercise> returnValue = new List<OwnExercise>();
            foreach (var item in exercises)
            {
                if (gender == GenderType.Male)
                {
                    if (!item.Preference.Equals(GenderPreference.Woman))
                    {
                        if (item.Phase.Equals(PhaseType.Core))
                        {
                            if (item.IsEquipmentRequired().Equals(requiresEquipment))
                            {
                                returnValue.Add(new OwnExercise(item));
                            }
                        }
                        else
                        {
                            returnValue.Add(new OwnExercise(item));
                        }
                    }
                }
                else
                {
                    if (!item.Preference.Equals(GenderPreference.Man))
                    {
                        if (item.Phase.Equals(PhaseType.Core))
                        {
                            if (item.IsEquipmentRequired().Equals(requiresEquipment))
                            {
                                returnValue.Add(new OwnExercise(item));
                            }
                        }
                        else
                        {
                            returnValue.Add(new OwnExercise(item));
                        }
                    }
                }
            }

            return returnValue;
        }
    }
}
