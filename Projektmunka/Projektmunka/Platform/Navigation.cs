﻿// <copyright file="Navigation.cs" company="OE-NIK">
// Copyright (c) OE-NIK. All rights reserved.
// </copyright>

using System;
using System.Threading.Tasks;
using Projektmunka.Platform.Interfaces;
using Xamarin.Forms;

namespace Projektmunka.Platform
{
    public class Navigation : INavigationService
    {
        public async Task PushAsync(Page page, bool animated = true)
        {
            await Shell.Current.Navigation.PushAsync(page, animated);
        }

        public async Task PopAsync()
        {
            await App.Current.MainPage.Navigation.PopAsync();
        }

        public void NewAppShell()
        {
            App.Current.MainPage = new AppShell();
        }

        public void ShellGoToAsync(string route)
        {
            Shell.Current.GoToAsync(route);
        }
    }
}
