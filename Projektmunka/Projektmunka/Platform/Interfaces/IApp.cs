﻿// <copyright file="IApp.cs" company="OE-NIK">
// Copyright (c) OE-NIK. All rights reserved.
// </copyright>

using Projektmunka.Data.Model;

namespace Projektmunka.Platform.Interfaces
{
    public interface IApp
    {
        public User CurrentUser { get; set; }
    }
}
