﻿// <copyright file="IPreferences.cs" company="OE-NIK">
// Copyright (c) OE-NIK. All rights reserved.
// </copyright>

namespace Projektmunka.Platform.Interfaces
{
    public interface IPreferences
    {
        string Get(string key, string defaultValue = default);

        void Set(string key, string value);

        void Remove(string key);

        void Clear();
    }
}
