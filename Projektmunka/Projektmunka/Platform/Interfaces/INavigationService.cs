﻿// <copyright file="INavigationService.cs" company="OE-NIK">
// Copyright (c) OE-NIK. All rights reserved.
// </copyright>

using System.Threading.Tasks;
using Xamarin.Forms;

namespace Projektmunka.Platform.Interfaces
{
    public interface INavigationService
    {
        Task PushAsync(Page page, bool animated = true);

        Task PopAsync();

        void NewAppShell();

        void ShellGoToAsync(string route);
    }
}
