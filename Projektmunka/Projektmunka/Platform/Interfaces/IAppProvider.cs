﻿// <copyright file="IAppProvider.cs" company="OE-NIK">
// Copyright (c) OE-NIK. All rights reserved.
// </copyright>

namespace Projektmunka.Platform.Interfaces
{
    public interface IAppProvider
    {
        public IApp GetApp();
    }
}
