﻿// <copyright file="AppProvider.cs" company="OE-NIK">
// Copyright (c) OE-NIK. All rights reserved.
// </copyright>

using Projektmunka.Platform.Interfaces;
using Xamarin.Forms;

namespace Projektmunka.Platform
{
    public class AppProvider : IAppProvider
    {
        public IApp GetApp()
        {
            return (IApp)Application.Current;
        }
    }
}
