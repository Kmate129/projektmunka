﻿// <copyright file="AppPreferences.cs" company="OE-NIK">
// Copyright (c) OE-NIK. All rights reserved.
// </copyright>

using Projektmunka.Platform.Interfaces;
using Xamarin.Essentials;

namespace Projektmunka.Platform
{
    public class AppPreferences : IPreferences
    {
        public string Get(string key, string defaultValue = default(string))
        {
            return Preferences.Get(key, defaultValue);
        }

        public void Set(string key, string value)
        {
            Preferences.Set(key, value);
        }

        public void Remove(string key)
        {
            Preferences.Remove(key);
        }

        public void Clear()
        {
            Preferences.Clear();
        }
    }
}
