﻿// <copyright file="ValueCollector.cs" company="OE-NIK">
// Copyright (c) OE-NIK. All rights reserved.
// </copyright>

using System.Collections.Generic;
using Projektmunka.BLE.Interfaces;
using Projektmunka.BLE.Models;
using Xamarin.Forms;

namespace Projektmunka.BLE
{
    public class ValueCollector : IValueCollector
    {
        private readonly IHrServiceEmulator hrEmulator;
        private readonly IList<double> values = new List<double>();

        /// <summary>
        /// Initializes a new instance of the <see cref="ValueCollector"/> class.
        /// </summary>
        public ValueCollector()
            : this(DependencyService.Get<IHrServiceEmulator>())
        {
        }

        public ValueCollector(IHrServiceEmulator hrEmulator)
        {
            this.hrEmulator = hrEmulator;
            hrEmulator.ValueRead += this.HandleValueRead;
        }

        public IEnumerable<double> GetValues()
        {
            return this.values;
        }

        public void Listen()
        {
            this.hrEmulator.StartMeasure();
        }

        public void Stop()
        {
            this.hrEmulator.StopMeasure();
        }

        private void HandleValueRead(object sender, HrValue value)
        {
            this.values.Add(value.Value);
        }
    }
}
