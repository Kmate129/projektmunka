﻿// <copyright file="IValueCollector.cs" company="OE-NIK">
// Copyright (c) OE-NIK. All rights reserved.
// </copyright>

using System.Collections.Generic;

namespace Projektmunka.BLE.Interfaces
{
    public interface IValueCollector
    {
        public void Listen();

        public void Stop();

        public IEnumerable<double> GetValues();
    }
}
