﻿// <copyright file="IHrServiceEmulator.cs" company="OE-NIK">
// Copyright (c) OE-NIK. All rights reserved.
// </copyright>

using System;
using Projektmunka.BLE.Models;

namespace Projektmunka.BLE.Interfaces
{
    public interface IHrServiceEmulator
    {
        public event EventHandler<HrValue> ValueRead;

        public void StartMeasure();

        public void StopMeasure();
    }
}
