﻿// <copyright file="IHrRegulator.cs" company="OE-NIK">
// Copyright (c) OE-NIK. All rights reserved.
// </copyright>

using Projektmunka.BLE.Models;

namespace Projektmunka.BLE.Interfaces
{
    public interface IHrRegulator
    {
        public Intensity GetPriodIntensity(int age);

        public void StartCollectingData();

        public void StopCollectingData();
    }
}
