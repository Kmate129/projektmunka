﻿// <copyright file="Intensity.cs" company="OE-NIK">
// Copyright (c) OE-NIK. All rights reserved.
// </copyright>

namespace Projektmunka.BLE.Models
{
    public enum Intensity
    {
        Light,
        Moderate,
        Vigorious,
        Maximum
    }
}
