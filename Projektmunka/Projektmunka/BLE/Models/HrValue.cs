﻿// <copyright file="HrValue.cs" company="OE-NIK">
// Copyright (c) OE-NIK. All rights reserved.
// </copyright>

using System;

namespace Projektmunka.BLE.Models
{
    public class HrValue : EventArgs
    {
        public double Value { get; set; }
    }
}
