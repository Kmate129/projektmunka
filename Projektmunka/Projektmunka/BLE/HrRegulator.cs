﻿// <copyright file="HrRegulator.cs" company="OE-NIK">
// Copyright (c) OE-NIK. All rights reserved.
// </copyright>

using System.Linq;
using Projektmunka.BLE.Interfaces;
using Projektmunka.BLE.Models;
using Xamarin.Forms;

namespace Projektmunka.BLE
{
    public class HrRegulator : IHrRegulator
    {
        private readonly IValueCollector valueCollector;

        /// <summary>
        /// Initializes a new instance of the <see cref="HrRegulator"/> class.
        /// </summary>
        public HrRegulator()
            : this(DependencyService.Get<IValueCollector>())
        {
        }

        public HrRegulator(IValueCollector valueCollector)
        {
            this.valueCollector = valueCollector;
        }

        public Intensity GetPriodIntensity(int age)
        {
            Intensity intensity;
            double average = this.valueCollector.GetValues().Average();
            float maxHR = 220 - age;

            double activity = 1 / (maxHR / average);

            if (activity < 0.64f)
            {
                intensity = Intensity.Light;
            }
            else
            {
                if (activity < 0.76f)
                {
                    intensity = Intensity.Moderate;
                }
                else
                {
                    if (activity < 0.93f)
                    {
                        intensity = Intensity.Vigorious;
                    }
                    else
                    {
                        intensity = Intensity.Maximum;
                    }
                }
            }

            return intensity;
        }

        public void StartCollectingData()
        {
            this.valueCollector.Listen();
        }

        public void StopCollectingData()
        {
            this.valueCollector.Stop();
        }
    }
}
