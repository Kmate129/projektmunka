﻿// <copyright file="HrServiceEmulator.cs" company="OE-NIK">
// Copyright (c) OE-NIK. All rights reserved.
// </copyright>

using System;
using System.Threading.Tasks;
using Projektmunka.BLE.Interfaces;
using Projektmunka.BLE.Models;

namespace Projektmunka.BLE
{
    public class HrServiceEmulator : IHrServiceEmulator
    {
        private readonly Random random = new Random();
        private readonly int baseHR = 80;
        private readonly int rndLift;
        private bool stop = false;
        private int index = 1;

        /// <summary>
        /// Initializes a new instance of the <see cref="HrServiceEmulator"/> class.
        /// </summary>
        public HrServiceEmulator()
        {
            this.rndLift = this.random.Next(20, 60);
        }

        public event EventHandler<HrValue> ValueRead;

        public void StartMeasure()
        {
            Task.Run(async () =>
            {
                while (!this.stop)
                {
                    await Task.Delay(1000);
                    var cleanHR = ((0.5 * Math.Sqrt(this.index / 100.0)) + 1) * (this.baseHR + this.rndLift);
                    var noise = this.random.NextDouble();
                    if (noise < 0.5)
                    {
                        cleanHR += noise * -2;
                    }
                    else
                    {
                        cleanHR += noise * 2;
                    }

                    this.index++;

                    this.ValueRead?.Invoke(this, new HrValue() { Value = cleanHR });
                }
            });

        }

        public void StopMeasure()
        {
            this.stop = true;
        }
    }
}
