﻿// <copyright file="ConstParameters.cs" company="OE-NIK">
// Copyright (c) OE-NIK. All rights reserved.
// </copyright>

namespace Projektmunka
{
    /// <summary>
    /// Constant parameters.
    /// </summary>
    public static class ConstParameters
    {
        /// <summary>
        /// Lowest multiplier to exercise rating.
        /// </summary>
        public const float LOWEST_RATE_MULTIPLIER = 2.5f;

        /// <summary>
        /// Lower multiplier to exercise rating.
        /// </summary>
        public const float LOWER_RATE_MULTIPLIER = 1.5f;

        /// <summary>
        /// Base multiplier to exercise rating.
        /// </summary>
        public const float BASE_RATE_MULTIPLIER = 1f;

        /// <summary>
        /// Higher multiplier to exercise rating.
        /// </summary>
        public const float HIGHER_RATE_MULTIPLIER = 0.5f;

        /// <summary>
        /// Highest multiplier to exercise rating.
        /// </summary>
        public const float HIGHEST_RATE_MULTIPLIER = 0.2f;

        /// <summary>
        /// Auto login key to Preferences.
        /// </summary>
        public const string AUTO_LOGIN_KEY = "autologin";
    }
}
