﻿// <copyright file="App.xaml.cs" company="OE-NIK">
// Copyright (c) OE-NIK. All rights reserved.
// </copyright>

using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using Projektmunka.BLE;
using Projektmunka.BLE.Interfaces;
using Projektmunka.Data;
using Projektmunka.Data.Model;
using Projektmunka.Logic;
using Projektmunka.Logic.Interfaces;
using Projektmunka.Platform;
using Projektmunka.Platform.Interfaces;
using Projektmunka.Repository;
using Projektmunka.Repository.Interfaces;
using Projektmunka.View.Mappers;
using Projektmunka.View.Models;
using Projektmunka.View.Tools;
using Projektmunka.View.Validators;
using Projektmunka.View.ViewModels;
using Projektmunka.View.Views;
using Xamarin.Forms;

namespace Projektmunka
{
    /// <summary>
    /// Core class of application.
    /// </summary>
    public partial class App : Application, IApp
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="App"/> class.
        /// </summary>
        public App()
        {
            this.InitializeComponent();

            // COMPONENTS
            DependencyService.Register<IUserManagement, UserManagement>();
            DependencyService.Register<INutritionCalculator, NutritionCalculator>();
            DependencyService.Register<IPlanGenerator, PlanGeneratorBT>();
            DependencyService.Register<ITrainingPlanHandler, TrainingPlanHandler>();
            DependencyService.Register<IUserComposer, UserComposer>();
            DependencyService.Register<IBle, Ble>();
            DependencyService.Register<IDailyPlanControl, DailyPlanControl>();
            DependencyService.Register<ITimeManager, TimeManager>();
            DependencyService.Register<IHrServiceEmulator, HrServiceEmulator>();
            DependencyService.Register<IValueCollector, ValueCollector>();
            DependencyService.Register<IHrRegulator, HrRegulator>();

            // VIEW - MAPPERS
            DependencyService.Register<IMapper<UserDto, User>, UserMapper>();
            DependencyService.Register<IMapper<OwnExercise, OwnExerciseDto>, OwnExerciseMapper>();
            DependencyService.Register<IMapper<TrainingPlan, ICollection<WeeklyPlanDto>>, TrainingPlanMapper>();
            DependencyService.Register<IMapper<ExerciseDetail, ExerciseDto>, ExerciseMapper>();

            // REPOSITORIES
            DependencyService.Register<IUserRepository, UserRepository>();
            DependencyService.Register<IExerciseRepository, ExerciseRepository>();
            DependencyService.Register<IOwnExerciseRepository, OwnExerciseRepository>();

            // VIEWMODELS
            DependencyService.Register<LoginViewModel>();
            DependencyService.Register<AllExercisesViewModel>();

            // OTHER SERVICES
            DependencyService.Register<IMessageService, MessageService>();
            DependencyService.Register<DbContext, DatabaseContext>();
            DependencyService.Register<LoginValidation>();
            DependencyService.Register<RegistrationValidation>();
            DependencyService.Register<WeightValidation>();
            DependencyService.Register<IAppProvider, AppProvider>();
            DependencyService.Register<INavigationService, Navigation>();
            DependencyService.Register<IPreferences, AppPreferences>();

            // ROUTE REGISTRATION
            Routing.RegisterRoute("loginPage", typeof(LoginPage));
            Routing.RegisterRoute("registrationPage", typeof(RegistrationPage));
            Routing.RegisterRoute("devices", typeof(DevicesPage));
            Routing.RegisterRoute("training_setup", typeof(TrainingSetupPage));

            this.MainPage = new AppShell();
            Shell.Current.CurrentItem = new LoginPage();
        }

        /// <summary>
        /// Gets or sets currently logged in user.
        /// </summary>
        public User CurrentUser { get; set; }

        public bool Authenticated { get; set; }

        /// <inheritdoc/>
        protected override void OnStart()
        {
        }

        /// <inheritdoc/>
        protected override void OnSleep()
        {
        }

        /// <inheritdoc/>
        protected override void OnResume()
        {
        }
    }
}
