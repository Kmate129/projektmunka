﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Projektmunka
{
    public interface IBackgroundTask
    {
        void RunTaskInBackground();
    }
}
