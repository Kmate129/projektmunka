﻿// <copyright file="AbstractRepository.cs" company="OE-NIK">
// Copyright (c) OE-NIK. All rights reserved.
// </copyright>

using Microsoft.EntityFrameworkCore;

namespace Projektmunka.Repository
{
    /// <summary>
    /// Requires methods should be implemented by a repository.
    /// </summary>
    public abstract class AbstractRepository
    {
        /// <summary>
        /// Database context field.
        /// </summary>
        private readonly DbContext ctx;

        /// <summary>
        /// Initializes a new instance of the <see cref="AbstractRepository"/> class.
        /// </summary>
        /// <param name="ctx">DbContext object whose data are handled by repository.</param>
        protected AbstractRepository(DbContext ctx)
        {
            this.ctx = ctx;
        }

        /// <summary>
        /// Gets context of database.
        /// </summary>
        /// <returns>DbContext.</returns>
        public DbContext GetContext()
        {
            return this.ctx;
        }
    }
}
