﻿// <copyright file="OwnExerciseRepository.cs" company="OE-NIK">
// Copyright (c) OE-NIK. All rights reserved.
// </copyright>

using System.Linq;
using Microsoft.EntityFrameworkCore;
using Projektmunka.Data.Model;
using Projektmunka.Repository.Interfaces;
using Xamarin.Forms;

namespace Projektmunka.Repository
{
    /// <summary>
    /// Repository to manage OwnExercises in database.
    /// </summary>
    public class OwnExerciseRepository : AbstractRepository, IOwnExerciseRepository
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="OwnExerciseRepository"/> class.
        /// </summary>
        public OwnExerciseRepository()
            : this(DependencyService.Get<DbContext>())
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="OwnExerciseRepository"/> class.
        /// </summary>
        /// <param name="dbContext">Database context.</param>
        public OwnExerciseRepository(DbContext dbContext)
            : base(dbContext)
        {
        }

        /// <inheritdoc/>
        public OwnExercise GetOwnExerciseById(int id)
        {
            return this.GetContext().Set<OwnExercise>().Find(id);
        }

        /// <inheritdoc/>
        public IQueryable<OwnExercise> GetAllOwnExercisesOfUser(int id)
        {
            return this.GetContext().Set<OwnExercise>().Where(x => x.User.Id.Equals(id));
        }

        /// <inheritdoc/>
        public bool UpdateOwnExercise(OwnExercise ownExercise)
        {
            var result = this.GetContext().Update(ownExercise);
            if (result != null && result.State == EntityState.Modified)
            {
                try
                {
                    return this.GetContext().SaveChanges() > 0;
                }
                catch (DbUpdateException)
                {
                    return false;
                }
            }

            return false;
        }
    }
}
