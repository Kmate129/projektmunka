﻿// <copyright file="UserRepository.cs" company="OE-NIK">
// Copyright (c) OE-NIK. All rights reserved.
// </copyright>

using System;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using Projektmunka.Data.Model;
using Projektmunka.Repository.Interfaces;
using Xamarin.Forms;

namespace Projektmunka.Repository
{
    /// <summary>
    /// Repository to manage users in database.
    /// </summary>
    public class UserRepository : AbstractRepository, IUserRepository
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="UserRepository"/> class.
        /// </summary>
        public UserRepository()
            : this(DependencyService.Get<DbContext>())
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="UserRepository"/> class.
        /// </summary>
        /// <param name="dbContext">Database context.</param>
        public UserRepository(DbContext dbContext)
            : base(dbContext)
        {
        }

        /// <inheritdoc/>
        public bool InsertUser(User user)
        {
            var result = this.GetContext().Add(user);
            if (result.State == EntityState.Added)
            {
                try
                {
                    return this.GetContext().SaveChanges() > 0;
                }
                catch (DbUpdateException)
                {
                    return false;
                }
            }

            return false;
        }

        /// <inheritdoc/>
        public bool UpdateUser(User user)
        {
            var result = this.GetContext().Update(user);
            if (result.State == EntityState.Modified)
            {
                try
                {
                    return this.GetContext().SaveChanges() > 0;
                }
                catch (DbUpdateException)
                {
                    return false;
                }
            }

            return false;
        }

        /// <inheritdoc/>
        public User GetUserByUsername(string username)
        {
            return this.GetContext().Set<User>().Where(x => x.Username == username).FirstOrDefault();
        }

        /// <inheritdoc/>
        public User GetUserById(int id)
        {
            return this.GetContext().Set<User>().Find(id);
        }

        public Type GetEntityType(object entity)
        {
            if (entity == null || this.GetContext() == null)
            {
                throw new ArgumentNullException();
            }

            return this.GetContext().Model.FindRuntimeEntityType(entity.GetType()).ClrType;
        }
    }
}
