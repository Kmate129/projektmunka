﻿// <copyright file="ExerciseRepository.cs" company="OE-NIK">
// Copyright (c) OE-NIK. All rights reserved.
// </copyright>

using System.Linq;
using Microsoft.EntityFrameworkCore;
using Projektmunka.Data.Model;
using Projektmunka.Repository.Interfaces;
using Xamarin.Forms;

namespace Projektmunka.Repository
{
    /// <summary>
    /// Repository to manage exercises in database.
    /// </summary>
    public class ExerciseRepository : AbstractRepository, IExerciseRepository
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ExerciseRepository"/> class.
        /// </summary>
        public ExerciseRepository()
            : this(DependencyService.Get<DbContext>())
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ExerciseRepository"/> class.
        /// </summary>
        /// <param name="dbContext">Database context.</param>
        public ExerciseRepository(DbContext dbContext)
            : base(dbContext)
        {
        }

        /// <inheritdoc/>
        public Exercise GetExerciseById(int id)
        {
            return this.GetContext().Set<Exercise>().Find(id);
        }

        /// <inheritdoc/>
        public IQueryable<Exercise> GetAllExercises()
        {
            return this.GetContext().Set<Exercise>();
        }
    }
}
