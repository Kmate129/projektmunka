﻿// <copyright file="IOwnExerciseRepository.cs" company="OE-NIK">
// Copyright (c) OE-NIK. All rights reserved.
// </copyright>

using System.Linq;
using Projektmunka.Data.Model;

namespace Projektmunka.Repository.Interfaces
{
    /// <summary>
    /// Prescribes methods that are required by OwnExercise repository.
    /// </summary>
    public interface IOwnExerciseRepository
    {
        /// <summary>
        /// Finds the OwnExercise with given ID in the database.
        /// </summary>
        /// <param name="id">ID of OwnExercise to find.</param>
        /// <returns>The task result contains OwnExercise found in database or null.</returns>
        public OwnExercise GetOwnExerciseById(int id);

        /// <summary>
        /// Return all OwnExercises of user with given ID in the database.
        /// </summary>
        /// <param name="id">ID of user.</param>
        /// <returns>The task result contains list of users.</returns>
        public IQueryable<OwnExercise> GetAllOwnExercisesOfUser(int id);

        /// <summary>
        /// Updates OwnExercise in database.
        /// </summary>
        /// <param name="ownExercise">OwnExercise to update.</param>
        /// <returns>The task result contains whether update were successful or not.</returns>
        public bool UpdateOwnExercise(OwnExercise ownExercise);
    }
}
