﻿// <copyright file="IUserRepository.cs" company="OE-NIK">
// Copyright (c) OE-NIK. All rights reserved.
// </copyright>

using Microsoft.EntityFrameworkCore;
using Projektmunka.Data.Model;
using System;

namespace Projektmunka.Repository.Interfaces
{
    /// <summary>
    /// Prescribes methods that are required by user repository.
    /// </summary>
    public interface IUserRepository
    {
        /// <summary>
        /// Inserts user into database.
        /// </summary>
        /// <param name="user">User to insert.</param>
        /// <returns>The task result contains whether insertion were successful or not.</returns>
        public bool InsertUser(User user);

        /// <summary>
        /// Updates user in database.
        /// </summary>
        /// <param name="user">User to update.</param>
        /// <returns>The task result contains whether update were successful or not.</returns>
        public bool UpdateUser(User user);

        /// <summary>
        /// Finds the user with given username in the database.
        /// </summary>
        /// <param name="username">Username of user to find.</param>
        /// <returns>The task result contains the user found in database or null.</returns>
        public User GetUserByUsername(string username);

        /// <summary>
        /// Finds the user with auto login set.
        /// </summary>
        /// <param name="id">ID of user.</param>
        /// <returns>The task result contains the user found in database or null.</returns>
        public User GetUserById(int id);

        public Type GetEntityType(object entity);
    }
}
