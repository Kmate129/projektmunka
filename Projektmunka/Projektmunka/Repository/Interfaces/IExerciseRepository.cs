﻿// <copyright file="IExerciseRepository.cs" company="OE-NIK">
// Copyright (c) OE-NIK. All rights reserved.
// </copyright>

using System.Linq;
using Projektmunka.Data.Model;

namespace Projektmunka.Repository.Interfaces
{
    /// <summary>
    /// Prescribes methods that are required by exercise repository.
    /// </summary>
    public interface IExerciseRepository
    {
        /// <summary>
        /// Finds the exercise with given ID in the database.
        /// </summary>
        /// <param name="id">ID of exercise to find.</param>
        /// <returns>The task result contains exercise found in database or null.</returns>
        public Exercise GetExerciseById(int id);

        /// <summary>
        /// Return all exercises in the database.
        /// </summary>
        /// <returns>The task result contains list of exercises.</returns>
        public IQueryable<Exercise> GetAllExercises();
    }
}
