﻿// <copyright file="MultiLinesChart.cs" company="OE-NIK">
// Copyright (c) OE-NIK. All rights reserved.
// </copyright>

using System;
using System.Collections.Generic;
using System.Linq;
using Microcharts;
using SkiaSharp;
using Xamarin.Forms.Internals;

namespace Projektmunka.View.Helpers
{
    /// <summary>
    /// Chart that contains multiple lines.
    /// </summary>
    public class MultiLinesChart : LineChart
    {
        private readonly List<SKPoint> points = new List<SKPoint>();
        private bool initiated = false;
        private float multiLineMax;
        private float multiLineMin = 1000;

        /// <summary>
        /// Gets or sets data of lines.
        /// </summary>
        public IEnumerable<IEnumerable<ChartEntry>> MultiLineEntries { get; set; }

        /// <summary>
        /// Gets or sets axis names.
        /// </summary>
        public List<string> LegendNames { get; set; }

        /// <summary>
        /// Draws the diagram.
        /// </summary>
        /// <param name="canvas">Canvas.</param>
        /// <param name="width">Width.</param>
        /// <param name="height">Height.</param>
        public override void DrawContent(SKCanvas canvas, int width, int height)
        {
            this.Init();

            this.Entries = this.MultiLineEntries.ElementAt(0);
            var valueLabelSizes = this.MeasureLabels(this.MultiLineEntries.ElementAt(0).Select(x => x.Label).ToArray());
            var footerHeight = this.CalculateFooterHeaderHeight(valueLabelSizes, Orientation.Horizontal);
            var itemSize = this.CalculateItemSize(width, height, footerHeight, 100);
            var origin = this.CalculateYOrigin(itemSize.Height, 100);

            foreach (IEnumerable<ChartEntry> l in this.MultiLineEntries)
            {
                this.Entries = l;
                var tempPoints = this.CalculateMultiLinePoints(itemSize, 100);
                this.points.AddRange(tempPoints);
                this.DrawLine(canvas, tempPoints, itemSize);
                this.DrawPoints(canvas, tempPoints);

                if (this.MultiLineEntries.IndexOf(l) == 0)
                {
                    this.DrawArea(canvas, tempPoints, itemSize, origin);
                    this.DrawFooter(canvas, l.Select(x => x.Label).ToArray(), valueLabelSizes, tempPoints, itemSize, height, footerHeight);
                }
            }

            this.DrawLegends(canvas);
        }

        private void Init()
        {
            if (this.initiated)
            {
                return;
            }

            this.initiated = true;

            this.LineMode = LineMode.Straight;
            foreach (IEnumerable<ChartEntry> line in this.MultiLineEntries)
            {
                foreach (ChartEntry entry in line)
                {
                    if (entry.Value > this.multiLineMax)
                    {
                        this.multiLineMax = entry.Value;
                    }

                    if (entry.Value < this.multiLineMin)
                    {
                        this.multiLineMin = entry.Value;
                    }
                }
            }

            if (this.multiLineMax - this.multiLineMin == 0)
            {
                this.multiLineMin -= 2.5f;
                this.multiLineMax += 2.5f;
            }
        }

        private void DrawLegends(SKCanvas canvas)
        {
            if (!this.LegendNames.Any())
            {
               return;
            }

            List<SKColor> colors = new List<SKColor>();

            foreach (List<ChartEntry> l in this.MultiLineEntries)
            {
                colors.Add(l[0].Color);
            }

            int radiusSize = 30;

            using (var paint = new SKPaint())
            {
                paint.TextSize = this.LabelTextSize;
                paint.IsAntialias = true;
                paint.IsStroke = false;

                float x = 200 + (radiusSize * 2);
                float y = 50;

                foreach (string legend in this.LegendNames)
                {
                    paint.Color = SKColor.Parse("#000000");
                    canvas.DrawText(legend, x + radiusSize + 10, y, paint);

                    paint.Color = colors.ElementAt(this.LegendNames.IndexOf(legend));
                    canvas.DrawCircle(x, y - (radiusSize / 2) - (radiusSize / 4), radiusSize, paint);

                    x += (radiusSize * 2) + (this.LabelTextSize * ((legend.Length / 2) + 2));
                }

                var minPoint = this.points.Min(p => p.Y);
                var maxPoint = this.points.Max(p => p.Y);

                if (minPoint == maxPoint)
                {
                    minPoint -= 300;
                    maxPoint += 300;
                }

                paint.Color = SKColor.Parse("#000000");
                paint.TextSize = 20;

                var step = (maxPoint - minPoint) / 9;
                var valueStep = (this.multiLineMax - this.multiLineMin) / 9;
                for (int i = 0; i < 10; i++)
                {
                    canvas.DrawCircle(12, maxPoint - (step * i), 7, paint);
                    canvas.DrawText((this.multiLineMin + Math.Round(valueStep * i, 1)).ToString(), 0, maxPoint - (step * i) - 20, paint);
                }
            }
        }

        private SKPoint[] CalculateMultiLinePoints(SKSize itemSize, int headerHeight)
        {
            var result = new List<SKPoint>();

            for (int i = 0; i < this.Entries.Count(); i++)
            {
                var entry = this.Entries.ElementAt(i);
                var x = this.Margin + (itemSize.Width / 2) + (i * (itemSize.Width + this.Margin));
                var y = headerHeight + ((this.multiLineMax - entry.Value) / (this.multiLineMax - this.multiLineMin) * itemSize.Height);
                var point = new SKPoint(x, y);
                result.Add(point);
            }

            return result.ToArray();
        }
    }
}