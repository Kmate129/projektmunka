﻿// <copyright file="MultiBarChart.cs" company="OE-NIK">
// Copyright (c) OE-NIK. All rights reserved.
// </copyright>

using System;
using System.Collections.Generic;
using System.Linq;
using Microcharts;
using SkiaSharp;
using Xamarin.Forms.Internals;

namespace Projektmunka.View.Helpers
{
    public class MultiBarChart : BarChart
    {
        private List<SKPoint> points = new List<SKPoint>();
        private bool initiated = false;
        private float multibarMin = 0;
        private float multibarMax;

        public List<SKColor> LegendColor { get; set; }

        public bool FakeInserted { get; set; }

        public IEnumerable<IEnumerable<ChartEntry>> MultiBarEntries { get; set; }

        public List<string> LegendNames { get; set; }

        public override void DrawContent(SKCanvas canvas, int width, int height)
        {
            this.Init();

            int i = 0;
            var headerHeight = 100;

            this.Entries = this.MultiBarEntries.ElementAt(0);
            var valueLableSizes = this.MeasureLabels(this.MultiBarEntries.ElementAt(0).Select(x => x.Label).ToArray());
            var footerHeight = this.CalculateFooterHeaderHeight(valueLableSizes, Orientation.Horizontal);
            var itemSize = this.CalculateItemSize(width, height, footerHeight, headerHeight);
            var origin = this.CalculateYOrigin(itemSize.Height, headerHeight);

            foreach (IEnumerable<ChartEntry> l in this.MultiBarEntries)
            {
                this.Entries = l;
                var points = this.CalculateMultilinePoints(itemSize, headerHeight, i);
                this.points.AddRange(points);
                this.DrawBars(canvas, points, itemSize, origin, headerHeight);
                this.DrawPoints(canvas, points);

                if (this.MultiBarEntries.IndexOf(l) == 0)
                {
                    this.DrawBarAreas(canvas, points, itemSize, headerHeight);
                    this.DrawFooter(canvas, l.Select(x => x.Label).ToArray(), valueLableSizes, points, itemSize, height, footerHeight);
                }

                i++;
            }

            this.DrawLegend(canvas);
        }

        private void Init()
        {
            if (this.initiated)
            {
                return;
            }

            this.initiated = true;

            foreach (IEnumerable<ChartEntry> l in this.MultiBarEntries)
            {
                foreach (ChartEntry e in l)
                {
                    if (e.Value > this.multibarMax)
                    {
                        this.multibarMax = e.Value;
                    }

                    if (e.Value < this.multibarMin)
                    {
                        this.multibarMin = e.Value;
                    }
                }
            }
        }

        private void DrawLegend(SKCanvas canvas)
        {
            if (!this.LegendNames.Any())
            {
                return;
            }

            int rectWidth = 50;

            using (var paint = new SKPaint())
            {
                paint.TextSize = this.LabelTextSize;
                paint.IsAntialias = true;
                paint.IsStroke = false;

                float x = 200 + (rectWidth * 2);
                float y = 50;

                foreach (string legend in this.LegendNames)
                {
                    paint.Color = SKColor.Parse("#000000");
                    canvas.DrawText(legend, x + rectWidth + 10, y, paint);

                    paint.Color = this.LegendColor.ElementAt(this.LegendNames.IndexOf(legend));
                    var rect = SKRect.Create(x, y - rectWidth, rectWidth, rectWidth);
                    canvas.DrawRect(rect, paint);

                    x += (rectWidth * 2) + (this.LabelTextSize * ((legend.Length / 2) + 2));
                }

                var minPoint = this.points.Min(p => p.Y);
                var maxPoint = 700;

                paint.Color = SKColor.Parse("#000000");
                paint.TextSize = 20;

                var step = (maxPoint - minPoint) / 5;
                var valueStep = this.multibarMax / 5;
                for (int i = 0; i < 6; i++)
                {
                    canvas.DrawCircle(12, maxPoint - (step * i), 7, paint);
                    canvas.DrawText(Math.Round(valueStep * i, 1).ToString(), 0, maxPoint - (step * i) - 20, paint);
                }
            }
        }

        private SKPoint[] CalculateMultilinePoints(SKSize itemSize, float headerHeight, int param)
        {
            var result = new List<SKPoint>();

            for (int i = 0; i < this.Entries.Count(); i++)
            {
                var entry = this.Entries.ElementAt(i);
                var x = this.Margin + (itemSize.Width / 2) + (i * (itemSize.Width + this.Margin)) + (param * 20);
                var y = headerHeight + ((this.multibarMax - entry.Value) / (this.multibarMax - this.multibarMin) * itemSize.Height);
                var point = new SKPoint(x, y);
                result.Add(point);
            }

            return result.ToArray();
        }

        protected void DrawBars(SKCanvas canvas, SKPoint[] points, SKSize itemSize, float origin, float headerHeight)
        {
            const float MinBarHeight = 0;
            if (points.Length > 0)
            {
                for (int i = 0; i < this.Entries.Count(); i++)
                {
                    var entry = this.Entries.ElementAt(i);
                    var point = points[i];

                    using (var paint = new SKPaint
                    {
                        Style = SKPaintStyle.Fill,
                        Color = entry.Color,
                    })
                    {
                        var x = point.X - (itemSize.Width / 6);
                        var y = Math.Min(origin, point.Y);
                        var height = Math.Max(MinBarHeight, Math.Abs(origin - point.Y));
                        if (height < MinBarHeight)
                        {
                            height = MinBarHeight;
                            if (y + height > this.Margin + itemSize.Height)
                            {
                                y = headerHeight + itemSize.Height - height;
                            }
                        }

                        var rect = SKRect.Create(x, y, itemSize.Width / 4, height);
                        canvas.DrawRect(rect, paint);
                    }
                }
            }
        }
    }
}
