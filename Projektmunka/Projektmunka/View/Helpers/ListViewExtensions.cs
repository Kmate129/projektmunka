﻿// <copyright file="ListViewExtensions.cs" company="OE-NIK">
// Copyright (c) OE-NIK. All rights reserved.
// </copyright>

using System.Windows.Input;
using Xamarin.Forms;

namespace Projektmunka.View.Helpers
{
    public static class ListViewExtensions
    {
        public static readonly BindableProperty ItemTappedCommandProperty =
            BindableProperty.CreateAttached("ItemTappedCommand", typeof(ICommand), typeof(ListViewExtensions), null, propertyChanged: OnItemTappedCommandPropertyChanged);

        public static ICommand GetItemTappedCommand(BindableObject bindable)
        {
            return (ICommand)bindable.GetValue(ItemTappedCommandProperty);
        }

        public static void SetItemTappedCommand(BindableObject bindable, ICommand value)
        {
            bindable.SetValue(ItemTappedCommandProperty, value);
        }

        private static void OnItemTappedCommandPropertyChanged(BindableObject bindable, object oldValue, object newValue)
        {
            if (bindable is ListView listView)
            {
                listView.ItemTapped += (sender, args) =>
                {
                    if (args.Item != null && newValue is ICommand command && command.CanExecute(args.Item))
                    {
                        command.Execute(args.Item);
                    }

                    listView.SelectedItem = null;
                };
            }
        }
    }
}
