﻿// <copyright file="ExerciseDetailViewModel.cs" company="OE-NIK">
// Copyright (c) OE-NIK. All rights reserved.
// </copyright>

using System.Windows.Input;
using CommunityToolkit.Mvvm.ComponentModel;
using CommunityToolkit.Mvvm.Input;
using Projektmunka.Data.Enums;
using Projektmunka.Data.Model;
using Projektmunka.Logic.Interfaces;
using Projektmunka.Platform.Interfaces;
using Projektmunka.View.Mappers;
using Projektmunka.View.Models;
using Xamarin.Forms;

namespace Projektmunka.View.ViewModels
{
    /// <summary>
    /// Viewmodel to ExerciseDetailPage view.
    /// </summary>
    public class ExerciseDetailViewModel : ObservableObject
    {
        private readonly ITrainingPlanHandler logic;
        private readonly IMapper<OwnExercise, OwnExerciseDto> mapper;
        private OwnExerciseDto ownExercise;

        /// <summary>
        /// Initializes a new instance of the <see cref="ExerciseDetailViewModel"/> class.
        /// </summary>
        public ExerciseDetailViewModel()
            : this(
                DependencyService.Get<ITrainingPlanHandler>(), DependencyService.Get<IMapper<OwnExercise, OwnExerciseDto>>(), DependencyService.Get<IPreferences>())
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ExerciseDetailViewModel"/> class.
        /// </summary>
        private ExerciseDetailViewModel(ITrainingPlanHandler logic, IMapper<OwnExercise, OwnExerciseDto> mapper, IPreferences preferences)
        {
            this.logic = logic;
            this.mapper = mapper;
            this.SuperDislikeCommand = new RelayCommand(() => { this.RateMyExercise(RateFlag.SuperDislike); });
            this.DislikeCommand = new RelayCommand(() => { this.RateMyExercise(RateFlag.Dislike); });
            this.NeutralCommand = new RelayCommand(() => { this.RateMyExercise(RateFlag.Neutral); });
            this.LikeCommand = new RelayCommand(() => { this.RateMyExercise(RateFlag.Like); });
            this.SuperLikeCommand = new RelayCommand(() => { this.RateMyExercise(RateFlag.SuperLike); });
            var id = int.Parse(preferences.Get("exercise_detail_id", "1"));
            preferences.Remove("exercise_detail_id");
            this.Refresh(id);
        }

        /// <summary>
        /// Gets or sets exercise to display.
        /// </summary>
        public OwnExerciseDto OwnExercise
        {
            get { return this.ownExercise; }
            set { this.SetProperty(ref this.ownExercise, value); }
        }

        /// <summary>
        /// Gets command of registration.
        /// </summary>
        public ICommand SuperDislikeCommand { get; private set; }

        /// <summary>
        /// Gets command of registration.
        /// </summary>
        public ICommand DislikeCommand { get; private set; }

        /// <summary>
        /// Gets command of registration.
        /// </summary>
        public ICommand NeutralCommand { get; private set; }

        /// <summary>
        /// Gets command of registration.
        /// </summary>
        public ICommand LikeCommand { get; private set; }

        /// <summary>
        /// Gets command of registration.
        /// </summary>
        public ICommand SuperLikeCommand { get; private set; }

        /// <summary>
        /// Refreshes.
        /// </summary>
        public void Refresh(int id)
        {
            this.OwnExercise = this.mapper.Map(this.logic.GetOwnExerciseById(id));
        }

        private void RateMyExercise(RateFlag flag)
        {
            this.logic.RateExercise(this.ownExercise.Id, flag);
            this.Refresh(this.OwnExercise.Id);
        }
    }
}
