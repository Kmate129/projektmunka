﻿// <copyright file="DevicesViewModel.cs" company="OE-NIK">
// Copyright (c) OE-NIK. All rights reserved.
// </copyright>

using System;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using System.Windows.Input;
using CommunityToolkit.Mvvm.Input;
using Plugin.BLE;
using Plugin.BLE.Abstractions.Contracts;
using Plugin.BLE.Abstractions.Exceptions;

namespace Projektmunka.View.ViewModels
{
    public class DevicesViewModel
    {
        public DevicesViewModel()
        {
            // ble = CrossBluetoothLE.Current;
            // adapter = CrossBluetoothLE.Current.Adapter;
            // adapter.ScanMode = ScanMode.Balanced;
            // adapter.ScanTimeout = 30000;
            // deviceList = new ObservableCollection<IDevice>();
            // lv.ItemsSource = deviceList;
            // adapter.DeviceDiscovered += _bleAdapterDeviceDiscovered;
            this.BTScanCommand = new RelayCommand(async () => { await this.BtScan(); });
            this.BTConnectCommand = new RelayCommand(async () => { await this.BtConnect(); });
        }

        /// <summary>
        /// Gets or sets connection message.
        /// </summary>
        public string ConnectMessage { get; set; }

        /// <summary>
        /// Gets command of bluetooth scan.
        /// </summary>
        public ICommand BTScanCommand { get; }

        /// <summary>
        /// Gets command of bluetooth connection.
        /// </summary>
        public ICommand BTConnectCommand { get; }

        private async Task BtScan()
        {
            try
            {
                // this.deviceList.Clear();
                // this.adapter.DeviceDiscovered += (s, a) =>
                // {
                // this.deviceList.Add(a.Device);
                // };

                // if (!this.ble.Adapter.IsScanning)
                // {
                // await this.adapter.StartScanningForDevicesAsync();
                // }
            }
            catch (Exception ex)
            {
                this.ConnectMessage = ex.Message;
            }
        }

        private async Task BtConnect()
        {
            try
            {
                /*if (this.device != null)
                {
                    await this.adapter.ConnectToDeviceAsync(this.device);
                }
                else
                {
                    this.ConnectMessage = "No Device selected!";
                }*/
            }
            catch (DeviceConnectionException ex)
            {
                this.ConnectMessage = ex.Message;
            }
        }
    }
}
