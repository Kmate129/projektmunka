﻿// <copyright file="TrainingSetupViewModel.cs" company="OE-NIK">
// Copyright (c) OE-NIK. All rights reserved.
// </copyright>

using System.Threading.Tasks;
using System.Windows.Input;
using CommunityToolkit.Mvvm.ComponentModel;
using CommunityToolkit.Mvvm.Input;
using Projektmunka.Data.Model;
using Projektmunka.Logic.Interfaces;
using Projektmunka.Platform.Interfaces;
using Projektmunka.Repository.Interfaces;
using Projektmunka.View.Views;
using Xamarin.Forms;

namespace Projektmunka.View.ViewModels
{
    /// <summary>
    /// Viewmodel to TrainingSetup view.
    /// </summary>
    public class TrainingSetupViewModel : ObservableObject
    {
        private readonly ITimeManager timeManager;
        private readonly INavigationService navigation;
        private readonly IUserRepository repo;
        private readonly IDailyPlanControl dailyPlanControl;

        /// <summary>
        /// Initializes a new instance of the <see cref="TrainingSetupViewModel"/> class.
        /// </summary>
        public TrainingSetupViewModel()
            : this(DependencyService.Get<IBle>(), DependencyService.Get<ITimeManager>(), DependencyService.Get<INavigationService>(), DependencyService.Get<IUserRepository>(), DependencyService.Get<IDailyPlanControl>())
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="TrainingSetupViewModel"/> class.
        /// </summary>
        /// <param name="bleLogic">Bluetooth logic.</param>
        /// <param name="timeManager">Time manager.</param>
        public TrainingSetupViewModel(IBle bleLogic, ITimeManager timeManager, INavigationService navigation, IUserRepository userRepository, IDailyPlanControl dailyPlanControl)
        {
            this.timeManager = timeManager;
            this.navigation = navigation;
            this.repo = userRepository;
            this.dailyPlanControl = dailyPlanControl;
            this.GoToDevicesCommand = new RelayCommand(() => { this.NavigateToDevicesPage(); });
            this.StartWorkoutCommand = new RelayCommand(async () => { await this.NavigateToWorkout(); });

            if (bleLogic.ConnectedDevice == null)
            {
                this.Message = "You have not connected Bluetooth device yet\nSet up or start the workout without dynamic HR monitoring";
            }
            else
            {
                this.Message = $"Your Bluetooth device to use is {bleLogic.ConnectedDevice.Name}";
            }
        }

        /// <summary>
        /// Gets or sets message to user.
        /// </summary>
        public string Message { get; set; }

        /// <summary>
        /// Gets navigation command to devices menu.
        /// </summary>
        public ICommand GoToDevicesCommand { get; }

        /// <summary>
        /// Gets command to start workout.
        /// </summary>
        public ICommand StartWorkoutCommand { get; }

        private void NavigateToDevicesPage()
        {
            this.navigation.ShellGoToAsync("///devices");
        }

        private async Task NavigateToWorkout()
        {
            this.timeManager.StartTotalTimer();
            this.timeManager.ClearActiveTimer();

            if (this.repo.GetEntityType(this.dailyPlanControl.GetCurrentExercise().OwnExercise.Exercise).Equals(typeof(RepExercise)))
            {
                DailyPlanRepExercisePage newPage = new DailyPlanRepExercisePage();
                (newPage.BindingContext as DailyPlanRepExerciseViewModel).Counter = 0;
                await this.navigation.PushAsync(newPage, false);
            }
            else
            {
                DailyPlanPerExercisePage newPage = new DailyPlanPerExercisePage();
                (newPage.BindingContext as DailyPlanPerExerciseViewModel).Counter = 0;
                await this.navigation.PushAsync(newPage, false);
            }
        }
    }
}
