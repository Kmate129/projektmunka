﻿// <copyright file="DailyPlanDoneViewModel.cs" company="OE-NIK">
// Copyright (c) OE-NIK. All rights reserved.
// </copyright>

using System;
using System.Windows.Input;
using CommunityToolkit.Mvvm.Input;
using Projektmunka.Logic.Interfaces;
using Projektmunka.Platform.Interfaces;
using Projektmunka.View.Views;
using Xamarin.Forms;

namespace Projektmunka.View.ViewModels
{
    /// <summary>
    /// Viewmodel to DailyPlanDonePage.
    /// </summary>
    public class DailyPlanDoneViewModel
    {
        private readonly INavigationService navigation;
        private readonly IDailyPlanControl dailyPlanControl;

        /// <summary>
        /// Initializes a new instance of the <see cref="DailyPlanDoneViewModel"/> class.
        /// </summary>
        public DailyPlanDoneViewModel()
            : this(DependencyService.Get<ITimeManager>(), DependencyService.Get<INavigationService>(), DependencyService.Get<IDailyPlanControl>())
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="DailyPlanDoneViewModel"/> class.
        /// </summary>
        /// <param name="timeManager">Time manager.</param>
        /// <param name="navigation">Navigation service.</param>
        public DailyPlanDoneViewModel(ITimeManager timeManager, INavigationService navigation, IDailyPlanControl dailyPlanControl)
        {
            this.navigation = navigation;
            this.dailyPlanControl = dailyPlanControl;
            this.BackButtonPressed = new RelayCommand(() => { this.Navigation(); });

            timeManager.StopTotalTimer();
            var total = timeManager.GetTotalTime();
            var active = timeManager.GetActiveTime();

            this.TotalTime = "Total time: " + this.Formalize(total);
            this.ActiveTime = "Active time: " + this.Formalize(active);
            timeManager.StoreDailyTotalTime(total);
            timeManager.StoreDailyActiveTime(active);
        }

        /// <summary>
        /// Gets or sets total time spent with training.
        /// </summary>
        public string TotalTime { get; set; }

        /// <summary>
        /// Gets or sets time spent with doing exercise.
        /// </summary>
        public string ActiveTime { get; set; }

        /// <summary>
        /// Gets or sets back button pressed command.
        /// </summary>
        public ICommand BackButtonPressed { get; set; }

        private void Navigation()
        {
            if (this.dailyPlanControl.IsCurrentExerciseTheFirstInWeek())
            {
                this.navigation.PushAsync(new WeeklyPlanDonePage());
            }
            else
            {
                this.navigation.NewAppShell();
            }
        }

        private string Formalize(TimeSpan timespan)
        {
            return timespan.Hours + "h:" + timespan.Minutes + "m:" + timespan.Seconds + "s";
        }
    }
}
