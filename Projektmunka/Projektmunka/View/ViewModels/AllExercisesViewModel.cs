﻿// <copyright file="AllExercisesViewModel.cs" company="OE-NIK">
// Copyright (c) OE-NIK. All rights reserved.
// </copyright>

using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Input;
using CommunityToolkit.Mvvm.ComponentModel;
using CommunityToolkit.Mvvm.Input;
using Projektmunka.Data.Model;
using Projektmunka.Platform.Interfaces;
using Projektmunka.View.Mappers;
using Projektmunka.View.Models;
using Projektmunka.View.Views;
using Xamarin.Forms;
using Xamarin.Forms.Internals;

namespace Projektmunka.View.ViewModels
{
    /// <summary>
    /// Viewmodel to AllExercisesPage view.
    /// </summary>
    public class AllExercisesViewModel : ObservableObject
    {
        private readonly IAppProvider appProvider;
        private readonly INavigationService navigation;
        private readonly IPreferences preferences;
        private readonly IMapper<OwnExercise, OwnExerciseDto> mapper;
        private ObservableCollection<OwnExerciseDto> myExercises;

        /// <summary>
        /// Initializes a new instance of the <see cref="AllExercisesViewModel"/> class.
        /// </summary>
        public AllExercisesViewModel()
            : this(DependencyService.Get<IMapper<OwnExercise, OwnExerciseDto>>(), DependencyService.Get<IAppProvider>(), DependencyService.Get<INavigationService>(), DependencyService.Get<IPreferences>())
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="AllExercisesViewModel"/> class.
        /// </summary>
        /// <param name="mapper">IMapper.</param>
        /// <param name="appProvider">Application wrapper.</param>
        private AllExercisesViewModel(IMapper<OwnExercise, OwnExerciseDto> mapper, IAppProvider appProvider, INavigationService navigation, IPreferences preferences)
        {
            this.appProvider = appProvider;
            this.navigation = navigation;
            this.preferences = preferences;
            this.mapper = mapper;
            this.OwnExercises = new ObservableCollection<OwnExerciseDto>();
            this.NavigationCommand = new RelayCommand<OwnExerciseDto>(async (exercise) => { await this.Navigate(exercise); });
            this.SetExerciseList();
        }

        /// <summary>
        /// Gets or sets exercises to display.
        /// </summary>
        public ObservableCollection<OwnExerciseDto> OwnExercises
        {
            get { return this.myExercises; }
            set { this.SetProperty(ref this.myExercises, value); }
        }

        public ICommand NavigationCommand { get; set; }

        private async Task Navigate(OwnExerciseDto exercise)
        {
            var id = exercise.Id;
            this.preferences.Set("exercise_detail_id", id.ToString());
            await this.navigation.PushAsync(new ExerciseDetailPage());
        }

        /// <summary>
        /// Refreshes collection of exercises to display.
        /// </summary>
        private void SetExerciseList()
        {
            this.appProvider.GetApp().CurrentUser.OwnExercises.OrderBy(x => x.Exercise.Name).ForEach(x => this.OwnExercises.Add(this.mapper.Map(x)));
        }
    }
}
