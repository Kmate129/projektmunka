﻿// <copyright file="DailyPlanRepExerciseViewModel.cs" company="OE-NIK">
// Copyright (c) OE-NIK. All rights reserved.
// </copyright>

using Projektmunka.BLE.Interfaces;
using Projektmunka.Data.Model;
using Projektmunka.Logic;
using Projektmunka.Logic.Interfaces;
using Projektmunka.Platform.Interfaces;
using Projektmunka.View.Mappers;
using Projektmunka.View.Models;
using Xamarin.Forms;

namespace Projektmunka.View.ViewModels
{
    /// <summary>
    /// Viewmodel to DailyPlanDetail view.
    /// </summary>
    public class DailyPlanRepExerciseViewModel : DailyPlanExerciseViewModel
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="DailyPlanRepExerciseViewModel"/> class.
        /// </summary>
        public DailyPlanRepExerciseViewModel()
            : this(DependencyService.Get<IDailyPlanControl>(), DependencyService.Get<ITimeManager>(), DependencyService.Get<IMapper<ExerciseDetail, ExerciseDto>>(), DependencyService.Get<INavigationService>(), DependencyService.Get<IHrRegulator>(), DependencyService.Get<IAppProvider>(), DependencyService.Get<ITrainingPlanHandler>())
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="DailyPlanRepExerciseViewModel"/> class.
        /// </summary>
        /// <param name="dailyPlanControl">Daily plan controller.</param>
        /// <param name="timeManager">Time manager.</param>
        /// <param name="exerciseMapper">Exercise mapper.</param>
        public DailyPlanRepExerciseViewModel(IDailyPlanControl dailyPlanControl, ITimeManager timeManager, IMapper<ExerciseDetail, ExerciseDto> exerciseMapper, INavigationService navigation, IHrRegulator hr, IAppProvider appProvider, ITrainingPlanHandler trainingPlanHandler)
            : base(dailyPlanControl, timeManager, exerciseMapper, navigation, hr, appProvider, trainingPlanHandler)
        {
        }
    }
}
