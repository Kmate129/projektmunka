﻿// <copyright file="WeeklyPlanDoneViewModel.cs" company="OE-NIK">
// Copyright (c) OE-NIK. All rights reserved.
// </copyright>

using System;
using System.Linq;
using System.Reflection;
using System.Windows.Input;
using CommunityToolkit.Mvvm.ComponentModel;
using CommunityToolkit.Mvvm.Input;
using Projektmunka.Data.Enums;
using Projektmunka.Data.Model;
using Projektmunka.Logic.Interfaces;
using Projektmunka.Platform.Interfaces;
using Projektmunka.Repository.Interfaces;
using Projektmunka.View.Validators;
using Xamarin.Forms;

namespace Projektmunka.View.ViewModels
{
    /// <summary>
    /// Viewmodel to WeeklyPlanDonePage.
    /// </summary>
    public class WeeklyPlanDoneViewModel : ObservableObject
    {
        private readonly INavigationService navigation;
        private readonly IAppProvider appProvider;
        private readonly IUserRepository repository;
        private readonly IUserComposer composer;
        private readonly WeightValidation validation;
        private float weight;
        private string weightError;
        private float seconds;

        /// <summary>
        /// Initializes a new instance of the <see cref="WeeklyPlanDoneViewModel"/> class.
        /// </summary>
        public WeeklyPlanDoneViewModel()
            : this(DependencyService.Get<INavigationService>(), DependencyService.Get<IAppProvider>(), DependencyService.Get<IUserRepository>(), DependencyService.Get<WeightValidation>(), DependencyService.Get<IUserComposer>(), DependencyService.Get<ITimeManager>())
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="WeeklyPlanDoneViewModel"/> class.
        /// </summary>
        /// <param name="navigation">Navigation service.</param>
        /// <param name="appProvider">Application wrapper.</param>
        /// <param name="validation">Validator.</param>
        public WeeklyPlanDoneViewModel(INavigationService navigation, IAppProvider appProvider, IUserRepository repository, WeightValidation validation, IUserComposer composer, ITimeManager timeManager)
        {
            this.navigation = navigation;
            this.appProvider = appProvider;
            this.repository = repository;
            this.validation = validation;
            this.composer = composer;
            this.WeightSubmitted = new RelayCommand(() => { this.SetWeight(); });
            this.Seconds = timeManager.GetWeeklyTotalTime();

            timeManager.StoreAverages();
        }

        /// <summary>
        /// Gets or sets total seconds spent with doing sport.
        /// </summary>
        public float Seconds
        {
            get => this.seconds;
            set { this.SetProperty(ref this.seconds, value); }
        }

        /// <summary>
        /// Gets or sets new weight.
        /// </summary>
        public float Weight
        {
            get => this.weight;
            set { this.SetProperty(ref this.weight, value); }
        }

        /// <summary>
        /// Gets or sets username error message.
        /// </summary>
        public string WeightError
        {
            get { return this.weightError; }
            set { this.SetProperty(ref this.weightError, value); }
        }

        /// <summary>
        /// Gets or sets submition command.
        /// </summary>
        public ICommand WeightSubmitted { get; set; }

        private void SetWeight()
        {
            var result = this.validation.Validate(this);
            if (result.IsValid)
            {
                this.appProvider.GetApp().CurrentUser.WeightHistory.Insert(this.Weight);
                this.repository.UpdateUser(this.appProvider.GetApp().CurrentUser);
            }

            var weighterror = result.Errors.Where(x => x.PropertyName.Equals("Weight"));
            if (weighterror.Any())
            {
                this.WeightError = weighterror.First().ErrorMessage.ToString();
            }
            else
            {
                this.WeightError = string.Empty.ToString();
            }

            this.composer.RegenerateNutritionScheme(this.appProvider.GetApp().CurrentUser);
            this.composer.RegenerateTrainingPlan(this.appProvider.GetApp().CurrentUser);
            this.repository.UpdateUser(this.appProvider.GetApp().CurrentUser);

            this.navigation.NewAppShell();
        }
    }
}
