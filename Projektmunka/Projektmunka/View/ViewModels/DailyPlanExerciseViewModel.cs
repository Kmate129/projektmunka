﻿// <copyright file="DailyPlanExerciseViewModel.cs" company="OE-NIK">
// Copyright (c) OE-NIK. All rights reserved.
// </copyright>

using System.Threading.Tasks;
using System.Windows.Input;
using CommunityToolkit.Mvvm.ComponentModel;
using CommunityToolkit.Mvvm.Input;
using Projektmunka.BLE.Interfaces;
using Projektmunka.Data.Model;
using Projektmunka.Logic.Interfaces;
using Projektmunka.Platform.Interfaces;
using Projektmunka.View.Mappers;
using Projektmunka.View.Models;
using Projektmunka.View.Views;

namespace Projektmunka.View.ViewModels
{
    /// <summary>
    /// Abstract viewmodel to DailyPlan workout display.
    /// </summary>
    public abstract class DailyPlanExerciseViewModel : ObservableObject
    {
        private readonly IDailyPlanControl dailyPlanControl;
        private readonly ITimeManager timeManager;
        private readonly INavigationService navigation;
        private readonly IHrRegulator hr;
        private readonly IAppProvider appProvider;
        private readonly ITrainingPlanHandler trainingPlanHandler;
        private readonly int round;
        private int counter;

        /// <summary>
        /// Initializes a new instance of the <see cref="DailyPlanExerciseViewModel"/> class.
        /// </summary>
        /// <param name="dailyPlanControl">Daily plan controller.</param>
        /// <param name="timeManager">Time manager.</param>
        /// <param name="exerciseMapper">Exercise mapper.</param>
        /// <param name="navigation">Navigation service.</param>
        protected DailyPlanExerciseViewModel(IDailyPlanControl dailyPlanControl, ITimeManager timeManager, IMapper<ExerciseDetail, ExerciseDto> exerciseMapper, INavigationService navigation, IHrRegulator hr, IAppProvider appProvider, ITrainingPlanHandler trainingPlanHandler)
        {
            this.timeManager = timeManager;
            this.dailyPlanControl = dailyPlanControl;
            this.navigation = navigation;
            this.hr = hr;
            this.appProvider = appProvider;
            this.trainingPlanHandler = trainingPlanHandler;
            this.DoneCommand = new RelayCommand(async () => { await this.Done(); });

            this.Exercise = exerciseMapper.Map(this.dailyPlanControl.GetCurrentExercise());
            this.round = this.dailyPlanControl.GetCurrentExercise().OwnExercise.Exercise.BaseRound;

            this.timeManager.StartActiveTimer();
            //this.hr.StartCollectingData();
        }

        /// <summary>
        /// Gets or sets daily plan DTO.
        /// </summary>
        public ExerciseDto Exercise { get; set; }

        /// <summary>
        /// Gets or sets round counter.
        /// </summary>
        public int Counter
        {
            get { return this.counter; }
            set { this.SetProperty(ref this.counter, value); }
        }

        /// <summary>
        /// Gets or sets command to finish exercise.
        /// </summary>
        public ICommand DoneCommand { get; set; }

        private async Task Done()
        {
            this.timeManager.StopAndAddActiveTimer();
            //this.hr.StopCollectingData();
            //this.trainingPlanHandler.ModifyExercise(this.dailyPlanControl.GetCurrentExercise(), this.hr.GetPriodIntensity(this.appProvider.GetApp().CurrentUser.Age));
            if (this.Counter == this.round - 1 && this.dailyPlanControl.IsCurrentExerciseTheLastInDailyPlan())
            {
                this.dailyPlanControl.CurrentExerciseDone();
                await this.navigation.PushAsync(new DailyPlanDonePage(), false);
            }
            else
            {
                DailyPlanRestPage newPage = new DailyPlanRestPage();
                (newPage.BindingContext as DailyPlanRestViewModel).Counter = this.Counter;
                await this.navigation.PushAsync(newPage, false);
            }
        }
    }
}
