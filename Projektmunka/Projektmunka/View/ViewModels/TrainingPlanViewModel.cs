﻿// <copyright file="TrainingPlanViewModel.cs" company="OE-NIK">
// Copyright (c) OE-NIK. All rights reserved.
// </copyright>

using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Input;
using CommunityToolkit.Mvvm.ComponentModel;
using Projektmunka.Data.Model;
using Projektmunka.Platform.Interfaces;
using Projektmunka.View.Mappers;
using Projektmunka.View.Models;
using Projektmunka.View.Views;
using Xamarin.Forms;

namespace Projektmunka.View.ViewModels
{
    /// <summary>
    /// Viewmodel to TrainingPlan view.
    /// </summary>
    public class TrainingPlanViewModel : ObservableObject
    {
        private readonly IAppProvider appProvider;
        private readonly INavigationService navigation;
        private readonly IMapper<TrainingPlan, ICollection<WeeklyPlanDto>> mapper;

        /// <summary>
        /// Initializes a new instance of the <see cref="TrainingPlanViewModel"/> class.
        /// </summary>
        public TrainingPlanViewModel()
            : this(DependencyService.Get<IMapper<TrainingPlan, ICollection<WeeklyPlanDto>>>(), DependencyService.Get<IAppProvider>(), DependencyService.Get<INavigationService>())
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="TrainingPlanViewModel"/> class.
        /// </summary>
        /// <param name="mapper">IMapper.</param>
        /// <param name="appProvider">Application wrapper.</param>
        private TrainingPlanViewModel(IMapper<TrainingPlan, ICollection<WeeklyPlanDto>> mapper, IAppProvider appProvider, INavigationService navigation)
        {
            this.appProvider = appProvider;
            this.navigation = navigation;
            this.WeeklyPlans = new ObservableCollection<WeeklyPlanDto>();
            this.mapper = mapper;
            this.NavigationCommand = new Command(async () => { await this.Navigation(); });

            this.RefreshPlan();
        }

        /// <summary>
        /// Gets or sets navigation command.
        /// </summary>
        public ICommand NavigationCommand { get; set; }

        /// <summary>
        /// Gets or sets weekly plans.
        /// </summary>
        public ObservableCollection<WeeklyPlanDto> WeeklyPlans { get; set; }

        private void RefreshPlan()
        {
            this.WeeklyPlans.Clear();
            foreach (WeeklyPlanDto weekly in this.mapper.Map(this.appProvider.GetApp().CurrentUser.TrainingPlan))
            {
                this.WeeklyPlans.Add(weekly);
            }
        }

        private async Task Navigation()
        {
            await this.navigation.PushAsync(new TrainingSetupPage());
        }
    }
}
