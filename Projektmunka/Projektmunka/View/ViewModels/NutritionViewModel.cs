﻿// <copyright file="NutritionViewModel.cs" company="OE-NIK">
// Copyright (c) OE-NIK. All rights reserved.
// </copyright>

using CommunityToolkit.Mvvm.ComponentModel;
using Projektmunka.Data.Model;
using Projektmunka.Platform.Interfaces;
using Xamarin.Forms;

namespace Projektmunka.View.ViewModels
{
    /// <summary>
    /// Viewmodel to NutritionPage view.
    /// </summary>
    public class NutritionViewModel : ObservableObject
    {
        private readonly IAppProvider appProvider;
        private NutritionScheme nutritionScheme;

        /// <summary>
        /// Initializes a new instance of the <see cref="NutritionViewModel"/> class.
        /// </summary>
        public NutritionViewModel()
            : this(DependencyService.Get<IAppProvider>())
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="NutritionViewModel"/> class.
        /// </summary>
        /// <param name="appProvider">Application wrapper.</param>
        public NutritionViewModel(IAppProvider appProvider)
        {
            this.appProvider = appProvider;
            this.OnCreating();
        }

        /// <summary>
        /// Gets or sets nutrition scheme.
        /// </summary>
        public NutritionScheme NutritionScheme
        {
            get { return this.nutritionScheme; }
            set { this.SetProperty(ref this.nutritionScheme, value); }
        }

        private void OnCreating()
        {
            this.nutritionScheme = this.appProvider.GetApp().CurrentUser.Nutrients;
        }
    }
}
