﻿// <copyright file="StatisticPageViewModel.cs" company="OE-NIK">
// Copyright (c) OE-NIK. All rights reserved.
// </copyright>

using System.Collections.Generic;
using System.Linq;
using CommunityToolkit.Mvvm.ComponentModel;
using Microcharts;
using Projektmunka.Platform.Interfaces;
using Projektmunka.View.Helpers;
using SkiaSharp;
using Xamarin.Forms;

namespace Projektmunka.View.ViewModels
{
    /// <summary>
    /// Viewmodel of statistics page.
    /// </summary>
    public class StatisticPageViewModel : ObservableObject
    {
        private SKColor grayColor = SKColor.Parse("#808080");
        private SKColor blueColor = SKColor.Parse("#0000FF");
        private SKColor greenColor = SKColor.Parse("#3AEB34");
        private SKColor transparent = SKColor.Parse("00FFFFFF");
        private MultiLinesChart multiLinesChart;
        private MultiBarChart multiBarChart;
        private string[] weeks;
        private float[] expected;
        private float[] real;
        private int[] total;
        private int[] active;

        /// <summary>
        /// Initializes a new instance of the <see cref="StatisticPageViewModel"/> class.
        /// </summary>
        public StatisticPageViewModel()
            : this(DependencyService.Get<IAppProvider>())
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="StatisticPageViewModel"/> class.
        /// </summary>
        /// <param name="appProvider">Application wrapper.</param>
        public StatisticPageViewModel(IAppProvider appProvider)
        {
            var weightHistory = appProvider.GetApp().CurrentUser.WeightHistory;
            this.expected = weightHistory.GetExpectedValues();
            this.real = weightHistory.GetRealValues();
            this.weeks = new string[this.real.Length];
            for (int i = 0; i < this.expected.Length; i++)
            {
                this.weeks[i] = $"{i}.";
            }

            this.InitWeightData();

            var timeHistory = appProvider.GetApp().CurrentUser.TimeHistory;
            this.total = timeHistory.GetTotalValues();
            this.active = timeHistory.GetActiveValues();

            this.InitTimeData();
        }

        /// <summary>
        /// Gets or sets multi lines chart.
        /// </summary>
        public MultiLinesChart MultiLinesChart
        {
            get { return this.multiLinesChart; }
            set { this.SetProperty(ref this.multiLinesChart, value); }
        }

        /// <summary>
        /// Gets or sets multi bars chart.
        /// </summary>
        public MultiBarChart MultiBarsChart
        {
            get { return this.multiBarChart; }
            set { this.SetProperty(ref this.multiBarChart, value); }
        }

        private void InitWeightData()
        {
            var entries = new List<List<ChartEntry>>();
            List<ChartEntry> expectedEntries = new List<ChartEntry>();
            List<ChartEntry> realEntries = new List<ChartEntry>();

            int i = 0;
            foreach (var data in this.expected)
            {
                expectedEntries.Add(new ChartEntry(data)
                {
                    Color = this.grayColor,
                    ValueLabel = $"{data}",
                    Label = this.weeks[i],
                });

                i++;
            }

            i = 0;
            foreach (var data in this.real)
            {
                if (data != 0f)
                {
                    realEntries.Add(new ChartEntry(data)
                    {
                        Color = this.blueColor,
                        ValueLabel = $"{data}",
                        Label = this.weeks[i],
                    });

                    i++;
                }
            }

            entries.Add(expectedEntries);
            entries.Add(realEntries);

            this.MultiLinesChart = new MultiLinesChart()
            {
                MultiLineEntries = entries,
                LabelTextSize = 30f,
                LabelOrientation = Orientation.Horizontal,
                LineAreaAlpha = 0,
                PointAreaAlpha = 0,
                LegendNames = new List<string>() { "Expected weight", "Real weight" },
                IsAnimated = false,
            };
        }

        private void InitTimeData()
        {
            var entries = new List<List<ChartEntry>>();
            List<ChartEntry> totalEntries = new List<ChartEntry>();
            List<ChartEntry> activeEntries = new List<ChartEntry>();

            bool insertFake = false;
            if (this.total.All(x => x.Equals(0)) && this.active.All(x => x.Equals(0)))
            {
                insertFake = true;
            }

            int i = 0;
            foreach (var data in this.total)
            {
                if (insertFake && i == 0)
                {
                    totalEntries.Add(new ChartEntry(40)
                    {
                        Color = this.transparent,
                        ValueLabel = $"{data}",
                        Label = this.weeks[i + 1],
                    });
                }
                else
                {
                    if(data == 0)
                    {
                        totalEntries.Add(new ChartEntry(data)
                        {
                            Color = this.transparent,
                            ValueLabel = $"{data}",
                            Label = this.weeks[i + 1],
                        });
                    }
                    else
                    {
                        totalEntries.Add(new ChartEntry(data)
                        {
                            Color = this.blueColor,
                            ValueLabel = $"{data}",
                            Label = this.weeks[i + 1],
                        });
                    }
                }

                i++;
            }

            i = 0;
            foreach (var data in this.active)
            {
                if (data == 0)
                {
                    activeEntries.Add(new ChartEntry(data)
                    {
                        Color = this.transparent,
                        ValueLabel = $"{data}",
                        Label = this.weeks[i + 1],
                    });
                }
                else
                {
                    activeEntries.Add(new ChartEntry(data)
                    {
                        Color = this.greenColor,
                        ValueLabel = $"{data}",
                        Label = this.weeks[i + 1],
                    });
                }

                i++;
            }

            entries.Add(activeEntries);
            entries.Add(totalEntries);

            this.MultiBarsChart = new MultiBarChart()
            {
                LegendColor = new List<SKColor>() { greenColor, blueColor },
                FakeInserted = insertFake,
                MultiBarEntries = entries,
                LabelTextSize = 30f,
                LabelOrientation = Orientation.Horizontal,
                PointAreaAlpha = 0,
                LegendNames = new List<string> { "Active time", "Total time" },
                IsAnimated = false,
            };
        }
    }
}