﻿// <copyright file="HeaderViewModel.cs" company="OE-NIK">
// Copyright (c) OE-NIK. All rights reserved.
// </copyright>

using CommunityToolkit.Mvvm.ComponentModel;
using Projektmunka.Logic.Interfaces;
using Projektmunka.Platform.Interfaces;
using Xamarin.Forms;

namespace Projektmunka.View.ViewModels
{
    /// <summary>
    /// Viewmodel of header element of flyout menu.
    /// </summary>
    public class HeaderViewModel : ObservableObject
    {
        private string username;
        private string avatar;
        private int remainingDailyPlan;

        /// <summary>
        /// Initializes a new instance of the <see cref="HeaderViewModel"/> class.
        /// </summary>
        public HeaderViewModel()
            : this(DependencyService.Get<ITrainingPlanHandler>(), DependencyService.Get<IAppProvider>())
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="HeaderViewModel"/> class.
        /// </summary>
        /// <param name="logic">Training handler.</param>
        /// <param name="appProvider">Application wrapper.</param>
        public HeaderViewModel(ITrainingPlanHandler logic, IAppProvider appProvider)
        {
            if (appProvider.GetApp().CurrentUser != null)
            {
                this.Username = appProvider.GetApp().CurrentUser.Username;
                this.Avatar = appProvider.GetApp().CurrentUser.Avatar;
                this.RemainingDailyPlans = logic.GetCountOfRemainingDailyPlans();
            }
        }

        /// <summary>
        /// Gets or sets username.
        /// </summary>
        public string Username
        {
            get { return this.username; }
            set { this.SetProperty(ref this.username, value); }
        }

        /// <summary>
        /// Gets or sets avatar.
        /// </summary>
        public string Avatar
        {
            get { return this.avatar; }
            set { this.SetProperty(ref this.avatar, value); }
        }

        /// <summary>
        /// Gets or sets remaining daily plans.
        /// </summary>
        public int RemainingDailyPlans
        {
            get { return this.remainingDailyPlan; }
            set { this.SetProperty(ref this.remainingDailyPlan, value); }
        }
    }
}
