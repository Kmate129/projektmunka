﻿// <copyright file="FooterViewModel.cs" company="OE-NIK">
// Copyright (c) OE-NIK. All rights reserved.
// </copyright>

using System.Linq;
using System.Windows.Input;
using CommunityToolkit.Mvvm.Input;
using Projektmunka.Data.Enums;
using Projektmunka.Logic.Interfaces;
using Projektmunka.Platform.Interfaces;
using Projektmunka.View.Views;
using Xamarin.Forms;

namespace Projektmunka.View.ViewModels
{
    /// <summary>
    /// Viewmodel of footer element of flyout menu.
    /// </summary>
    public class FooterViewModel
    {
        private readonly IAppProvider appProvider;
        private readonly IUserManagement logic;

        /// <summary>
        /// Initializes a new instance of the <see cref="FooterViewModel"/> class.
        /// </summary>
        public FooterViewModel()
            : this(DependencyService.Get<IUserManagement>(), DependencyService.Get<IAppProvider>())
        {
        }

        private FooterViewModel(IUserManagement logic, IAppProvider appProvider)
        {
            this.appProvider = appProvider;
            this.logic = logic;
            if (appProvider.GetApp().CurrentUser != null)
            {
                this.LogoutCommand = new RelayCommand(() => { this.Logout(); });
                this.Progression = appProvider.GetApp().CurrentUser.TrainingPlan.GetProgression();
                this.Rate = this.FormatRate();
            }
        }

        /// <summary>
        /// Gets login command.
        /// </summary>
        public ICommand LogoutCommand { get; private set; }

        /// <summary>
        /// Gets user progression in training plan.
        /// </summary>
        public float Progression { get; private set; }

        /// <summary>
        /// Gets rate of done and undone weekly plans.
        /// </summary>
        public string Rate { get; private set; }

        private void Logout()
        {
            this.logic.Logout();
            Shell.Current.CurrentItem = new LoginPage();
        }

        private string FormatRate()
        {
            var all = this.appProvider.GetApp().CurrentUser.TrainingPlan.WeeklyPlans.Count;
            var done = this.appProvider.GetApp().CurrentUser.TrainingPlan.WeeklyPlans.Where(x => x.Status.Equals(Status.Done)).Count();
            return string.Concat("Your progression: ", done, "/", all);
        }
    }
}
