﻿// <copyright file="DailyPlanRestViewModel.cs" company="OE-NIK">
// Copyright (c) OE-NIK. All rights reserved.
// </copyright>

using System;
using System.Threading.Tasks;
using System.Windows.Input;
using CommunityToolkit.Mvvm.ComponentModel;
using CommunityToolkit.Mvvm.Input;
using Projektmunka.Data.Model;
using Projektmunka.Logic.Interfaces;
using Projektmunka.Platform.Interfaces;
using Projektmunka.Repository.Interfaces;
using Projektmunka.View.Views;
using Xamarin.Forms;

namespace Projektmunka.View.ViewModels
{
    /// <summary>
    /// Viewmodel to DailyPlanRestPage.
    /// </summary>
    public class DailyPlanRestViewModel : ObservableObject
    {
        private readonly IDailyPlanControl dailyPlanControl;
        private readonly IUserRepository userRepository;
        private readonly INavigationService navigation;
        private readonly int round;
        private int rest;

        /// <summary>
        /// Initializes a new instance of the <see cref="DailyPlanRestViewModel"/> class.
        /// </summary>
        public DailyPlanRestViewModel()
            : this(DependencyService.Get<IDailyPlanControl>(), DependencyService.Get<IUserRepository>(), DependencyService.Get<INavigationService>())
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="DailyPlanRestViewModel"/> class.
        /// </summary>
        /// <param name="dailyPlanControl">Daily plan controller.</param>
        /// <param name="userRepository">Repository of users.</param>
        /// <param name="navigation">Navigation service.</param>
        public DailyPlanRestViewModel(IDailyPlanControl dailyPlanControl, IUserRepository userRepository, INavigationService navigation)
        {
            this.userRepository = userRepository;
            this.navigation = navigation;
            this.dailyPlanControl = dailyPlanControl;
            this.DoneCommand = new RelayCommand(async () => { await this.Done(); });

            this.round = this.dailyPlanControl.GetCurrentExercise().OwnExercise.Exercise.BaseRound;
            this.RestTime = this.dailyPlanControl.GetCurrentRestTime();
            this.Rest = this.RestTime;

            Device.StartTimer(TimeSpan.FromSeconds(1), () =>
            {
                this.Rest--;
                if (this.Rest == 0)
                {
                    return false;
                }

                return true;
            });
        }

        /// <summary>
        /// Gets or sets rest time.
        /// </summary>
        public int RestTime { get; set; }

        /// <summary>
        /// Gets or sets remaning time to rest.
        /// </summary>
        public int Rest
        {
            get { return this.rest; }
            set { this.SetProperty(ref this.rest, value); }
        }

        /// <summary>
        /// Gets or sets round counter.
        /// </summary>
        public int Counter { get; set; }

        /// <summary>
        /// Gets or sets command to finish exercise.
        /// </summary>
        public ICommand DoneCommand { get; set; }

        private async Task Done()
        {
            this.Counter++;
            if (this.Counter == this.round)
            {
                this.dailyPlanControl.CurrentExerciseDone();
                if (this.userRepository.GetEntityType(this.dailyPlanControl.GetCurrentExercise().OwnExercise.Exercise).Equals(typeof(RepExercise)))
                {
                    DailyPlanRepExercisePage newPage = new DailyPlanRepExercisePage();
                    (newPage.BindingContext as DailyPlanRepExerciseViewModel).Counter = 0;
                    await this.navigation.PushAsync(newPage, false);
                }
                else
                {
                    DailyPlanPerExercisePage newPage = new DailyPlanPerExercisePage();
                    (newPage.BindingContext as DailyPlanPerExerciseViewModel).Counter = 0;
                    await this.navigation.PushAsync(newPage, false);
                }
            }
            else
            {
                if (this.userRepository.GetEntityType(this.dailyPlanControl.GetCurrentExercise().OwnExercise.Exercise).Equals(typeof(RepExercise)))
                {
                    DailyPlanRepExercisePage newPage = new DailyPlanRepExercisePage();
                    (newPage.BindingContext as DailyPlanRepExerciseViewModel).Counter = this.Counter;
                    await this.navigation.PushAsync(newPage, false);
                }
                else
                {
                    DailyPlanPerExercisePage newPage = new DailyPlanPerExercisePage();
                    (newPage.BindingContext as DailyPlanPerExerciseViewModel).Counter = this.Counter;
                    await this.navigation.PushAsync(newPage, false);
                }
            }
        }
    }
}
