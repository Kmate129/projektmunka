﻿// <copyright file="LoginViewModel.cs" company="OE-NIK">
// Copyright (c) OE-NIK. All rights reserved.
// </copyright>

using System.Linq;
using System.Threading.Tasks;
using System.Windows.Input;
using CommunityToolkit.Mvvm.ComponentModel;
using CommunityToolkit.Mvvm.Input;
using Projektmunka.Logic.Interfaces;
using Projektmunka.Platform.Interfaces;
using Projektmunka.View.Models;
using Projektmunka.View.Tools;
using Projektmunka.View.Validators;
using Xamarin.Forms;

namespace Projektmunka.View.ViewModels
{
    /// <summary>
    /// Viewmodel to LoginPage view.
    /// </summary>
    public class LoginViewModel : ObservableObject
    {
        private readonly UserDto userDTO;
        private readonly IUserManagement logic;
        private readonly IMessageService messageService;
        private readonly INavigationService navigation;
        private readonly LoginValidation validation;
        private bool autoLogin;
        private string usernameError;
        private string passwordError;

        /// <summary>
        /// Initializes a new instance of the <see cref="LoginViewModel"/> class.
        /// </summary>
        public LoginViewModel()
            : this(DependencyService.Get<IUserManagement>(), DependencyService.Get<IMessageService>(), DependencyService.Get<LoginValidation>(), DependencyService.Get<INavigationService>())
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="LoginViewModel"/> class.
        /// </summary>
        /// <param name="logic">Business logic implementation.</param>
        /// <param name="messageService">Message service implementation.</param>
        /// <param name="validation">Validator.</param>
        public LoginViewModel(IUserManagement logic, IMessageService messageService, LoginValidation validation, INavigationService navigation)
        {
            this.logic = logic;
            this.navigation = navigation;
            this.TryAutoLogin();
            this.validation = validation;
            this.messageService = messageService;
            this.LoginCommand = new RelayCommand(async () => { await this.Login(); });
            this.GoToRegistrationCommand = new RelayCommand(() => { this.GoToRegistrationPage(); });
            this.userDTO = new UserDto();
            this.autoLogin = false;
        }

        /// <summary>
        /// Gets or sets name.
        /// </summary>
        public string Username
        {
            get => this.userDTO.Username;
            set => this.SetProperty(this.userDTO.Username, value, this.userDTO, (u, n) => u.Username = n);
        }

        /// <summary>
        /// Gets or sets username error message.
        /// </summary>
        public string UsernameError
        {
            get { return this.usernameError; }
            set { this.SetProperty(ref this.usernameError, value); }
        }

        /// <summary>
        /// Gets or sets password.
        /// </summary>
        public string Password
        {
            get => this.userDTO.Password;
            set => this.SetProperty(this.userDTO.Password, value, this.userDTO, (u, n) => u.Password = n);
        }

        /// <summary>
        /// Gets or sets password error message.
        /// </summary>
        public string PasswordError
        {
            get { return this.passwordError; }
            set { this.SetProperty(ref this.passwordError, value); }
        }

        /// <summary>
        /// Gets or sets a value indicating whether auto login checked or not.
        /// </summary>
        public bool AutoLogin
        {
            get => this.autoLogin;
            set => this.SetProperty(ref this.autoLogin, value);
        }

        /// <summary>
        /// Gets login command.
        /// </summary>
        public ICommand LoginCommand { get; private set; }

        /// <summary>
        /// Gets navigation command.
        /// </summary>
        public ICommand GoToRegistrationCommand { get; private set; }

        private void GoToRegistrationPage()
        {
            this.navigation.ShellGoToAsync("registrationPage");
        }

        private async Task Login()
        {
            var result = this.validation.Validate(this);
            if (result.IsValid)
            {
                this.UsernameError = string.Empty;
                this.PasswordError = string.Empty;
                if (this.logic.Login(this.Username, this.Password, this.AutoLogin))
                {
                    this.navigation.NewAppShell();
                }
                else
                {
                    await this.messageService.ShowAsync("Oops", "Username or password is incorrect", "Understood");
                }
            }
            else
            {
                var username = result.Errors.Where(x => x.PropertyName.Equals("Username"));
                if (username.Count() > 0)
                {
                    this.UsernameError = username.First().ErrorMessage;
                }
                else
                {
                    this.UsernameError = string.Empty;
                }

                var password = result.Errors.Where(x => x.PropertyName.Equals("Password"));
                if (password.Count() > 0)
                {
                    this.PasswordError = password.First().ErrorMessage;
                }
                else
                {
                    this.PasswordError = string.Empty;
                }
            }
        }

        private void TryAutoLogin()
        {
            if (this.logic.AutoLogin())
            {
                this.navigation.NewAppShell();
            }
        }
    }
}
