﻿// <copyright file="RegistrationViewModel.cs" company="OE-NIK">
// Copyright (c) OE-NIK. All rights reserved.
// </copyright>

using System.Linq;
using System.Threading.Tasks;
using System.Windows.Input;
using Acr.UserDialogs;
using CommunityToolkit.Mvvm.ComponentModel;
using CommunityToolkit.Mvvm.Input;
using Projektmunka.Data.Enums;
using Projektmunka.Data.Model;
using Projektmunka.Logic.Interfaces;
using Projektmunka.View.Mappers;
using Projektmunka.View.Models;
using Projektmunka.View.Tools;
using Projektmunka.View.Validators;
using Projektmunka.View.Views;
using Xamarin.Forms;

namespace Projektmunka.View.ViewModels
{
    /// <summary>
    /// Viewmodel to RegistrationPage view.
    /// </summary>
    public class RegistrationViewModel : ObservableObject
    {
        private readonly IMapper<UserDto, User> mapper;
        private readonly IUserManagement logic;
        private readonly IMessageService messageService;
        private readonly UserDto userDTO;
        private readonly RegistrationValidation validation;
        private float goalWeight;
        private float currentWeight;
        private bool hasEquipment;
        private int numberOfDays;
        private string usernameError;
        private string passwordError;
        private string heightError;
        private string weightError;
        private string ageError;
        private string genderError;
        private string goalWeightError;
        private string hasEquipmentError;
        private string numberOfDaysError;

        /// <summary>
        /// Initializes a new instance of the <see cref="RegistrationViewModel"/> class.
        /// </summary>
        public RegistrationViewModel()
            : this(DependencyService.Get<IUserManagement>(), DependencyService.Get<IMessageService>(), DependencyService.Get<RegistrationValidation>(), DependencyService.Get<IMapper<UserDto, User>>())
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="RegistrationViewModel"/> class.
        /// </summary>
        /// <param name="logic">Business logic implementation.</param>
        /// <param name="messageService">Message service implementation.</param>
        /// <param name="validation">Validator.</param>
        private RegistrationViewModel(IUserManagement logic, IMessageService messageService, RegistrationValidation validation, IMapper<UserDto, User> userMapper)
        {
            this.validation = validation;
            this.mapper = userMapper;
            this.logic = logic;
            this.messageService = messageService;
            this.RegisterCommand = new RelayCommand(async () => { await this.Register(); });
            this.userDTO = new UserDto();
            this.hasEquipment = true;
        }

        /// <summary>
        /// Gets command of registration.
        /// </summary>
        public ICommand RegisterCommand { get; private set; }

        /// <summary>
        /// Gets or sets name of user to register.
        /// </summary>
        public string Username
        {
            get => this.userDTO.Username;
            set => this.SetProperty(this.userDTO.Username, value, this.userDTO, (u, n) => u.Username = n.ToString());
        }

        /// <summary>
        /// Gets or sets username error message.
        /// </summary>
        public string UsernameError
        {
            get { return this.usernameError; }
            set { this.SetProperty(ref this.usernameError, value); }
        }

        /// <summary>
        /// Gets or sets password of user to register.
        /// </summary>
        public string Password
        {
            get => this.userDTO.Password;
            set => this.SetProperty(this.userDTO.Password, value, this.userDTO, (u, n) => u.Password = n.ToString());
        }

        /// <summary>
        /// Gets or sets password error message.
        /// </summary>
        public string PasswordError
        {
            get { return this.passwordError; }
            set { this.SetProperty(ref this.passwordError, value); }
        }

        /// <summary>
        /// Gets or sets height of user to register.
        /// </summary>
        public float Height
        {
            get => this.userDTO.Height;
            set => this.SetProperty(this.userDTO.Height, value, this.userDTO, (u, n) => u.Height = n);
        }

        /// <summary>
        /// Gets or sets height error message.
        /// </summary>
        public string HeightError
        {
            get { return this.heightError; }
            set { this.SetProperty(ref this.heightError, value); }
        }

        /// <summary>
        /// Gets or sets age of user to register.
        /// </summary>
        public int Age
        {
            get => this.userDTO.Age;
            set => this.SetProperty(this.userDTO.Age, value, this.userDTO, (u, n) => u.Age = n);
        }

        /// <summary>
        /// Gets or sets age error message.
        /// </summary>
        public string AgeError
        {
            get { return this.ageError; }
            set { this.SetProperty(ref this.ageError, value); }
        }

        /// <summary>
        /// Gets or sets gender of user to register.
        /// </summary>
        public GenderType Gender
        {
            get => this.userDTO.Gender;
            set => this.SetProperty(this.userDTO.Gender, value, this.userDTO, (u, n) => u.Gender = n);
        }

        /// <summary>
        /// Gets or sets gender error message.
        /// </summary>
        public string GenderError
        {
            get { return this.genderError; }
            set { this.SetProperty(ref this.genderError, value); }
        }

        /// <summary>
        /// Gets or sets goal weight of user to register.
        /// </summary>
        public float GoalWeight
        {
            get { return this.goalWeight; }
            set { this.SetProperty(ref this.goalWeight, value); }
        }

        /// <summary>
        /// Gets or sets goal weight error message.
        /// </summary>
        public string GoalWeightError
        {
            get { return this.goalWeightError; }
            set { this.SetProperty(ref this.goalWeightError, value); }
        }

        /// <summary>
        /// Gets or sets current weight of user to register.
        /// </summary>
        public float CurrentWeight
        {
            get { return this.currentWeight; }
            set { this.SetProperty(ref this.currentWeight, value); }
        }

        /// <summary>
        /// Gets or sets current weight error message.
        /// </summary>
        public string CurrentWeightError
        {
            get { return this.weightError; }
            set { this.SetProperty(ref this.weightError, value); }
        }

        /// <summary>
        /// Gets or sets a value indicating whether equipment required by user.
        /// </summary>
        public bool HasEquipment
        {
            get { return this.hasEquipment; }
            set { this.SetProperty(ref this.hasEquipment, value); }
        }

        /// <summary>
        /// Gets or sets equipment error message.
        /// </summary>
        public string HasEquipmentError
        {
            get { return this.hasEquipmentError; }
            set { this.SetProperty(ref this.hasEquipmentError, value); }
        }

        /// <summary>
        /// Gets or sets number of training days of user to register.
        /// </summary>
        public int NumberOfDays
        {
            get { return this.numberOfDays; }
            set { this.SetProperty(ref this.numberOfDays, value); }
        }

        /// <summary>
        /// Gets or sets number of days error message.
        /// </summary>
        public string NumberOfDaysError
        {
            get { return this.numberOfDaysError; }
            set { this.SetProperty(ref this.numberOfDaysError, value); }
        }

        /// <summary>
        /// Registers user with datas of form.
        /// </summary>
        /// <returns>A <see cref="Task"/> representing the result of the asynchronous operation.</returns>
        public async Task Register()
        {
            var result = this.validation.Validate(this);
            if (result.IsValid)
            {
                UserDialogs.Instance.ShowLoading("Creating your profile...", MaskType.Black);
                await Task.Delay(100);
                User model = this.mapper.Map(this.userDTO);
                var creatingResult = this.logic.CreateUser(model, this.CurrentWeight, this.GoalWeight, this.NumberOfDays, this.HasEquipment);
                UserDialogs.Instance.HideLoading();

                if (creatingResult)
                {
                    Shell.Current.CurrentItem = new LoginPage();
                }
                else
                {
                    await this.messageService.ShowAsync("Oops", "Couldn't register this account", "Understood");
                }
            }

            var username = result.Errors.Where(x => x.PropertyName.Equals("Username"));
            if (username.Any())
            {
                this.UsernameError = username.First().ErrorMessage.ToString();
            }
            else
            {
                this.UsernameError = string.Empty.ToString();
            }

            var password = result.Errors.Where(x => x.PropertyName.Equals("Password"));
            if (password.Any())
            {
                this.PasswordError = password.First().ErrorMessage.ToString();
            }
            else
            {
                this.PasswordError = string.Empty.ToString();
            }

            var height = result.Errors.Where(x => x.PropertyName.Equals("Height"));
            if (height.Any())
            {
                this.HeightError = height.First().ErrorMessage.ToString();
            }
            else
            {
                this.HeightError = string.Empty.ToString();
            }

            var age = result.Errors.Where(x => x.PropertyName.Equals("Age"));
            if (age.Any())
            {
                this.AgeError = age.First().ErrorMessage.ToString();
            }
            else
            {
                this.AgeError = string.Empty.ToString();
            }

            var gender = result.Errors.Where(x => x.PropertyName.Equals("Gender"));
            if (gender.Any())
            {
                this.GenderError = gender.First().ErrorMessage.ToString();
            }
            else
            {
                this.GenderError = string.Empty.ToString();
            }

            var goalweight = result.Errors.Where(x => x.PropertyName.Equals("GoalWeight"));
            if (goalweight.Any())
            {
                this.GoalWeightError = goalweight.First().ErrorMessage.ToString();
            }
            else
            {
                this.GoalWeightError = string.Empty.ToString();
            }

            var currentweight = result.Errors.Where(x => x.PropertyName.Equals("CurrentWeight"));
            if (goalweight.Any())
            {
                this.CurrentWeightError = currentweight.First().ErrorMessage.ToString();
            }
            else
            {
                this.CurrentWeightError = string.Empty.ToString();
            }

            var hasequipment = result.Errors.Where(x => x.PropertyName.Equals("EquipmentRequirement"));
            if (hasequipment.Any())
            {
                this.HasEquipmentError = hasequipment.First().ErrorMessage.ToString();
            }
            else
            {
                this.HasEquipmentError = string.Empty.ToString();
            }

            var numberofdays = result.Errors.Where(x => x.PropertyName.Equals("NumberOfDays"));
            if (numberofdays.Any())
            {
                this.NumberOfDaysError = numberofdays.First().ErrorMessage.ToString();
            }
            else
            {
                this.NumberOfDaysError = string.Empty.ToString();
            }
        }
    }
}
