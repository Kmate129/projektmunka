﻿// <copyright file="DailyPlanPerExerciseViewModel.cs" company="OE-NIK">
// Copyright (c) OE-NIK. All rights reserved.
// </copyright>

using System;
using System.ComponentModel;
using System.Windows.Input;
using CommunityToolkit.Mvvm.Input;
using Projektmunka.BLE.Interfaces;
using Projektmunka.Data.Model;
using Projektmunka.Logic.Interfaces;
using Projektmunka.Platform.Interfaces;
using Projektmunka.View.Mappers;
using Projektmunka.View.Models;
using Xamarin.Forms;

namespace Projektmunka.View.ViewModels
{
    /// <summary>
    /// Voewmodel to DailyPlanPerExercisePage.
    /// </summary>
    public class DailyPlanPerExerciseViewModel : DailyPlanExerciseViewModel
    {
        private int time;
        private bool stop;
        private string startLabel;
        private string stopLabel;
        private bool startEnabled;
        private bool stopEnabled;

        /// <summary>
        /// Initializes a new instance of the <see cref="DailyPlanPerExerciseViewModel"/> class.
        /// </summary>
        public DailyPlanPerExerciseViewModel()
            : this(DependencyService.Get<IDailyPlanControl>(), DependencyService.Get<ITimeManager>(), DependencyService.Get<IMapper<ExerciseDetail, ExerciseDto>>(), DependencyService.Get<INavigationService>(), DependencyService.Get<IHrRegulator>(), DependencyService.Get<IAppProvider>(), DependencyService.Get<ITrainingPlanHandler>())
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="DailyPlanPerExerciseViewModel"/> class.
        /// </summary>
        /// <param name="dailyPlanControl">Daily plan controller.</param>
        /// <param name="timeManager">Time manager.</param>
        /// <param name="exerciseMapper">Exercise mapper.</param>
        public DailyPlanPerExerciseViewModel(IDailyPlanControl dailyPlanControl, ITimeManager timeManager, IMapper<ExerciseDetail, ExerciseDto> exerciseMapper, INavigationService navigation, IHrRegulator hr, IAppProvider appProvider, ITrainingPlanHandler trainingPlanHandler)
            : base(dailyPlanControl, timeManager, exerciseMapper, navigation, hr, appProvider, trainingPlanHandler)
        {
            this.Time = int.Parse(this.Exercise.Amount);
            this.StartCommand = new RelayCommand(() => { this.StartCounting(); });
            this.StopCommand = new RelayCommand(() => { this.StopCounting(); });
            this.StartLabel = "Start";
            this.StopLabel = "Stop";
            this.StartEnabled = true;
            this.StopEnabled = false;
        }

        /// <summary>
        /// Gets or sets remaning time to rest.
        /// </summary>
        public int Time
        {
            get { return this.time; }
            set { this.SetProperty(ref this.time, value); }
        }

        /// <summary>
        /// Gets or sets start command.
        /// </summary>
        public ICommand StartCommand { get; set; }

        /// <summary>
        /// Gets or sets stop command.
        /// </summary>
        public ICommand StopCommand { get; set; }

        /// <summary>
        /// Gets or sets start button label.
        /// </summary>
        public string StartLabel
        {
            get { return this.startLabel; }
            set { this.SetProperty(ref this.startLabel, value); }
        }

        /// <summary>
        /// Gets or sets stop button label.
        /// </summary>
        public string StopLabel
        {
            get { return this.stopLabel; }
            set { this.SetProperty(ref this.stopLabel, value); }
        }

        /// <summary>
        /// Gets or sets a value indicating whether start button is locked.
        /// </summary>
        public bool StartEnabled
        {
            get { return this.startEnabled; }
            set { this.SetProperty(ref this.startEnabled, value); }
        }

        /// <summary>
        /// Gets or sets a value indicating whether stop button is locked.
        /// </summary>
        public bool StopEnabled
        {
            get { return this.stopEnabled; }
            set { this.SetProperty(ref this.stopEnabled, value); }
        }

        private void StartCounting()
        {
            if (this.stop)
            {
                this.stop = false;
            }

            Device.StartTimer(TimeSpan.FromSeconds(1), () =>
            {
                this.Time--;
                if (this.Time == 0 || this.stop)
                {
                    return false;
                }

                return true;
            });

            this.StartEnabled = false;
            this.StopEnabled = true;
        }

        private void StopCounting()
        {
            this.stop = true;
            this.StartLabel = "Resume";
            this.StartEnabled = true;
            this.StopEnabled = false;
        }
    }
}
