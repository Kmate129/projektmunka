﻿// <copyright file="ExerciseDto.cs" company="OE-NIK">
// Copyright (c) OE-NIK. All rights reserved.
// </copyright>

namespace Projektmunka.View.Models
{
    /// <summary>
    /// Exercise dto to providing data to view.
    /// </summary>
    public class ExerciseDto
    {
        /// <summary>
        /// Gets or sets name of exercise.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets visual to exercise.
        /// </summary>
        public string Visual { get; set; }

        /// <summary>
        /// Gets or sets primary equipments.
        /// </summary>
        public string PrimaryEquipments { get; set; }

        /// <summary>
        /// Gets or sets secondary equipments.
        /// </summary>
        public string SecondaryEquipments { get; set; }

        /// <summary>
        /// Gets or sets repetition or period.
        /// </summary>
        public string Amount { get; set; }
    }
}
