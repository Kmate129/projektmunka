﻿// <copyright file="EquipmentDto.cs" company="OE-NIK">
// Copyright (c) OE-NIK. All rights reserved.
// </copyright>

namespace Projektmunka.View.Models
{
    /// <summary>
    /// DTO to Equipment.
    /// </summary>
    public class EquipmentDto
    {
        /// <summary>
        /// Gets or sets ID.
        /// </summary>
        public int ID { get; set; }

        /// <summary>
        /// Gets or sets name.
        /// </summary>
        public string Name { get; set; }
    }
}
