﻿// <copyright file="DailyPlanDto.cs" company="OE-NIK">
// Copyright (c) OE-NIK. All rights reserved.
// </copyright>

namespace Projektmunka.View.Models
{
    /// <summary>
    /// DTO to DailyPlan.
    /// </summary>
    public class DailyPlanDto
    {
        /// <summary>
        /// Gets or sets ID of daily plan.
        /// </summary>
        public int DailyPlanId { get; set; }

        /// <summary>
        /// Gets or sets index of daily plan.
        /// </summary>
        public int Index { get; set; }

        /// <summary>
        /// Gets or sets day number.
        /// </summary>
        public string Day { get; set; }

        /// <summary>
        /// Gets or sets name of daily plan.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether it is startable.
        /// </summary>
        public bool IsVisible { get; set; }

        /// <summary>
        /// Gets or sets picture url.
        /// </summary>
        public string Picture { get; set; }
    }
}
