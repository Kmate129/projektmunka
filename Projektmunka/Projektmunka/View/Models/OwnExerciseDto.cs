﻿// <copyright file="OwnExerciseDto.cs" company="OE-NIK">
// Copyright (c) OE-NIK. All rights reserved.
// </copyright>

using System.Collections.Generic;

namespace Projektmunka.View.Models
{
    /// <summary>
    /// OwnExercise DTO.
    /// </summary>
    public class OwnExerciseDto
    {
        /// <summary>
        /// Gets or sets iD.
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Gets or sets name.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets phase.
        /// </summary>
        public string Phase { get; set; }

        /// <summary>
        /// Gets or sets muscle muscle.
        /// </summary>
        public string Target { get; set; }

        /// <summary>
        /// Gets or sets base round.
        /// </summary>
        public int BaseRound { get; set; }

        /// <summary>
        /// Gets or sets recommended gender.
        /// </summary>
        public string Recommended { get; set; }

        /// <summary>
        /// Gets or sets path of visual.
        /// </summary>
        public string Visual { get; set; }

        /// <summary>
        /// Gets or sets rating.
        /// </summary>
        public string Rating { get; set; }

        /// <summary>
        /// Gets or sets primary equipments.
        /// </summary>
        public ICollection<EquipmentDto> PrimaryEquipments { get; set; }

        /// <summary>
        /// Gets or sets secondary equipments.
        /// </summary>
        public ICollection<EquipmentDto> SecondaryEquipments { get; set; }
    }
}
