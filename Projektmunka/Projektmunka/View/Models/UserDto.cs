﻿// <copyright file="UserDto.cs" company="OE-NIK">
// Copyright (c) OE-NIK. All rights reserved.
// </copyright>

using Projektmunka.Data.Enums;

namespace Projektmunka.View.Models
{
    /// <summary>
    /// WeightData transfer object to user model.
    /// </summary>
    public class UserDto
    {
        /// <summary>
        /// Gets or sets identification number.
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Gets or sets username.
        /// </summary>
        public string Username { get; set; }

        /// <summary>
        /// Gets or sets password.
        /// </summary>
        public string Password { get; set; }

        /// <summary>
        /// Gets or sets height.
        /// </summary>
        public float Height { get; set; }

        /// <summary>
        /// Gets or sets current weight.
        /// </summary>
        public float CurrentWeight { get; set; }

        /// <summary>
        /// Gets or sets age.
        /// </summary>
        public int Age { get; set; }

        /// <summary>
        /// Gets or sets gender.
        /// </summary>
        public GenderType Gender { get; set; }

        /// <summary>
        /// Gets or sets goal weight.
        /// </summary>
        public float GoalWeight { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether user has equipment or not.
        /// </summary>
        public bool HasEquipment { get; set; }

        /// <summary>
        /// Gets or sets number of training days.
        /// </summary>
        public int NumberOfDays { get; set; }
    }
}
