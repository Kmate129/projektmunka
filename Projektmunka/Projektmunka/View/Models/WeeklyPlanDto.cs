﻿// <copyright file="WeeklyPlanDto.cs" company="OE-NIK">
// Copyright (c) OE-NIK. All rights reserved.
// </copyright>

using System.Collections.Generic;

namespace Projektmunka.View.Models
{
    /// <summary>
    /// DTO to WeeklyPlan.
    /// </summary>
    public class WeeklyPlanDto
    {
        /// <summary>
        /// Gets or sets ID of WeeklyPlan.
        /// </summary>
        public int WeeklyPlanId { get; set; }

        /// <summary>
        /// Gets or sets index of WeeklyPlan.
        /// </summary>
        public int Index { get; set; }

        /// <summary>
        /// Gets or sets name.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets color.
        /// </summary>
        public string Color { get; set; }

        /// <summary>
        /// Gets or sets path of status icon.
        /// </summary>
        public string StatusIcon { get; set; }

        /// <summary>
        /// Gets or sets status.
        /// </summary>
        public string Status { get; set; }

        /// <summary>
        /// Gets or sets related daily plans.
        /// </summary>
        public List<DailyPlanDto> DailyPlanDTOs { get; set; } = new List<DailyPlanDto>();
    }
}
