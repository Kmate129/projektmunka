﻿// <copyright file="WorkoutDto.cs" company="OE-NIK">
// Copyright (c) OE-NIK. All rights reserved.
// </copyright>

using System.Collections.Generic;

namespace Projektmunka.View.Models
{
    /// <summary>
    /// Workout DTO.
    /// </summary>
    public class WorkoutDto
    {
        /// <summary>
        /// Gets or sets name.
        /// </summary>
        public string WorkoutName { get; set; }

        /// <summary>
        /// Gets or sets exercise names.
        /// </summary>
        public ICollection<string> ExerciseNames { get; set; }
    }
}
