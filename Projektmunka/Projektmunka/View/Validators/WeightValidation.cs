﻿// <copyright file="WeightValidation.cs" company="OE-NIK">
// Copyright (c) OE-NIK. All rights reserved.
// </copyright>

using FluentValidation;
using Projektmunka.View.ViewModels;

namespace Projektmunka.View.Validators
{
    /// <summary>
    /// Validates new weight input.
    /// </summary>
    public class WeightValidation : AbstractValidator<WeeklyPlanDoneViewModel>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="WeightValidation"/> class.
        /// </summary>
        public WeightValidation()
        {
            this.RuleFor(x => x.Weight).NotEmpty().Must(value => float.TryParse(value.ToString(), out _));
        }
    }
}
