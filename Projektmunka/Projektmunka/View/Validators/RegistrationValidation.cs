﻿// <copyright file="RegistrationValidation.cs" company="OE-NIK">
// Copyright (c) OE-NIK. All rights reserved.
// </copyright>

using FluentValidation;
using Projektmunka.Data.Enums;
using Projektmunka.View.ViewModels;

namespace Projektmunka.View.Validators
{
    /// <summary>
    /// Validates registration inputs.
    /// </summary>
    public class RegistrationValidation : AbstractValidator<RegistrationViewModel>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="RegistrationValidation"/> class.
        /// </summary>
        public RegistrationValidation()
        {
            this.RuleFor(x => x.Username).NotEmpty();
            this.RuleFor(x => x.Password).NotEmpty();
            this.RuleFor(x => x.Gender).Must(x => x == GenderType.Male || x == GenderType.Female);
            this.RuleFor(x => x.Age).GreaterThanOrEqualTo(18);
            this.RuleFor(x => x.Height).GreaterThan(0);
            this.RuleFor(x => x.CurrentWeight).NotEmpty();
            this.RuleFor(x => x.GoalWeight).NotEmpty();
            this.RuleFor(x => x.HasEquipment).Must(x => x == true || x == false);
            this.RuleFor(x => x.NumberOfDays).GreaterThanOrEqualTo(3).LessThanOrEqualTo(5);
        }
    }
}
