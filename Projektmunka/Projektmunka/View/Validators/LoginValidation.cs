﻿// <copyright file="LoginValidation.cs" company="OE-NIK">
// Copyright (c) OE-NIK. All rights reserved.
// </copyright>

using FluentValidation;
using Projektmunka.View.ViewModels;

namespace Projektmunka.View.Validators
{
    /// <summary>
    /// Validates login inputs.
    /// </summary>
    public class LoginValidation : AbstractValidator<LoginViewModel>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="LoginValidation"/> class.
        /// </summary>
        public LoginValidation()
        {
            this.RuleFor(x => x.Username).NotEmpty();
            this.RuleFor(x => x.Password).NotEmpty();
        }
    }
}
