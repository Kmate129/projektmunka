﻿// <copyright file="DailyPlanRestPage.xaml.cs" company="OE-NIK">
// Copyright (c) OE-NIK. All rights reserved.
// </copyright>

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Projektmunka.View.Views
{
    /// <summary>
    /// DailyPlanRestPage code behind.
    /// </summary>
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class DailyPlanRestPage : ContentPage
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="DailyPlanRestPage"/> class.
        /// </summary>
        public DailyPlanRestPage()
        {
            this.InitializeComponent();
        }
    }
}