﻿// <copyright file="ExerciseDetailPage.xaml.cs" company="OE-NIK">
// Copyright (c) OE-NIK. All rights reserved.
// </copyright>

using Projektmunka.View.ViewModels;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Projektmunka.View.Views
{
    /// <summary>
    /// ExerciseDetailPage code behind.
    /// </summary>
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ExerciseDetailPage : ContentPage
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ExerciseDetailPage"/> class.
        /// </summary>
        public ExerciseDetailPage()
        {
            this.InitializeComponent();
        }
    }
}