﻿// <copyright file="StatisticPage.xaml.cs" company="OE-NIK">
// Copyright (c) OE-NIK. All rights reserved.
// </copyright>

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Projektmunka.View.Views
{
    /// <summary>
    /// StatisticPage code behind.
    /// </summary>
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class StatisticPage : ContentPage
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="StatisticPage"/> class.
        /// </summary>
        public StatisticPage()
        {
            this.InitializeComponent();
        }
    }
}