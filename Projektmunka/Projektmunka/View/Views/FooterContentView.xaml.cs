﻿// <copyright file="FooterContentView.xaml.cs" company="OE-NIK">
// Copyright (c) OE-NIK. All rights reserved.
// </copyright>

using Xamarin.Forms.Xaml;

namespace Projektmunka.View.Views
{
    /// <summary>
    /// FooterContentView code behind.
    /// </summary>
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class FooterContentView // : ContentPage
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="FooterContentView"/> class.
        /// </summary>
        public FooterContentView()
        {
            this.InitializeComponent();
        }
    }
}