﻿// <copyright file="NutritionPage.xaml.cs" company="OE-NIK">
// Copyright (c) OE-NIK. All rights reserved.
// </copyright>

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Projektmunka.View.Views
{
    /// <summary>
    /// NutritionPage code behind.
    /// </summary>
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class NutritionPage : ContentPage
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="NutritionPage"/> class.
        /// </summary>
        public NutritionPage()
        {
            this.InitializeComponent();
        }
    }
}