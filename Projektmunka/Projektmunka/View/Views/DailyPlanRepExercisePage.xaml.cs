﻿// <copyright file="DailyPlanRepExercisePage.xaml.cs" company="OE-NIK">
// Copyright (c) OE-NIK. All rights reserved.
// </copyright>

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Projektmunka.View.Views
{
    /// <summary>
    /// DailyPlanRepExercisePage code behind.
    /// </summary>
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class DailyPlanRepExercisePage : ContentPage
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="DailyPlanRepExercisePage"/> class.
        /// </summary>
        public DailyPlanRepExercisePage()
        {
            this.InitializeComponent();
        }
    }
}