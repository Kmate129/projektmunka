﻿// <copyright file="DailyPlanDonePage.xaml.cs" company="OE-NIK">
// Copyright (c) OE-NIK. All rights reserved.
// </copyright>

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Projektmunka.View.Views
{
    /// <summary>
    /// DailyPlanDonePage code behind.
    /// </summary>
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class DailyPlanDonePage : ContentPage
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="DailyPlanDonePage"/> class.
        /// </summary>
        public DailyPlanDonePage()
        {
            this.InitializeComponent();
        }
    }
}