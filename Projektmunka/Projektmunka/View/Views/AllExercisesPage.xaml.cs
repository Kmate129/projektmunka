﻿// <copyright file="AllExercisesPage.xaml.cs" company="OE-NIK">
// Copyright (c) OE-NIK. All rights reserved.
// </copyright>

using Projektmunka.View.Models;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Projektmunka.View.Views
{
    /// <summary>
    /// AllExercisesPage code behind.
    /// </summary>
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class AllExercisesPage : ContentPage
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="AllExercisesPage"/> class.
        /// </summary>
        public AllExercisesPage()
        {
            this.InitializeComponent();
        }
    }
}