﻿// <copyright file="WeeklyPlanDonePage.xaml.cs" company="OE-NIK">
// Copyright (c) OE-NIK. All rights reserved.
// </copyright>

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Projektmunka.View.Views
{
    /// <summary>
    /// WeeklyPlanDonePage code-behind.
    /// </summary>
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class WeeklyPlanDonePage : ContentPage
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="WeeklyPlanDonePage"/> class.
        /// </summary>
        public WeeklyPlanDonePage()
        {
            this.InitializeComponent();
        }
    }
}