﻿// <copyright file="RegistrationPage.xaml.cs" company="OE-NIK">
// Copyright (c) OE-NIK. All rights reserved.
// </copyright>

using System;
using Projektmunka.Data.Enums;
using Projektmunka.View.ViewModels;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Projektmunka.View.Views
{
    /// <summary>
    /// RegistrationPage code behind.
    /// </summary>
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class RegistrationPage : ContentPage
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="RegistrationPage"/> class.
        /// </summary>
        public RegistrationPage()
        {
            this.InitializeComponent();
        }

        /// <summary>
        /// Sets gender type to male if radiobutton checked.
        /// </summary>
        /// <param name="sender">Radio button.</param>
        /// <param name="e">Event arguments.</param>
        private void RadioButton_CheckedMale(object sender, CheckedChangedEventArgs e)
        {
            (this.BindingContext as RegistrationViewModel).Gender = GenderType.Male;
        }

        /// <summary>
        /// Sets gender type to female if radiobutton checked.
        /// </summary>
        /// <param name="sender">Radio button.</param>
        /// <param name="e">Event arguments.</param>
        private void RadioButton_CheckedFemale(object sender, CheckedChangedEventArgs e)
        {
            (this.BindingContext as RegistrationViewModel).Gender = GenderType.Female;
        }

        /// <summary>
        /// Sets equipment requirement to yes if radiobutton checked.
        /// </summary>
        /// <param name="sender">Radio button.</param>
        /// <param name="e">Event arguments.</param>
        private void RadioButton_CheckedYes(object sender, CheckedChangedEventArgs e)
        {
            (this.BindingContext as RegistrationViewModel).HasEquipment = true;
        }

        /// <summary>
        /// Sets equipment requirement to no if radiobutton checked.
        /// </summary>
        /// <param name="sender">Radio button.</param>
        /// <param name="e">Event arguments.</param>
        private void RadioButton_CheckedNo(object sender, CheckedChangedEventArgs e)
        {
            (this.BindingContext as RegistrationViewModel).HasEquipment = false;
        }

        /// <summary>
        /// Sets number of days to picked value.
        /// </summary>
        /// <param name="sender">Radio button.</param>
        /// <param name="e">Event arguments.</param>
        private void OnPickerSelectedIndexChanged(object sender, EventArgs e)
        {
            var picker = (Picker)sender;

            if (picker.SelectedIndex == 0)
            {
                (this.BindingContext as RegistrationViewModel).NumberOfDays = 3;
            }
            else if (picker.SelectedIndex == 1)
            {
                (this.BindingContext as RegistrationViewModel).NumberOfDays = 4;
            }
            else
            {
                (this.BindingContext as RegistrationViewModel).NumberOfDays = 5;
            }
        }
    }
}