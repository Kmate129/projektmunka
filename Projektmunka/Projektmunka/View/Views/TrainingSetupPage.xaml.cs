﻿// <copyright file="TrainingSetupPage.xaml.cs" company="OE-NIK">
// Copyright (c) OE-NIK. All rights reserved.
// </copyright>

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Projektmunka.View.Views
{
    /// <summary>
    /// TrainingSetupPage code behind.
    /// </summary>
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class TrainingSetupPage : ContentPage
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="TrainingSetupPage"/> class.
        /// </summary>
        public TrainingSetupPage()
        {
            this.InitializeComponent();
        }
    }
}