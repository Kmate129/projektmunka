﻿// <copyright file="HeaderContentView.xaml.cs" company="OE-NIK">
// Copyright (c) OE-NIK. All rights reserved.
// </copyright>

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Projektmunka.View.Views
{
    /// <summary>
    /// HeaderContentView code behind.
    /// </summary>
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class HeaderContentView : ContentView
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="HeaderContentView"/> class.
        /// </summary>
        public HeaderContentView()
        {
            this.InitializeComponent();
        }
    }
}