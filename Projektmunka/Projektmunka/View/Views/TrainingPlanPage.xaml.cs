﻿// <copyright file="TrainingPlanPage.xaml.cs" company="OE-NIK">
// Copyright (c) OE-NIK. All rights reserved.
// </copyright>

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Projektmunka.View.Views
{
    /// <summary>
    /// TrainingPlanPage code behind.
    /// </summary>
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class TrainingPlanPage : ContentPage
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="TrainingPlanPage"/> class.
        /// </summary>
        public TrainingPlanPage()
        {
            this.InitializeComponent();
        }
    }
}