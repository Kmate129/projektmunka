﻿// <copyright file="DevicesPage.xaml.cs" company="OE-NIK">
// Copyright (c) OE-NIK. All rights reserved.
// </copyright>

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Projektmunka.View.Views
{
    /// <summary>
    /// DevicesPage code behind.
    /// </summary>
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class DevicesPage : ContentPage
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="DevicesPage"/> class.
        /// </summary>
        public DevicesPage()
        {
            this.InitializeComponent();
        }
    }
}