﻿// <copyright file="DailyPlanPerExercisePage.xaml.cs" company="OE-NIK">
// Copyright (c) OE-NIK. All rights reserved.
// </copyright>

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Projektmunka.View.Views
{
    /// <summary>
    /// DailyPlanPerExercisePage code behind.
    /// </summary>
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class DailyPlanPerExercisePage : ContentPage
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="DailyPlanPerExercisePage"/> class.
        /// </summary>
        public DailyPlanPerExercisePage()
        {
            this.InitializeComponent();
        }
    }
}