﻿// <copyright file="TypeConverter.cs" company="OE-NIK">
// Copyright (c) OE-NIK. All rights reserved.
// </copyright>

namespace Projektmunka.View.Mappers
{
    public abstract class TypeConverter
    {
        protected string EquipmentTypeToString(string enumString)
        {
            return enumString.Replace('_', ' ');
        }
    }
}
