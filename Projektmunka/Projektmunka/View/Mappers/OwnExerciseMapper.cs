﻿// <copyright file="OwnExerciseMapper.cs" company="OE-NIK">
// Copyright (c) OE-NIK. All rights reserved.
// </copyright>

using System;
using System.Collections.Generic;
using Projektmunka.Data.Enums;
using Projektmunka.Data.Model;
using Projektmunka.View.Models;

namespace Projektmunka.View.Mappers
{
    /// <summary>
    /// IMapper that maps between OwnExercise model and OwnExercise entity.
    /// </summary>
    public class OwnExerciseMapper : TypeConverter, IMapper<OwnExercise, OwnExerciseDto>
    {
        /// <summary>
        /// Maps OwnExercise model to OwnExercise DTO.
        /// </summary>
        /// <param name="ownExercise">OwnExercise model.</param>
        /// <returns>OwnExercise DTO.</returns>
        public OwnExerciseDto Map(OwnExercise ownExercise)
        {
            OwnExerciseDto dto = new OwnExerciseDto
            {
                Id = ownExercise.Id,
                Name = ownExercise.Exercise.Name,
                Phase = ownExercise.Exercise.Phase.ToString(),
                Target = ownExercise.Exercise.TargetMuscleGroup.ToString(),
                BaseRound = ownExercise.Exercise.BaseRound,
                Recommended = this.Recommendation(ownExercise.Exercise.Preference),
                Visual = ownExercise.Exercise.Visual,
                Rating = this.RatingToStars(ownExercise.Rating),
                PrimaryEquipments = this.MapEquipment(ownExercise.Exercise.PrimaryEquipments),
                SecondaryEquipments = this.MapEquipment(ownExercise.Exercise.SecondaryEquipments),
            };

            return dto;
        }

        private string Recommendation(GenderPreference preference)
        {
            if (preference.Equals(GenderPreference.Man))
            {
                return "Only men";
            }
            else
            {
                if (preference.Equals(GenderPreference.Woman))
                {
                    return "Only women";
                }
                else
                {
                    return "Men & Women";
                }
            }
        }

        private string RatingToStars(float rating)
        {
            switch (rating)
            {
                case ConstParameters.BASE_RATE_MULTIPLIER:
                    return "threestars.png";
                case ConstParameters.LOWER_RATE_MULTIPLIER:
                    return "twostars.png";
                case ConstParameters.LOWEST_RATE_MULTIPLIER:
                    return "onestar.png";
                case ConstParameters.HIGHER_RATE_MULTIPLIER:
                    return "fourstars.png";
                case ConstParameters.HIGHEST_RATE_MULTIPLIER:
                    return "fivestars.png";
                default:
                    throw new ArgumentException("Rating is not valid");
            }
        }

        private ICollection<EquipmentDto> MapEquipment(ICollection<EquipmentTypeEntity> equipments)
        {
            ICollection<EquipmentDto> returnValue = new List<EquipmentDto>();
            foreach (var equipment in equipments)
            {
                returnValue.Add(new EquipmentDto()
                {
                    ID = equipment.Id,
                    Name = this.EquipmentTypeToString(equipment.Value.ToString()),
                });
            }

            return returnValue;
        }
    }
}
