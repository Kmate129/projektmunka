﻿// <copyright file="UserMapper.cs" company="OE-NIK">
// Copyright (c) OE-NIK. All rights reserved.
// </copyright>

using Projektmunka.Data.Model;
using Projektmunka.View.Models;

namespace Projektmunka.View.Mappers
{
    /// <summary>
    /// IMapper that maps between user model and user entity.
    /// </summary>
    public class UserMapper : IMapper<UserDto, User>
    {
        /// <summary>
        /// Maps user data transfer object to user model.
        /// </summary>
        /// <param name="dto">User data transfer object.</param>
        /// <returns>User model.</returns>
        public User Map(UserDto dto)
        {
            User user = new User(
                dto.Username,
                dto.Password,
                dto.Height,
                dto.Age,
                dto.Gender);

            return user;
        }
    }
}