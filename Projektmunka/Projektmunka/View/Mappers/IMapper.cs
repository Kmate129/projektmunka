﻿// <copyright file="IMapper.cs" company="OE-NIK">
// Copyright (c) OE-NIK. All rights reserved.
// </copyright>

namespace Projektmunka.View.Mappers
{
    /// <summary>
    /// Requires method a mapepr should implement.
    /// </summary>
    /// <typeparam name="TC">Source type.</typeparam>
    /// <typeparam name="TD">Destination type.</typeparam>
    public interface IMapper<in TC, out TD>
        where TC : class
    {
        /// <summary>
        /// Maps from source object to destination object.
        /// </summary>
        /// <param name="source">Source object.</param>
        /// <returns>Destination object.</returns>
        public TD Map(TC source);
    }
}
