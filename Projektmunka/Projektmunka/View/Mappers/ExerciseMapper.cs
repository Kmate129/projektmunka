﻿// <copyright file="ExerciseMapper.cs" company="OE-NIK">
// Copyright (c) OE-NIK. All rights reserved.
// </copyright>

using System.Collections.Generic;
using System.Linq;
using System.Text;
using Projektmunka.Data.Enums;
using Projektmunka.Data.Model;
using Projektmunka.View.Models;

namespace Projektmunka.View.Mappers
{
    /// <summary>
    /// Mapper implementation between Exercise Detail and Exercise dto.
    /// </summary>
    public class ExerciseMapper : TypeConverter, IMapper<ExerciseDetail, ExerciseDto>
    {
        /// <summary>
        /// Maps ExerciseDetail to ExerciseDto.
        /// </summary>
        /// <param name="exercise">ExercsieDetail to map.</param>
        /// <returns>Exercise dto.</returns>
        public ExerciseDto Map(ExerciseDetail exercise)
        {
            ExerciseDto dto = new ExerciseDto
            {
                Name = exercise.OwnExercise.Exercise.Name,
                Visual = exercise.OwnExercise.Exercise.Visual,
                PrimaryEquipments = this.ConvertPrimary(exercise.OwnExercise.Exercise.PrimaryEquipments),
                SecondaryEquipments = this.ConvertSecondary(exercise.OwnExercise.Exercise.SecondaryEquipments),
            };

            if (exercise.OwnExercise.Exercise is RepExercise)
            {
                dto.Amount += (exercise.OwnExercise.Exercise as RepExercise).BaseRep.ToString() + " repetitions";
            }
            else
            {
                dto.Amount += (exercise.OwnExercise.Exercise as PerExercise).BaseTime.ToString();
            }

            return dto;
        }

        private string ConvertPrimary(ICollection<EquipmentTypeEntity> equipments)
        {
            string returnValue = "Primary equipment: ";

            if (equipments.First().Value == EquipmentType.Nothing)
            {
                returnValue += "there is no need of equipment";
                return returnValue;
            }

            return this.Convert(returnValue, equipments);
        }

        private string ConvertSecondary(ICollection<EquipmentTypeEntity> equipments)
        {
            string returnValue = "Alternative: ";

            if (equipments.First().Value == EquipmentType.Nothing)
            {
                returnValue += "nothing";
                return returnValue;
            }

            return this.Convert(returnValue, equipments);
        }

        private string Convert(string pre, ICollection<EquipmentTypeEntity> equipments)
        {
            StringBuilder sb = new StringBuilder(pre);
            foreach (var equipment in equipments)
            {
                if (equipment.Value == EquipmentType.Nothing)
                {
                    return null;
                }

                sb.Append(this.EquipmentTypeToString(equipment.Value.ToString()));
                sb.Append(", ");
            }

            sb.Remove(sb.ToString().Length - 2, 2);
            return sb.ToString();
        }
    }
}
