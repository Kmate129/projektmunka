﻿// <copyright file="TrainingPlanMapper.cs" company="OE-NIK">
// Copyright (c) OE-NIK. All rights reserved.
// </copyright>

using System.Collections.Generic;
using System.Linq;
using System.Text;
using Projektmunka.Data.Enums;
using Projektmunka.Data.Model;
using Projektmunka.View.Models;

namespace Projektmunka.View.Mappers
{
    /// <summary>
    /// IMapper class that maps between TrainingPlan and its DTO.
    /// </summary>
    public class TrainingPlanMapper : IMapper<TrainingPlan, ICollection<WeeklyPlanDto>>
    {
        /// <summary>
        /// Maps from TrainingPlan to list of WeeklyPlan DTOs.
        /// </summary>
        /// <param name="trainingPlan">Model.</param>
        /// <returns>List of WeeklyPlan DTOs.</returns>
        public ICollection<WeeklyPlanDto> Map(TrainingPlan trainingPlan)
        {
            List<WeeklyPlanDto> returnValue = new List<WeeklyPlanDto>();
            foreach (var weeklyPlan in trainingPlan.WeeklyPlans.OrderBy(x => x.Index))
            {
                WeeklyPlanDto dto = new WeeklyPlanDto();
                dto.WeeklyPlanId = weeklyPlan.Id;
                dto.Index = weeklyPlan.Index;
                dto.Name = "Week " + (weeklyPlan.Index + 1);

                if (weeklyPlan.Status == Status.Done)
                {
                    dto.Color = "#04B825";
                    dto.StatusIcon = "greentick.png";
                }
                else
                {
                    if (weeklyPlan.Status == Status.Ready)
                    {
                        dto.Color = "#D99521";
                        dto.StatusIcon = "readycircle.png";
                    }
                    else
                    {
                        dto.Color = "#737573";
                        dto.StatusIcon = "graycross.png";
                    }
                }

                foreach (var dailyPlan in weeklyPlan.DailyPlans.OrderBy(x => x.Index))
                {
                    DailyPlanDto dailyDto = new DailyPlanDto();
                    dailyDto.DailyPlanId = dailyPlan.Id;
                    dailyDto.Index = dailyPlan.Index;
                    dailyDto.Day = "Day " + (dailyPlan.Index + 1);
                    dailyDto.Name = this.NameThis(dailyPlan);

                    if (dailyPlan.Status == Status.Ready)
                    {
                        dailyDto.IsVisible = true;
                    }
                    else
                    {
                        if (dailyPlan.Status == Status.Done)
                        {
                            dailyDto.Picture = "greenrowtick.png";
                        }
                    }

                    dto.DailyPlanDTOs.Add(dailyDto);
                }

                returnValue.Add(dto);
            }

            return returnValue;
        }

        private string NameThis(DailyPlan dailyplan)
        {
            StringBuilder sb = new StringBuilder();

            bool sep = false;
            foreach (var workout in dailyplan.Workouts.OrderBy(x => x.Index))
            {
                if (sep)
                {
                    sb.Append(" & ");
                }

                sb.Append($" {workout.MuscleGroup} ").ToString();
                sep = true;
            }

            return sb.ToString();
        }
    }
}
