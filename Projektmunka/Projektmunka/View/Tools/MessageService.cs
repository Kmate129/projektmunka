﻿// <copyright file="MessageService.cs" company="OE-NIK">
// Copyright (c) OE-NIK. All rights reserved.
// </copyright>

using System.Threading.Tasks;

namespace Projektmunka.View.Tools
{
    /// <summary>
    /// Message service implementation.
    /// </summary>
    public class MessageService : IMessageService
    {
        /// <inheritdoc/>
        public async Task ShowAsync(string title, string message, string acceptance)
        {
            await App.Current.MainPage.DisplayAlert(title, message, acceptance);
        }
    }
}
