﻿// <copyright file="MinuteConverter.cs" company="OE-NIK">
// Copyright (c) OE-NIK. All rights reserved.
// </copyright>

using System;
using System.Globalization;
using Xamarin.Forms;

namespace Projektmunka.View.Tools
{
    /// <summary>
    /// Converts from repetition to formatted string.
    /// </summary>
    internal class MinuteConverter : IValueConverter
    {
        /// <inheritdoc/>
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            float seconds = (float)value;
            int minutes = (int)(seconds / 60f);
            return $"{minutes.ToString()} minutes spent with sport this week.";
        }

        /// <inheritdoc/>
        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
