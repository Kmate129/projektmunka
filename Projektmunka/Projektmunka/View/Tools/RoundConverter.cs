﻿// <copyright file="RoundConverter.cs" company="OE-NIK">
// Copyright (c) OE-NIK. All rights reserved.
// </copyright>

using System;
using System.Globalization;
using Xamarin.Forms;

namespace Projektmunka.View.Tools
{
    /// <summary>
    /// Converts from number to formatted string.
    /// </summary>
    public class RoundConverter : IValueConverter
    {
        /// <inheritdoc/>
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            int counter = (int)value + 1;
            string returnValue = $"Round {counter}.";
            return returnValue;
        }

        /// <inheritdoc/>
        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
