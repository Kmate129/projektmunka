﻿// <copyright file="IMessageService.cs" company="OE-NIK">
// Copyright (c) OE-NIK. All rights reserved.
// </copyright>

using System.Threading.Tasks;

namespace Projektmunka.View.Tools
{
    /// <summary>
    /// Requires methods to implement by a message service.
    /// </summary>
    public interface IMessageService
    {
        /// <summary>
        /// Pops up alert message.
        /// </summary>
        /// <param name="title">Title.</param>
        /// <param name="message">Message.</param>
        /// <param name="acceptance">Acceptance label.</param>
        /// <returns>A task that represents the asynchronous operation.</returns>
        Task ShowAsync(string title, string message, string acceptance);
    }
}
