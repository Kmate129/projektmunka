﻿// <copyright file="DataToGrammConverter.cs" company="OE-NIK">
// Copyright (c) OE-NIK. All rights reserved.
// </copyright>

using System;
using System.Globalization;
using Xamarin.Forms;

namespace Projektmunka.View.Tools
{
    /// <summary>
    /// Value converter between input value and gramms.
    /// </summary>
    public class DataToGrammConverter : IValueConverter
    {
        /// <inheritdoc/>
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            string weight = value.ToString();
            weight += "g";
            return weight;
        }

        /// <inheritdoc/>
        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            string text = (string)value;
            string returnValue = text.Remove(text.Length - 1);
            int ret;
            if (int.TryParse(returnValue, out ret))
            {
                return ret;
            }

            return null;
        }
    }
}
