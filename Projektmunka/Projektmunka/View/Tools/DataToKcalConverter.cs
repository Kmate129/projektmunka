﻿// <copyright file="DataToKcalConverter.cs" company="OE-NIK">
// Copyright (c) OE-NIK. All rights reserved.
// </copyright>

using System;
using System.Globalization;
using Xamarin.Forms;

namespace Projektmunka.View.Tools
{
    /// <summary>
    /// Value converter between input value and kilocalories.
    /// </summary>
    public class DataToKcalConverter : IValueConverter
    {
        /// <inheritdoc/>
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            string energy = value.ToString();
            energy += "kcal";
            return energy;
        }

        /// <inheritdoc/>
        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            string text = (string)value;
            string returnValue = text.Remove(text.Length - 5);
            int ret;
            if (int.TryParse(returnValue, out ret))
            {
                return ret;
            }

            return null;
        }
    }
}
